#-dontwarn okhttp3.**
#-dontwarn okio.**
#-dontwarn javax.annotation.**
#-dontwarn org.conscrypt.**
## A resource is loaded with a relative path so the package of this class must be preserved.
#-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase
#-keep class * implements com.coremedia.iso.boxes.Box {* ; }
#-dontwarn com.coremedia.iso.boxes.*
#-dontwarn com.googlecode.mp4parser.authoring.tracks.mjpeg.**
#-dontwarn com.googlecode.mp4parser.authoring.tracks.ttml.**
#-keep class com.divineray.app.activities.** {*;}
##-keep class com.divineray.app.Add.** {*;}
##-keep class com.divineray.app.BuyCoin.** {*;}
##-keep class com.divineray.app.Chat.** {*;}
##-keep class com.divineray.app.Connection.** {*;}
##-keep class com.divineray.app.FCM.** {*;}
##-keep class com.divineray.app.fonts.** {*;}
##-keep class com.divineray.app.Home.** {*;}
##-keep class com.divineray.app.interfaces.** {*;}
#-keep class com.divineray.app.model.** {*;}
#-keep class com.divineray.app.Agora.** {*;}
#-keep class com.divineray.app.Utils.** {*;}
##-keep class com.divineray.app.Notifications.** {*;}
##-keep class com.divineray.app.Profile.** {*;}
##-keep class com.divineray.app.Report.** {*;}
##-keep class com.divineray.app.Search.** {*;}
##-keep class com.divineray.app.SegmentProgress.** {*;}
##-keep class com.divineray.app.SelecteUserMore.** {*;}
##-keep class com.divineray.app.Settings.** {*;}
##-keep class com.divineray.app.ShareItem.** {*;}
##-keep class com.divineray.app.StringChange.** {*;}
##-keep class com.divineray.app.Utils.** {*;}
##-keep class com.divineray.app.Video_Recording.** {*;}
##-keep class com.divineray.app.VideoPick.** {*;}
##-keep class com.divineray.app.VideoPickFilter.** {*;}
#
#
#
#
#
##-keep class com.example.vicky.androidui.Model.Request {*;}
#-keep class io.agora.**{*;}