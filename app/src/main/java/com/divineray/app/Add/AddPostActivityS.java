package com.divineray.app.Add;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.VideoPickFilter.Constant;
import com.divineray.app.Video_Recording.GallerySelectedVideo.GallerySelectedVideo_A;
import com.divineray.app.activities.HomeActivity;
import com.divineray.app.R;
import com.divineray.app.Utils.Constants;
import com.divineray.app.Utils.Functions;
import com.divineray.app.Video_Recording.Video_Recorder_A;
import com.divineray.app.activities.BaseActivity;
import com.ligl.android.widget.iosdialog.IOSDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddPostActivityS extends BaseActivity implements ServiceCallback{


    /**
     * Getting the Current Class Name
     */
    String TAG = AddPostActivityS.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = AddPostActivityS.this;
    @BindView(R.id.add_tags_s)
    EditText add_tags;

    @BindView(R.id.add_description_s)
    EditText add_description;
    @BindView(R.id.add_savebutton_s)
    TextView add_savebutton;
    @BindView(R.id.add_video_image_s)
    ImageView add_video_image;
    @BindView(R.id.post_close_s)
    LinearLayout add_close;
    @BindView(R.id.lly_full)
    LinearLayout lly_full;

    Dialog progressDialog;

    File imageFile;

    private String imagePath="Video";
    String coverted_tag="";
    int mVideoWidth;
    int mVideoHeight;
    String video_path;
    String video_base64="",thumb_base_64="";
    ServiceCallback serviceCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_add_post_s);
        ButterKnife.bind(this);
        thumbnailset();
        dialogsetD();
    }



    @OnClick({R.id.post_close_s,R.id.add_savebutton_s,R.id.lly_full})
    public  void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.post_close_s:
                performCloseClick();
                break;
            case R.id.add_savebutton_s:
                performSaveBtnCLick();
                break;
            case R.id.lly_full:
                dismissKeyBoard();
                break;
        }

    }


    //Clicks
    private void performCloseClick() {
        showDialogd(getResources().getString(R.string.new1), getString(R.string.create_new));
    }

    private void performSaveBtnCLick() {
        String theString = add_tags.getText().toString();
        if (!theString.equals("")) {String[] words = theString.split(" ");

            // Manipulate them
            for (int i = 0; i < words.length; i++) {
                String s = words[i];
                String sw = String.valueOf(s.charAt(0));
                if (sw.equals("#")) {
                    words[i] = words[i];
                } else {
                    words[i] = "#" + words[i];
                }

            }

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                StringJoiner sj = new StringJoiner(" ");
                for (String word : words) {
                    sj.add(word);
                }
                coverted_tag = sj.toString();
            }
            else {
                coverted_tag=add_tags.getText().toString();
            }


        }


        if (isValidate()) {

            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                try {
                    scheduleJob();
                } catch (StringIndexOutOfBoundsException e)
                {
                    e.printStackTrace();
                }

                findViewById(R.id.add_savebutton_s).setClickable(false);



            }

    }
    }
    private void dismissKeyBoard() {
        lly_full.setBackgroundColor(Color.TRANSPARENT);
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(lly_full.getWindowToken(), 0);

    }



    private void thumbnailset()
    {
        Bitmap bmThumbnail;
        File dir = new File(Constants.output_filter_file);
        File dir3=new File(Constants.gallery_resize_video);

        if (dir.exists()) {
            video_path= Constants.output_filter_file;
            System.out.println("Directory created");
        }
        else if (dir3.exists())
        {

            video_path=Constants.gallery_resize_video;
        }


        bmThumbnail = ThumbnailUtils.createVideoThumbnail(video_path,
                MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
        Bitmap bmThumbnail_resized=null;
        if (bmThumbnail!=null) {
            try {
                bmThumbnail_resized = Bitmap.createScaledBitmap(bmThumbnail, (int) (bmThumbnail.getWidth()), (int) (bmThumbnail.getHeight()), true);

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            bmThumbnail_resized=bmThumbnail;
        }
        if (bmThumbnail != null) {
            add_video_image.setImageBitmap(bmThumbnail_resized);
        } else {
        }
    }



    private void dialogsetD() {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);


    }

    @Override
    protected void onResume() {
        super.onResume();
        video_path = Constants.output_filter_file;
        File someFile = new File(video_path);
        String videfile = "Vid"+someFile;
        getVideoWidthOrHeight(someFile, videfile);

        // Get the video width
        mVideoWidth = getVideoWidthOrHeight(someFile, "width");
       // Get the video height
        mVideoHeight = getVideoWidthOrHeight(someFile, "height");
        DivineRayPrefernces divineRayPrefernces=new DivineRayPrefernces();
        SharedPreferences.Editor editor=divineRayPrefernces.getEditor(mActivity);
        editor.putString(DivineRayPrefernces.VIDEOWIDTH,String.valueOf(mVideoWidth));
        editor.putString(DivineRayPrefernces.VIDEOHEIGHT,String.valueOf(mVideoWidth));
        editor.apply();
        Constants.vid_width=String.valueOf(mVideoWidth);
           Constants.vid_height=String.valueOf(mVideoHeight);
    }

    private void scheduleJob() {
        progressDialog.show();
        File someFile = new File(video_path);
        String videfile = "Vid"+someFile;
        getVideoWidthOrHeight(someFile, videfile);


        Constants.vid_desc= add_description.getText().toString().trim();
        Constants.vid_tags=coverted_tag;

        ComponentName componentName = new ComponentName(this, UploadJobService.class);
        JobInfo info = new JobInfo.Builder(123, componentName)
                .setRequiresCharging(false)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(false)
               // .setPersisted(false)
                //.setPeriodic(15*60* 1000)
                .build();
        JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        int resultCode = scheduler.schedule(info);
        if (resultCode == JobScheduler.RESULT_SUCCESS) {
            Log.e("Mine", "Job scheduled");

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();

                    startActivity(new Intent(AddPostActivityS.this, HomeActivity.class));
                    Toast.makeText(mActivity, "Video start uploading", Toast.LENGTH_SHORT).show();
                }
            },1000);
        } else {
            Log.e("Mine", "Job scheduling failed");
        }
    }

    void cancelJob()
    {
        JobScheduler scheduler=(JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        scheduler.cancel(123);
        Log.e("Mine","Job Cancelled");
    }

    public int getVideoWidthOrHeight(File file, String widthOrHeight) {
        MediaMetadataRetriever retriever = null;
        Bitmap bmp = null;
        FileInputStream inputStream = null;
        int mWidthHeight = 0;
        try {
            retriever = new  MediaMetadataRetriever();
            inputStream = new FileInputStream(file.getAbsolutePath());
            retriever.setDataSource(inputStream.getFD());
            bmp = retriever.getFrameAtTime();
            if (widthOrHeight.equals("width")){
                mWidthHeight = bmp.getWidth();
            }else {
                mWidthHeight = bmp.getHeight();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally{
            if (retriever != null){
                retriever.release();
            }if (inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return mWidthHeight;
    }

    public boolean isValidate() {
                boolean flag = true;
                String word="a";
                String word2=" ";
                String s=add_tags.getText().toString();
                String regex = "#[A-Za-z0-9_]+";
                Matcher m = Pattern.compile(regex).matcher(s);
                if (add_tags.getText().toString().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_tag));
                    flag = false;
                }
                else if (!m.find())
                {

                    showAlertDialog(mActivity, getString(R.string.please_enter_only_tags));
                    flag=false;
                }
                else if (m.equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_tag));
                    flag = false;
                }
                else if (add_description.getText().toString().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_description));
                    flag = false;
                }
                return flag;
            }


            //this will start the service for uploading the video into database
    public void Start_Service(){
showProgressDialog(mActivity);



        serviceCallback=this;
        Log.e("Test","1");
        Upload_Service mService =
                new Upload_Service(serviceCallback);
        if (!Functions.isMyServiceRunning(this,mService.getClass())) {
            Log.e("Test","2");
            Intent mServiceIntent = new Intent(this.getApplicationContext(), mService.getClass());
            mServiceIntent.setAction("startservice");
            mServiceIntent.putExtra("uri",""+ Uri.fromFile(new File(video_path)));
            mServiceIntent.putExtra("desc",""+add_description.getText().toString());
            mServiceIntent.putExtra("tag",""+coverted_tag);
            mServiceIntent.putExtra("videoWidth",""+mVideoWidth);
            mServiceIntent.putExtra("videoHeight",""+mVideoHeight);
            startService(mServiceIntent);
           // progressDialog.dismiss();

            Intent intent = new Intent(this, Upload_Service.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

            Log.e("Test","3");


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();

                    startActivity(new Intent(AddPostActivityS.this, HomeActivity.class));
                    Toast.makeText(mActivity, getString(R.string.upload_start), Toast.LENGTH_SHORT).show();
                }
            },1000);



        }
        else {
            Log.e("Test","4");
            Toast.makeText(this, getString(R.string.upload_progress), Toast.LENGTH_LONG).show();
        }


    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.e("Test","5");
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
       showDialogd("Notnew","Don't want to upload video ?");
    }

    private void showDialogd(String type, String s) {
        new IOSDialog.Builder(AddPostActivityS.this)
                .setMessage(s)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int ik) {
                        dismissProgressDialog();
                        finish();
                        overridePendingTransition(R.anim.in_from_top,R.anim.out_from_bottom);
                        if (type.equals("Notnew"))
                        {
                            Intent intent=new Intent(mActivity, HomeActivity.class);
                            startActivity(intent);
                            Video_Recorder_A.DeleteFile();
                            overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
                        }
                        else
                        {
                                finish();
                        }

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                }).show();
    }



    // when the video is uploading successfully it will restart the appliaction
    @Override
    public void ShowResponce(final String responce) {
        Log.e("Test","6");
        if(mConnection!=null)
            unbindService(mConnection);
        Log.e("Test","7");
        Toast.makeText(mActivity, responce, Toast.LENGTH_LONG).show();

               // finishAffinity();


    }


    // this is importance for binding the service to the activity
    Upload_Service mService;
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            Log.e("Test","10");
            Upload_Service.LocalBinder binder = (Upload_Service.LocalBinder) service;
            mService = binder.getService();

            mService.setCallbacks(AddPostActivityS.this);



        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {

        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    // this function will stop the the ruuning service
    public void Stop_Service(){

        serviceCallback=this;
        Log.e("Test","11");
        Upload_Service mService = new Upload_Service(serviceCallback);

        if (Functions.isMyServiceRunning(this,mService.getClass())) {
            Intent mServiceIntent = new Intent(this.getApplicationContext(), mService.getClass());
            mServiceIntent.setAction("stopservice");
            startService(mServiceIntent);

        }


    }

}