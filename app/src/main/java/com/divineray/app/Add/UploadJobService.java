package com.divineray.app.Add;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.Constants;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.HomeActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.AddModel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadJobService extends JobService {
    int notifyId=12,notifyId2=13;
    private static final String TAG = "ExampleJobService";
    private boolean jobCancelled = false;
    PendingIntent pendingIntenta;
    NotificationManager notificationManager,notificationManager2;
    JobParameters jobParameters;
    Boolean check=true;
    int count=0;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public boolean onStartJob(JobParameters params) {
        Log.e("Mine", "Job started");
        showNotification();
        doBackgroundWork(params);
        return true;
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void doBackgroundWork(JobParameters params) {
       String authtoken= DivineRayPrefernces.readString(getApplicationContext(),DivineRayPrefernces.AUTHTOKEN,"");
        new Thread(new Runnable() {
            @Override
            public void run() {

                String videoP = null;

                File dir = new File(Constants.output_filter_file);
                File dir3=new File(Constants.gallery_resize_video);
                if (dir.exists()) {
                    videoP= Constants.output_filter_file;
                    System.out.println("Directory created");
                    //dir.mkdir();
                } 
                else if (dir3.exists())
                {

                    videoP=Constants.gallery_resize_video;
                }
              
              //  String videoP= Constants.output_filter_file;
             String description=Constants.vid_desc;
                String tags=Constants.vid_tags;
               String mVideoWidth=Constants.vid_width;
              String mVideoHeight=Constants.vid_height;



                        Log.e("Test1","3");

                        Uri uri = Uri.fromFile(new File(videoP));
                        Bitmap bmThumbnail;
                        bmThumbnail = ThumbnailUtils.createVideoThumbnail(uri.getPath(),MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);

                        Bitmap bmThumbnail_resized= ImageResizer.reduceBitmapSize(bmThumbnail,480000);
                        File reduce_file=getBitmapFile(bmThumbnail_resized);


                        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
                      RequestBody token = RequestBody.create(MediaType.parse("multipart/form-data"),authtoken );

                        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), DivineRayPrefernces.readString(getApplicationContext(), DivineRayPrefernces.USERID, null) );
                        RequestBody description_n = RequestBody.create(MediaType.parse("multipart/form-data"),description );

                        RequestBody tags_n;
                        tags_n = RequestBody.create(MediaType.parse("multipart/form-data"), tags);

                        RequestBody postHeight = RequestBody.create(MediaType.parse("multipart/form-data"), mVideoHeight);
                        RequestBody postWidth = RequestBody.create(MediaType.parse("multipart/form-data"),mVideoWidth);
                        RequestBody musicId = RequestBody.create(MediaType.parse("multipart/form-data"), Constants.musicid);




                        MultipartBody.Part postVideo=null;
                        File   file = new File(uri.getPath());
                        RequestBody ppostVideo = RequestBody.create(MediaType.parse("*/*"), file);
                        postVideo = MultipartBody.Part.createFormData("postVideo", file.getName(), ppostVideo);

                        MultipartBody.Part postImage =null;
                        RequestBody ppostImage = RequestBody.create(MediaType.parse("multipart/form-data"), reduce_file);
                        postImage = MultipartBody.Part.createFormData("postImage", reduce_file.getName(), ppostImage);

                        Call<AddModel> call1 = mApiInterface.addPostDataRequest(authtoken, user_id,postImage, postVideo,description_n,tags_n,postHeight,postWidth,musicId);
                        call1.enqueue(new Callback<AddModel>() {
                            @Override
                            public void onResponse(Call<AddModel> call, Response<AddModel> response) {
                                notificationManager.cancel(notifyId);
                                jobFinished(params, true);
                                AddModel mModel = response.body();

                                assert mModel != null;
                                if(mModel.getStatus()==1)
                                {
                                    showNotification2();
                                }
                                else {

                                    if (response.body().getStatus()==0)
                                    {
                                        if (check) {
                                             check = false;

                                            doBackgroundWork(params);

                                        }
                                        else
                                        {
                                            Toast.makeText(UploadJobService.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                                        }


                                      //  stopForeground(true);

                                       // Toast.makeText(getApplicationContext(), "Unsuccessful adding Video", Toast.LENGTH_SHORT).show();

                                    }
                                    else {}
                                }


                            }

                            @Override
                            public void onFailure(Call<AddModel> call, Throwable t) {
                             //   Toast.makeText(UploadJobService.this, "Trying again", Toast.LENGTH_SHORT).show();
                               try {


                                   if (check) {
                                      // check = false;
                                        count=count+1;
                                        if (count==2)
                                        {
                                            check=false;
                                        }
                                       doBackgroundWork(params);

                                   }
                               }
                               catch (Exception e)
                               {
                                   e.printStackTrace();
                               }
                                notificationManager.cancel(notifyId);
                              //  showNotification2();
                                jobFinished(params, true);


                            }
                        });


                Log.e("Mine", "Job finished");
                jobFinished(params, false);
            }
        }).start();
    }
    @Override
    public boolean onStopJob(JobParameters params) {
        Log.e("Mine", "Job cancelled before completion");
        jobCancelled = true;
        //stopForeground(true);
        return true;
    }


    private File getBitmapFile(Bitmap bmThumbnail_resized) {
        File file=new File(Environment.getExternalStorageDirectory()+File.separator+"reduced_size");
        ByteArrayOutputStream bos=new ByteArrayOutputStream();
        bmThumbnail_resized.compress(Bitmap.CompressFormat.JPEG,30,bos);
        byte[] bitmapdata=bos.toByteArray();
        OutputStream os;
        try {
            file.createNewFile();
            FileOutputStream osw = new FileOutputStream(file);
            osw.write(bitmapdata);
            osw.flush();
            osw.close();
            return file;
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return  file;
    }


    // this will show the sticky notification during uploading video

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showNotification2() {



        Intent notificationIntent = new Intent(this, HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);
       // notifyId=1;
        final String CHANNEL_ID = "my_channel2";
        final String CHANNEL_NAME = "my_channel2";




        NotificationChannel defaultChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);





        Notification notification =  new NotificationCompat.Builder(this)
                 .setSmallIcon(R.drawable.w_logo)
                .setContentTitle(" Video Upload  Successful")
              //  .setContentText("Please wait! Video is uploading....")
               // .setSmallIcon(android.R.drawable.stat_sys_upload)
                .setChannelId(CHANNEL_ID)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        notificationManager2 = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager2.createNotificationChannel(defaultChannel);
        notificationManager2.notify(notifyId2,notification);


    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showNotification() {



        Intent notificationIntent = new Intent(this, HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);
        // notifyId=1;
        final String CHANNEL_ID = "my_channel";
        final String CHANNEL_NAME = "my_channel";




        NotificationChannel defaultChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);





        Notification notification =  new NotificationCompat.Builder(this)
                // .setSmallIcon(android.R.drawable.stat_sys_upload)
                .setContentTitle("Uploading Video")
                .setContentText("Please wait! Video is uploading....")
                .setSmallIcon(android.R.drawable.stat_sys_upload)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                        android.R.drawable.stat_sys_upload))
                .setChannelId(CHANNEL_ID)
                .setAutoCancel(true)
                .build();
         notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(defaultChannel);
        notificationManager.notify(notifyId,notification);


    }

}



