package com.divineray.app.Add;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.divineray.app.Utils.Constants;
import com.divineray.app.Utils.KotUtils;
import com.divineray.app.activities.HomeActivity;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.AddModel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Upload_Service extends Service {
    File imageFile;
    private String imagePath="Video";

    private final IBinder mBinder = new LocalBinder();



    public class LocalBinder extends Binder {
        public Upload_Service getService() {
            return Upload_Service.this;
        }
    }

    boolean mAllowRebind;
    ServiceCallback Callback;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return mAllowRebind;
    }



    Uri uri;

    String video_base64="",thumb_base_64="",Gif_base_64="";

    String description;
    File ff;
    SharedPreferences sharedPreferences;

    public Upload_Service() {
        super();
    }

    public Upload_Service(ServiceCallback serviceCallback) {
        Callback=serviceCallback;
    }

    public void setCallbacks(ServiceCallback serviceCallback){
        Callback=serviceCallback;
    }


    @Override
    public void onCreate() {
        sharedPreferences=getSharedPreferences(Constants.pref_name,MODE_PRIVATE);
    }




    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
Log.e("Test1","1");
        if(intent!=null){
            if (intent.getAction().equals("startservice")) {
                showNotification();
                Log.e("Test1","2");
                String videoP;

                File dir = new File(Constants.output_filter_file);
                File dir2 = new File(Constants.output_filter_file);
                File dir3=new File(Constants.gallery_resize_video);

                // video_path= Constants.output_filter_file;
                if (dir.exists()) {
                    videoP= Constants.output_filter_file;
                    System.out.println("Directory created");
                    //dir.mkdir();
                } else if (dir2.exists()){
                    videoP= Constants.outputfile2;
                    System.out.println("Directory is not created");
                }
                else if (dir3.exists())
                {

                    videoP=Constants.gallery_resize_video;
                }
                else {
                    videoP= Constants.outputfile2;
                }




                //uri = Uri.parse(uri_string);
                description=intent.getStringExtra("desc");
                String tags=intent.getStringExtra("tag");
                String mVideoHeight=intent.getStringExtra("videoHeight");
                String mVideoWidth=intent.getStringExtra("videoWidth");
                String authtoken= DivineRayPrefernces.readString(getApplicationContext(),DivineRayPrefernces.AUTHTOKEN,"");



                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("Test1","3"+"::::"+videoP);
                       // Uri uriF =KotUtils.getVideoContentUri(getApplicationContext(),new File(videoP));
                        Uri uri = Uri.fromFile(new File(videoP));


                        //  Toast.makeText(mActivity, videfile+"wid"+mVideoWidth+"height"+mVideoHeight, Toast.LENGTH_SHORT).show();
                        Bitmap bmThumbnail;
                        bmThumbnail = ThumbnailUtils.createVideoThumbnail(uri.getPath(),
                                MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
                       Bitmap bmThumbnail_resized= ImageResizer.reduceBitmapSize(bmThumbnail,300000);
                       File reduce_file=getBitmapFile(bmThumbnail_resized);
                       Log.e("DATA2",video_base64+"hgchcchc"+thumb_base_64);

                        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

                        RequestBody token = RequestBody.create(MediaType.parse("multipart/form-data"),authtoken );

                        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), DivineRayPrefernces.readString(getApplicationContext(), DivineRayPrefernces.USERID, null) );

                        RequestBody description_n = RequestBody.create(MediaType.parse("multipart/form-data"),description );
                        RequestBody tags_n;

                            tags_n = RequestBody.create(MediaType.parse("multipart/form-data"), tags);
                            RequestBody postHeight = RequestBody.create(MediaType.parse("multipart/form-data"), mVideoHeight);
                        RequestBody postWidth = RequestBody.create(MediaType.parse("multipart/form-data"),mVideoWidth);
                        RequestBody musicId = RequestBody.create(MediaType.parse("multipart/form-data"), com.divineray.app.Utils.Constants.musicid);

                        MultipartBody.Part postVideo=null;

                        File   file = new File(uri.getPath());
                        RequestBody ppostVideo = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        postVideo = MultipartBody.Part.createFormData("postVideo", file.getName(), ppostVideo);



                        MultipartBody.Part postImage =null;

                        RequestBody ppostImage = RequestBody.create(MediaType.parse("multipart/form-data"), reduce_file);

                        postImage = MultipartBody.Part.createFormData("postImage", reduce_file.getName(), ppostImage);

                        Call<AddModel> call1 = mApiInterface.addPostDataRequest(authtoken, user_id,postImage, postVideo,description_n,tags_n,postHeight,postWidth,musicId);
                        Log.d("Data**", "" + call1);
                        call1.enqueue(new Callback<AddModel>() {
                            @Override
                            public void onResponse(Call<AddModel> call, Response<AddModel> response) {

                                Log.e("here","6");


                                Log.d("RESULT**",""+response.body());
                                AddModel mModel = response.body();
                                assert mModel != null;
                                if(mModel.getStatus()==1)
                                {
                                    stopForeground(true);
                                    stopSelf();

                                    Callback.ShowResponce("Your Video is uploaded Successfully");



                                }
                                else {
                                    assert response.body() != null;
                                    if (response.body().getStatus()==0)
                                    {
                                        stopForeground(true);
                                        stopSelf();

                                        Callback.ShowResponce("Upload Unsuccessfull");

                                        //Toast.makeText(getApplicationContext(), "Unsuccessful adding Video", Toast.LENGTH_SHORT).show();

                                    }
                                    else {}
                                }


                            }

                            @Override
                            public void onFailure(Call<AddModel> call, Throwable t) {
                                Log.e("here","7");

                                stopForeground(true);
                                stopSelf();

                                Callback.ShowResponce("Video successfully uploaded");


                            }
                        });
                }}).start();



            }
            else if(intent.getAction().equals("stopservice")){
                stopForeground(true);
                stopSelf();
            }

        }



        return Service.START_STICKY;
    }

    private File getBitmapFile(Bitmap bmThumbnail_resized) {
        File file=new File(Environment.getExternalStorageDirectory()+File.separator+"reduced_size");
        ByteArrayOutputStream bos=new ByteArrayOutputStream();
        bmThumbnail_resized.compress(Bitmap.CompressFormat.JPEG,30,bos);
        byte[] bitmapdata=bos.toByteArray();
        OutputStream os;
        try {
            file.createNewFile();
           FileOutputStream osw = new FileOutputStream(file);
            osw.write(bitmapdata);
            osw.flush();
            osw.close();
            return file;
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return  file;
    }


    // this will show the sticky notification during uploading video
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showNotification() {

        Intent notificationIntent = new Intent(this, HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);
            int notifyId=1;
        final String CHANNEL_ID = "my_channel";
        final String CHANNEL_NAME = "my_channel";




        NotificationChannel defaultChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);





        Notification notification =  new NotificationCompat.Builder(this)
               // .setSmallIcon(android.R.drawable.stat_sys_upload)
                .setContentTitle("Uploading Video")
                .setContentText("Please wait! Video is uploading....")
                .setSmallIcon(android.R.drawable.stat_sys_upload)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                        android.R.drawable.stat_sys_upload))
                .setChannelId(CHANNEL_ID)
                .build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(defaultChannel);
       // notificationManager.notify(notifyId,notification);
       // Notification notification = builder.build();
        startForeground(notifyId, notification);
    }





    void persistImage(Bitmap bitmap, String name) {
        File filesDir = getApplicationContext().getFilesDir();
        imageFile = new File(filesDir, name + ".jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, os);
            os.flush();
            os.close();
            return;
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
    }



}
