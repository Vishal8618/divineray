package com.divineray.app.Agora.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.text.TextUtils;
import android.widget.Toast;

import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Random;

import com.divineray.app.Agora.ConstantsA;
import com.divineray.app.Agora.external2.RtmTokenBuilder;
import com.divineray.app.Agora.external2.SignalingToken;
import com.divineray.app.Agora.externl.DynamicKey;
import com.divineray.app.Agora.externl.DynamicKey5;
import com.divineray.app.Agora.externl.RtcTokenBuilder;
import com.divineray.app.Agora.rtc.EventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.video.VideoCanvas;
import io.agora.rtc.video.VideoEncoderConfiguration;
import com.divineray.app.R;

public abstract class RtcBaseActivity extends BaseActivity implements EventHandler {
    static String appId = "95aa63d5a1344a6b96a1224e6062ae95";
    static String appCertificate = "b9c547ea49d84529959edd86ab34e709";
    static String channelName = "righh";
    static String userAccount = "2082341233";
    static int uid = (int) (System.currentTimeMillis() / 1000L);
    ;
    static int expirationTimeInSeconds = 360000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerRtcEventHandler(this);
        RtcTokenBuilder token = new RtcTokenBuilder();
        int timestamp = (int) (System.currentTimeMillis() / 1000 + expirationTimeInSeconds);
        channelName = ConstantsA.channelNameA;
        try {
            uid = Integer.parseInt(ConstantsA.useridA);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            uid = (int) System.currentTimeMillis();
        }
        String result = token.buildTokenWithUserAccount(appId, appCertificate, channelName, userAccount, RtcTokenBuilder.Role.Role_Publisher, timestamp);
        System.out.println(result);
        result = token.buildTokenWithUid(appId, appCertificate, channelName, uid, RtcTokenBuilder.Role.Role_Publisher, timestamp);
        System.out.println(result);
        joinChannel(result, channelName);
        //joinChannel("00695aa63d5a1344a6b96a1224e6062ae95IACIt2o2lHQ6ajfGgBhYeuAEnNE9e19IDOWP2Ezep+3cmk1XQdkAAAAAIgAGMGkoEOGdYQQAAQAQ4Z1hAgAQ4Z1hAwAQ4Z1hBAAQ4Z1h","dirty");
    }

    public void RtcInit()
    {
        registerRtcEventHandler(this);
        RtcTokenBuilder token = new RtcTokenBuilder();
        int timestamp = (int) (System.currentTimeMillis() / 1000 + expirationTimeInSeconds);
        channelName = ConstantsA.channelNameA;
        try {
            uid = Integer.parseInt(ConstantsA.useridA);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            uid = (int) System.currentTimeMillis();
        }
        String result = token.buildTokenWithUserAccount(appId, appCertificate, channelName, userAccount, RtcTokenBuilder.Role.Role_Publisher, timestamp);
        System.out.println(result);
        result = token.buildTokenWithUid(appId, appCertificate, channelName, uid, RtcTokenBuilder.Role.Role_Publisher, timestamp);
        System.out.println(result);
        joinChannel(result, channelName);

    }

    private void configVideo() {
        VideoEncoderConfiguration configuration = new VideoEncoderConfiguration(
                ConstantsA.VIDEO_DIMENSIONS[config().getVideoDimenIndex()],
                VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_15,
                VideoEncoderConfiguration.STANDARD_BITRATE,
                VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT
        );
        configuration.mirrorMode = ConstantsA.VIDEO_MIRROR_MODES[config().getMirrorEncodeIndex()];
        rtcEngine().setVideoEncoderConfiguration(configuration);
    }

    private void joinChannel(String result,String channelName) {
        String token = result;

        String accessToken = getApplicationContext().getString(R.string.agora_access_token);
        if (TextUtils.equals(accessToken, "") || TextUtils.equals(accessToken, "00682771a92047c468cb6617ee1bf98b7a8IADV0ZRODBCursMiiZzpxDEoVaC2bFvkoWI9KUEPrBDuwCMZ8YwAAAAAEAAHiSHUurPuYAEAAQC4s+5g")) {
        }
        rtcEngine().setChannelProfile(io.agora.rtc.Constants.CHANNEL_PROFILE_LIVE_BROADCASTING);
        rtcEngine().enableVideo();
        configVideo();
        rtcEngine().joinChannel(result, channelName, "Divine" + " " + "Ray", uid);
        config().setChannelName(channelName);
        Log.e("Agora","C_NAME: "+channelName+"  UID:"+uid+"  token:"+result);
        ConstantsA.channelNameA=channelName;
        int uid = (int) (System.currentTimeMillis() / 1000L);
        ConstantsA.useridA= String.valueOf(uid);
        ConstantsA.tokenA= result;

    }

    protected SurfaceView prepareRtcVideo(int uid, boolean local) {
        SurfaceView surface = RtcEngine.CreateRendererView(getApplicationContext());
        if (local) {
            rtcEngine().setupLocalVideo(
                    new VideoCanvas(
                            surface,
                            VideoCanvas.RENDER_MODE_HIDDEN,
                            0,
                            ConstantsA.VIDEO_MIRROR_MODES[config().getMirrorLocalIndex()]
                    )
            );
        } else {
            rtcEngine().setupRemoteVideo(
                    new VideoCanvas(
                            surface,
                            VideoCanvas.RENDER_MODE_HIDDEN,
                            uid,
                            ConstantsA.VIDEO_MIRROR_MODES[config().getMirrorRemoteIndex()]
                    )
            );
        }
        return surface;
    }

    protected void removeRtcVideo(int uid, boolean local) {
        if (local) {
            rtcEngine().setupLocalVideo(null);
        } else {
            rtcEngine().setupRemoteVideo(new VideoCanvas(null, VideoCanvas.RENDER_MODE_HIDDEN, uid));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeRtcEventHandler(this);
        rtcEngine().leaveChannel();
    }


}
