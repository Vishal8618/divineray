package com.divineray.app.Agora.externl;

public interface Packable {
    ByteBuf marshal(ByteBuf out);
}
