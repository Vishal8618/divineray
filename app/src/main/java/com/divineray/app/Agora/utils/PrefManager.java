package com.divineray.app.Agora.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.divineray.app.Agora.ConstantsA;

public class PrefManager {
    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(ConstantsA.PREF_NAME, Context.MODE_PRIVATE);
    }
}
