package com.divineray.app.Animation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.divineray.app.R;

public class TestHearActivity extends AppCompatActivity {

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        setContentView(R.layout.activity_test_hear);
       // emoji_one();
       // emoji_two();
        //emoji_three();
        ImageView imageView=findViewById(R.id.imLiv);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flyObject(R.drawable.heart,1500,Direction.BOTTOM,Direction.TOP,1f);


            }
        });
        ImageView imageView2=findViewById(R.id.imLiv2);
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               emoji_three();
            }
        });
    }
    public void flyEmoji(final int resId) {
        ZeroGravityAnimation animation = new ZeroGravityAnimation();
        animation.setCount(1);
        animation.setScalingFactor(0.2f);
        animation.setOriginationDirection(Direction.BOTTOM);
        animation.setDestinationDirection(Direction.TOP);
        animation.setImage(resId);
        animation.setAnimationListener(new Animation.AnimationListener() {
                                           @Override
                                           public void onAnimationStart(Animation animation) {

                                           }
                                           @Override
                                           public void onAnimationEnd(Animation animation) {

                                           }

                                           @Override
                                           public void onAnimationRepeat(Animation animation) {

                                           }
                                       }
        );

        ViewGroup container = findViewById(R.id.animation_holder);
        animation.play(this,container);

    }

    public void emoji_one() {
        // You can change the number of emojis that will be flying on screen
        for (int i = 0; i < 5; i++) {
            flyEmoji(R.drawable.exo_edit_mode_logo);
        }
    }
    // You can change the number of emojis that will be flying on screen

    public void emoji_two(){
        for(int i=0;i<5;i++) {
            flyEmoji(R.drawable.ic_send);
        }

    }
    // You can change the number of emojis that will be flying on screen

    public void emoji_three(){
        for(int i=0;i<5;i++) {
           // flyEmoji(R.drawable.heart);
            flyObject(R.drawable.heart,1500,Direction.BOTTOM,Direction.TOP,1f);
        }

    }


  //   This method will be used if You want to fly your Emois Over any view

    public void flyObject(final int resId, final int duration, final Direction from, final Direction to, final float scale) {

        ZeroGravityAnimation animation = new ZeroGravityAnimation();
        animation.setCount(3);
        animation.setScalingFactor(scale);
        animation.setOriginationDirection(from);
        animation.setDestinationDirection(to);
        animation.setImage(resId);
        animation.setDuration(duration);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

               // flyObject(resId, duration, from, to, scale);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        //ViewGroup container = (ViewGroup) findViewById(R.id.animation_bigger_objects_holder);
        LinearLayout container=findViewById(R.id.animation_holder);

        animation.play(this,container);

    }


}