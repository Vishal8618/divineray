package com.divineray.app.BuyCoin;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.bumptech.glide.Glide;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.AES;
import com.divineray.app.Utils.Constants;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.BuyCoinItemModel;
import com.divineray.app.model.BuyCoinSModel;
import com.divineray.app.model.BuyCoinTotalModel;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuyCoinAdapter extends BaseAdapter implements PurchasesUpdatedListener {
    String item_id;
    Activity mActivity;
    ArrayList<BuyCoinTotalModel.ProductCoins> mArrayList = new ArrayList<>();
    TextView tx_bucointotal;
    Dialog progressDialog;
    BaseActivity baseActivity;
   String product;
    String securityToken;

    private BillingClient billingClient0;
    private List skuList0 = new ArrayList();
    boolean check=true;
    String result0;
    String result1;
    private SkuDetails mSkuDetails;

    String details;

    String privatekeyCUS2="MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDfIQ9ZVZyi32rWHusXEK173mgBLVmrIoVQFRfMtTVqzdXMjn+GsoToFIdOY9dJGi8MQz+fflcGQIphf1jhdGxvkVVOjickejRsGAvLKoeUA5yfqYDAO7gkKfL+Jq4EfdDTdR0QH7pzG/EnLGaMLIzCHaz2Rp/yOcIOgEE4hNjH2fOnSoOGlGhl4TZ3FGp/GhVHpj5KUPmVuLPuHxdTeSv74VU1zAm67hERCTfT0h46VJHj9JD0GrnQrXxdUkYx6Y4c9+tYBYEz1I16Avw4CdnCpgv88UV5ExyWgwMw2dKSec+AenO7GvVCT5OjtdRjpV658hIJkl+Q6weW2lvEuz27AgMBAAECggEAZsWpdjJSpUpWwS0Yh1UtMwKr0O78gDC0SsCMZ8OHSRxpv4BbCMWtJeBeuiKOhKNEzFclNOdSWhTcKDAp1h5z26eLLVWYfdqzzp38wKVkMS7As2G1ZdWb6TxRKfLS8EqrJtqoekbC8EYMe0ieKy5R3w8gHYdGIKGt2lQEtm2qE9JZZ6GD9+i5f3wUbnaYHmYkmtT4cY3arHEsHT9EQyLhGZ6nHB5Ox3Zz8Hzp/ouaokYgFRucGJcZYbNyPJNMgHxGNIPuo2d3ejL0l2VuYrtb/QJYOmDyS5Wra/mFa7DBfP6sZ5fjl//B5QGgsUgxSi3T3cWZp7U+Om4Ijyq/dkcyYQKBgQD4/NyLBCp6 8vnZM29lK2CbYeTwn9kUWfgVQLm9Y3MrvmFbs+MFWise0SfAABYg+2iuEOfXrjJQ/1L6bM6dAMhoVwjn7WCeJrxhAgK9cxq4UqUiBYnuoRkBKRrDH/13ErxrLQLU0izLSqSlr8x8eG/sfhQ5iYbNbTTJ4s+CPi2aeQKBgQDlacO27JZAwxFCi5Ysr7AFJKbmhL5RU5oCvySUWX9TF8vsDOopsfrOO3aoSmraNFBmmqfCNcjGxxoN7DFsId/fstEeDUDOesw698WTwxIMWeq3wRU9ZZsY4tuPSGQof3M5bZo6psCFfDnVLmd7NmgXyBMtcwLaak2ukmnhM1FM0wKBgQC7L67GPG2LXVwEtKyMjJ3mcyRg2mui5zihEbYR8dBGFKyw+n40KeO9GVUe/XGlfhzmsAL8nRNq3WkOp/p1An+oIbaqVkWK4GhDLfXNY9m0tT08ZJmb8zUPME3UYBp4CHfqwz4brZ7LfuRlQ9MhyJGsY+TklMXpbq5/EKscqY4zUQKBgGTXwPg/tyxwkr21riWmdLFOEy9xWldVcHWUn/e1/4n/A9yXMLdzTNQqv1XqoF2acV0ozmVITyFRk9pa4L6vhUFH0TRsjvg0cmHZWDhDn0NeoxZzAWsjprGhxvGM2LoUjAnKa+ksM6R5ntJeu44ltEv/3q4zyCVKZSs/oNslO6+RAoGAdyAErja7CRp1C0w6yD1vLc0ufvrfOV71OQhVDSxKRWViZEuu/rgHLmDPuMyeMNj2a4QWSYkEYKhsr3yJov3AVxwreI5yfuhsOQAs/bpIGuzS9Rx49GHui516VDdqwi3WybRFK2GJsdtZ8Ok0 NzVBhgntb4J2zkhF1DkYsCAQTBA=";
    String publickKeyCus2="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3yEPWVWcot9q1h7rFxCte95oAS1ZqyKFUBUXzLU1as3VzI5/hrKE6BSHTmPXSRovDEM/n35XBkCKYX9Y4XRsb5FVTo4nJHo0bBgLyyqHlAOcn6mAwDu4JCny/iauBH3Q03UdEB+6cxvxJyxmjCyMwh2s9kaf8jnCDoBBOITYx9nzp0qDhpRoZeE2dxRqfxoVR6Y+SlD5lbiz7h8XU3kr++FVNcwJuu4REQk309IeOlSR4/SQ9Bq50K18XVJGMemOHPfrWAWBM9SNegL8OAnZwqYL/PFFeRMcloMDMNnSknnPgHpzuxr1Qk+To7XUY6VeufISCZJfkOsHltpbxLs9uwIDAQAB";


    public BuyCoinAdapter(Activity mActivity, List<BuyCoinTotalModel.ProductCoins> mArrayList, TextView tx_buycointotal, BillingClient billingClient, String securityToken, BaseActivity baseActivity) {
        this.mActivity = mActivity;
        this.mArrayList = (ArrayList<BuyCoinTotalModel.ProductCoins>) mArrayList;
        this.tx_bucointotal=tx_buycointotal;
        this.securityToken=securityToken;
        this.baseActivity=baseActivity;
       // this.billingClient0=billingClient;
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mActivity).inflate(R.layout.buy_coin_item, parent, false);
        }

        BuyCoinTotalModel.ProductCoins mModel = mArrayList.get(position);
        ImageView img = (ImageView) convertView.findViewById(R.id.bucoin_item_image);
        TextView tx_item_coin=convertView.findViewById(R.id.bucoin_item_coin);
        TextView tx_item_price=convertView.findViewById(R.id.bucoin_item_price);
        TextView tx_buycoin_item=convertView.findViewById(R.id.bucoin_item_buy);

        Glide.with(mActivity).load(mModel.getImage()).placeholder(R.drawable.bitcoin_blackbg).into(img);
        String strScore=mModel.getCoins();
        String coin=strScore.replaceFirst(".*?(\\d+).*", "$1");
        int val=Integer.parseInt(coin);
        if (val<=1)
        {
            tx_item_coin.setText(strScore);
        }
        else {
            tx_item_coin.setText(strScore);
        }

        tx_item_price.setText(mModel.getPrice());



        
        tx_buycoin_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tx_buycoin_item.setClickable(false);
                showProgressDialog(mActivity);

                String strScore=mModel.getCoins();
                String coin=strScore.replaceFirst(".*?(\\d+).*", "$1");
                switch (coin) {
                    case "10":

                        product = "com.divineray.app.10_coin";
                        break;
//                    case "10":
//
//                        product = "com.divineray.app.1_coin_test";
//                        break;

                    case "20":
                        product = "com.divineray.app.20_coins";
                        break;
                    case "50":
                        product = "com.divineray.app.50_coins";
                        break;
                    case "100":
                        product = "com.divineray.app.100_coins";
                        break;
                    case "220":
                        product = "com.divinerat.app.220_coins";
                        break;
                }
              //   product="android.test.purchased";
                skuList0.add(product);


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setupBillingClient();
                        tx_buycoin_item.setClickable(true);
                    }
                },1000);


                 item_id=mModel.getId();
            }
        });


        return convertView;
    }

    private void setupBillingClient() {

        dismissProgressDialog();
        billingClient0 = BillingClient.newBuilder(mActivity).enablePendingPurchases().setListener(this).build();
        billingClient0.startConnection(new BillingClientStateListener(){
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {

                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is setup successfully
                    loadAllSKUs();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        });
    }


    private void loadAllSKUs() {
        if (billingClient0.isReady())
        {
            SkuDetailsParams params = SkuDetailsParams.newBuilder()
                    .setSkusList(skuList0)
                    .setType(BillingClient.SkuType.INAPP)
                    .build();

            billingClient0.querySkuDetailsAsync(params, new SkuDetailsResponseListener() {
                @Override
                public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> skuDetailsList) {
                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK
                                && !skuDetailsList.isEmpty())

                    {
                        for (Object skuDetailsObject : skuDetailsList) {
                            final SkuDetails skuDetails = (SkuDetails) skuDetailsObject;

                            if (skuDetails.getSku().equals(product)) {
                                Purchase.PurchasesResult result = billingClient0.queryPurchases(BillingClient.SkuType.INAPP);
                                List<Purchase> purchases = result.getPurchasesList();

                                mSkuDetails = skuDetails;

                                BillingFlowParams billingFlowParams = BillingFlowParams
                                        .newBuilder()
                                        .setSkuDetails(skuDetails)
                                        .build();

                                if (check) {
                                    billingClient0.launchBillingFlow(mActivity, billingFlowParams);
                                check=false;
                                }
                            }

                                }


                        }
                }
            });
        }
        else
        {
            Log.e("Testy","Billing client not ready");
        }

    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> purchases) {
        int responseCode = billingResult.getResponseCode();
        if (responseCode == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
            Log.e("Testu", "Alerady bought");
        }
        else if (responseCode == BillingClient.BillingResponseCode.OK
                && purchases != null) {
            for (Purchase purchase : purchases) {
                handlePurchase(purchase);
            }
        }
        else if (responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
            check = true;
        } else {
            Log.e("Testu", ""+responseCode);
            check = true;
        }
    }

    private void handlePurchase(Purchase purchase) {
        //One time consume to show other time
        // Verify the purchase.
        // Ensure entitlement was not already granted for this purchaseToken.
        // Grant entitlement to the user.

        ConsumeParams consumeParams =
                ConsumeParams.newBuilder()
                        .setPurchaseToken(purchase.getPurchaseToken())
                        .build();

        ConsumeResponseListener listener = new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(BillingResult billingResult, String purchaseToken) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    String  orderid=purchase.getOrderId();
                    String d= String.valueOf(purchase.getPurchaseTime());
                    String purchaseTokena=String.valueOf(purchase.getPurchaseToken());
                    String devicemodel=android.os.Build.MANUFACTURER +" "+ android.os.Build.MODEL;
                    String devicetype=String.valueOf(2);
                    String userId=DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null);

                    Map<String, String> mMap2 = new HashMap<>();
                    mMap2.put("user_id",userId );
                    mMap2.put("productcoins_id",item_id);
                    mMap2.put("orderId",orderid);
                    mMap2.put("deviceName",devicemodel);
                    mMap2.put("deviceType",devicetype);
                    mMap2.put("purchaseToken",purchaseTokena);
 //                   KeyPair kp = getKeyPair();
//                    PublicKey publicKey = kp.getPublic();
//                    byte[] publicKeyBytes = publicKey.getEncoded();
//                    String publicKeyBytesBase64 = new String(Base64.encode(publicKeyBytes, Base64.DEFAULT));
//
//                    PrivateKey privateKey = kp.getPrivate();
//                    byte[] privateKeyBytes = privateKey.getEncoded();
//                    String privateKeyBytesBase64 = new String(Base64.encode(privateKeyBytes, Base64.DEFAULT));
//
//

                    JSONObject jsonObject=new JSONObject();
                    try {
                        jsonObject.put("user_id",userId);
                        jsonObject.put("id",item_id);
                        jsonObject.put("orderId",orderid);
                        jsonObject.put("deviceName",devicemodel);
                        jsonObject.put("deviceType",devicetype);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                 String enc=Base64.encodeToString(encryptaa(jsonObject.toString(), publickKeyCus2),Base64.DEFAULT);


                        try {
                            details=enc;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // String
                        // Handle the success of the consume operation.
                        executeBuyCoinItemapi();


                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }







                }
            }
        };

        billingClient0.consumeAsync(consumeParams, listener);


//One time
//        if (purchase.getSku().equals(product) && purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
//            if (!purchase.isAcknowledged())
//            {
//                AcknowledgePurchaseParams acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder().setPurchaseToken(purchase.getPurchaseToken()).build();
//                billingClient0.acknowledgePurchase(acknowledgePurchaseParams, new AcknowledgePurchaseResponseListener(){
//
//                    @Override
//                    public void onAcknowledgePurchaseResponse(BillingResult billingResult) {
//                        if(billingResult.getResponseCode()== BillingClient.BillingResponseCode.OK){
//                            Toast.makeText(mActivity, "Purchase Acknowledged", Toast.LENGTH_SHORT).show();
//                            executeBuyCoinItemapi();
//                        }
//                    }
//                });
//            }
//
//
//        }
    }


    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();


    }
    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams() {

        Map<String, String> mMap = new HashMap<>();
        mMap.put("details", details.trim());

      //  mMap.put("usertoken",securityToken);
        Log.e("PayR", "**PARAM**"+mMap.toString()+"::::::" + securityToken);
        return mMap;
    }
    private void executeBuyCoinItemapi() {

        showProgressDialog(mActivity);
       ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.buyCoinNew(securityToken,mParams()).enqueue(new Callback<BuyCoinSModel>() {
            @Override
            public void onResponse(Call<BuyCoinSModel> call, Response<BuyCoinSModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
               BuyCoinSModel model=response.body();

               if (model.getStatus()==1) {

                   String decrpt = model.getEncryptedString();

                   String decryptedString21 = null;
                   try {
                       decryptedString21 = decryptaa(decrpt, privatekeyCUS2);
                   } catch (IllegalBlockSizeException e) {
                       e.printStackTrace();
                   } catch (InvalidKeyException e) {
                       e.printStackTrace();
                   } catch (BadPaddingException e) {
                       e.printStackTrace();
                   } catch (NoSuchAlgorithmException e) {
                       e.printStackTrace();
                   } catch (NoSuchPaddingException e) {
                       e.printStackTrace();
                   }
                   JSONObject jsonObject1 = null;
                   try {
                       jsonObject1 = new JSONObject(decryptedString21);
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }
                   String fil = null;
                   try {
                       fil = String.valueOf(jsonObject1.get("totalCoins"));
                       Toast.makeText(mActivity, ""+model.getMessage(), Toast.LENGTH_SHORT).show();
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }
                   Log.e("Checkqw", "AAAANetQQ" + fil);
                   tx_bucointotal.setText(fil);

               }
               else
               {
                   showAlertDialog(mActivity,model.getMessage());
               }

            }


            @Override
            public void onFailure(Call<BuyCoinSModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });

    }
    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }














    public static byte[] encryptaa(String data, String publicKey) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding","BC");
        cipher.init(Cipher.ENCRYPT_MODE, getPublicKey(publicKey));
        return cipher.doFinal(data.getBytes());
    }

    public static String decryptaa(byte[] data, PrivateKey privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return new String(cipher.doFinal(data));
    }

    public static String decryptaa(String data, String base64PrivateKey) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        return decryptaa(Base64.decode(data.getBytes(),Base64.DEFAULT), getPrivateKey(base64PrivateKey));
    }


    public static KeyPair getKeyPair() {
        KeyPair kp = null;
        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(2048);
            kp = kpg.generateKeyPair();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return kp;
    }




    public static PublicKey getPublicKey(String base64PublicKey){
        PublicKey publicKey = null;
        try{
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.decode(base64PublicKey.getBytes(),Base64.DEFAULT));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(keySpec);
            return publicKey;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return publicKey;
    }

    public static PrivateKey getPrivateKey(String base64PrivateKey){

        PrivateKey privateKey = null;
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.decode(base64PrivateKey.getBytes(),Base64.DEFAULT));
        KeyFactory keyFactory = null;
        try {
            keyFactory = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            privateKey = keyFactory.generatePrivate(keySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return privateKey;
    }


    public static PrivateKey getPublicKey2(String base64PrivateKey){

        PrivateKey privateKey = null;
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.decode(base64PrivateKey.getBytes(),Base64.DEFAULT));
        KeyFactory keyFactory = null;
        try {
            keyFactory = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            privateKey = keyFactory.generatePrivate(keySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return privateKey;
    }


}
