package com.divineray.app.BuyCoin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;

import com.divineray.app.Utils.ExpandableHeightGridView;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.BuyCoinTotalModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuyCoins extends BaseActivity implements PurchasesUpdatedListener {
    String TAG = BuyCoins.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = BuyCoins.this;

    @BindView(R.id.buycoin_backbutton)
    ImageView buycoin_backbutton;
    @BindView(R.id.buycoin_totalcoins_val)
    TextView tx_buycointotal;
    @BindView(R.id.buycoin_grid_rly)
    RelativeLayout rly_grid;
    @BindView(R.id.lly_buycoins_nothing)
    LinearLayout lly_noposts;
    @BindView(R.id.buycoin_gridView)
    ExpandableHeightGridView simpleGrid;
    List<BuyCoinTotalModel.ProductCoins> mArrayList = new ArrayList<>();

    BillingClient billingClient;
    String product="com.divineray.app_p_50_coin";
    List<String> skulist = new ArrayList<>();
    BaseActivity baseActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTransparentStatusBarOnly(this);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_buy_coins);
        ButterKnife.bind(this);

        simpleGrid.setFastScrollEnabled(true);
        simpleGrid.setExpanded(true);
        executeTotalCoinApi();

    }



    @OnClick({R.id.buycoin_backbutton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buycoin_backbutton:
                back();
                break;

        }}

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("lastId","");
        mMap.put("perPage","100");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void executeTotalCoinApi() {
          showProgressDialog(mActivity);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getTotalCoins(getAuthToken(),mParams()).enqueue(new Callback<BuyCoinTotalModel>() {
            @Override
            public void onResponse(Call<BuyCoinTotalModel> call, Response<BuyCoinTotalModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                BuyCoinTotalModel mModel = response.body();
                //assert mGetDetailsModel != null;
                assert mModel != null;
                if (mModel.getStatus()==1) {
                tx_buycointotal.setText(mModel.getTotalCoins());
                    simpleGrid.setVisibility(View.VISIBLE);
                    lly_noposts.setVisibility(View.GONE);
                    mArrayList =response.body().getProductCoins();
                    if (mArrayList!=null){
                        setAdapter();
                    }
                    if (mArrayList.size()<12)
                    {
                        rly_grid.setMinimumHeight(800);
                    }
                } else if (response.body().getStatus()==0) {
                   showAlertDialog(mActivity,response.body().getMessage());
               }
            }

            @Override
            public void onFailure(Call<BuyCoinTotalModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
        // tabAllApi();

    }

    private void setAdapter() {

       BuyCoinAdapter customAdapter = new BuyCoinAdapter(mActivity, mArrayList,tx_buycointotal,billingClient,getAuthToken(),baseActivity);
        simpleGrid.setAdapter(customAdapter);
    }


    private void back() {
        finish();
    }

    @Override
    public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> list) {
    }
}