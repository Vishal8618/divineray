package com.divineray.app.Chat;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.divineray.app.Home.SelectedUserProfile;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.StringChange.StringFormatter;
import com.divineray.app.Utils.DivineRayAppplication;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.activities.HomeActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.ChatMessageModel;
import com.divineray.app.model.ChhatUneadMessageModel;
import com.divineray.app.model.RoomModel;
import com.google.gson.Gson;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends BaseActivity {
    /*
  Getting the Current Class Name
 */
    String TAG = ChatActivity.this.getClass().getSimpleName();

    /*
      Current Activity Instance
     */
    Activity mActivity = ChatActivity.this;

    @BindView(R.id.chat_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.chat_input_msg)
    EditText ed_inputMsg;
    @BindView(R.id.chat_send_msg)
    LinearLayout lly_chat_send_msg;
    TextView tx_chat_head;
    ImageView chat_back;

    String roomId,chatUserId,chatUsername;
    ArrayList<ChatMessageModel.DataChat> mArrayList = new ArrayList<>();
    String user_Id;
    String toServerUnicodeEncoded="";
    private Socket mSocket;
    private ChatMessageAdapter adapter;
    String chatMessageId="";
    TextView tx_username;
    LinearLayout lly_duumychat;
    TextView dummy_message,dummy_time;
    String toServer;
    SharedPreferences prefv;
    String msgid_new;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        chat_back=findViewById(R.id.chat_back);
        tx_chat_head=findViewById(R.id.txt_chat_head);
        tx_username=findViewById(R.id.txt_chat_head);
        dummy_message=findViewById(R.id.chat_dummymsg);
        lly_duumychat=findViewById(R.id.chat_dummylayout);
        dummy_time=findViewById(R.id.chati_dummytime);
        lly_chat_send_msg.setClickable(true);
        ed_inputMsg.setText(toServer);
        prefv=getSharedPreferences("pref", Context.MODE_PRIVATE);


        chatUserId=getIntent().getStringExtra("chatUserId");
        chatUsername=getIntent().getStringExtra("chatuserName");
        recyclerView.setPadding(0,0,0,40);


        getRoomId();
    //    Log.e("Cheklopo","VVV"+chatUsername+"::::");
        if (chatUsername!=null) {
            try {

                tx_chat_head.setText(StringFormatter.capitalizeWord(chatUsername));
            } catch (Exception e) {
                e.printStackTrace();
                tx_chat_head.setText("Unknown");
            }
        }
        else
        {
            tx_chat_head.setText("Unknown");
        }

        RoomApiFetch();
        getDetails();

        chat_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toServer = ed_inputMsg.getText().toString();
                String userId=chatUserId;
                prefv.edit().putString(userId,toServer).commit();
                if (getIntent().getStringExtra("isComingFromFCM")!=null) {
                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.putExtra("menuFragment", "favoritesMenuItem");
                    startActivity(intent);
                    overridePendingTransition(R.anim.in_from_top, R.anim.out_from_bottom);
                    finish();
                } else {
                    finish();
                }
            }
        });

        tx_username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mActivity, SelectedUserProfile.class);
                intent.putExtra("profileUserId",chatUserId);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        lly_chat_send_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    lly_chat_send_msg.setClickable(false);
                 toServer = ed_inputMsg.getText().toString();
                ed_inputMsg.setText("");

            //    if (isValidate()) {
                    if (!toServer.equals("") ) {
                        toServerUnicodeEncoded = StringEscapeUtils.escapeJava(toServer);
                        toServerUnicodeEncoded=StringEscapeUtils.unescapeJava(toServer);
                        if (chatMessageId.equals(""))
                        {

                        recyclerView.setPadding(0, 0, 0, 80);
                        lly_duumychat.setVisibility(View.GONE);
                        dummy_message.setText(toServer);
                        Date currentTime = Calendar.getInstance().getTime();
                        SimpleDateFormat sdf = new java.text.SimpleDateFormat(" hh:mm a ");
                        // give a timezone reference for formatting (see comment at the bottom)
                        sdf.setTimeZone(TimeZone.getDefault());

                        String formattedDate = sdf.format(currentTime);
                        dummy_time.setText(formattedDate);}
                        // }
                        // }
                        if (!isNetworkAvailable(mActivity)) {
                            Toast.makeText(mActivity, "No internet connection", Toast.LENGTH_SHORT).show();
                        } else {
                            lly_chat_send_msg.setClickable(false);
                            Log.e("Chatt",toServerUnicodeEncoded);
                            fetchData();

                        }
                    }
            }
        });


        //Screen wasn't scrolling to bottom , Temporary fixation 2 cases
        ed_inputMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        scrollToBottom();
                    }
                },200);
            }
        });
        ed_inputMsg.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scrollToBottom();
                        }
                    },200);
                }

            }
        });


    }

    private void getRoomId() {
        if (getIntent()!=null)
        {
            roomId=getIntent().getStringExtra("roomId");
            DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.ROOM_ID, roomId);
            chatUserId=getIntent().getStringExtra("chatUserId");
            chatUsername=getIntent().getStringExtra("chatuserName");

        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        toServer = ed_inputMsg.getText().toString();
        String userId=chatUserId;
        prefv.edit().putString(userId,toServer).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

        String message=prefv.getString(chatUserId,"");
        String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(message);
        ed_inputMsg.setText(fromServerUnicodeDecoded);
        ed_inputMsg.setSelection(ed_inputMsg.getText().length());
        //ed_inputMsg.requestFocus();
        String mm=getIntent().getStringExtra("message");
        if (mm!=null && mm.length()>1) {
            ed_inputMsg.setText(Objects.requireNonNull(getIntent()).getStringExtra("message"));
        }
        DivineRayAppplication app = (DivineRayAppplication) getApplication();

        mSocket = app.getSocket();
        mSocket.emit("ConncetedChat", roomId);
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("newMessage", onNewMessage);
        mSocket.on("leaveChat", onUserLeft);
        mSocket.on("online",onUserJoined);
        mSocket.connect();

       // executeDetailsApi();
        scrollToBottom();
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("Test","connect");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                 //   Toast.makeText(mActivity, "Connect", Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("Test","disconnect");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                 //   Toast.makeText(mActivity, "Disconnect", Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("Test", "Error connecting");

//                    Toast.makeText(getApplicationContext(),
//                            R.string.error_connect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.e("Test", "New message1");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.e("Test", "New message2"+args[1].toString());
                        JSONObject mJsonObject =  new JSONObject(args[1].toString());
                        Log.e("tesD",mJsonObject.toString());
                        ChatMessageModel.DataChat model = new Gson().fromJson(mJsonObject.toString(), ChatMessageModel.DataChat.class);

                        if (!mArrayList.get(mArrayList.size()-1).getMessageTime().equals(model.getMessageTime()))
                        {
                            mArrayList.add(model);
                            adapter.notifyDataSetChanged();

                        }

                        if (model != null) {
                            recyclerView.scrollToPosition(mArrayList.size() - 1);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };
    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.e("Test", "User joined");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject mJsonData = null;
                    try {
                        mJsonData = new JSONObject(String.valueOf(args[1]));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };

    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("Test", "User Left");
                }
            });
        }
    };

    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    boolean isTyping = (boolean) args[1];
                    Log.e("Test", "User Typing");
                }
            });
        }
    };

    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
Log.e("Test","user stop typing");
                }
            });
        }
    };

    private Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {

        }
    };


    private void fetchData() {
               executeChatApi();


    }

    private boolean isValidate() {
        boolean flag = true;

        if (toServer.equals("")) {
           flag = false;
        }



        return flag;
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("roomId",roomId);
        mMap.put("messageId",chatMessageId);
        mMap.put("message",toServerUnicodeEncoded);
        Log.e("Message", "Message" + mMap.toString()+ ""+chatUserId);
        return mMap;
    }

    private void executeChatApi() {
        SimpleDateFormat sdf = new SimpleDateFormat(" hh:mm a ");
        long currentTimestamp = System.currentTimeMillis()/1000L;
        String data = ed_inputMsg.getText().toString();

        if (chatMessageId.equals("")) {
            ChatMessageModel.DataChat mModel = new ChatMessageModel.DataChat();
            mModel.setUser_id(getUserID());
            mModel.setMessageTime(String.valueOf(currentTimestamp));

            mModel.setMessage(toServer);
            mArrayList.add(mArrayList.size(), mModel);
        }

        try {
            adapter.notifyDataSetChanged();
        }
        catch (Exception e)
        {
            e.printStackTrace();

        }


            if (mArrayList.size()>1) {
                recyclerView.scrollToPosition(mArrayList.size() - 1);
            }



     ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.addUpdateChatMessage(getAuthToken(),mParams2()).enqueue(new Callback<ChatMessageModel>() {
            @Override
            public void onResponse(Call<ChatMessageModel> call, Response<ChatMessageModel> response) {
                dismissProgressDialog();
                ChatMessageModel chatMessageModel = response.body();
                Log.e("Checjkl", chatMessageModel.getData().get(0).toString());
                if (mArrayList == null || mArrayList.size() <
                        2) {
                    executeDetailsApi();
                }
                if (!chatMessageId.equals("")) {
                    executeDetailsApi();
                }
                recyclerView.setPadding(0, 0, 0, 60);
                recyclerView.setNestedScrollingEnabled(true);
                chatMessageId = "";
                toServerUnicodeEncoded = "";
                lly_chat_send_msg.setClickable(true);

                JSONObject mObject0=new JSONObject();

                Log.e("Jack", chatMessageModel.getData().toString());
                try {
                    mObject0.put("messagId",chatMessageModel.getData().get(0).getMessageId());
                    mObject0.put("user_id",chatMessageModel.getData().get(0).getUser_id());
                    mObject0.put("roomId",chatMessageModel.getData().get(0).getRoomId());
                    mObject0.put("message",chatMessageModel.getData().get(0).getMessage());
                    mObject0.put("messageTime",chatMessageModel.getData().get(0).getMessageTime());
                    mObject0.put("follow",chatMessageModel.getData().get(0).getFollow());
                    mObject0.put("username",chatMessageModel.getData().get(0).getUsername());
                    mObject0.put("userProfileImage",chatMessageModel.getData().get(0).getUserProfileImage());
                    mObject0.put("userProfileId",chatMessageModel.getData().get(0).getUserProfileId());

                } catch (Exception e)
                {e.printStackTrace();}
                mSocket.emit("newMessage", roomId, mObject0, ed_inputMsg.getText().toString());
                Log.e("", "**RESPONSE**" + response.body());
                if (adapter != null) {
                   recyclerView.scrollToPosition(mArrayList.size()-1);
                    lly_chat_send_msg.setClickable(true);
                }



            }

            @Override
            public void onFailure(Call<ChatMessageModel> call, Throwable t) {
           //     Toast.makeText(mActivity, "mesage unuccess"+ t.getMessage(), Toast.LENGTH_SHORT).show();
               // Log.e("Test",""+t.getMessage());
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });

    }

    private void getDetails() {
        if (!isNetworkAvailable(mActivity)) {
            Toast.makeText(mActivity, "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
            executeDetailsApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParamsR() {
        chatUserId=getIntent().getStringExtra("chatUserId");
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("chatUserId", chatUserId);
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void RoomApiFetch() {
        //showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.checkRoom(getAuthToken(),mParamsR()).enqueue(new Callback<RoomModel>() {
            @Override
            public void onResponse(Call<RoomModel> call, Response<RoomModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                RoomModel mGetDetailsModel = response.body();
                if (mGetDetailsModel.getStatus()==1)
                {
                    roomId=mGetDetailsModel.getData().get(0).getRoomId();

                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.ROOM_ID, roomId);
                }
                else if (mGetDetailsModel.getStatus()==0)
                {
                    Log.e("Status","0");
                }

            }

            @Override
            public void onFailure(Call<RoomModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("chatUserId",chatUserId);
        mMap.put("lastMessageId","");
        mMap.put("perPage","100");
        mMap.put("roomId",roomId);
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    private void executeDetailsApi() {
      //  showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getUserChats(getAuthToken(),mParams()).enqueue(new Callback<ChatMessageModel>() {
            @Override
            public void onResponse(Call<ChatMessageModel> call, Response<ChatMessageModel> response) {
                lly_duumychat.setVisibility(View.GONE);
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                ChatMessageModel mGetDetailsModel = response.body();
                if (mGetDetailsModel!=null) {
                    if (mGetDetailsModel.getStatus() == 1) {
                        mArrayList = (ArrayList<ChatMessageModel.DataChat>) response.body().getData();
                        if (mArrayList != null) {
                            user_Id = DivineRayPrefernces.readString(mActivity, DivineRayPrefernces.USERID, null);
                            String chatUsernamee = mGetDetailsModel.getChatUserDetails().getName();
                            msgid_new=mGetDetailsModel.getData().get(0).getMessageId();
                            setSearchAdapter();
                            executReadApi();
                        }

                    } else {
                        Log.e("Data","0");  }
                }

            }

            @Override
            public void onFailure(Call<ChatMessageModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams1() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", chatUserId);
        mMap.put("roomId",roomId);
      //  mMap.put("lastMessageId","");

        return mMap;
    }

    private void executReadApi() {
        //  showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.sendReadMessage(getAuthToken(),mParams1()).enqueue(new Callback<ChhatUneadMessageModel>() {
            @Override
            public void onResponse(Call<ChhatUneadMessageModel> call, Response<ChhatUneadMessageModel> response) {

                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                ChhatUneadMessageModel mGetDetailsModel = response.body();
                if (mGetDetailsModel!=null) {
                    if (mGetDetailsModel.getStatus() == 1) {
                        Log.e("Data","1");

                    } else {
                        Log.e("Data","0");  }
                }

            }

            @Override
            public void onFailure(Call<ChhatUneadMessageModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }
    private void setSearchAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);


        Collections.sort( mArrayList, new Comparator<ChatMessageModel.DataChat>() {
            @Override
            public int compare(ChatMessageModel.DataChat o1, ChatMessageModel.DataChat o2) {
                int a=Integer.parseInt(o1.getMessageTime());
                int b=Integer.parseInt(o2.getMessageTime());

                return Integer.compare(a,b);
            }

        } );

        adapter = new ChatMessageAdapter(mActivity, (ArrayList<ChatMessageModel.DataChat>) mArrayList,user_Id,new ChatMessageAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int positon, ChatMessageModel.DataChat item, View view) {
                if (view.getId() == R.id.chat_right_msg_text_view) {
                    Show_video_option(item);
                }
            }
        });

        recyclerView.setAdapter(adapter);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollToBottom();
            }
        },1000);
        scrollToBottom();
     //   adapter.notifyDataSetChanged();
    }

    private void scrollToBottom() {
        if(mArrayList!=null&&mArrayList.size()>1) {
            recyclerView.scrollToPosition(mArrayList.size()-1);
        }
    }


    private void Show_video_option(final ChatMessageModel.DataChat mModel) {

        final CharSequence[] options = { "Edit" };

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mActivity,R.style.AlertDialogCustom);

        builder.setTitle(null);

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Edit"))
                {
                    chatMessageId=mModel.getMessageId();
                    //Toast.makeText(mActivity, ""+chatMessageId, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(mModel.getMessage());
                   ed_inputMsg.setText(fromServerUnicodeDecoded);
                    ed_inputMsg.setSelection(ed_inputMsg.getText().length());

                }


                else if (options[item].equals("Cancel")) {
                  //  chatMessageId="";
                    dialog.dismiss();

                }

            }

        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width =900;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        alertDialog.getWindow().setAttributes(layoutParams);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        executReadApi();
        toServer = ed_inputMsg.getText().toString();
        String userId=chatUserId;
        prefv.edit().putString(userId,toServer).commit();

        if (getIntent().getStringExtra("isComingFromFCM")!=null) {
              Intent intent = new Intent(mActivity, HomeActivity.class);
                intent.putExtra("menuFragment", "favoritesMenuItem");
                startActivity(intent);
                overridePendingTransition(R.anim.in_from_top, R.anim.out_from_bottom);
                finish();
            } else {
                finish();
            }

    }
}
