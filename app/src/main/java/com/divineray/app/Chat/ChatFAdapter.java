package com.divineray.app.Chat;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.divineray.app.R;
import com.divineray.app.Search.RecyclerViewAdapter;
import com.divineray.app.StringChange.StringFormatter;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.model.ChatModel;
import com.divineray.app.model.SearchModelget;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatFAdapter extends RecyclerView.Adapter<ChatFAdapter.ViewHolder>implements Filterable {
    Context mContext;
    ArrayList<ChatModel.DataC> mArrayList = new ArrayList<>();
    ArrayList<ChatModel.DataC> FilteredmArrayList =new ArrayList<>();
    String fcm_otheruserid,fcm_roomid,fcm_otherusername,datatype;
    ChatModel mGetDetailsModel;
    String user_Id;
    public ChatFAdapter(Context context, ArrayList<ChatModel.DataC> mArrayList, String user_Id, ChatModel mGetDetailsModel, String datatype, String fcm_roomid, String fcm_otherusername, String fcm_otheruserid) {
        this.mContext=context;
        this.mArrayList=mArrayList;
        this.user_Id=user_Id;
        this.mGetDetailsModel=mGetDetailsModel;
        this.datatype=datatype;
    //    this.FilteredmArrayList=mArrayList;

        this.fcm_otheruserid=fcm_otheruserid;
        this.fcm_otherusername=fcm_otherusername;
        this.fcm_roomid=fcm_roomid;
    }

    @NonNull
    @Override
    public ChatFAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_message_item, parent, false);
        return new ChatFAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ChatModel.DataC mModel = mArrayList.get(position);



        Glide.with(mContext)
                .load(mModel.getUserProfileImage())
                .placeholder(mContext.getResources().getDrawable(R.drawable.unknown_user))
                .into(holder.photo);
        if (Integer.parseInt(mModel.getUnReadCount())>0) {
            holder.tx_msgcount.setText(mModel.getUnReadCount());
            holder.tx_msgcount.setVisibility(View.VISIBLE);
        }
        String serverResponse =mModel.getLastMessage();

        String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(serverResponse);
        holder.tx_lastmessage.setText(fromServerUnicodeDecoded);
        if (mModel.getUsername()!=null&&!mModel.getUsername().equals("")) {
            try {
                holder.tx_username.setText(StringFormatter.capitalizeWord(mModel.getUsername()));

            } catch (Exception e) {
                e.printStackTrace();
                holder.tx_username.setText(mModel.getUsername());

            }
        }
        else
        {
            holder.tx_username.setText("Unknown");
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent=new Intent(mContext,ChatActivity.class);
                intent.putExtra("roomId",mModel.getRoomId());
                String user_Id = DivineRayPrefernces.readString(mContext, DivineRayPrefernces.USERID, null);
                if (mModel.getChatUserId().equals(user_Id))
                {
                    intent.putExtra("chatUserId",mModel.getUser_id());
                    //    Toast.makeText(mContext, ""+mModel.getUser_id(), Toast.LENGTH_SHORT).show();
                }
                else {
                    intent.putExtra("chatUserId",mModel.getChatUserId());
                       //  Toast.makeText(mContext, user_Id+"AAA"+mModel.getChatUserId(), Toast.LENGTH_SHORT).show();
                }

                intent.putExtra("chatuserName",mModel.getUsername());
                mContext.startActivity(intent);

            }
        });
    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mArrayList=FilteredmArrayList;

                } else {
                    ArrayList<ChatModel.DataC> filteredList = new ArrayList<>();
                    for (ChatModel.DataC row : FilteredmArrayList) {


                        //change this to filter according to your case
                        if (row.getUsername().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    mArrayList = (ArrayList<ChatModel.DataC>) filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values =mArrayList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mArrayList = (ArrayList) filterResults.values;
                notifyDataSetChanged();

            }
        };
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView photo;
        TextView tx_username,tx_lastmessage,tx_msgcount;
        LinearLayout lly_goto;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            photo=itemView.findViewById(R.id.chatf_photo);
            tx_username=itemView.findViewById(R.id.chatf_username);
            tx_lastmessage=itemView.findViewById(R.id.chatf_usermessage);
            lly_goto=itemView.findViewById(R.id.chatf_goto_button);
            tx_msgcount=itemView.findViewById(R.id.message_count);
        }
    }
}
