package com.divineray.app.Chat;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseFragment;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.ChatModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChatFragment extends BaseFragment {
    RecyclerView recyclerView;
    List<ChatModel.DataC> mArrayList = new ArrayList<>();
    public Dialog progressDialog;
    EditText tx_searchtab;
    TextView tx_searchAllusers;
    ChatFAdapter chatFAdapter;
    SwipeRefreshLayout swiperefresh;
    ProgressBar p_bar;
    ChatModel mGetDetailsModel;
    LinearLayout lly_chatf_nothing;

    public String fcm_otheruserid="",fcm_roomid="",fcm_otherusername="";
    String strtext="",datatype="";
    String user_Id;
    Context context;

    public ChatFragment()
    {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.chat_fragment, container, false);
        tx_searchtab=view.findViewById(R.id.chatf_searchTab);
        recyclerView = view.findViewById(R.id.chatf_recyclerr);
        swiperefresh=view.findViewById(R.id.swiperefresh_chat);
        p_bar=view.findViewById(R.id.p_bar_chat);
        lly_chatf_nothing=view.findViewById(R.id.lly_chatf_nothing);
        tx_searchAllusers=view.findViewById(R.id.chatf_searchTab_text);
        swiperefresh.setProgressViewOffset(false, 0, 200);
        context=getContext();

      
         //   Log.e("Vishv","if"+"userid: "+fcm_otheruserid+" username: "+fcm_otherusername+" roomid"+fcm_roomid);
//    strtext = getArguments().getString("Datatype");
//    if (!(strtext==null&&strtext.equals("")))
//    {

   // }
         //   Toast.makeText(getContext(), "nnjj"+fcm_otheruserid+"dd"+fcm_otherusername+"sj"+fcm_roomid, Toast.LENGTH_SHORT).show();




        swiperefresh.setColorSchemeResources(R.color.black);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getDetails();
            }
        });


        getDetails();

        tx_searchAllusers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(getContext(), SearchTabScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });

        tx_searchtab.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if (mArrayList.size()>1) {
                    chatFAdapter.getFilter().filter(string);
                }
                else {}
            }
        });

        return  view;
    }

    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();


    }

    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    private void getDetails() {
        if (!isNetworkAvailable(context)) {
            Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
            executeDetailsApi();
        }
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.USERID,null));
        mMap.put("perPage","1000");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    @Override
    public void onResume() {
        super.onResume();
        user_Id =  DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.USERID,null);

       executeDetailsApi();

    }

    private void executeDetailsApi() {
       String authtoken= DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.AUTHTOKEN,null);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getAllChats(authtoken,mParams()).enqueue(new Callback<ChatModel>() {
            @Override
            public void onResponse(Call<ChatModel> call, Response<ChatModel> response) {
                dismissProgressDialog();
                swiperefresh.setRefreshing(false);
                Log.e("", "**RESPONSE**" + response.body());
                 mGetDetailsModel = response.body();
                if (mGetDetailsModel!=null) {
                    if (mGetDetailsModel.getStatus() == 1) {
                        lly_chatf_nothing.setVisibility(View.GONE);
                        mArrayList = response.body().getData();

                        //   if (mArrayList!=null){


                        Log.e("Tgn", "0");
                        String id_user;
                        for (int i = 0; i < mArrayList.size(); i++) {
                            Log.e("Tgn", "1" + mGetDetailsModel.getData().get(0).getChatUserId());
                            if (datatype != null && datatype.equals("FCM")) {
                                if (mGetDetailsModel.getData().get(i).getChatUserId().equals(user_Id)) {
                                    id_user = mGetDetailsModel.getData().get(i).getUser_id();
                                    //    Toast.makeText(mContext, ""+mModel.getUser_id(), Toast.LENGTH_SHORT).show();
                                } else {
                                    id_user = mGetDetailsModel.getData().get(i).getChatUserId();
                                    //     Toast.makeText(mContext, ""+mModel.getChatUserId(), Toast.LENGTH_SHORT).show();
                                }

                                if (id_user.equals(fcm_otheruserid)) {
                                    Log.e("Bingo", "if" + mGetDetailsModel.getData().get(i).getUsername());


                                    OpenChat(mGetDetailsModel.getData().get(i), id_user);


                                    Log.e("Jadu", "roomid: " + mGetDetailsModel.getData().get(i).getRoomId() + " chatuserid:" + id_user + " chatuserName" + mGetDetailsModel.getData().get(i).getUsername());

                                }

                            }

                        }
                        setChatAdapter();
                        //   }

                    } else if (mGetDetailsModel.getStatus() == 0) {
                        lly_chatf_nothing.setVisibility(View.VISIBLE);
                        //Toast.makeText(getActivity(), "Error Getting daa", Toast.LENGTH_SHORT).show();
                    } else {
                        showDialogLogOut(getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void OpenChat(ChatModel.DataC dataC, String id_user) {

        Intent intentq = new Intent(getActivity(), ChatActivity.class);
        intentq.putExtra("roomId", dataC.getRoomId());
        intentq.putExtra("chatUserId", id_user);
        intentq.putExtra("chatuserName",dataC.getUsername());
        intentq.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intentq);
    }

    private void setChatAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);




    Log.e("Tgb","elsek");
    chatFAdapter = new ChatFAdapter(context, (ArrayList<ChatModel.DataC>) mArrayList,user_Id,mGetDetailsModel,datatype,fcm_roomid,fcm_otherusername,fcm_otheruserid);


       recyclerView.setAdapter(chatFAdapter);
    }
}