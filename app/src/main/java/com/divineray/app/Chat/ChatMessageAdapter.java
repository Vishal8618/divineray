package com.divineray.app.Chat;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.divineray.app.Home.VideoAdapter;
import com.divineray.app.R;
import com.divineray.app.StringChange.StringFormatter;
import com.divineray.app.model.ChatMessageModel;
import com.divineray.app.model.ChatModel;
import com.divineray.app.model.HomeModel;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatMessageAdapter extends RecyclerView.Adapter<ChatMessageAdapter.ViewHolder> {
    Context mContext;
    ArrayList<ChatMessageModel.DataChat> mArrayList = new ArrayList<>();
    String senderUserId;
    private ChatMessageAdapter.OnItemClickListener listener;
    String formattedDate;
    // meker the onitemclick listener interface and this interface is impliment in Chatinbox activity
    // for to do action when user click on item
    public interface OnItemClickListener {
        void onItemClick(int positon,ChatMessageModel.DataChat item, View view);
    }

    public ChatMessageAdapter(Context context, ArrayList<ChatMessageModel.DataChat> mArrayList,  String senderUserId, OnItemClickListener listener) {
        this.mContext = context;
        this.mArrayList = mArrayList;

        this.senderUserId = senderUserId;
        this.listener = listener;

    }

    @NonNull
    @Override
    public ChatMessageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item, parent, false);
        return new ChatMessageAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatMessageAdapter.ViewHolder holder, int position) {
        ChatMessageModel.DataChat mModel = mArrayList.get(position);
        holder.bind(position, mModel, listener);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Toast.makeText(mContext, ""+mModel.getMessageId(), Toast.LENGTH_SHORT).show();
            }
        });

            if (senderUserId.equals(mModel.getUser_id())) {
                long coment_time = Long.parseLong(mModel.getMessageTime());
                timeChange(coment_time);
                holder.tx_RightChatTime.setText(formattedDate);
                String serverResponse = mModel.getMessage();
                String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(serverResponse);

                holder.tx_RightChatMessage.setText(fromServerUnicodeDecoded);
                holder.lly_right.setVisibility(View.VISIBLE);
                holder.tx_RightChatTime.setVisibility(View.VISIBLE);
                holder.tx_leftChatTime.setVisibility(View.GONE);
                holder.lly_left.setVisibility(View.GONE);

            } else {
                long coment_time = Long.parseLong(mModel.getMessageTime());
                timeChange(coment_time);
                holder.tx_leftChatTime.setText(mModel.getUsername() + ", " + formattedDate);
                Glide.with(mContext).load(mModel.getUserProfileImage()).placeholder(mContext.getResources().getDrawable(R.drawable.unknown_user))
                        .into(holder.photo);
                // Glide.with(mContext).load(mModel.getUserProfileImage()).req.into(holder.photo);
                String serverResponse = mModel.getMessage();
                String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(serverResponse);
                holder.tx_leftChatMessage.setText(fromServerUnicodeDecoded);

                holder.lly_left.setVisibility(View.VISIBLE);
                holder.lly_right.setVisibility(View.GONE);
                holder.tx_leftChatTime.setVisibility(View.VISIBLE);
                holder.tx_RightChatTime.setVisibility(View.GONE);
            }


    }
void timeChange(long coment_time)
{
    Date date = new java.util.Date(coment_time * 1000L);
    SimpleDateFormat sdf = new java.text.SimpleDateFormat(" hh:mm a ");
    sdf.setTimeZone(TimeZone.getDefault());
    formattedDate = sdf.format(date);
}

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView photo;
        TextView tx_leftChatTime, tx_leftChatMessage, tx_RightChatTime, tx_RightChatMessage;
        LinearLayout lly_left,lly_right;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            photo = itemView.findViewById(R.id.chati_left_photo);
            tx_leftChatTime = itemView.findViewById(R.id.chati_left_time);
            tx_leftChatMessage = itemView.findViewById(R.id.chat_left_msg_text_view);
            tx_RightChatTime = itemView.findViewById(R.id.chati_right_time);
            tx_RightChatMessage = itemView.findViewById(R.id.chat_right_msg_text_view);
            lly_left=itemView.findViewById(R.id.chat_left_msg_layout);
            lly_right=itemView.findViewById(R.id.chat_right_msg_layout);
        }

        public void bind(int position, ChatMessageModel.DataChat mModel, OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });
            tx_RightChatMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });

        }
    }
}
