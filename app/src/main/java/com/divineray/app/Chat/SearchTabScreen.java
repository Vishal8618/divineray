package com.divineray.app.Chat;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.GetAllUserModel;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchTabScreen extends BaseActivity {
    List<GetAllUserModel.Data> mArrayList = new ArrayList<>();

    public Dialog progressDialog;
    RecyclerView recyclerView;
    SearchView searchView;


    @BindView(R.id.search_toolbar)
    Toolbar search_toolbar;
    @BindView(R.id.text_no_result_chattab)
    TextView text_no_result;
    @BindView(R.id.swiperefresh_chattab)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.p_bar_chattab)
    ProgressBar p_bar;
    @BindView(R.id.shimmer_view_container_chat)
    public ShimmerFrameLayout mShimmerViewContainer;
    String search_name="";


    EditText ed_searchBar;
    TextView search_button;

    SearchTabAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_search_tab_screen);
        setSupportActionBar((Toolbar) search_toolbar);
        ButterKnife.bind(this);
        //custom_searchview=findViewById(R.id.Tab_searchViewD);
        recyclerView = findViewById(R.id.searchtab_recycler);


        ed_searchBar = search_toolbar.findViewById(R.id.search_text);
        search_button=search_toolbar.findViewById(R.id.searchtab_search_button);

        swiperefresh.setColorSchemeResources(R.color.black);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getDetails();
            }
        });

        getDetails();




       ed_searchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    search_name=ed_searchBar.getText().toString().trim();
                    getDetails();
                    return true;
                }
                return false;
            }
        });

        ed_searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if (adapter!=null) {
                    adapter.getFilter().filter(string);
                }
            }
        });
    }





    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    private void getDetails() {
        if (!isNetworkAvailable(this)) {
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
            executeDetailsApi();
        }
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(this,DivineRayPrefernces.USERID,null));
        mMap.put("search",search_name);
        mMap.put("perPage","10000");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeDetailsApi() {
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getUserList(getAuthToken(),mParams()).enqueue(new Callback<GetAllUserModel>() {
            @Override
            public void onResponse(Call<GetAllUserModel> call, Response<GetAllUserModel> response) {
                swiperefresh.setRefreshing(false);
                Log.e("Test","success"+response.body().toString());
                Log.e("", "**RESPONSE**" + response.body());
                GetAllUserModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {
                    mArrayList =response.body().getData();
                    if (mArrayList!=null){
                        setSearchAdapter();
                        ed_searchBar.requestFocus();
                    }

                } else {
                    text_no_result.setVisibility(View.VISIBLE);
                    text_no_result.setText("No Results found for "+"'"+ed_searchBar.getText().toString()+"'");
                }
            }

            @Override
            public void onFailure(Call<GetAllUserModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Test","Failure"+t.getMessage());
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setSearchAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
         adapter = new SearchTabAdapter(this, (ArrayList<GetAllUserModel.Data>) mArrayList,text_no_result,mShimmerViewContainer);
        recyclerView.setAdapter(adapter);

    }



    @Override
    protected void onRestart() {
        super.onRestart();
        getDetails();
        ed_searchBar.setText("");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.setVisibility(View.GONE);
        super.onPause();
    }


    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.setVisibility(View.VISIBLE);



    }
}