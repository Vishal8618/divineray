package com.divineray.app.FCM;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.widget.Toast;


import androidx.core.app.NotificationCompat;

import com.divineray.app.Agora.ConstantsA;
import com.divineray.app.Chat.ChatActivity;
import com.divineray.app.Notifications.GetAllNotifications;
import com.divineray.app.R;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.LiveStreaming2Activity;
import com.divineray.app.activities.SplashActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import io.agora.rtc.Constants;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
   Intent intent;
    String title , message = "";
    private static int NOTIFICATION_ID = 6578;
    String notificationid;
    Spanned mssg;
    String  notification_type = "";
    JSONObject json;
    String chatUsername,roomId,chatUserId;
    String val="";

    String val2;
    static public String newMsg;
    String newUserID;
    String newNotifyType;

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.e("newToken", token);
        //Add your token in your sharepreferences.
        getSharedPreferences("_", MODE_PRIVATE).edit().putString("fcm_token", token).apply();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e("Vishv", "From: " + remoteMessage.getData());
        Log.e("Vishv", "From: " + remoteMessage.getFrom());
        title=remoteMessage.getData().get("title");
        val=remoteMessage.getData().get("body");
        try {
            val2 = val.replaceAll("\\\\n", "");
            val2 = val.replaceAll("\\\\u", "");
            val2 = val.replaceAll("\\\\r", "");
            val2 = val.replaceAll("\\\\t", "");

        }
        catch ( Exception e)
        {
            e.printStackTrace();
        }

   if (val!=null&&!val.equals("N/A")) {
       mssg = Html.fromHtml(val);

   }
   else {
       val="";
   }


        try {
           json = new JSONObject(remoteMessage.getData().get("notificationData"));
           notification_type = json.getString("notificationType");
         if (notification_type.equals("Message"))
         {
           chatUsername= json.getString("name");
           roomId= json.getString("roomId");
           chatUserId=json.getString("chatUserId");
          try {
          if (newUserID!=null&&newUserID.equals(chatUserId)&&newMsg!=null)
           {
            if (newMsg.equals("")||newMsg!=null)
            {
                val=val;

            }
            else {

                val=val;
            }

        }
        else {
            newUserID=chatUserId;
            newMsg=val;


        }
    }
    catch (Exception e)
    {
        e.printStackTrace();
    }




}

            notificationid=json.getString("notificationId");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            sendNotification(json,title, message,notification_type,notificationid,chatUserId,chatUsername,roomId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        newNotifyType=notification_type;
    }
    public static String encodeEmoji (String message) {
        try {
            return URLEncoder.encode(message,
                    "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }
    private void sendNotification(JSONObject json, String title, String message, String notifyType, String NotifyId, String chatUserIdd, String chatUsernamed, String roomIdd) throws JSONException {
        if (notifyType != null && notification_type!=null) {
            Log.e("Rs",""+notification_type);

            if (notification_type.equals("Message")) {

              Log.e("Notify","Message"+"RRRRRR");
                intent = new Intent(this, ChatActivity.class);

                intent.putExtra("roomId", roomIdd);
                intent.putExtra("chatUserId", chatUserIdd);
                intent.putExtra("chatuserName",chatUsernamed);
                intent.putExtra("isComingFromFCM","isComing");


            }
            else if(notification_type.equals("6")) {
                intent = new Intent(this, SplashActivity.class);
            }
            else if(notification_type.equals("7")) {
                DivineRayPrefernces.writeString(this, DivineRayPrefernces.STREAM_USERID, json.getString("stream_user_id"));
                DivineRayPrefernces.writeString(this, DivineRayPrefernces.STREAM_ROOMID,json.getString("stream_room"));
                DivineRayPrefernces.writeString(this, DivineRayPrefernces.STREAM_USERNAME, json.getString("stream_user_name"));
                DivineRayPrefernces.writeString(this, DivineRayPrefernces.STREAM_ID, json.getString("stream_id"));
                DivineRayPrefernces.writeString(this, DivineRayPrefernces.STREAM_USERPHOTO, json.getString("stream_user_photo"));
                DivineRayPrefernces.writeString(this, DivineRayPrefernces.COMMENT_DISABLE, json.getString("disable_comment"));
                DivineRayPrefernces.writeString(this, DivineRayPrefernces.LIKE_DISABLE, json.getString("disable_like"));

                gotoRoleActivity(json.getString("stream_room"), json.getString("own_user_id"));

                intent = new Intent(this, LiveStreaming2Activity.class);
            }
            else {
                Log.e("Notify","Other");
                intent = new Intent(this, GetAllNotifications.class);
                intent.putExtra("notificationType", notifyType);
                intent.putExtra("notificationId", NotifyId);
            }
        }
       PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        String channelId = getString(R.string.default_notification_channel_id);
          Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder;
          if (val.equals(""))
          {
              notificationBuilder =
                      new NotificationCompat.Builder(this, channelId)
                              .setSmallIcon(R.mipmap.ic_launcher)
                              .setContentTitle(title)
                              .setAutoCancel(true)
                              .setSound(defaultSoundUri)
                              .setContentIntent(pendingIntent);

          }
          else {
              notificationBuilder =
                      new NotificationCompat.Builder(this, channelId)
                              .setSmallIcon(R.mipmap.ic_launcher)
                              .setContentTitle(title)
                              .setStyle(new NotificationCompat.BigTextStyle().bigText(val2))
                              .setContentText(val2)
                              .setAutoCancel(true)

                              .setSound(defaultSoundUri)
                              .setContentIntent(pendingIntent);
          }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {

                NotificationChannel channel = new NotificationChannel(channelId, getString(R.string.default_notification_channel_name),
                        NotificationManager.IMPORTANCE_DEFAULT);
                channel.enableVibration(true);
                channel.setShowBadge(true);
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null) {
            if (newNotifyType!=null &&!newNotifyType.equals(notification_type))
            {
                NOTIFICATION_ID++;
            }
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());

        }


    }

    public void gotoRoleActivity(String stream_room, String userID) {
        int mil= (int) (System.currentTimeMillis()/1000L);
        ConstantsA.channelNameA=stream_room;
        ConstantsA.useridA=userID;
        onJoinAsAudience();
    }

    public void onJoinAsBroadcaster() {
        gotoLiveActivity(Constants.CLIENT_ROLE_BROADCASTER);
    }
    public void onJoinAsAudience() {
        gotoLiveActivity(Constants.CLIENT_ROLE_AUDIENCE);
    }

    private void gotoLiveActivity(int role) {
        //
        intent=new Intent(this, LiveStreaming2Activity.class);
        intent.putExtra(ConstantsA.KEY_CLIENT_ROLE, role);
    }

}