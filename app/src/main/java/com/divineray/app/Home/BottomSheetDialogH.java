package com.divineray.app.Home;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.GetCommentsModel;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.divineray.app.Home.ItemAdapter.decodeEmoji;
import static com.divineray.app.Notifications.SingleVideoView.home_comments_singlevid;
import static com.divineray.app.Notifications.SingleVideoView.totalcomments_singlevid;


public class BottomSheetDialogH extends BottomSheetDialogFragment implements ItemAdapter.ItemListener{
    ImageView bottomsheet_closebutton;
    RecyclerView recyclerView_bottom;
    LinearLayout lly_commentf_nothing;
    ItemAdapter mAdapter;
    EditText ed_commenttext;
    LinearLayout lly_addcommnet;
    TextView tx_bottom_head;
    Dialog progressDialog;

    String comment_videoId;
    String comment_userid;

    String b_userid,b_videoid;
    ItemAdapter itemAdapter;
    int count_vid=0;
    String commentId="";
    String toServerUnicodeEncoded="";
    boolean singleVideo;
    Fragment_Data_Send fragment_data_send;
     int comment_count=0;
    ArrayList<GetCommentsModel.Dataw> mQuotesArrayList2 = new ArrayList<>();

    public BottomSheetDialogH(int comment_counnt, Fragment_Data_Send fragment_data_send) {
       this.comment_count=comment_counnt;
        this.fragment_data_send=fragment_data_send;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate( R.layout.bottom_sheet_layout, container, false);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        bottomsheet_closebutton=v.findViewById(R.id.bottomsheet_closebutton);
        recyclerView_bottom = v.findViewById(R.id.recyclerView_bottomSheet);
        ed_commenttext=v.findViewById(R.id.bottomsheet_addcomment_text);
        lly_addcommnet=v.findViewById(R.id.bottomsheet_commentadd);
        tx_bottom_head=v.findViewById(R.id.bottomsheet_heading_commentno);
        lly_addcommnet.setVisibility(View.GONE);
        lly_addcommnet.setClickable(true);
        ed_commenttext.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        ed_commenttext.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                switch (motionEvent.getAction()& MotionEvent.ACTION_MASK)
                {
                    case  MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });
        ed_commenttext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s=editable.toString();
                if (s.equals("")||s==null)
                {
                    lly_addcommnet.setVisibility(View.GONE);
                }
                else {
                    lly_addcommnet.setClickable(true);
                    lly_addcommnet.setVisibility(View.VISIBLE);
                }

            }
        });
        lly_commentf_nothing=v.findViewById(R.id.lly_commentf_nothing);
        lly_addcommnet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValidate()) {
                    lly_addcommnet.setClickable(false);
                    String toServer = ed_commenttext.getText().toString();
                    toServerUnicodeEncoded = StringEscapeUtils.escapeJava(toServer);
                    toServerUnicodeEncoded=StringEscapeUtils.unescapeJava(toServer);
                    //toServerUnicodeEncoded=encodeEmoji(toServer);
                    Log.e("Commentt",toServerUnicodeEncoded);
                    AddCommentApi();
                }
            }

        });

        bottomsheet_closebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        recyclerView_bottom.setHasFixedSize(true);
        recyclerView_bottom.setLayoutManager(new LinearLayoutManager(getContext()));

        //rrayList<GetCommentsModel.Dataw> mQuotesArrayList2 = new ArrayList<>();
       // mAdapter = new ItemAdapter(getContext(),mQuotesArrayList2);
      //  recyclerView_bottom.setAdapter(mAdapter);
        Bundle mArgs=getArguments();
         b_userid=mArgs.getString("b_comment_userid");
         b_videoid=mArgs.getString("b_comment_videoId");
        itemAdapter=new ItemAdapter(getActivity());
        recyclerView_bottom.setAdapter(itemAdapter);
        executeCommentApi();
        return v;

    }

    public static String encodeEmoji (String message) {
        try {
            return URLEncoder.encode(message,
                    "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }
    private boolean isValidate() {
        boolean flag = true;
        if (ed_commenttext.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), "Please add comment", Toast.LENGTH_SHORT).show();
            flag = false;

        }
        else if (ed_commenttext.getText().length()>100) {
            Toast.makeText(getContext(), "Can't add greater than 100 words", Toast.LENGTH_SHORT).show();
            flag = false;

        }
        return flag;
    }



    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();


    }

    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(getContext(),DivineRayPrefernces.ID,null));
        mMap.put("videoId",b_videoid);
        mMap.put("comment",toServerUnicodeEncoded);

        mMap.put("commentId",commentId);

        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void AddCommentApi() {
        String authtoken= DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.AUTHTOKEN,null);

        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addComment(authtoken,mParams()).enqueue(new Callback<GetCommentsModel>() {
            @Override
            public void onResponse(Call<GetCommentsModel> call, Response<GetCommentsModel> response) {
                GetCommentsModel mHomeModel = response.body();
dismissProgressDialog();
            ed_commenttext.setText("");
            lly_addcommnet.setClickable(true);
                singleVideo=false;
                executeCommentApi();

              //  Toast.makeText(getContext(), ""+totalcomments_singlevid, Toast.LENGTH_SHORT).show();

               Log.e("Commentc",""+commentId+""+singleVideo+"");

                //Toast.makeText(getActivity(), "Api responsed sccess", Toast.LENGTH_SHORT).show();

           //     Toast.makeText(getActivity(), "Api re", Toast.LENGTH_SHORT).show();



            }


            @Override
            public void onFailure(Call<GetCommentsModel> call, Throwable t) {
                   dismissProgressDialog();
                executeCommentApi();

                    singleVideo=true;


                ed_commenttext.setText("");
                count_vid=count_vid+1;

                Log.e("", "API_error" + t.getMessage());
              //  Toast.makeText(getContext(), "Modal error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Map<String, String> mParam2s() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", b_userid);
        mMap.put("videoId",b_videoid);
        mMap.put("lastId", "0");

        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void executeCommentApi() {
        String authtoken= DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.AUTHTOKEN,null);

        // showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getComments(authtoken,mParam2s()).enqueue(new Callback<GetCommentsModel>() {
            @Override
            public void onResponse(Call<GetCommentsModel> call, Response<GetCommentsModel> response) {
                Log.e("Vall",""+totalcomments_singlevid);
                Log.e("", "**RESPONSE**" + response.body());
                GetCommentsModel mHomeModel = response.body();
              //  Toast.makeText(getContext(), ""+singleVideo, Toast.LENGTH_SHORT).show();
            if (singleVideo)
            {

                if (totalcomments_singlevid!=null)
                {
                    int val=Integer.parseInt(totalcomments_singlevid);
                    int val0=val+1;
                    totalcomments_singlevid=String.valueOf(val0);

                    home_comments_singlevid.setText(String.valueOf(val0));
                }

            }
                    comment_count++;
                if(fragment_data_send!=null){
                    if (commentId.equals(""))
                    {
                    //    Toast.makeText(getContext(), "if", Toast.LENGTH_SHORT).show();
                      //  comment_count=comment_count-1;
                        fragment_data_send.onDataSent(""+comment_count);
                    }
                    else {
                        comment_count=comment_count-1;
                        fragment_data_send.onDataSent(""+comment_count);
                      //  Toast.makeText(getContext(), "else", Toast.LENGTH_SHORT).show();
                    }
                    commentId="";
                   }
                if (mHomeModel.getStatus()==1) {
                    lly_commentf_nothing.setVisibility(View.GONE);
                    recyclerView_bottom.setVisibility(View.VISIBLE);
                    mQuotesArrayList2= (ArrayList<GetCommentsModel.Dataw>) response.body().getData();





                    tx_bottom_head.setText("Comments"+"("+mHomeModel.getTotalComments()+")");
                    setAdapterC();


                    Log.e("", "**RESPONSE**" + response.body());


                }
                else if(response.body().getStatus()==0){
                    lly_commentf_nothing.setVisibility(View.VISIBLE);
                    recyclerView_bottom.setVisibility(View.GONE);
                    Log.e("Data","0");
                }
            }

            @Override
            public void onFailure(Call<GetCommentsModel> call, Throwable t) {

                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setAdapterC() {
        itemAdapter.updateList(mQuotesArrayList2, new ItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int positon, GetCommentsModel.Dataw item, View view) {
                if (view.getId() == R.id.bottomsheet_usercomment) {
                    if (item.getUser_id().equals(DivineRayPrefernces.readString(getContext(),DivineRayPrefernces.ID,null)))
                    {
                        Show_comment_option(item);
                        Log.e("User","Another");
                    }
                    else
                    {
                      Log.e("User","Other");
                    }
                }
                else if (view.getId() == R.id.bottomsheet_usercomment_time){
                    if (item.getUser_id().equals(DivineRayPrefernces.readString(getContext(),DivineRayPrefernces.ID,null)))
                    {
                        Show_comment_option(item);
                        Log.e("User","Another");
                    }
                    else
                    {
                        Log.e("User","Other");
                    }
                }
                else if (view.getId() == R.id.bottomsheet_usercomment_username)
                {
                OpenProfile(item);
                }
                else if (view.getId() == R.id.bottomsheet_usercomment_photo)
                {
                    OpenProfile(item);
                }
            }
        });
    }

    private void OpenProfile(GetCommentsModel.Dataw item) {
        Intent intent=new Intent(getContext(),SelectedUserProfile.class);
        intent.putExtra("profileUserId",item.getUser_id());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);
    }

    private void Show_comment_option(GetCommentsModel.Dataw mModel) {
        final CharSequence[] options = { "Edit" };

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(getContext()),R.style.AlertDialogCustom);

        builder.setTitle(null);

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Edit"))
                {
                    commentId=mModel.getCommentId();
                    dialog.dismiss();
                    String serverResponse =mModel.getComment();
                    // String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(serverResponse);
                    String fromServerUnicodeDecoded=decodeEmoji(serverResponse);
                    ed_commenttext.setText(fromServerUnicodeDecoded);
                    ed_commenttext.setSelection(ed_commenttext.getText().length());

                }


                else if (options[item].equals("Cancel")) {
                    commentId="";
                    dialog.dismiss();

                }

            }

        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width =900;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        alertDialog.getWindow().setAttributes(layoutParams);

    }

    @Override
    public void onItemClick(String item) {

    }
}
