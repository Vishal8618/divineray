package com.divineray.app.Home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.divineray.app.Agora.ConstantsA;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseFragment;
import com.divineray.app.activities.LiveStreaming1Activity;
import com.divineray.app.activities.LiveStreaming2Activity;
import com.divineray.app.adapters.LiveuserHomeScreenAdapter;
import com.divineray.app.adapters.LiveuserScreenAdapter;
import com.divineray.app.adapters.StreamRecHomeScreenAdapter;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.DataItem;
import com.divineray.app.model.LiveUsersModel;
import com.divineray.app.model.StatusModel;
import com.divineray.app.model.StreamRecordedModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.agora.rtc.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Home2Fragment extends BaseFragment {
    @BindView(R.id.recyclerLiveUsers)
    RecyclerView recyclerLiveUsers;
    @BindView(R.id.recyclerStreamUsers)
    RecyclerView recyclerStreamUsers;
    @BindView(R.id.llyGoLive)
    LinearLayout llyGoLive;
    @BindView(R.id.swiperefresh_liveusers)
    SwipeRefreshLayout swiperefresh_liveusers;
    ArrayList<LiveUsersModel.Data> mArraylistLiveUsers=new ArrayList<>();

    LiveuserHomeScreenAdapter adapter1;

    public Home2Fragment() {
        // Required empty public constructor
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_home2, container, false);
        ButterKnife.bind(this,view);
        config().setVideoDimenIndex(4);
        setDataLiveUsers();

        swiperefresh_liveusers.setProgressViewOffset(false, 0, 200);

        swiperefresh_liveusers.setColorSchemeResources(R.color.black);
        swiperefresh_liveusers.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setDataLiveUsers();
            }
        });

        return  view;
    }

    @OnClick({R.id.llyGoLive})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.llyGoLive:
                performLiveClick();
                break;

        }
    }

    public  void  setDataLiveUsers()
    {
        if (isNetworkAvailable((Activity) getContext())) {
            executeLiveUsersApi();
            setRecordedStreams();

        }
        else
        {
            swiperefresh_liveusers.setRefreshing(false);
            showToast((Activity) getContext(), getResources().getString(R.string.internet_conection));
        }
    }

    void executeLiveUsersApi() {
        swiperefresh_liveusers.setRefreshing(true);
        String authtoken = DivineRayPrefernces.readString(getActivity(), DivineRayPrefernces.AUTHTOKEN, null);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.showLiveUsers(authtoken).enqueue(new Callback<LiveUsersModel>() {
            @Override
            public void onResponse(Call<LiveUsersModel> call, Response<LiveUsersModel> response) {
                swiperefresh_liveusers.setRefreshing(false);
                LiveUsersModel mModel = response.body();
                mArraylistLiveUsers.clear();
                if (mModel != null) {
                    if (mModel.getStatus() == 1) {
                        mArraylistLiveUsers.addAll(mModel.getData());
                    }

                }
                setLiveUsersAdapter(mArraylistLiveUsers);
            }

            @Override
            public void onFailure(Call<LiveUsersModel> call, Throwable t) {
                swiperefresh_liveusers.setRefreshing(false);
                ;

            }
        });
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParamS() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        mMap.put("page_no", "1");
        mMap.put("per_page", "100");
        return mMap;
    }

    private void setRecordedStreams() {

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getAllStreams(getAuthToken(), mParamS()).enqueue(new Callback<StreamRecordedModel>() {
            @Override
            public void onResponse(Call<StreamRecordedModel> call, Response<StreamRecordedModel> response) {
                StreamRecordedModel model=response.body();
                if (model.getStatus() == 1) {

                    setRecordedStreamAdapter(model.getData());
                } else {
                    showToast(model.getMessage());

                }
            }

            @Override
            public void onFailure(Call<StreamRecordedModel> call, Throwable t) {

            }
        });
    }

    private void setRecordedStreamAdapter(List<DataItem> data) {

        recyclerStreamUsers.setLayoutManager(new LinearLayoutManager(getContext()));
        StreamRecHomeScreenAdapter adapter= new StreamRecHomeScreenAdapter(data, getContext());
        recyclerStreamUsers.setAdapter(adapter);
    }

    private void setLiveUsersAdapter(ArrayList<LiveUsersModel.Data> mArraylistLiveUsers) {

        recyclerLiveUsers.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter1= new LiveuserHomeScreenAdapter(mArraylistLiveUsers, getContext(), new LiveuserHomeScreenAdapter.onItemClickClickListener() {
            @Override
            public void onItemClick(int position, ArrayList<LiveUsersModel.Data> mArrayList, View view) {
                switch (view.getId())
                {
                    case R.id.imLiveUserImage:
                        performJoinLiveUser(mArrayList,position);
                        break;
                }
            }
        });
        recyclerLiveUsers.setAdapter(adapter1);

    }

    //Go live
    private void performLiveClick() {
        if (isNetworkAvailable(getContext())) {
            startActivity(new Intent(getContext(), LiveStreaming1Activity.class));
            getActivity().overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        } else
        {
            showAlertDialog2((Activity) getContext(),getResources().getString(R.string.internet_conection) );
        }
    }



    //See live user stream
    private void performJoinLiveUser(ArrayList<LiveUsersModel.Data> mArrayList, int position) {
        if (isNetworkAvailable(getContext())) {
            DivineRayPrefernces.writeString(getContext(), DivineRayPrefernces.STREAM_USERID, mArrayList.get(position).getUser_id());
            DivineRayPrefernces.writeString(getContext(), DivineRayPrefernces.STREAM_ROOMID, mArrayList.get(position).getStream_room());
            DivineRayPrefernces.writeString(getContext(), DivineRayPrefernces.STREAM_USERNAME, mArrayList.get(position).getName());
            DivineRayPrefernces.writeString(getContext(), DivineRayPrefernces.STREAM_ID, mArrayList.get(position).getStream_id());
            DivineRayPrefernces.writeString(getContext(), DivineRayPrefernces.STREAM_USERPHOTO, mArrayList.get(position).getPhoto());
            DivineRayPrefernces.writeString(getContext(), DivineRayPrefernces.COMMENT_DISABLE, mArrayList.get(position).getDisable_comment());
            DivineRayPrefernces.writeString(getContext(), DivineRayPrefernces.LIKE_DISABLE, mArrayList.get(position).getDisable_like());
            gotoRoleActivity(mArrayList.get(position).getStream_room(), mArrayList.get(position).getUser_id());
        }
        else
        {
            showAlertDialog2((Activity) getContext(),getResources().getString(R.string.internet_conection));
        }
    }


    public void gotoRoleActivity(String stream_room, String userID) {
        int mil= (int) (System.currentTimeMillis()/1000L);
        // Intent intent = new Intent(mActivity, LiveStreaming2Activity.class);
        String room = String.valueOf(mil);
        //  config().setChannelName(stream_room);
        ConstantsA.channelNameA=stream_room;
        // Toast.makeText(getContext(), ""+userID, Toast.LENGTH_SHORT).show();
        //  config().setUid(getUserID());
        ConstantsA.useridA=getUserID();
        onJoinAsAudience();
        // onJoinAsBroadcaster();
    }
    public void onJoinAsBroadcaster() {
        gotoLiveActivity(Constants.CLIENT_ROLE_BROADCASTER);
    }
    public void onJoinAsAudience() {
        gotoLiveActivity(Constants.CLIENT_ROLE_AUDIENCE);
    }

    private void gotoLiveActivity(int role) {
        Intent intent = new Intent();
        intent.putExtra(ConstantsA.KEY_CLIENT_ROLE, role);
        intent.setClass(getContext(), LiveStreaming2Activity.class);
        startActivity(intent);
    }


}