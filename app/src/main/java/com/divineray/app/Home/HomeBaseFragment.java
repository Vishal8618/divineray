package com.divineray.app.Home;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.divineray.app.R;
import com.divineray.app.Search.SearchFragment;
import com.divineray.app.activities.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeBaseFragment extends BaseFragment {
    @BindView(R.id.txForYou)
    TextView txForYou;
    @BindView(R.id.txLiveEvents)
    TextView txLiveEvents;
    @BindView(R.id.viewHeader)
    View viewHeader;
    boolean home1Data=false;
    boolean home2Data=false;


    public HomeBaseFragment() {
        // Required empty public constructor
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_home_base, container, false);
        ButterKnife.bind(this,view);
        setHome1Data();
        return  view;
    }

    @OnClick({R.id.txForYou,R.id.txLiveEvents})
    public void onlicked(View view)
    {
        switch (view.getId())
        {
            case R.id.txForYou:
                setHome1Data();
                break;
            case R.id.txLiveEvents:
                setHome2Data();
                break;
        }
    }

    private void setHome1Data() {
        if (!home1Data) {
            home1Data=true;
            home2Data=false;
            txForYou.setTextColor(getResources().getColor(R.color.colorAccent));
            txLiveEvents.setTextColor(getResources().getColor(R.color.colorWhite));
            viewHeader.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            HomeFragment myFragment = new HomeFragment();
            transaction.replace(R.id.frame_layout_home, myFragment);
            transaction.commit();


        }
    }

    private void setHome2Data() {
        if (!home2Data) {
            home1Data = false;
            home2Data = true;
            txForYou.setTextColor(getResources().getColor(R.color.colorBlack));
            viewHeader.setBackgroundColor(getResources().getColor(R.color.colorBlack));
            txLiveEvents.setTextColor(getResources().getColor(R.color.colorAccent));
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            Home2Fragment myFragment = new Home2Fragment();
            transaction.replace(R.id.frame_layout_home, myFragment);
            transaction.commit();
        }
    }

}