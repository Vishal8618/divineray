package com.divineray.app.Home;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaCodec;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.divineray.app.R;
import com.divineray.app.Report.ReportVideo;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Search.HashtagItemView;
import com.divineray.app.Utils.Constants;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.Utils.Functions;
import com.divineray.app.Utils.ShareUtils;
import com.divineray.app.activities.BaseFragment;
import com.divineray.app.activities.LiveStreaming1Activity;
import com.divineray.app.activities.LiveUsersScreen;
import com.divineray.app.activities.LoginActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.HomeModel;
import com.divineray.app.model.SearchModeltagged;
import com.divineray.app.model.VideofavModel;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.downloader.request.DownloadRequest;
import com.facebook.login.LoginManager;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;

import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ligl.android.widget.iosdialog.IOSDialog;

import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.TELEPHONY_SERVICE;
import static android.os.Environment.DIRECTORY_DOCUMENTS;

import org.apache.commons.lang3.builder.Diff;


public class HomeFragment extends BaseFragment implements Player.EventListener , Fragment_Data_Send{
    Context context;
    SimpleExoPlayer player;

    RecyclerView recyclerView;
    ArrayList<HomeModel.Data> data_list;
    int currentPage = -1;
    LinearLayoutManager layoutManager;

    int pausePage=-1;
    VideoAdapter adapter;
    ProgressBar p_bar;
    SwipeRefreshLayout swiperefresh;

    boolean is_user_stop_video = false;
    int lastPosition=0;
    String videoID;
    Dialog progressDialog;
    HomeModel mHomeModel;
    MediaPlayer mediaPlayerBackground;
    //FastScroller fastScroller;
    int positionCheck;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }




    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        builder.detectFileUriExposure();
        makeLocation();
        context=getContext();
        p_bar=view.findViewById(R.id.p_bar);
        recyclerView=view.findViewById(R.id.recylerview_frag_home);
        layoutManager=new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);
        SnapHelper snapHelper =  new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);

        // this is the scroll listener of recycler view which will tell the current item number
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);


            }
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //here we find the current item number
                final int scrollOffset = recyclerView.computeVerticalScrollOffset();
                final int height = recyclerView.getHeight();
                int page_no=scrollOffset / height;
                if(page_no!=currentPage ){
                    currentPage=page_no;
                    pausePage=page_no;
                    Release_Privious_Player();
                    try {
                        Set_Player(currentPage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });



        swiperefresh=view.findViewById(R.id.swiperefresh);
        swiperefresh.setProgressViewOffset(false, 0, 200);

        swiperefresh.setColorSchemeResources(R.color.black);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage=-1;
                getData();
            }
        });
        getData();
        return view;

    }

    private void redirectioncheck() {
        String valueType=Constants.Redirection_type;
        String valueCheck=Constants.Redirection_value;
        if (!valueType.equals("")&&!valueCheck.equals("")) {
           int positionCheck = 0;
            if (data_list.size() > 0) {
                for (int i = 0; i <= data_list.size() - 1; i++) {
                    if (data_list.get(i).getVideoId().equals(valueCheck)) {
                        positionCheck = i;
                        Collections.swap(data_list,0,positionCheck);
                        Set_Adapter();
                        Constants.Redirection_type = "";
                        Constants.Redirection_value = "";
                    }
                } }
        }
            else
            {
                Constants.Redirection_type="";
                Constants.Redirection_value="";
            }
        }


    private void makeLocation() {
        if (check_permissions()) {
           File dir = new File(Environment.getExternalStorageDirectory() + "/DivineRayVideo/" + "DivineVideoCreator/");

           // File file;
            //for sdk 30
//            File dir;
//            if (Build.VERSION_CODES.O > Build.VERSION.SDK_INT) {
//                dir = new File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOCUMENTS).getPath()
//                        +  "//DivineRayVideo//" + "DivineVideoCreator");
//            } else {
//                dir = new File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOCUMENTS).getPath()
//                        +  "//DivineRayVideo//" + "DivineVideoCreator");
//            }
            try {
                if (!dir.exists()) {
                    Log.e("Checklop::","Directory created");
                    System.out.println("Directory created");
                    dir.mkdirs();
                } else {
                    dir.mkdirs();
                    Log.e("Checklop::","Directory is not created");
                    System.out.println("Directory is not created");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            File dir;
            dir = new File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOCUMENTS).getPath()
                    +  "//DivineRayVideo//" + "DivineVideoCreator");
            if (!dir.exists()) {
                Log.e("Checklop::","Directory created");
                System.out.println("Directory created");
                dir.mkdirs();
            }
        }

    }


    // when we swipe for another video this will relaese the privious player
    SimpleExoPlayer privious_player;
    public void Release_Privious_Player(){
        if(privious_player!=null) {
            privious_player.removeListener(this);
            privious_player.release();
        }
    }

    // this will call when swipe for another video and
    // this function will set the player to the current video
    public void Set_Player(final int currentPage) throws Exception{
      //  try {


        final HomeModel.Data item= data_list.get(currentPage);
        View layout=layoutManager.findViewByPosition(currentPage);
        layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);


        int i=0;
        int l=data_list.size();
        if (data_list.size()>3) {
            // Top
            if (lastPosition > currentPage) {

                if (currentPage > 3) {
                    Log.e("Ted", "1st if");
                    performN(currentPage);

                }

            } else if (lastPosition < currentPage) {//Bottom

                if (currentPage < data_list.size() - 3) {
                    Log.e("Ted", "2nd if");
                    perform(currentPage);
                } else {
                }
            } else if (currentPage == 0) {
                Log.e("Ted", "3rd if");
                perform(currentPage);
            }
            lastPosition = currentPage;
        }


        layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);

        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
        final SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(context, trackSelector);
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(context,
                Util.getUserAgent(context, "DivineRay"));
        CacheDataSourceFactory cacheDataSourceFactory= new CacheDataSourceFactory(DownloadUtil.getCache(context),dataSourceFactory);
        MediaSource videoSource = new ExtractorMediaSource.Factory(cacheDataSourceFactory)
                .createMediaSource(Uri.parse(data_list.get(currentPage).getPostVideo()));

      //  MediaSource mediaSource = buildMediaSource(Uri.parse("https://dharmaniapps.s3.ap-south-1.amazonaws.com/9520c7b1714cf49867ccd6992c267698_kyEVO6m7Y8x659kK6uTS5OP6YCgr8xxgTayw6lfy.m3u8"));

        player.prepare(videoSource);
        player.setRepeatMode(Player.REPEAT_MODE_ALL);

        player.addListener(this);
        videoID=item.getVideoId();
        ViewIncrease(currentPage,item);
        final PlayerView playerView=layout.findViewById(R.id.playerview);

     float checkVal=Float.parseFloat(item.getPostHeight());
        float checkVal2=Float.parseFloat(item.getPostWidth());
        float val2=checkVal-checkVal2;
         if (checkVal==checkVal2)
        {
            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
            player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            playerView.setPlayer(player);
        }
       else if (checkVal2>checkVal||val2<200)
        {
            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
            player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            playerView.setPlayer(player);
        }
        else if (checkVal2<checkVal) {
            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
            player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            playerView.setPlayer(player);
        }




        player.setPlayWhenReady(true);

        privious_player=player;
        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {

                    case Player.STATE_BUFFERING:
                      //  layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);
                      break;
                    case Player.STATE_IDLE:
                        layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);


                        break;

                    case Player.STATE_READY:

                        layout.findViewById(R.id.playerview).setVisibility(View.VISIBLE);
                        layout.findViewById(R.id.frame_thumbnail).setVisibility(View.INVISIBLE);
                        dismissProgressDialog();
                        break;

                    default:
                      //  layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });




       // final RelativeLayout mainlayout = layout.findViewById(R.id.mainlayout);
        SimpleExoPlayer finalPlayer = player;
        playerView.setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                    super.onFling(e1, e2, velocityX, velocityY);
                    float deltaX = e1.getX() - e2.getX();
                    float deltaXAbs = Math.abs(deltaX);
                    // Only when swipe distance between minimal and maximal distance value then we treat it as effective swipe
                    if((deltaXAbs > 100) && (deltaXAbs < 1000)) {
                        if(deltaX > 0)
                        {
                          //  OpenProfile(item,true);
                        }
                    }


                    return true;
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    super.onSingleTapUp(e);
                    if(!finalPlayer.getPlayWhenReady()){
                        is_user_stop_video=false;
                        privious_player.setPlayWhenReady(true);
                    }else{
                        is_user_stop_video=true;
                        privious_player.setPlayWhenReady(false);
                    }


                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Show_video_option(item);
                        }
                    },1000);


                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {

                    if(!finalPlayer.getPlayWhenReady()){
                        is_user_stop_video=false;
                        privious_player.setPlayWhenReady(true);
                    }
                    return super.onDoubleTap(e);

                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });

    }
    // this will call when go to the home tab From other tab.
    // this is very importent when for video play and pause when the focus is changes
    boolean is_visible_to_user;
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        is_visible_to_user=isVisibleToUser;

        if(privious_player!=null && (isVisibleToUser && !is_user_stop_video)){
            privious_player.setPlayWhenReady(true);
      }else if(privious_player!=null && !isVisibleToUser){
            privious_player.setPlayWhenReady(false);
        }
    }


    private MediaSource buildMediaSource(Uri uri) {
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context, "exoplayer-codelab");
        return new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
    }

    private void Show_video_option(final HomeModel.Data home_get_set) {

        final CharSequence[] options = { "Report Video","Save Video","Cancel" };

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context,R.style.AlertDialogCustom);

        builder.setTitle(null);

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Report Video"))

                {
                    dialog.dismiss();

                    Intent intent=new Intent(getContext(), ReportVideo.class);
                    intent.putExtra("videoId",home_get_set.getVideoId());
                    startActivity(intent);


                }
               else if (options[item].equals("Save Video"))

                {
                    dialog.dismiss();
                    if(Functions.Checkstoragepermision(getActivity()))
                        Save_Video(home_get_set);

                }

                else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }

            }

        });

        builder.show();

    }
    public void Save_Video(final HomeModel.Data item){
        String path =   Constants.app_folder3;
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdir();
        }

        String newF=getMD5EncryptedString(item.getPostVideo())+".mp4";
        String path2 =   Constants.app_folder3+newF;
        File dir2 = new File(path2);
        if (dir2.exists()) {
            Toast.makeText(context, "Video already downloaded", Toast.LENGTH_SHORT).show();
        }
        else {
            Functions.Show_determinent_loader(context,false,false);
            PRDownloader.initialize(getActivity().getApplicationContext());
            DownloadRequest prDownloader= PRDownloader.download(item.getPostVideo(), Constants.app_folder3, newF)
                    .build()
                    .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                        @Override
                        public void onStartOrResume() {

                        }
                    })
                    .setOnPauseListener(new OnPauseListener() {
                        @Override
                        public void onPause() {

                        }
                    })
                    .setOnCancelListener(new OnCancelListener() {
                        @Override
                        public void onCancel() {

                        }
                    })
                    .setOnProgressListener(new OnProgressListener() {
                        @Override
                        public void onProgress(Progress progress) {

                            int prog=(int)((progress.currentBytes*100)/progress.totalBytes);
                            Functions.Show_loading_progress(prog/2);

                        }
                    });


            prDownloader.start(new OnDownloadListener() {
                @Override
                public void onDownloadComplete() {
                    Functions.cancel_determinent_loader();
                    Toast.makeText(context, "Video stored to"+Constants.app_folder3, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(Error error) {

                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    Functions.cancel_determinent_loader();
                }


            });
        }






    }

    /*
     * Check Internet Connections
     * */
    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void getData() {


        if (!isNetworkAvailable(getActivity())) {
            Toast.makeText(context, "No internet connection", Toast.LENGTH_LONG).show();
        } else {
            executeDataApi();
        }

    }

    /*
     * Execute api
     * */
    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.setCancelable(false);
       if (progressDialog != null) {
           progressDialog.show();

       }
    }



    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(getActivity(), DivineRayPrefernces.ID, null));
        mMap.put("lastId", "");
        mMap.put("perPage", "100");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

     void executeDataApi() {
         String authtoken= DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.AUTHTOKEN,null);
         showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getVideos(authtoken,mParams()).enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
               try {
                   dismissProgressDialog();
               }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                swiperefresh.setRefreshing(false);
                Log.e("", "**RESPONSE**" + response.body());

                mHomeModel = response.body();
                Log.e("Home", "**RESPONSE**" + mHomeModel.getStatus());

                if (response.body() != null) {
                    if (mHomeModel.getStatus()==1) {
                          if (response.body().getData() != null) {
                              data_list = (ArrayList<HomeModel.Data>) response.body().getData();
                           redirectioncheck();
                              Set_Adapter();
                          } else {

                          }
                      }
                      else if (mHomeModel.getStatus()==0)
                      {
                          Log.e("Check","No Data");

                      }
                      else
                      {
                          showDialogLogOut(getActivity());
                      }



                }


                Log.e("", "**RESPONSE**" + response.body());
            }

            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void Set_Adapter() {
        dismissProgressDialog();
        adapter = new VideoAdapter(context, data_list,mHomeModel, new VideoAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int positon, HomeModel.Data item, View view) {
                switch (view.getId()) {
                    case R.id.shome_profile:
                        onPause();
                        OpenProfile(item,false);
                        break;

                    case R.id.home_title:
                      //  onPause();
                       // OpenProfile(item,false);
                        break;
                    case R.id.home_like_imagee:
                        videoID=item.getVideoId();
                        Like_Video(positon, item,currentPage);
                        break;

                    case R.id.home_dislike_imagee:
                        videoID=item.getVideoId();
                       // Toast.makeText(context, "diskil", Toast.LENGTH_SHORT).show();
                        DisLike_Video(positon, item);
                        break;

                    case R.id.home_comments_image_new:
                        OpenComment(item);
                        break;

                    case R.id.home_share_link_h:
                        ShareOpen(item);
                        break;
                    case R.id.linear_sound_h:
                        onPause();
                        OpenProfile(item,false);
                        break;
                    case R.id.imGoLive:
                        onPause();
                        OpenLiveStreaming();
                        break;


                }

            }
        });

        adapter.setHasStableIds(true);
        recyclerView.setAdapter(adapter);
//        new Handler().postDelayed(new Runnable() {
//          @Override
//          public void run() {

//          }
//       },1500);



    }

    private void OpenLiveStreaming() {
       startActivity(new Intent(context, LiveUsersScreen.class));
    }

    private void OpenMuscicVid(HomeModel.Data item) {
                        Intent intent=new Intent(context, HashtagItemView.class);
                intent.putExtra("musicId",item.getMusicId());
                intent.putExtra("tagName",item.getTitle());

        if((item.getMusicName()==null || item.getMusicName().equals("") || item.getMusicName().equals("null"))){
            intent.putExtra("tagD",item.getUserDetails().getName());
        }else {
            intent.putExtra("tagD",item.getMusicName());
        }

                context.startActivity(intent);
    }

    private void ShareOpen(HomeModel.Data item) {

        if (check_permissions()) {
            File dir = Environment.getExternalStorageDirectory();
            File dcim = new File(dir.getAbsolutePath() + "/Download/DivineRayVideo/" + videoID + ".mp4");
            if (dcim.exists()) {
                setVideoLinkData(Uri.fromFile(dcim), item, context);
            } else {
                showProgressDialog((Activity) context);
                downloadFileNew(item.getPostVideo(), item.getVideoId(), item);
            }}
    }

    private void setVideoLinkData(Uri fromFile, HomeModel.Data item, Context context) {

        showBottomSheetDilaog(fromFile,item,context);
    }

    private void showBottomSheetDilaog(Uri fromFile, HomeModel.Data item, Context context) {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.share_dialog_layout, null);
        view.setBackgroundColor(Color.TRANSPARENT);
        view.setBackgroundTintMode(PorterDuff.Mode.CLEAR);
        view.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        view.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getContext());
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.show();


        //bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        //fetching ids
        LinearLayout llyTwitter = view.findViewById(R.id.llyTwitter);
        LinearLayout llyInsta = view.findViewById(R.id.llyInsta);
        LinearLayout llyFb = view.findViewById(R.id.llyFb);
        LinearLayout llyWhatsapp = view.findViewById(R.id.llyWhatsapp);


        //Click Listeners
        llyTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                ShareUtils.shareTwitter(getActivity(), HomeFragment.this.context.getString(R.string.check_on_divineray),item.getBranchShareUrl(),"","");
            }
        });
        llyFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                ShareUtils.shareFacebook(getActivity(), HomeFragment.this.context.getString(R.string.check_on_divineray),item.getBranchShareUrl());
            }
        });
        llyWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                ShareUtils.shareWhatsAppData(fromFile,item.getBranchShareUrl().toString(),getActivity());
            }
        });
        llyInsta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                ShareUtils.shareInsta(getActivity(),fromFile,item.getBranchShareUrl());
            }
        });

    }


    @SuppressLint("StaticFieldLeak")
    public void downloadFileNew(final String uRl, final String idValue, final HomeModel.Data homeModel) {
        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... strings) {
                File direct = new File(Environment.getExternalStorageDirectory() + "/DivineRayVideo");

                if (!direct.exists()) {
                    direct.mkdirs();
                }

                DownloadManager mgr = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                Uri downloadUri = Uri.parse(uRl);
                String filename2 = "";
                filename2 = idValue + "" + ".mp4";
                DownloadManager.Request request = new DownloadManager.Request(downloadUri);

                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                        .setAllowedOverRoaming(false).setTitle("Demo")
                        .setDescription("Something useful. No, really.")
                        .setVisibleInDownloadsUi(false)
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/DivineRayVideo/" + filename2);

                mgr.enqueue(request);


                return null;
            }

            @SuppressLint("WrongThread")
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        File dir = Environment.getExternalStorageDirectory();
                        File dcim = new File(dir.getAbsolutePath() + "/Download/DivineRayVideo/" + idValue + ".mp4");

                        if (dcim.exists()) {
                        }
                        Uri uriPath = Uri.fromFile(dcim);
                        dismissProgressDialog();
                        setVideoLinkData(uriPath, homeModel, context);
                    }
                }, 10000);
            }
        }.execute();
    }

    private void OpenComment(HomeModel.Data item) {
        int value=Integer.parseInt(item.getTotalComments())-1;
        int comment_counnt=value;

        Fragment_Data_Send fragment_data_send=this;


        String comment_userid=item.getUser_id();
        String comment_videoId=item.getVideoId();

        // fun();
        Bundle args=new Bundle();
        args.putString("b_comment_userid",comment_userid);
        args.putString("b_comment_videoId",comment_videoId);


        BottomSheetDialogH bottomSheet = new BottomSheetDialogH(comment_counnt,fragment_data_send);
        bottomSheet.setArguments(args);
        bottomSheet.show(getFragmentManager(), "BottomSheet");



    }

    private void DisLike_Video(int positon, HomeModel.Data item) {

        String action=item.getIsLiked();

        if(action.equals("1")){
            action="0";
            item.setTotalLike(String.valueOf(Integer.parseInt(item.getTotalLike()) -1));
        }else {
            action="1";
            item.setTotalLike(String.valueOf(Integer.parseInt(item.getTotalLike()) +1));
        }


        data_list.remove(positon);
        item.setIsLiked(action);
        data_list.add(positon,item);
        adapter.notifyDataSetChanged();
        String authtoken= DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.AUTHTOKEN,null);

        showProgressDialog((Activity) getContext());
                ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
                mApiInterface1.addFavVideo(authtoken,mParams2()).enqueue(new Callback<VideofavModel>() {
                    @Override
                    public void onResponse(Call<VideofavModel> call, Response<VideofavModel> response) {
                        dismissProgressDialog();
                       Log.e("", "**RESPONSE**" + response.body());
                        VideofavModel mGetDetailsModel = response.body();

                        if (mGetDetailsModel.getStatus() == 1) {

                        } else {


                            //Toast.makeText(mActivity, "Error Getting daa", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<VideofavModel> call, Throwable t) {

                        Log.e("", "**ERROR**" + t.getMessage());
                    }
                });

            }


    /*
     * Execute api
     * */
    private Map<String, String> mParamsVi() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(context,DivineRayPrefernces.USERID,null));
        mMap.put("videoId",videoID);
        mMap.put("isView","1");
        mMap.put("indexCall","" );
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
     void ViewIncrease(int positon, HomeModel.Data item) {
         String authtoken= DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.AUTHTOKEN,null);

         ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.addFavVideo(authtoken,mParamsVi()).enqueue(new Callback<VideofavModel>() {
            @Override
            public void onResponse(Call<VideofavModel> call, Response<VideofavModel> response) {
                item.setTotalViews(String.valueOf(Integer.parseInt(item.getTotalViews()) +1));
                data_list.remove(positon);
                data_list.add(positon,item);
                adapter.notifyDataSetChanged();
                Log.e("", "**RESPONSE**" + response.body());
                VideofavModel mGetDetailsModel = response.body();

//                assert mGetDetailsModel != null;
//                if (mGetDetailsModel.getStatus() == 1) {
//
//                } else {
//
//
//                    //Toast.makeText(mActivity, "Error Getting daa", Toast.LENGTH_SHORT).show();
//                }

            }

            @Override
            public void onFailure(Call<VideofavModel> call, Throwable t) {

                Log.e("", "**ERROR**" + t.getMessage());
            }
        });

    }



    /*
     * Execute api
     * */
    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(context,DivineRayPrefernces.USERID,null));
        mMap.put("videoId",videoID);
        mMap.put("isView","0");
        mMap.put("indexCall","" );
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void Like_Video(int positon, HomeModel.Data item,int currentPage) {
        String action=item.getIsLiked();

        if(action.equals("1")){
            action="0";
            item.setTotalLike(String.valueOf(Integer.parseInt(item.getTotalLike()) -1));
        }else {
            action="1";
            item.setTotalLike(String.valueOf(Integer.parseInt(item.getTotalLike()) +1));
        }


        data_list.remove(positon);
        item.setIsLiked(action);
        data_list.add(positon,item);
        adapter.notifyDataSetChanged();
      //  Toast.makeText(context, "Working", Toast.LENGTH_SHORT).show();
        String authtoken= DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.AUTHTOKEN,null);

        showProgressDialog((Activity) getContext());
                ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
                mApiInterface1.addFavVideo(authtoken,mParams2()).enqueue(new Callback<VideofavModel>() {
                    @Override
                    public void onResponse(Call<VideofavModel> call, Response<VideofavModel> response) {

                        VideofavModel mGetDetailsModel = response.body();
                        if (mGetDetailsModel.getStatus() == 1) {
                            dismissProgressDialog();
                            Log.e("", "test" + "1"+response.body());

                            //  Toast.makeText(mContext, "islike"+isView+"fc:  "+video_id+"Use  "+DivineRayPrefernces.readString(mContext,DivineRayPrefernces.USERID,null), Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e("", "test" + "2"+response.body());
                            //Toast.makeText(mContext, "Error Getting daa", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<VideofavModel> call, Throwable t) {

                        Log.e("", "**ERROR**" + t.getMessage());
                    }
                });
            }
    public boolean is_fragment_exits(){
        FragmentManager fm = getActivity().getSupportFragmentManager();
        if(fm.getBackStackEntryCount()==0){
            return false;
        }else {
            return true;
        }

    }

    // this is lifecyle of the Activity which is importent for play,pause video or relaese the player
    @Override
    public void onResume() {

        super.onResume();


        if (player!=null) {
            privious_player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

        }
        if(privious_player!=null  ){
         privious_player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
         privious_player.setPlayWhenReady(true);
        }
        AudioManager ama = (AudioManager)getContext().getSystemService(Context.AUDIO_SERVICE);

// Request audio focus for playback
        int result = ama.requestAudioFocus(focusChangeListener,
// Use the music stream.
                AudioManager.STREAM_MUSIC,
// Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);


        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
// other app had stopped playing song now , so u can do u stuff now .
        }

    }


    private AudioManager.OnAudioFocusChangeListener focusChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                public void onAudioFocusChange(int focusChange) {
                    try {
                        MediaPlayer mediaPlayerBackground=new MediaPlayer();
                        AudioManager am =(AudioManager) Objects.requireNonNull(getContext()).getSystemService(Context.AUDIO_SERVICE);
                        switch (focusChange) {

                            case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) :
                                // Lower the volume while ducking.
                                mediaPlayerBackground.setVolume(0.2f, 0.2f);
                                break;
                            case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) :
                                mediaPlayerBackground.stop();
                                break;

                            case (AudioManager.AUDIOFOCUS_LOSS) :
                                mediaPlayerBackground.stop();

                                break;

                            case (AudioManager.AUDIOFOCUS_GAIN) :
                                // Return the volume to normal and resume if paused.
                                mediaPlayerBackground.setVolume(1f, 1f);
                                mediaPlayerBackground.start();
                                break;
                            default: break;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

    @Override
    public void onPause() {
        super.onPause();
        if(privious_player!=null){
            privious_player.setPlayWhenReady(false);
            privious_player.setVideoScalingMode( C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
        }


    }


    @Override
    public void onStop() {
        super.onStop();
        if(privious_player!=null){
            privious_player.setPlayWhenReady(false);
           privious_player.setVideoScalingMode( C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(privious_player!=null){
            privious_player.release();
        }
    }



    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if(playbackState==Player.STATE_BUFFERING){
            p_bar.setVisibility(View.VISIBLE);
        }
        else if(playbackState==Player.STATE_READY){
            p_bar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    private void OpenProfile(HomeModel.Data item, boolean from_right_to_left) {
        String new_profileId= DivineRayPrefernces.readString(getContext(),DivineRayPrefernces.USERID,null);
        if (item.getUser_id().equals(new_profileId)) {
            Intent intent = new Intent(context, SelectedUserOwnProfile.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

        }
        else {
            Intent intent = new Intent(context, SelectedUserProfile.class);
            intent.putExtra("profileUserId", item.getUser_id());
            intent.putExtra("followingsb", item.getUserDetails().getTotalFollowing());
            intent.putExtra("followersb", item.getUserDetails().getTotalFollowers());
            intent.putExtra("photo", item.getUserDetails().getPhoto());
            intent.putExtra("name", item.getUserDetails().getName());
            intent.putExtra("descriptionb", item.getUserDetails().getDescription());
            intent.putExtra("followornot", item.getUserDetails().getFollow());
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getContext().startActivity(intent);
        }


    }


    @Override
    public void onDataSent(String yourData) {
        int comment_count =Integer.parseInt(yourData);
        HomeModel.Data item=data_list.get(currentPage);
        item.setTotalComments(String.valueOf(comment_count));
        data_list.remove(currentPage);
        data_list.add(currentPage,item);
        adapter.notifyDataSetChanged();

    }
    public  String getMD5EncryptedString(String encTarget){
        MessageDigest mdEnc = null;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Exception while encrypting to md5");
            e.printStackTrace();
        } // Encryption algorithm
        mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
        String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
        while ( md5.length() < 32 ) {
            md5 = "0"+md5;
        }
        return md5;
    }

    private void perform(int currentPage) {


        for (int i=currentPage;i<=currentPage+3;i++) {
            DefaultTrackSelector trackSelectorq = new DefaultTrackSelector();

            SimpleExoPlayer playerq = ExoPlayerFactory.newSimpleInstance(context, trackSelectorq);
            final HomeModel.Data item= data_list.get(i);
            DefaultDataSourceFactory dataSourceFactoryq = new DefaultDataSourceFactory(context,
                    Util.getUserAgent(context, "DivineRay"));
            CacheDataSourceFactory cacheDataSourceFactoryq = new CacheDataSourceFactory(DownloadUtil.getCache(context), dataSourceFactoryq, CacheDataSource.FLAG_BLOCK_ON_CACHE | CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);
            MediaSource videoSourceq = new ExtractorMediaSource.Factory(cacheDataSourceFactoryq)
                    .createMediaSource(Uri.parse(item.getPostVideo()));
            Log.e("Testq", "" + i + "  vg:" + item.getPostVideo());
            playerq.prepare(videoSourceq);
            playerq.addListener(new Player.EventListener() {


                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    switch (playbackState) {
                        case Player.STATE_READY:

                            playerq.release();
                            break;

                        default:
                            //  layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);
                            break;
                    }
                }
            });
        }

    }

    private void performN(int currentPage) {

        for (int i = currentPage - 4; i <= currentPage - 1; i++) {
            DefaultTrackSelector trackSelectorq = new DefaultTrackSelector();

            SimpleExoPlayer playerq = ExoPlayerFactory.newSimpleInstance(getContext(), trackSelectorq);
            final HomeModel.Data item= data_list.get(currentPage);
            DefaultDataSourceFactory dataSourceFactoryq = new DefaultDataSourceFactory(getContext(),
                    Util.getUserAgent(getContext(), "DivineRay"));
            CacheDataSourceFactory cacheDataSourceFactoryq = new CacheDataSourceFactory(DownloadUtil.getCache(getContext()), dataSourceFactoryq, CacheDataSource.FLAG_BLOCK_ON_CACHE | CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);
            MediaSource videoSourceq = new ExtractorMediaSource.Factory(cacheDataSourceFactoryq)
                    .createMediaSource(Uri.parse(item.getPostVideo()));
            Log.e("Testq", "" + i + "  vg:" + item.getPostVideo());
            playerq.prepare(videoSourceq);
            playerq.addListener(new Player.EventListener() {


                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    switch (playbackState) {


                        case Player.STATE_READY:

                            playerq.release();
                            break;

                        default:
                            //  layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);
                            break;
                    }
                }
            });
        }

    }
    // we need 4 permission during creating an video so we will get that permission
    // before start the video recording
    public boolean check_permissions() {

        String[] PERMISSIONS = new String[0];

        PERMISSIONS = new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.CAMERA
        };

        if (!hasPermissions(context, PERMISSIONS)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS, 2);
            }
        }else if (hasPermissions(context, PERMISSIONS)) {
//setUpCamera();
            return  true;
        }
        else {
            return true;
        }



        return false;
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }

            }
        }
        return true;


    }




}
