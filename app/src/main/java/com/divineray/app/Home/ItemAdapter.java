package com.divineray.app.Home;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.divineray.app.Chat.ChatMessageAdapter;
import com.divineray.app.R;
import com.divineray.app.model.ChatMessageModel;
import com.divineray.app.model.GetCommentsModel;
import com.twitter.sdk.android.core.SessionManager;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {
    Context context;
    ArrayList<GetCommentsModel.Dataw> mArrayList = new ArrayList<>();

    static public String commentId_w;
    String convTime = null;
    private SessionManager session;
    private ItemAdapter.OnItemClickListener listener;
    public interface OnItemClickListener {
        void onItemClick(int positon, GetCommentsModel.Dataw item, View view);
    }


    public ItemAdapter(Context context) {
        this.context=context;

    }
    public void updateList(ArrayList<GetCommentsModel.Dataw> itemList,ItemAdapter.OnItemClickListener listener){
        mArrayList.clear();
        this.mArrayList = itemList;
        this.listener=listener;
        notifyDataSetChanged();
        Log.e("data_refreshed", itemList.toString());
    }

    @NonNull
    @Override
    public ItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bottom_sheet_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemAdapter.ViewHolder holder, int position) {
        GetCommentsModel.Dataw mModel = mArrayList.get(position);
        holder.bind(position,mModel,listener);
        holder.tx_username.setText(mModel.getComment_by());
        String serverResponse =mModel.getComment();
       String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(serverResponse);
      // String fromServerUnicodeDecoded=decodeEmoji(serverResponse);
        holder.tx_commentdata.setText(fromServerUnicodeDecoded);

        commentId_w=mModel.getCommentId();
        long coment_time= Long.parseLong(mModel.getCommentTime());

        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.unknown_user)
                .error(R.drawable.unknown_user)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();

        Glide.with(context).load(mModel.getImage_user()).apply(options).into(holder.circleImageView);
        Date date = new java.util.Date(coment_time*1000L);

// the format of your date
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
// give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(TimeZone.getDefault());
        String formattedDate = sdf.format(date);
        covertTimeToText(formattedDate);
        holder.tx_commentTime.setText(convTime);
       // holder.itemView.notify();

    }
    public static String encodeEmoji (String message) {
        try {
            return URLEncoder.encode(message,
                    "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }
    public void covertTimeToText(String dataDate) {


        String prefix = "a";
        String suffix = "ago";

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date pasTime = dateFormat.parse(dataDate);

            Date nowTime = new Date();

            long dateDiff = nowTime.getTime() - pasTime.getTime();

            long second = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
            long minute = TimeUnit.MILLISECONDS.toMinutes(dateDiff);
            long hour = TimeUnit.MILLISECONDS.toHours(dateDiff);
            long day = TimeUnit.MILLISECONDS.toDays(dateDiff);
            if (second<2)
            {
                convTime="a second ago";
            }
           else if (second < 60) {
                convTime = second + " Seconds " + suffix;
            } else if (minute < 60) {
                convTime = minute + " Minute " + suffix;
            } else if (hour < 24) {
                convTime = hour + " Hours " + suffix;
            } else if (day >= 7) {
                if (day > 360) {
                    convTime = (day / 360) + " Years " + suffix;
                } else if (day > 30) {
                    convTime = (day / 30) + " Months " + suffix;
                } else {
                    convTime = (day / 7) + " Week " + suffix;
                }
            } else if (day < 7&& day>1) {
                convTime = day + " Days " + suffix;
            }
           else if (day==1)
            {
                convTime = "a day " + suffix;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("ConvTimeE", e.getMessage());
        }




    }
    public static String decodeEmoji (String message) {
        String myString = null;
        try {
            return URLDecoder.decode(
                    message, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }
        @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    interface ItemListener {
        void onItemClick(String item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView circleImageView;
        TextView tx_username,tx_commentdata,tx_commentTime;


        public ViewHolder(@NonNull View itemView) {
            super( itemView );
            circleImageView=itemView.findViewById(R.id.bottomsheet_usercomment_photo);
            tx_username=itemView.findViewById(R.id.bottomsheet_usercomment_username);
            tx_commentdata=itemView.findViewById(R.id.bottomsheet_usercomment);
            tx_commentTime=itemView.findViewById(R.id.bottomsheet_usercomment_time);
        }

        public void bind(int position, GetCommentsModel.Dataw mModel, OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });
           tx_commentdata.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });
            tx_commentTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });
            tx_username.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });
            circleImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });
        }
    }
}
