package com.divineray.app.Home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.divineray.app.Utils.ExpandableHeightGridView;
import com.divineray.app.Profile.PersonDetails;
import com.divineray.app.Profile.CustomAdapter;
import com.divineray.app.Profile.CustomAdapterFav;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.DeepLinkModel;
import com.divineray.app.model.FavModel;
import com.divineray.app.model.GetProfileModel;
import com.divineray.app.model.ProfileGetVideoModel;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectedUserOwnProfile extends BaseActivity {
    Activity mActivity = SelectedUserOwnProfile.this;


    @BindView(R.id.sprofile_share)
    TextView im_share;
    ImageView im_back;
    CircleImageView profile_image;
    TextView tx_name,tx_following,tx_followers,tx_description;
    ExpandableHeightGridView simpleGrid;
    RelativeLayout rly_grid;
    static  public TabLayout tabLayout;
    TabItem tab_all,tab_fav;
    String currentPage="tab_all";
    LinearLayout lly_follow_followers,lly_followings,lly_folllowers,lly_back;

    List<ProfileGetVideoModel.Data> mQuotesArrayList = new ArrayList<>();
    List<FavModel.FavVideos> mQuotesArrayList2 = new ArrayList<>();
    LinearLayout lly_noposts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTransparentStatusBarOnly(mActivity);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_selected_user_own_profile);
        ButterKnife.bind(this);
        //id fetch
        simpleGrid = findViewById(R.id.gridview_mine);
        getUserProfileDetails();

        im_back=findViewById(R.id.profile_mine_backbutton);
        tx_name=findViewById(R.id.profilef_name_mine);
        tabLayout=findViewById(R.id.tabs_profilefrag_mine);
        tx_description=findViewById(R.id.profilef_description_mine);
        tx_followers=findViewById(R.id.profilef_followers_mine);
        tx_following=findViewById(R.id.profilef_following_mine);
        profile_image=findViewById(R.id.profilef_image_mine);
        tab_all=findViewById(R.id.Tabitem_all_mine);
        tab_fav=findViewById(R.id.Tabitem_favs_mine);
        lly_follow_followers=findViewById(R.id.lly_follow_following_mine);
        lly_folllowers=findViewById(R.id.lly_followers_mine);
        lly_followings=findViewById(R.id.lly_followings_mine);
        lly_noposts=findViewById(R.id.lly_noposts_mine);
        rly_grid=findViewById(R.id.rely_gridy_mine);
        lly_back=findViewById(R.id.lly_profile_mine_backbutton);


        simpleGrid.setFastScrollEnabled(true);
        simpleGrid.setExpanded(true);
        rly_grid.setMinimumHeight(1000);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
//                        codes related to the first tab
                        tabAllApi();
                        mQuotesArrayList2.clear();
                        currentPage="tab_all";
                        showProgressDialog(mActivity);
                        break;
                    case 1:
//                        codes related to the second tab
                        tabFavApi();
                        currentPage="tab_fav";
                        break;



                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            //noinspection ConstantConditions
            TextView tv = (TextView) LayoutInflater.from(mActivity).inflate(R.layout.custom_tab_layout,null);
            // tv.setTypeface(Typeface);
            tabLayout.getTabAt(i).setCustomView(tv);
        }

        lly_followings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(mActivity, PersonDetails.class);
                intent.putExtra("head_name",tx_name.getText().toString().trim());
                intent.putExtra("followers",tx_followers.getText().toString().trim());
                intent.putExtra("followings",tx_following.getText().toString().trim());
                intent.putExtra("layoutType","followings");
                intent.putExtra("profileUserId", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
                startActivity(intent);
            }
        });

        lly_follow_followers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(mActivity, PersonDetails.class);
                intent.putExtra("head_name",tx_name.getText().toString().trim());
                intent.putExtra("followers",tx_followers.getText().toString().trim());
                intent.putExtra("followings",tx_following.getText().toString().trim());
                intent.putExtra("layoutType","followers");
                intent.putExtra("profileUserId",DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
                startActivity(intent);
            }
        });

        lly_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        im_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performDeeplink();
            }
        });

    }
    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    private void getUserProfileDetails() {
        if (!isNetworkAvailable(mActivity)) {
            Toast.makeText(mActivity, "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
            executeGetUserDetailsApi();
        }
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("lastId","");
        mMap.put("perPage","100");
        mMap.put("profileUserId", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetUserDetailsApi() {
        //  showProgressDialog(getActivity());

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getProfileDetailsRequest(getAuthToken(),mParams()).enqueue(new Callback<GetProfileModel>() {
            @Override
            public void onResponse(Call<GetProfileModel> call, Response<GetProfileModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                GetProfileModel mGetDetailsModel = response.body();
                //assert mGetDetailsModel != null;
                if (mGetDetailsModel.getStatus().equals(1)) {
                    //showToast(mActivity, m);
                    Log.d("**PROFILEIMAGE", "" + mGetDetailsModel.getData().getPhoto());

                    String imageurl = mGetDetailsModel.getData().getPhoto();
                    Log.d("error1", "" + "error2" + imageurl + mGetDetailsModel);
//

                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.unknown_user)
                            .error(R.drawable.unknown_user)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();
                    if (imageurl != null&&mActivity!=null) {
                        try {
                            Glide.with(getApplicationContext())
                                    .load(imageurl)
                                    .apply(options)
                                    .into(profile_image);
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }

                    } else {
                        Log.e("Data","null");

                    }
                    if (mGetDetailsModel.getData().getName()!=null||mGetDetailsModel.getData().getName().equals("")) {
                        tx_name.setText(mGetDetailsModel.getData().getName());
                    }
                    else {
                        tx_name.setText("Unknown");
                    }
                    // tx_name.setText(mGetDetailsModel.getData().getName());
                    tx_followers.setText(mGetDetailsModel.getData().getTotalFollowers());
                    tx_following.setText(mGetDetailsModel.getData().getTotalFollowing());

                    String description="Description: ";
                    SpannableString ss=new SpannableString(description+mGetDetailsModel.getData().getDescription());
                    StyleSpan boldSpan=new StyleSpan(Typeface.BOLD);
                    ss.setSpan(boldSpan,0,12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    if (!mGetDetailsModel.getData().getDescription().equals("N/A"))
                    {
                        tx_description.setText(mGetDetailsModel.getData().getDescription());
                    }
                    else
                    {
                        tx_description.setText("");
                    }

                } else if (response.body().getStatus().equals(0)) {
                    // Toast.makeText(getActivity(), "" + mGetDetailsModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
        // tabAllApi();

    }
    private Map<String, String> mParams3() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("profileUserId", getUserID());
        mMap.put("type", "1");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void performDeeplink() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getDeepLInk(getAuthToken(),mParams3()).enqueue(new Callback<DeepLinkModel>() {
            @Override
            public void onResponse(Call<DeepLinkModel> call, Response<DeepLinkModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                DeepLinkModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {

                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                    shareIntent.putExtra(Intent.EXTRA_TEXT, mGetDetailsModel.getShareUrl());
                    startActivity(Intent.createChooser(shareIntent, getString(R.string.app_name)));

                } else {
                    showAlertDialog(mActivity, mGetDetailsModel.getMessage());

                }

            }

            @Override
            public void onFailure(Call<DeepLinkModel> call, Throwable t) {
                dismissProgressDialog();

                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
        if (currentPage.equals("tab_all"))
        {

            tabAllApi();
        }
        else if (currentPage.equals("tab_fav"))
        {

            tabFavApi();
        }
        executeGetUserDetailsApi();


    }

    public  void tabAllApi(){

        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getProfileVideos(getAuthToken(),mParams()).enqueue(new Callback<ProfileGetVideoModel>() {
            @Override
            public void onResponse(Call<ProfileGetVideoModel> call, Response<ProfileGetVideoModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                ProfileGetVideoModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {
                    simpleGrid.setVisibility(View.VISIBLE);
                    lly_noposts.setVisibility(View.GONE);
                    mQuotesArrayList =response.body().getData();
                    if (mQuotesArrayList!=null){
                        setProfileAdapter();
                    }

                } else {
                    lly_noposts.setVisibility(View.VISIBLE);
                    simpleGrid.setVisibility(View.GONE);
                    //   Toast.makeText(getActivity(), "Error Getting daa", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ProfileGetVideoModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });

    }

    public  void tabFavApi(){
        //showProgressDialog(getActivity());
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getFavVideos(getAuthToken(),mParams()).enqueue(new Callback<FavModel>() {
            @Override
            public void onResponse(Call<FavModel> call, Response<FavModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                FavModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {
                    simpleGrid.setVisibility(View.VISIBLE);
                    lly_noposts.setVisibility(View.GONE);
                    mQuotesArrayList2 =response.body().getFavVideos();
                    if (mQuotesArrayList2!=null){
                        setProfileAdapter2();
                    }

                } else {
                    simpleGrid.setVisibility(View.GONE);
                    lly_noposts.setVisibility(View.VISIBLE);
                    // Toast.makeText(getActivity(), "Error Getting daa", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<FavModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });

    }

    private void setProfileAdapter() {
        CustomAdapter customAdapter = new CustomAdapter(mActivity, (ArrayList<ProfileGetVideoModel.Data>) mQuotesArrayList);
        simpleGrid.setAdapter(customAdapter);


    }
    private void setProfileAdapter2() {
        String profileUserId=DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null);


        CustomAdapterFav customAdapter = new CustomAdapterFav(mActivity, (ArrayList<FavModel.FavVideos>) mQuotesArrayList2,profileUserId);
        simpleGrid.setAdapter(customAdapter);
    }


}