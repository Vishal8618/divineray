package com.divineray.app.Home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.divineray.app.Chat.ChatActivity;
import com.divineray.app.Utils.ExpandableHeightGridView;
import com.divineray.app.Profile.PersonDetails;
import com.divineray.app.Profile.CustomAdapterFav;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.SelecteUserMore.AppointmentBooking;
import com.divineray.app.SelecteUserMore.GiftItem;
import com.divineray.app.SelecteUserMore.ProductPurchase;
import com.divineray.app.SelecteUserMore.ViddeoAudio;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.DeepLinkModel;
import com.divineray.app.model.FavModel;
import com.divineray.app.model.GetProfileModel;
import com.divineray.app.model.ProfileGetVideoModel;
import com.divineray.app.model.RemoveModel;
import com.divineray.app.model.RoomModel;
import com.google.android.material.tabs.TabLayout;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.security.spec.ECField;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.ContentMetadata;
import io.branch.referral.util.LinkProperties;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectedUserProfile extends BaseActivity {

    /*
      Getting the Current Class Name
     */
    String TAG = SelectedUserProfile.this.getClass().getSimpleName();

    /*
      Current Activity Instance
     */
    Activity mActivity = SelectedUserProfile.this;
    ExpandableHeightGridView simpleGrid;
    int logos[] = {R.drawable.img2,
            R.drawable.imggg,
            R.drawable.imgg,
            R.drawable.img2,
    };
    TextView tx_followButton;
    @BindView(R.id.selectedPerson_followers)
    LinearLayout lly_selectedPerson_followers;
    @BindView(R.id.selectedPerson_following)
    LinearLayout lly_selectedPerson_following;
    @BindView(R.id.sprofilef_image)
    CircleImageView sprofilef_image;
    @BindView(R.id.sprofileff_following)
    TextView sprofilef_following;
    @BindView(R.id.sprofileff_followers)
    TextView sprofilef_followers;
    String profileUserId;
    @BindView(R.id.sprofilef_name)
    TextView sprofilef_name;
    @BindView(R.id.sprofilef_followb)
    TextView sprofilef_followb;
    @BindView(R.id.sprofile_back)
    ImageView sprofile_back;
    @BindView(R.id.s_profile_follow_following_lly)
    LinearLayout lly_follow_followings;
    @BindView(R.id.profilesf_description)
    TextView s_description;
    @BindView(R.id.sprofilef_unfollowb)
    TextView sprofilef_unfollowb;
    @BindView(R.id.sprofilef_message)
    TextView sprofilef_message;
    @BindView(R.id.linear_follow)
    LinearLayout lly_linear;
    @BindView(R.id.tabs_select_user)
    TabLayout  tabLayout;
    @BindView(R.id.lly_selected_user_Moretab)
    LinearLayout lly_Moretab;
    @BindView(R.id.lly_noposts_selectedUser)
    LinearLayout lly_noposts;
    @BindView(R.id.tx_appointment)
    TextView tx_appointment;
    @BindView(R.id.tx_audiovideo)
    TextView tx_audiovideo;
    @BindView(R.id.tx_productpurchase)
    TextView tx_productpurchase;
    @BindView(R.id.tx_giftitem)
    TextView tx_giftitem;
    @BindView(R.id.sprofile_share)
    TextView im_share;
    LinkProperties lp;
    String profilname;


    List<ProfileGetVideoModel.Data> mQuotesArrayList = new ArrayList<>();
    List<FavModel.FavVideos> mQuotesArrayList2 = new ArrayList<>();
    List<RoomModel.DataR> mRoomArrayList = new ArrayList<>();
   public int countf;
    public boolean stateChanged;
    String followornot;
    String roomId,name;
    String followingorunfollow="";
    RelativeLayout rly_user_profile;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_selected_user_profile);

        ButterKnife.bind(this);
        Intent intent = getIntent();
        profileUserId = intent.getStringExtra("profileUserId");
       lly_noposts.setVisibility(View.VISIBLE);
        tx_followButton = findViewById(R.id.sprofilef_followb);
        rly_user_profile=findViewById(R.id.rely_grid);
        simpleGrid = findViewById(R.id.s_gridview); // init GridView
        simpleGrid.setFastScrollEnabled(true);
        simpleGrid.setExpanded(true);
        simpleGrid.setVisibility(View.GONE);
        lly_noposts.setVisibility(View.GONE);
        lly_Moretab.setVisibility(View.GONE);


        String new_profileId= DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null);
        if (profileUserId.equals( new_profileId))
        {
           lly_linear.setVisibility(View.GONE);

        }
        else {
           lly_linear.setVisibility(View.VISIBLE);
        }


        tx_audiovideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(mActivity, ViddeoAudio.class);
                intent1.putExtra("other_user_id",profileUserId);
                intent1.putExtra("username",profilname);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
            }
        });
        tx_productpurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(mActivity, ProductPurchase.class);
                intent1.putExtra("other_user_id",profileUserId);
                intent1.putExtra("username",profilname);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
            }
        });
        tx_giftitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(mActivity, GiftItem.class);
                intent1.putExtra("other_user_id",profileUserId);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
            }
        });
        tx_appointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent1=new Intent(mActivity, AppointmentBooking.class);
                intent1.putExtra("other_user_id",profileUserId);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
            }
        });

        im_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
performDeeplink();
            }
        });

        executeGetUserDetailsZApi();

        getUserProfileDetails();



        rly_user_profile.setMinimumHeight(1000);
        tabLayout.clearOnTabSelectedListeners();
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        mQuotesArrayList2.clear();

                        lly_Moretab.setVisibility(View.GONE);
                        lly_noposts.setVisibility(View.GONE);
                        if (mQuotesArrayList.size()==0)
                        {

                        simpleGrid.setVisibility(View.GONE);
                          //  lly_noposts.setVisibility(View.VISIBLE);
                        }
                        else {

                            simpleGrid.setVisibility(View.VISIBLE);
                         //   lly_noposts.setVisibility(View.GONE);
                        }
//                        codes related to the first tab


                        executeGetUserDetailsApi();
                        simpleGrid.setVisibility(View.VISIBLE);
                        tabLayout.setClickable(true);
                        break;
                    case 1:
                        mQuotesArrayList.clear();
                        simpleGrid.setVisibility(View.VISIBLE);
                        lly_Moretab.setVisibility(View.GONE);
                        lly_noposts.setVisibility(View.GONE);
                        if (mQuotesArrayList2.size()==0)
                        {
                           // Toast.makeText(mActivity, ""+mQuotesArrayList2.size(), Toast.LENGTH_SHORT).show();
                            simpleGrid.setVisibility(View.GONE);
                            //lly_noposts.setVisibility(View.VISIBLE);
                        }
                        else {
                         simpleGrid.setVisibility(View.VISIBLE);
                         lly_noposts.setVisibility(View.GONE);
                        }
//
                                executeFavVideosApi();
                        tabLayout.setClickable(true);

                        break;
                    case 2:
                        simpleGrid.setVisibility(View.GONE);
                        mQuotesArrayList.clear();

                        showProgressDialog(mActivity);

                       mQuotesArrayList2.clear();

                        lly_noposts.setVisibility(View.GONE);


                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                               // dismissProgressDialog();
                                lly_Moretab.setVisibility(View.VISIBLE);
                                lly_noposts.setVisibility(View.GONE);
                                simpleGrid.setVisibility(View.GONE);
                                lly_Moretab.setVisibility(View.VISIBLE);
                                progressDialog.dismiss();
                            }
                        },800);
                      //  tabLayout.setClickable(true);
                      //  dismissProgressDialog();



//                        codes related to the second tab
                       // tabMoreApi();
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            //noinspection ConstantConditions
            TextView tv = (TextView) LayoutInflater.from(mActivity).inflate(R.layout.custom_tab_layout,null);
            // tv.setTypeface(Typeface);

            tabLayout.getTabAt(i).setCustomView(tv);
        }
        lly_selectedPerson_following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(SelectedUserProfile.this, PersonDetails.class);
                intent.putExtra("head_name",sprofilef_name.getText().toString().trim());
                intent.putExtra("followers",sprofilef_followers.getText().toString().trim());
                intent.putExtra("followings",sprofilef_following.getText().toString().trim());
                intent.putExtra("layoutType","followings");
                intent.putExtra("profileUserId",profileUserId);
                startActivity(intent);
            }
        });


        lly_selectedPerson_followers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(mActivity, PersonDetails.class);
                intent.putExtra("head_name",sprofilef_name.getText().toString().trim());
                intent.putExtra("followers",sprofilef_followers.getText().toString().trim());
                intent.putExtra("followings",sprofilef_following.getText().toString().trim());
                intent.putExtra("layoutType","followers");
                intent.putExtra("profileUserId",profileUserId);
                startActivity(intent);
            }
        });
        sprofilef_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   Toast.makeText(mActivity, "Hell", Toast.LENGTH_SHORT).show();
                RoomApiFetch();

            }
        });

        sprofile_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tx_followButton.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                  countf=1;

                  executeFollowapi();
                tx_followButton.setVisibility(View.GONE);
                sprofilef_unfollowb.setVisibility(View.VISIBLE);
                if (sprofilef_followers.getText().toString()!=null &&!sprofilef_followers.getText().toString().equals(""))
                {
                    int a = Integer.parseInt(sprofilef_followers.getText().toString());
                    int b = 1;
                    int c = a + b;
                    sprofilef_followers.setText(String.valueOf(c));
                }
                else
                {
                    sprofilef_followers.setText("1");

                }


            }
        });

        sprofilef_unfollowb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countf=0;
                executeFollowapi();
                tx_followButton.setVisibility(View.VISIBLE);
                sprofilef_unfollowb.setVisibility(View.GONE);
                if (sprofilef_followers.getText().toString()!=null &&!sprofilef_followers.getText().toString().equals(""))
                {
                    int a= Integer.parseInt(sprofilef_followers.getText().toString());
                    int b=1;
                    int c=a-b;
                    sprofilef_followers.setText(String.valueOf(c));
                }
                else
                {
                    sprofilef_followers.setText("0");
                }



            }
        });



        // implement setOnItemClickListener event on GridView
//        simpleGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                // set an Intent to Another Activity
//                Intent intent = new Intent(S.this, SecondActivity.class);
//                intent.putExtra("image", logos[position]); // put image data in Intent
//                startActivity(intent); // start Intent
//            }
//        });
    }
    private Map<String, String> mParams3() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("profileUserId", profileUserId);
        mMap.put("type", "1");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void performDeeplink() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getDeepLInk(getAuthToken(),mParams3()).enqueue(new Callback<DeepLinkModel>() {
            @Override
            public void onResponse(Call<DeepLinkModel> call, Response<DeepLinkModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                DeepLinkModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {

                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                   shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                    shareIntent.putExtra(Intent.EXTRA_TEXT, mGetDetailsModel.getShareUrl());
                    startActivity(Intent.createChooser(shareIntent, getString(R.string.app_name)));

                } else {
               showAlertDialog(mActivity,mGetDetailsModel.getMessage());

                }

            }

            @Override
            public void onFailure(Call<DeepLinkModel> call, Throwable t) {
                dismissProgressDialog();

                Log.e("", "**ERROR**" + t.getMessage());
            }
        });




        }

    private void tabAllApi() {
        executeGetUserDetailsApi();
    }

    private void executeAddMessagApi() {

    }

    /*
     * Execute api
     * */
    private Map<String, String> mParamsR() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("chatUserId", profileUserId);
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void RoomApiFetch() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.checkRoom(getAuthToken(),mParamsR()).enqueue(new Callback<RoomModel>() {
            @Override
            public void onResponse(Call<RoomModel> call, Response<RoomModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                RoomModel mGetDetailsModel = response.body();
               String roomId=mGetDetailsModel.getData().get(0).getRoomId();
               String chatUserId=mGetDetailsModel.getData().get(0).getChatUserId();
                if (mGetDetailsModel.getStatus() == 1) {
                   // Toast.makeText(mActivity, ""+, Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(mActivity, ChatActivity.class);
                  //  Toast.makeText(mActivity, ""+chatUserId, Toast.LENGTH_SHORT).show();
                    intent.putExtra("roomId",roomId);

                    intent.putExtra("chatUserId",chatUserId);
                    intent.putExtra("chatuserName",sprofilef_name.getText().toString());
                    startActivity(intent);
                   // mRoomArrayList =  response.body().getData();

                     //                    sprofilef_followb.setText("Followed");
//
//                        int initial= Integer.parseInt(sprofilef_followers.getText().toString());
//                        int total=initial+countf;
//                        sprofilef_followers.setText(String.valueOf(total));



                } else {
                    Toast.makeText(mActivity, "Already Followed", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<RoomModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParamss() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("lastId","");
        mMap.put("perPage","100");
        mMap.put("profileUserId", profileUserId);
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    public  void executeGetUserDetailsZApi() {

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getProfileDetailsRequest(getAuthToken(),mParamss()).enqueue(new Callback<GetProfileModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<GetProfileModel> call, Response<GetProfileModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                GetProfileModel mGetDetailsModel = response.body();
                assert mGetDetailsModel != null;
                if (mGetDetailsModel.getStatus().equals(1)) {
                    //showToast(mActivity, m);
                    Log.d("**PROFILEIMAGE", "" + mGetDetailsModel.getData().getPhoto());

                    String imageurl = mGetDetailsModel.getData().getPhoto();
                    Log.d("error1", "" + "error2" + imageurl + mGetDetailsModel);
//

                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.unknown_user)
                            .error(R.drawable.unknown_user)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();
                    if (imageurl != null&& mActivity!=null) {
                        try {
                            Glide.with(getApplicationContext() )
                                    .load(imageurl)
                                    .apply(options)
                                    .into(sprofilef_image);
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }

                    } else {


                    }

                   // sprofilef_name.setText(mGetDetailsModel.getData().getName());
                    followingorunfollow=mGetDetailsModel.getData().getFollow();
                    sprofilef_followers.setText(mGetDetailsModel.getData().getTotalFollowers());
                    sprofilef_following.setText(mGetDetailsModel.getData().getTotalFollowing());
                    profilname=mGetDetailsModel.getData().getName();
                    String description="Description: ";
                    SpannableString ss=new SpannableString(description+mGetDetailsModel.getData().getDescription());
                    StyleSpan boldSpan=new StyleSpan(Typeface.BOLD);
                    ss.setSpan(boldSpan,0,12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    if (response.body()!=null) {
                        if (mGetDetailsModel.getData().getDescription()!=null&&!mGetDetailsModel.getData().getDescription().equals("")&&!mGetDetailsModel.getData().getDescription().equals("N/A")) {
                            s_description.setText(mGetDetailsModel.getData().getDescription());
                        } else {
                            s_description.setText("");
                        }
                    }
                   // s_description.setText(ss);
                    if (followingorunfollow.equals("1"))
                    {
//            tx_followButton.setText("Following");
//            stateChanged=false;
                        tx_followButton.setVisibility(View.GONE);
                        sprofilef_unfollowb.setVisibility(View.VISIBLE);

                    }
                    else if (followingorunfollow.equals("0")){
//            tx_followButton.setText("Follow");
//            stateChanged=true;
                        tx_followButton.setVisibility(View.VISIBLE);
                        sprofilef_unfollowb.setVisibility(View.GONE);

                    }
                    else {
                        tx_followButton.setVisibility(View.VISIBLE);
                        sprofilef_unfollowb.setVisibility(View.GONE);
                    }

                    if (mGetDetailsModel.getData().getName()!=null&&!mGetDetailsModel.getData().getName().equals("")) {
                        try {


                            if (!(mGetDetailsModel.getData().getName().length() == 0)) {
                                sprofilef_name.setText(mGetDetailsModel.getData().getName());
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else {
                        sprofilef_name.setText("Unknown");
                        tx_followButton.setVisibility(View.GONE);
                    }

                } else if (response.body().getStatus().equals(0)) {
                   // Toast.makeText(mActivity, "" + mGetDetailsModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });


    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity, DivineRayPrefernces.USERID, null));
        mMap.put("follow_id", profileUserId);
        mMap.put("followType", String.valueOf(countf));
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void executeFollowapi() {

        showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.followUnfollow(getAuthToken(),mParams2()).enqueue(new Callback<RemoveModel>() {
            @Override
            public void onResponse(Call<RemoveModel> call, Response<RemoveModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                RemoveModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {


                } else {
                    Toast.makeText(mActivity, "Already Followed", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<RemoveModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void getUserProfileDetails() {
        if (!isNetworkAvailable(mActivity)) {
            Toast.makeText(mActivity, "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
            executeGetUserDetailsApi();
        }
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity, DivineRayPrefernces.USERID, null));
        mMap.put("lastId", "");
        mMap.put("perPage", "100");
        mMap.put("profileUserId", profileUserId);
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetUserDetailsApi() {
        showProgressDialog(mActivity);
        executeGetUserDetailsZApi();

        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getProfileVideos(getAuthToken(),mParams()).enqueue(new Callback<ProfileGetVideoModel>() {

            public void onResponse(Call<ProfileGetVideoModel> call, Response<ProfileGetVideoModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" +response.body());
                ProfileGetVideoModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {
                    simpleGrid.setVisibility(View.VISIBLE);
                    mQuotesArrayList =response.body().getData();
                    if (mQuotesArrayList!=null){
                        setProfileAdapter();

                    }

                } else {
                    simpleGrid.setVisibility(View.GONE);
                    lly_noposts.setVisibility(View.VISIBLE);

                 //   Toast.makeText(mActivity, "Error Getting daa", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ProfileGetVideoModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParamsn() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity, DivineRayPrefernces.USERID, null));
        mMap.put("lastId", "");
        mMap.put("perPage", "100");
        mMap.put("profileUserId", profileUserId);
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeFavVideosApi() {
        showProgressDialog(mActivity);
        executeGetUserDetailsZApi();

        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getFavVideos(getAuthToken(),mParamsn()).enqueue(new Callback<FavModel>() {

            public void onResponse(Call<FavModel> call, Response<FavModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" +response.body());
                FavModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {
                    simpleGrid.setVisibility(View.VISIBLE);
                    mQuotesArrayList2 =response.body().getFavVideos();
                    if (mQuotesArrayList!=null){
                        setProfileAdapter2();
                    }

                } else {
                    simpleGrid.setVisibility(View.GONE);
                    lly_noposts.setVisibility(View.VISIBLE);
                    //   Toast.makeText(mActivity, "Error Getting daa", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<FavModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }
    private void setProfileAdapter() {
        SelectedUserProfileAdapter customAdapter = new SelectedUserProfileAdapter(getApplicationContext(), (ArrayList<ProfileGetVideoModel.Data>) mQuotesArrayList);
        simpleGrid.setAdapter(customAdapter);
    }
    private void setProfileAdapter2() {
        CustomAdapterFav customAdapter = new CustomAdapterFav(mActivity, (ArrayList<FavModel.FavVideos>) mQuotesArrayList2,profileUserId);
        simpleGrid.setAdapter(customAdapter);
    }


}