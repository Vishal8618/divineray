package com.divineray.app.Home;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.divineray.app.Profile.ProfieVideoView;
import com.divineray.app.R;
import com.divineray.app.model.ProfileGetVideoModel;

import java.util.ArrayList;

public class SelectedUserProfileAdapter extends BaseAdapter {
    Context context;
    ArrayList<ProfileGetVideoModel.Data> mArrayList = new ArrayList<>();
    LayoutInflater inflter;
    public SelectedUserProfileAdapter(Context applicationContext, ArrayList<ProfileGetVideoModel.Data> mArrayList) {
        this.context = applicationContext;
        this.mArrayList=mArrayList;
        inflter = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return mArrayList.size();
    }
    @Override
    public Object getItem(int i) {
        return null;
    }
    @Override
    public long getItemId(int i) {
        return 0;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.sub_profile_image, null); // inflate the layout
        ImageView icon = (ImageView) view.findViewById(R.id.icon); // get the reference of ImageView
        ProfileGetVideoModel.Data mModel = mArrayList.get(i);
        // set logo images
        Glide.with(context)
                .load(mModel.getPostImage())
                .into(icon);

        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context, ProfieVideoView.class);
                intent.putExtra("videoId", String.valueOf(i));
                intent.putExtra("profileUserId",mModel.getUser_id());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });
        return view;
    }
}
