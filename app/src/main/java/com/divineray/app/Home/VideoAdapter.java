package com.divineray.app.Home;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.divineray.app.R;
import com.divineray.app.Utils.Constants;
import com.divineray.app.model.HomeModel;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.downloader.request.DownloadRequest;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.CustomViewHolder >{


    public Context context;
    private OnItemClickListener listener;
    private ArrayList<HomeModel.Data> dataList;
    int lastPosition=0;
    HomeModel mHomeModel;
    DownloadRequest prDownloader;

    public interface OnItemClickListener {
        void onItemClick(int positon,HomeModel.Data item, View view);
    }


    public VideoAdapter(Context context, ArrayList<HomeModel.Data> dataList, HomeModel mHomeModel, OnItemClickListener listener) {
        this.context = context;
        this.dataList = dataList;
        this.mHomeModel=mHomeModel;
        this.listener = listener;


    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_data_item1,null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT));
        VideoAdapter.CustomViewHolder viewHolder = new VideoAdapter.CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        final HomeModel.Data item= dataList.get(position);
        holder.setIsRecyclable(false);
Log.e("daTh",dataList.toString());
        if (position==0){

            perform(position);
        }
        // Top
        if (lastPosition > position) {

            if (position>3) {
                performN(position);

            }

        }else if (lastPosition < position) {//Bottom

            if (position<dataList.size()-3) {
                perform(position);
            }
            else {
            }
        }

        lastPosition = position;

        String newF=getMD5EncryptedString(item.getPostImage())+".jpg";


        String path =  Constants.app_folder2+newF;
        File dir = new File(path);


        if (dir.exists())
        {
            Log.e("Testr","dir exists"+newF);
            Glide.with(context).load(path).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.thumbnail);

        }
        else {

            Log.e("Testr","dir not exists"+newF);

            Glide.with(context).load(item.getPostImage()).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.thumbnail);


        }




        try {

            holder.bind(position,item,listener);

            holder.home_title.setText(item.getDescription());


            if((item.getMusicName()==null || item.getMusicName().equals("") || item.getMusicName().equals("null"))){
                holder.sound_name.setText("   @Original   voice -   "+item.getUserDetails().getName()+"                                ");
            }else {
                if (item.getMusicName().length()<=4) {
                    holder.sound_name.setText("    @Music - " + item.getMusicName() + "                            ");
                }
                else {
                    holder.sound_name.setText("    @Music -    " + item.getMusicName() + "                                                            ");
                }
               // holder.sound_name.setWidth(20);
            }
            holder.sound_name.setSelected(true);


            holder.home_tags.setText(item.getTags());




            float checkVal=Float.parseFloat(item.getPostHeight());
            float checkVal2=Float.parseFloat(item.getPostWidth());
            float val2=checkVal-checkVal2;
            if (checkVal==checkVal2)
            {

                holder.thumbnail.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
            else if (checkVal2>checkVal||val2<200)
            {


                holder.thumbnail.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
            else if (checkVal2<checkVal) {
                holder.thumbnail.setScaleType(ImageView.ScaleType.CENTER_CROP);

            }


            Glide.with(context).
                    load(item.getUserDetails().getPhoto())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(context.getResources().getDrawable(R.drawable.unknown_user))
                    .into(holder.home_profile);






                    if (item.getIsLiked().equals("0"))
        {
            holder.im_like.setVisibility(View.VISIBLE);
            holder.im_dislike.setVisibility(View.GONE);
        }
        else {
            holder.im_like.setVisibility(View.GONE);
            holder.im_dislike.setVisibility(View.VISIBLE);
        }


            holder.home_views.setText(item.getTotalViews());
            holder.home_likes.setText(item.getTotalLike());
            holder.home_comments.setText(item.getTotalComments());



        }catch (Exception e){

        }
    }

    private void performN(int ii) {

        int i= ii;
        int i1 = i;
        int i2 = i1 - 1;
        int i3 = i2 - 1;

        String url1 = mHomeModel.getData().get(i1).getPostImage();
        String newF=getMD5EncryptedString(url1)+".jpg";
        String path =  Constants.app_folder2 +newF;
        File dir = new File(path);
        if (!dir.exists())
        {

            new DownloadFileFromURL().execute(url1,newF);
        }


        String url2 = mHomeModel.getData().get(i2).getPostImage();
        String newF2=getMD5EncryptedString(url2)+".jpg";

        String path2 =  Constants.app_folder2+newF2;
        File dir2 = new File(path2);
        if (!dir2.exists())
        {
            new DownloadFileFromURL().execute(url2,newF2);
        }




        String url3 = mHomeModel.getData().get(i3).getPostImage();
        String newF3=getMD5EncryptedString(url3)+".jpg";

        String path3 =   Constants.app_folder2+newF3;
        ;
        File dir3 = new File(path3);
        if (!dir3.exists())
        {
            new DownloadFileFromURL().execute(url3,newF3);
        }

    }
    private void perform(int ii) {
try {
    for (int i=0;i<=3;i++)
    {

        String url1 = mHomeModel.getData().get(ii).getPostImage();

        String newF=getMD5EncryptedString(url1)+".jpg";
        String path1 =   Constants.app_folder2+newF;
        File dir = new File(path1);
        if (!dir.exists())
        {

            new DownloadFileFromURL().execute(url1,newF);
            Log.e("Testy","Downloading"+url1+"  po"+ii);
        }
        else if (dir.exists()){
        }
        ii=ii+1;
    }

}
catch (Exception e)
{
    e.printStackTrace();
}

    }

    public  String getMD5EncryptedString(String encTarget){
        MessageDigest mdEnc = null;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Exception while encrypting to md5");
            e.printStackTrace();
        } // Encryption algorithm
        mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
        String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
        while ( md5.length() < 32 ) {
            md5 = "0"+md5;
        }
        return md5;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        CircleImageView home_profile;
        AppCompatImageView thumbnail;
        LinearLayout lly_sound;
        ImageView im_comment,imGoLive;
        ImageButton im_like,im_dislike,im_share;

        TextView sound_name;
        TextView  home_views, home_likes, home_title, home_tags;
        public TextView home_comments;
        FrameLayout frame_thumb;
        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            home_profile = itemView.findViewById(R.id.shome_profile);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            im_comment=itemView.findViewById(R.id.home_comments_image_new);
            home_comments = itemView.findViewById(R.id.home_comments);
            home_views = itemView.findViewById(R.id.home_views);
            im_like=itemView.findViewById(R.id.home_like_imagee);
            home_likes = itemView.findViewById(R.id.home_likes);
            home_title = itemView.findViewById(R.id.home_title);
            home_tags = itemView.findViewById(R.id.home_tags);
            im_dislike=itemView.findViewById(R.id.home_dislike_imagee);
            im_share=itemView.findViewById(R.id.home_share_link_h);
            frame_thumb=itemView.findViewById(R.id.frame_thumbnail);
            sound_name=itemView.findViewById(R.id.sound_name_homef);
            lly_sound=itemView.findViewById(R.id.linear_sound_h);
            imGoLive=itemView.findViewById(R.id.imGoLive);

        }
        public void bind(final int postion, final HomeModel.Data item, final VideoAdapter.OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(postion,item,v);
                }
            });


           home_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onItemClick(postion,item,v);
                }
            });

            home_title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onItemClick(postion,item,v);
                }
            });


            im_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onItemClick(postion,item,v);
                }
            });
            im_dislike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onItemClick(postion,item,v);
                }
            });

            im_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onItemClick(postion,item,v);
                }
            });

            im_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onItemClick(postion,item,v);
                }
            });

            lly_sound.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onItemClick(postion,item,v);
                }
            });
            imGoLive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onItemClick(postion,item,v);
                }
            });

        }

    }

    public class DownloadFileFromURL extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog = new ProgressDialog(context);

        @Override
        protected String doInBackground(String... f_url) {

            URL url = null;
            String urlt = null;
            String fileN1=null;

            try {
                url = new URL(f_url[0]);
                urlt=f_url[0];
                fileN1 = f_url[1];
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            prDownloader= PRDownloader.download(urlt, Constants.app_folder2, fileN1)
                    .build()
                    .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                        @Override
                        public void onStartOrResume() {

                        }
                    })
                    .setOnPauseListener(new OnPauseListener() {
                        @Override
                        public void onPause() {

                        }
                    })
                    .setOnCancelListener(new OnCancelListener() {
                        @Override
                        public void onCancel() {

                        }
                    })
                    .setOnProgressListener(new OnProgressListener() {
                        @Override
                        public void onProgress(Progress progress) {

                        }
                    });

            prDownloader.start(new OnDownloadListener() {
                @Override
                public void onDownloadComplete() {
                    Log.e("Testy","downloadedC "+ Arrays.toString(f_url));

                }
                @Override
                public void onError(Error error) {

                }
            });

  return null;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }
    }}
