package com.divineray.app.Notifications;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.divineray.app.R;
import com.divineray.app.SelecteUserMore.VideoAudioAdaper;
import com.divineray.app.StringChange.StringFormatter;
import com.divineray.app.model.FavModel;
import com.divineray.app.model.NotificationAllModel;
import com.divineray.app.model.VideoAudioModel;

import java.security.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class GetAllNotificationAdapter extends RecyclerView.Adapter<GetAllNotificationAdapter.CustomViewHolder > {
    Activity context;
    ArrayList<NotificationAllModel.Data0> data_list;
    NotificationAllModel model;
    String formattedDate;

    private GetAllNotificationAdapter.OnItemClickListener listener;

    public GetAllNotificationAdapter(Activity context, ArrayList<NotificationAllModel.Data0> data_list, NotificationAllModel mModel, OnItemClickListener onItemClickListener) {

        this.context=context;
        this.data_list=data_list;
        this.model=mModel;
        this.listener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(int positon, NotificationAllModel.Data0 item, View view);
    }

    @NonNull
    @Override
    public GetAllNotificationAdapter.CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.get_notifications_item,null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        GetAllNotificationAdapter.CustomViewHolder viewHolder = new GetAllNotificationAdapter.CustomViewHolder(view);
        return viewHolder;
    }
    void timeChange(long coment_time)
    {
        Date date = new java.util.Date(coment_time * 1000L);

// the format of your date

        SimpleDateFormat sdf = new java.text.SimpleDateFormat(" hh:mm a ");
// give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(TimeZone.getDefault());

        formattedDate = sdf.format(date);
    }
    @Override
    public void onBindViewHolder(@NonNull GetAllNotificationAdapter.CustomViewHolder holder, int position) {
        final NotificationAllModel.Data0 item= data_list.get(position);
        try {
            holder.bind(position,item,listener);

            String name= StringFormatter.capitalizeWord(item.getUserDetails().getName());
           // SpannableString ss=new SpannableString(name+item.getMessage());
           // StyleSpan boldSpan=new StyleSpan(Typeface.BOLD);
           // ss.setSpan(boldSpan,0,name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.tx_message.setText(item.getMessage());
            long dv = Long.valueOf(item.getNotificationTime())*1000;// its need to be in milisecond
            Date df = new java.util.Date(dv);
            String vv = new SimpleDateFormat("d-MMM-yyyy").format(df);
            String vv2 = new SimpleDateFormat("hh:mm a").format(df);
          //  holder.tx_message.setText(item.getUserDetails().getName()+);

            holder.tx_time.setText(vv+" at "+vv2);
            Glide.with(context).load(item.getPhoto()).placeholder(R.drawable.unknown_user).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.photo);
            if (item.getSeen().equals("0"))
            {
                holder.linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.notification_unread_color));
            }
            else
            {
                holder.linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.notification_read_color));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        CircleImageView photo;
        TextView tx_message,tx_time;
        LinearLayout linearLayout;

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            photo=itemView.findViewById(R.id.notification_photo);
            tx_message=itemView.findViewById(R.id.notifications_message);
            tx_time=itemView.findViewById(R.id.notifications_time);
            linearLayout=itemView.findViewById(R.id.notification_lly);
        }

        public void bind(int position, NotificationAllModel.Data0 item, OnItemClickListener listener) {

            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // Toast.makeText(context, "djhd", Toast.LENGTH_SHORT).show();
                   listener.onItemClick(position,item,v);
                }
            });

        }

    }
}
