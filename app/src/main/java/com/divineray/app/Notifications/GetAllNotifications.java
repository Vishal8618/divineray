package com.divineray.app.Notifications;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.divineray.app.Add.AddPostActivityS;
import com.divineray.app.Home.SelectedUserOwnProfile;
import com.divineray.app.Home.SelectedUserProfile;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.SelecteUserMore.ViddeoAudio;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.activities.HomeActivity;
import com.divineray.app.activities.LoginActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.ChhatUneadMessageModel;
import com.divineray.app.model.LoginModel;
import com.divineray.app.model.NotificationAllModel;
import com.divineray.app.model.VideoAudioModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetAllNotifications extends BaseActivity {
    String TAG = GetAllNotifications.this.getClass().getSimpleName();
    Activity mActivity = GetAllNotifications.this;


    @BindView(R.id.p_bar_notifications)
    ProgressBar p_bar;
    @BindView(R.id.swiperefresh_notifications)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.im_notifications_back)
    ImageView im_back;
    @BindView(R.id.lly_notifications_nothing)
    LinearLayout lly_nothing;
    @BindView(R.id.recylerview_notifications)
    RecyclerView recyclerView;
    ArrayList<NotificationAllModel.Data0> data_list;
    GetAllNotificationAdapter getAllNotificationAdapter;
    NotificationAllModel mModel;

    String getNotificationType,notificationId,notificationId_new;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTransparentStatusBarOnly(this);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_get_all_notifications);
        ButterKnife.bind(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        Intent inten=getIntent();
        if (inten!=null)
        {
            getNotificationType=inten.getStringExtra("notificationType");
            notificationId=inten.getStringExtra("notificationId");
            Log.e("Stest","notifyType"+getNotificationType+"  notifyId:"+notificationId);

        }

        im_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // player.setPlayWhenReady(false);
                finish();
            }
        });

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                doApiCall();
            }
        });



    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id",  DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("lastNotificationId", "");
        mMap.put("perPage","100");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    @Override
    protected void onResume() {
        super.onResume();
        doApiCall();
    }

    private void doApiCall() {

       // showProgressDialog(mActivity);
        //Toast.makeText(mActivity, ""+strDeviceToken, Toast.LENGTH_SHORT).show();
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getAllNotifications(getAuthToken(),mParams()).enqueue(new retrofit2.Callback<NotificationAllModel>() {
            @Override
            public void onResponse(Call<NotificationAllModel> call, Response<NotificationAllModel> response) {
               dismissProgressDialog();
swiperefresh.setRefreshing(false);

               // Toast.makeText(mActivity, ""+response.body(), Toast.LENGTH_SHORT).show();
                 mModel = response.body();
                if (mModel.getStatus()==1) {


                    recyclerView.setVisibility(View.VISIBLE);
                    lly_nothing.setVisibility(View.GONE);
                    data_list= (ArrayList<NotificationAllModel.Data0>) response.body().getData();
                    for (int i=0;i<data_list.size();i++)
                    {
                        String val=mModel.getData().get(i).getNotificationId();
                        if (val.equals(notificationId))
                        {
                            if (mModel.getData().get(i).getNotificationType().equals("1")||mModel.getData().get(i).getNotificationType().equals("2")) {
                                notificationId_new=mModel.getData().get(i).getNotificationId();
                                executReadApi();
                                OpenProfile2(mModel.getData().get(i), false);
                                notificationId="";
                            }
                            else if (mModel.getData().get(i).getNotificationType().equals("3")||mModel.getData().get(i).getNotificationType().equals("4"))
                            {
                                notificationId_new=mModel.getData().get(i).getNotificationId();
                                executReadApi();
                                OpenVideo2(mModel.getData().get(i),false);
                                notificationId="";

                            }
                        }
                        else {
                            Log.e("Tg","3");

                        }
                        Log.e("Testn",val);
                    }
                    setAdapter();
                   // Toast.makeText(mActivity, ""+data_list, Toast.LENGTH_SHORT).show();



                } else if (response.body().getStatus()==0) {
                    recyclerView.setVisibility(View.GONE);
                    lly_nothing.setVisibility(View.VISIBLE);
                   // showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<NotificationAllModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Testr", "**RESPONSE**" + t.getMessage());
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });


    //  setAdapter();

    }

    private void executReadApi() {
          //showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.sendReadNotification(getAuthToken(),mParams2()).enqueue(new Callback<ChhatUneadMessageModel>() {
            @Override
            public void onResponse(Call<ChhatUneadMessageModel> call, Response<ChhatUneadMessageModel> response) {

                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                ChhatUneadMessageModel mGetDetailsModel = response.body();
                if (mGetDetailsModel!=null) {
                    if (mGetDetailsModel.getStatus() == 1) {
                        Log.e("Data","1");

                    } else {
                        Log.e("Data","0");  }
                }

            }

            @Override
            public void onFailure(Call<ChhatUneadMessageModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setAdapter() {
        getAllNotificationAdapter=new GetAllNotificationAdapter(mActivity,data_list,mModel, new GetAllNotificationAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int positon, NotificationAllModel.Data0 item, View view) {
                switch (view.getId()) {
                    case R.id.notification_lly:
                        notificationId_new = item.getNotificationId();
                        executReadApi();

                        if (item.getNotificationType().equals("1") || item.getNotificationType().equals("2")) {
                            //    notificationId_new=item.getNotificationId();
                            // executReadApi();
                            OpenProfile(item, false);
                            break;
                        } else if (item.getNotificationType().equals("3") || item.getNotificationType().equals("4")) {
                            //    notificationId_new=item.getNotificationId();

                            //  executReadApi();
                            try {
                                OpenVideo(item, false);
                                break;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            try {
                                OpenProfile(item, false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;
                        }

                }
            }});

       // Log.e("Testn","d : "+mModel.getData().toString()+" size:"+data_list.size() );
        recyclerView.setAdapter(getAllNotificationAdapter);
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("notificationId",notificationId_new);
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void OpenVideo(NotificationAllModel.Data0 item, boolean b) {
        Intent intent = new Intent(mActivity, SingleVideoView.class);
        intent.putExtra("profileUserId", item.getUser_id());
        intent.putExtra("videouri", item.getVideo_details().getPostVideo());
        intent.putExtra("thumbnail", item.getVideo_details().getPostImage());
        intent.putExtra("width", item.getVideo_details().getPostWidth());
        intent.putExtra("height", item.getVideo_details().getPostHeight());
        intent.putExtra("totalcomments", item.getVideo_details().getTotalComments());
        intent.putExtra("videoid", item.getVideo_details().getVideoId());
        intent.putExtra("totallikes", item.getVideo_details().getTotalLike());
        intent.putExtra("description", item.getVideo_details().getDescription());
        intent.putExtra("tags", item.getVideo_details().getTags());
        intent.putExtra("userphoto", item.getVideo_details().getUserDetails().getPhoto());
        intent.putExtra("views", item.getVideo_details().getTotalViews());
        intent.putExtra("music", item.getVideo_details().getMusicName());
        intent.putExtra("isLiked", item.getVideo_details().getIsLiked());
        intent.putExtra("username", item.getVideo_details().getUserDetails().getName());
        intent.putExtra("userfollowings", item.getVideo_details().getUserDetails().getTotalFollowing());
        intent.putExtra("userfollowers", item.getVideo_details().getUserDetails().getTotalFollowers());
        intent.putExtra("userdescription", item.getVideo_details().getUserDetails().getDescription());



        //intent.putExtra("followornot", item.getUserDetails().getFollow());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void OpenProfile(NotificationAllModel.Data0 item, boolean b) {
      //  Toast.makeText(mActivity, "dd", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(mActivity, SelectedUserProfile.class);
            intent.putExtra("profileUserId", item.getNotifyBy());
            intent.putExtra("followingsb", item.getUserDetails().getTotalFollowing());
            intent.putExtra("followersb", item.getUserDetails().getTotalFollowers());
            intent.putExtra("photo", item.getUserDetails().getPhoto());
            intent.putExtra("name", item.getUserDetails().getName());
            intent.putExtra("descriptionb", item.getUserDetails().getDescription());
            //intent.putExtra("followornot", item.getUserDetails().getFollow());
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

    }
    private void OpenProfile2(NotificationAllModel.Data0 item, boolean b) {
        Intent intent = new Intent(mActivity, SelectedUserProfile.class);
        intent.putExtra("profileUserId", item.getNotifyBy());
        intent.putExtra("followingsb", item.getUserDetails().getTotalFollowing());
        intent.putExtra("followersb", item.getUserDetails().getTotalFollowers());
        intent.putExtra("photo", item.getUserDetails().getPhoto());
        intent.putExtra("name", item.getUserDetails().getName());
        intent.putExtra("descriptionb", item.getUserDetails().getDescription());
        //intent.putExtra("followornot", item.getUserDetails().getFollow());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }
    private void OpenVideo2(NotificationAllModel.Data0 item, boolean b) {
        Intent intent = new Intent(mActivity, SingleVideoView.class);
        intent.putExtra("profileUserId", item.getUser_id());
        intent.putExtra("videouri", item.getVideo_details().getPostVideo());
        intent.putExtra("thumbnail", item.getVideo_details().getPostImage());
        intent.putExtra("width", item.getVideo_details().getPostWidth());
        intent.putExtra("height", item.getVideo_details().getPostHeight());
        intent.putExtra("totalcomments", item.getVideo_details().getTotalComments());
        intent.putExtra("videoid", item.getVideo_details().getVideoId());
        intent.putExtra("totallikes", item.getVideo_details().getTotalLike());
        intent.putExtra("description", item.getVideo_details().getDescription());
        intent.putExtra("tags", item.getVideo_details().getTags());
        intent.putExtra("userphoto", item.getVideo_details().getUserDetails().getPhoto());
        intent.putExtra("views", item.getVideo_details().getTotalViews());
        intent.putExtra("music", item.getVideo_details().getMusicName());
        intent.putExtra("isLiked", item.getVideo_details().getIsLiked());
        intent.putExtra("username", item.getVideo_details().getUserDetails().getName());
        intent.putExtra("userfollowings", item.getVideo_details().getUserDetails().getTotalFollowing());
        intent.putExtra("userfollowers", item.getVideo_details().getUserDetails().getTotalFollowers());
        intent.putExtra("userdescription", item.getVideo_details().getUserDetails().getDescription());



        //intent.putExtra("followornot", item.getUserDetails().getFollow());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}