package com.divineray.app.Notifications;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.divineray.app.Home.BottomSheetDialogH;
import com.divineray.app.Home.DownloadUtil;
import com.divineray.app.Home.Fragment_Data_Send;
import com.divineray.app.Home.SelectedUserOwnProfile;
import com.divineray.app.Home.SelectedUserProfile;
import com.divineray.app.R;
import com.divineray.app.Report.ReportVideo;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.Constants;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.Utils.Functions;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.HomeModel;
import com.divineray.app.model.NotificationAllModel;
import com.divineray.app.model.ProfileGetVideoModel;
import com.divineray.app.model.VideofavModel;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.downloader.request.DownloadRequest;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SingleVideoView extends BaseActivity implements Player.EventListener , Fragment_Data_Send {
    Activity mactivity=SingleVideoView.this;

    CircleImageView home_profile;

    AppCompatImageView thumbnail;
    LinearLayout lly_sound;
    ImageView im_comment,im_back;
    ImageButton im_like,im_dislike,im_share;

    TextView sound_name;
    TextView  home_views, home_likes, home_title, home_tags;
    public static TextView home_comments_singlevid;
    FrameLayout frame_thumb;
    SimpleExoPlayer player;
    PlayerView playerView;
    boolean is_user_stop_video = false;

    String profileUserId,videouri,thumbnaill,username,width,height,music,videoid,totallikes,isLiked,description,tags,views;
    String userphoto,userfollowings,userfollowers,userdescription;
public  static String totalcomments_singlevid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTransparentStatusBarOnly(this);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_single_video_view);
        home_profile = findViewById(R.id.singlevid_shome_profile);
        thumbnail = findViewById(R.id.singlevid_thumbnail);
        im_comment=findViewById(R.id.singlevid__comments_image_new);
        home_comments_singlevid = findViewById(R.id.singlevid__comments);
        home_views = findViewById(R.id.singlevid__views);
        im_like=findViewById(R.id.singlevid__like_imagee);
        im_back=findViewById(R.id.singlevid_backpress);
        home_likes = findViewById(R.id.singlevid__likes);
        home_title = findViewById(R.id.singlevid__title);
        home_tags = findViewById(R.id.singlevid__tags);
        im_dislike=findViewById(R.id.singlevid__dislike_imagee);
        im_share=findViewById(R.id.singlevid__share_link_h);
        frame_thumb=findViewById(R.id.singlevid_frame_thumbnail);
        sound_name=findViewById(R.id.singlevid__name_homef);
        lly_sound=findViewById(R.id.singlevid__sound_h);
        playerView=findViewById(R.id.singlevid_playerview);


        profileUserId=getIntent().getStringExtra("profileUserId");
        videouri=getIntent().getStringExtra("videouri");
        thumbnaill=getIntent().getStringExtra("thumbnail");
        width=getIntent().getStringExtra("width");
        height=getIntent().getStringExtra("height");
    String    totalcomments=getIntent().getStringExtra("totalcomments");
        videoid=getIntent().getStringExtra("videoid");
        totallikes=getIntent().getStringExtra("totallikes");
        isLiked=getIntent().getStringExtra("isLiked");
        views=getIntent().getStringExtra("views");
        description=getIntent().getStringExtra("description");
        tags=getIntent().getStringExtra("tags");
        music=getIntent().getStringExtra("music");
        username=getIntent().getStringExtra("username");
        String newF=getMD5EncryptedString(thumbnaill)+".jpg";
        userphoto =getIntent().getStringExtra("userphoto");

        home_title.setText(description);
        home_tags.setText(tags);
        if (isLiked.equals("0"))
        {
            im_like.setVisibility(View.VISIBLE);
            im_dislike.setVisibility(View.GONE);
        }
        else {
            im_like.setVisibility(View.GONE);
            im_dislike.setVisibility(View.VISIBLE);
        }
        Glide.with(mactivity).load(userphoto).diskCacheStrategy(DiskCacheStrategy.ALL).into(home_profile);

        String path =  Constants.app_folder2+newF;
        File dir = new File(path);

        home_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenProfile( true);
            }
        });

        im_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        home_views.setText(views);
home_likes.setText(totallikes);
home_comments_singlevid.setText(totalcomments);
totalcomments_singlevid=home_comments_singlevid.getText().toString();

im_share.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "DivineRay");
            String shareMessage= "\nSee this Video\n";
            shareMessage = shareMessage + videouri;
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch(Exception e) {
            //e.toString();
        }
    }
});
        if((music==null || music.equals("") || music.equals("null"))){
            sound_name.setText("   @Original   voice -       "+username+"                                  ");
        }else {
            if (username.length()<=4) {
                sound_name.setText("    @Music -    " + username + "                               ");
            }
            else {
                sound_name.setText("    @Music -        " + username + "                                                              ");
            }
            // holder.sound_name.setWidth(20);
        }
im_like.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
Like_Video("1");
    }
});
im_dislike.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Like_Video("0");
    }
});



im_comment.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int value=Integer.parseInt(totalcomments_singlevid);
        int comment_counnt=value;

        Fragment_Data_Send fragment_data_send = null;


        String comment_userid=profileUserId;
        String comment_videoId=videoid;

        // fun();
        Bundle args=new Bundle();
        args.putString("b_comment_userid",comment_userid);
        args.putString("b_comment_videoId",comment_videoId);

        totalcomments_singlevid=home_comments_singlevid.getText().toString();
        BottomSheetDialogH bottomSheet = new BottomSheetDialogH(comment_counnt,fragment_data_send);
        bottomSheet.setArguments(args);
        bottomSheet.show(getSupportFragmentManager(), "BottomSheet");

    }
});



        if (dir.exists())
        {
            Log.e("Testr","dir exists"+newF);
            Glide.with(mactivity).load(path).diskCacheStrategy(DiskCacheStrategy.ALL).into(thumbnail);

        }
        else {

            Log.e("Testr","dir not exists"+newF);

            Glide.with(mactivity).load(path).diskCacheStrategy(DiskCacheStrategy.ALL).into(thumbnail);


        }
        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
//
        player = ExoPlayerFactory.newSimpleInstance(mactivity, trackSelector);

        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(mactivity,
                Util.getUserAgent(mactivity, "DivineRay"));
        CacheDataSourceFactory cacheDataSourceFactory= new CacheDataSourceFactory(DownloadUtil.getCache(mactivity),dataSourceFactory, CacheDataSource.FLAG_BLOCK_ON_CACHE|CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);
        MediaSource videoSource= new ExtractorMediaSource.Factory(cacheDataSourceFactory)
                .createMediaSource(Uri.parse(videouri));

        player.prepare(videoSource);
        player.seekTo(0);
        float checkVal=Float.parseFloat(height);
        float checkVal2=Float.parseFloat(width);
        float val2=checkVal-checkVal2;

        ViewIncrease();

        if (checkVal==checkVal2)
        {
            thumbnail.setScaleType(ImageView.ScaleType.FIT_CENTER);
            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
            //    player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            playerView.setPlayer(player);
        }
        else if (checkVal2>checkVal||val2<200)
        {
            thumbnail.setScaleType(ImageView.ScaleType.FIT_CENTER);
            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
            // player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            playerView.setPlayer(player);
        }
        else if (checkVal2<checkVal) {
            thumbnail.setScaleType(ImageView.ScaleType.CENTER_CROP);
            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
            //  player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            playerView.setPlayer(player);
        }
thumbnail.setVisibility(View.VISIBLE);




        player.setPlayWhenReady(true);

        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {

                    case Player.STATE_BUFFERING:
                       // thumbnail.setVisibility(View.VISIBLE);
                        break;
                    case Player.STATE_IDLE:
                        thumbnail.setVisibility(View.VISIBLE);


                        break;

                    case Player.STATE_READY:

                        playerView.setVisibility(View.VISIBLE);
                        thumbnail.setVisibility(View.INVISIBLE);
                        dismissProgressDialog();
                        break;

                    default:
                        //  layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });

      //  SimpleExoPlayer finalPlayer = player;
        playerView.setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(mactivity, new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                    super.onFling(e1, e2, velocityX, velocityY);
                    float deltaX = e1.getX() - e2.getX();
                    float deltaXAbs = Math.abs(deltaX);
                    // Only when swipe distance between minimal and maximal distance value then we treat it as effective swipe
                    if((deltaXAbs > 100) && (deltaXAbs < 1000)) {
                        if(deltaX > 0)
                        {
                            //  OpenProfile(item,true);
                        }
                    }


                    return true;
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    super.onSingleTapUp(e);
                    if(!player.getPlayWhenReady()){
                        is_user_stop_video=false;
                        player.setPlayWhenReady(true);
                    }else{
                        is_user_stop_video=true;
                        player.setPlayWhenReady(false);
                    }


                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    Show_video_option(videouri,videoid);

                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {

                    if(!player.getPlayWhenReady()){
                        is_user_stop_video=false;
                        player.setPlayWhenReady(true);
                    }


//                    if(Variables.sharedPreferences.getBoolean(Variables.islogin,false)) {
//                        Show_heart_on_DoubleTap(item, mainlayout, e);
//                        Like_Video(currentPage, item);
//                    }else {
//                        Toast.makeText(context, "Please Login into app", Toast.LENGTH_SHORT).show();
//                    }
                    return super.onDoubleTap(e);

                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });

    }
    private Map<String, String> mParamsVi() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mactivity,DivineRayPrefernces.USERID,null));
        mMap.put("videoId",videoid);
        mMap.put("isView","1");
        mMap.put("indexCall","" );
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void ViewIncrease() {

        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.addFavVideo(getAuthToken(),mParamsVi()).enqueue(new Callback<VideofavModel>() {
            @Override
            public void onResponse(Call<VideofavModel> call, Response<VideofavModel> response) {
                int val=Integer.parseInt(home_views.getText().toString())+1;
                home_views.setText(String.valueOf(val));
                Log.e("", "**RESPONSE**" + response.body());
                VideofavModel mGetDetailsModel = response.body();

//                assert mGetDetailsModel != null;
//                if (mGetDetailsModel.getStatus() == 1) {
//
//                } else {
//
//
//                    //Toast.makeText(mActivity, "Error Getting daa", Toast.LENGTH_SHORT).show();
//                }

            }

            @Override
            public void onFailure(Call<VideofavModel> call, Throwable t) {

                Log.e("", "**ERROR**" + t.getMessage());
            }
        });


    }


    boolean is_visible_to_user;

    private void OpenProfile( boolean b) {
        String new_profileId= DivineRayPrefernces.readString(mactivity,DivineRayPrefernces.USERID,null);
        if (profileUserId.equals(new_profileId)) {
            Intent intent = new Intent(mactivity, SelectedUserOwnProfile.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

        }
        else {
            Intent intent = new Intent(mactivity, SelectedUserProfile.class);
            intent.putExtra("profileUserId", profileUserId);
            intent.putExtra("followingsb",userfollowings );
            intent.putExtra("followersb", userfollowers);
            intent.putExtra("photo", userphoto);
            intent.putExtra("name", username);
            intent.putExtra("descriptionb", userdescription);
            //intent.putExtra("followornot", item.getUserDetails().getFollow());
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        //  Toast.makeText(mActivity, "dd", Toast.LENGTH_SHORT).show();


    }

    @Override
    protected void onResume() {
        super.onResume();

        if((player!=null && (is_visible_to_user && !is_user_stop_video)) ){
            // Toast.makeText(context, "Start Play1", Toast.LENGTH_SHORT).show();

            player.setPlayWhenReady(true);
        }
        AudioManager ama = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

// Request audio focus for playback
        int result = ama.requestAudioFocus(focusChangeListener,
// Use the music stream.
                AudioManager.STREAM_MUSIC,
// Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);


        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
// other app had stopped playing song now , so u can do u stuff now .
        }
    }

    public  String getMD5EncryptedString(String encTarget){
        MessageDigest mdEnc = null;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Exception while encrypting to md5");
            e.printStackTrace();
        } // Encryption algorithm
        mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
        String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
        while ( md5.length() < 32 ) {
            md5 = "0"+md5;
        }
        return md5;
    }
    private void Show_video_option(String videouri,String videoid) {

        final CharSequence[] options = { "Report Video","Save Video","Cancel" };

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mactivity,R.style.AlertDialogCustom);

        builder.setTitle(null);

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Report Video"))

                {
                    Intent intent=new Intent(mactivity, ReportVideo.class);
                    intent.putExtra("videoId",videoid);
                    startActivity(intent);
                    dialog.dismiss();

                }
                else if (options[item].equals("Save Video"))

                {
                    if(Functions.Checkstoragepermision(mactivity))
                        Save_Video(videouri);

                }

                else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }

            }

        });

        builder.show();

    }
    public void Save_Video(String videouri){
        String path =   Constants.app_folder3;
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdir();
        }

        String newF=getMD5EncryptedString(videouri)+".mp4";
        String path2 =   Constants.app_folder3+newF;
        File dir2 = new File(path2);
        if (dir2.exists()) {
            Toast.makeText(mactivity, "Video already downloaded", Toast.LENGTH_SHORT).show();
        }
        else {
            Functions.Show_determinent_loader(mactivity,false,false);
            PRDownloader.initialize(getApplicationContext());
            DownloadRequest prDownloader= PRDownloader.download(videouri, Constants.app_folder3, newF)
                    .build()
                    .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                        @Override
                        public void onStartOrResume() {

                        }
                    })
                    .setOnPauseListener(new OnPauseListener() {
                        @Override
                        public void onPause() {

                        }
                    })
                    .setOnCancelListener(new OnCancelListener() {
                        @Override
                        public void onCancel() {

                        }
                    })
                    .setOnProgressListener(new OnProgressListener() {
                        @Override
                        public void onProgress(Progress progress) {

                            int prog=(int)((progress.currentBytes*100)/progress.totalBytes);
                            Functions.Show_loading_progress(prog/2);

                        }
                    });


            prDownloader.start(new OnDownloadListener() {
                @Override
                public void onDownloadComplete() {
                    Functions.cancel_determinent_loader();
                    Toast.makeText(mactivity, "Video stored to"+Constants.app_folder3, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(Error error) {

                    Toast.makeText(mactivity, "Error", Toast.LENGTH_SHORT).show();
                    Functions.cancel_determinent_loader();
                }


            });
        }






    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mactivity,DivineRayPrefernces.USERID,null));
        mMap.put("videoId",videoid);
        mMap.put("isView","0");
        mMap.put("indexCall","" );
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void Like_Video(String data) {

        showProgressDialog(mactivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.addFavVideo(getAuthToken(),mParams2()).enqueue(new Callback<VideofavModel>() {
            @Override
            public void onResponse(Call<VideofavModel> call, Response<VideofavModel> response) {

                VideofavModel mGetDetailsModel = response.body();
                if (mGetDetailsModel.getStatus() == 1) {
                    dismissProgressDialog();
                    Log.e("", "**RESPONSE**" + response.body());
if (data.equals("0"))
{
    home_likes.setText(String.valueOf(Integer.parseInt(home_likes.getText().toString())-1));

    im_like.setVisibility(View.VISIBLE);
    im_dislike.setVisibility(View.GONE);
}
else {
    home_likes.setText(String.valueOf(Integer.parseInt(home_likes.getText().toString())+1));
  //  totallikes=String.valueOf(Integer.parseInt(totallikes)-1);
        im_like.setVisibility(View.GONE);
        im_dislike.setVisibility(View.VISIBLE);
    //}
}


                    //  Toast.makeText(mContext, "islike"+isView+"fc:  "+video_id+"Use  "+DivineRayPrefernces.readString(mContext,DivineRayPrefernces.USERID,null), Toast.LENGTH_SHORT).show();
                } else {

                    //Toast.makeText(mContext, "Error Getting daa", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<VideofavModel> call, Throwable t) {

                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private AudioManager.OnAudioFocusChangeListener focusChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                public void onAudioFocusChange(int focusChange) {
                    try {
                        MediaPlayer mediaPlayerBackground = new MediaPlayer();
                        AudioManager am = (AudioManager) Objects.requireNonNull(getApplicationContext()).getSystemService(Context.AUDIO_SERVICE);
                        switch (focusChange) {

                            case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK):
                                // Lower the volume while ducking.
                                mediaPlayerBackground.setVolume(0.2f, 0.2f);
                                break;
                            case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT):
                                mediaPlayerBackground.stop();
                                break;

                            case (AudioManager.AUDIOFOCUS_LOSS):
                                mediaPlayerBackground.stop();

                                break;

                            case (AudioManager.AUDIOFOCUS_GAIN):
                                // Return the volume to normal and resume if paused.
                                mediaPlayerBackground.setVolume(1f, 1f);
                                mediaPlayerBackground.start();
                                break;
                            default:
                                break;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

    @Override
    public void onPause() {
        super.onPause();
        if(player!=null){
            player.setPlayWhenReady(false);
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if(player!=null){
            player.setPlayWhenReady(false);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(player!=null){
            player.release();
        }
    }

    @Override
    public void onDataSent(String yourData) {
        int comment_count =Integer.parseInt(yourData);

        home_comments_singlevid.setText(comment_count);
    }
}