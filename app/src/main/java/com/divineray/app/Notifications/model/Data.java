package com.divineray.app.Notifications.model;

public class Data
{
    private String detailId;

    private String photo;

    private String notificationType;

    private String message;

    private UserDetails userDetails;

    private String seen;

    private String user_id;

    private String notificationTime;

    private String name;

    private Video_details video_details;

    private String notificationId;

    private String typeId;

    private String notifyBy;

    public String getDetailId ()
    {
        return detailId;
    }

    public void setDetailId (String detailId)
    {
        this.detailId = detailId;
    }

    public String getPhoto ()
    {
        return photo;
    }

    public void setPhoto (String photo)
    {
        this.photo = photo;
    }

    public String getNotificationType ()
    {
        return notificationType;
    }

    public void setNotificationType (String notificationType)
    {
        this.notificationType = notificationType;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public UserDetails getUserDetails ()
    {
        return userDetails;
    }

    public void setUserDetails (UserDetails userDetails)
    {
        this.userDetails = userDetails;
    }

    public String getSeen ()
    {
        return seen;
    }

    public void setSeen (String seen)
    {
        this.seen = seen;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getNotificationTime ()
    {
        return notificationTime;
    }

    public void setNotificationTime (String notificationTime)
    {
        this.notificationTime = notificationTime;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Video_details getVideo_details ()
    {
        return video_details;
    }

    public void setVideo_details (Video_details video_details)
    {
        this.video_details = video_details;
    }

    public String getNotificationId ()
    {
        return notificationId;
    }

    public void setNotificationId (String notificationId)
    {
        this.notificationId = notificationId;
    }

    public String getTypeId ()
    {
        return typeId;
    }

    public void setTypeId (String typeId)
    {
        this.typeId = typeId;
    }

    public String getNotifyBy ()
    {
        return notifyBy;
    }

    public void setNotifyBy (String notifyBy)
    {
        this.notifyBy = notifyBy;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [detailId = "+detailId+", photo = "+photo+", notificationType = "+notificationType+", message = "+message+", userDetails = "+userDetails+", seen = "+seen+", user_id = "+user_id+", notificationTime = "+notificationTime+", name = "+name+", video_details = "+video_details+", notificationId = "+notificationId+", typeId = "+typeId+", notifyBy = "+notifyBy+"]";
    }
}

