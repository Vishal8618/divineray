package com.divineray.app.Notifications.model;

public class UserDetails
{
    private String paypal_token;

    private String paypal_verification;

    private String country;

    private String facebookId;

    private String totalFollowing;

    private String verified;

    private String photo;

    private String description;

    private String created_at;

    private String allowPush;

    private String device_type;

    private String appleId;

    private String twitterId;

    private String password;

    private String user_id;

    private String countryCode;

    private String total_coins;

    private String device_token;

    private String name;

    private String verificateCode;

    private String disabled;

    private String paypal_id;

    private String totalFollowers;

    private String email;

    public String getPaypal_token ()
    {
        return paypal_token;
    }

    public void setPaypal_token (String paypal_token)
    {
        this.paypal_token = paypal_token;
    }

    public String getPaypal_verification ()
    {
        return paypal_verification;
    }

    public void setPaypal_verification (String paypal_verification)
    {
        this.paypal_verification = paypal_verification;
    }

    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        this.country = country;
    }

    public String getFacebookId ()
    {
        return facebookId;
    }

    public void setFacebookId (String facebookId)
    {
        this.facebookId = facebookId;
    }

    public String getTotalFollowing ()
    {
        return totalFollowing;
    }

    public void setTotalFollowing (String totalFollowing)
    {
        this.totalFollowing = totalFollowing;
    }

    public String getVerified ()
    {
        return verified;
    }

    public void setVerified (String verified)
    {
        this.verified = verified;
    }

    public String getPhoto ()
    {
        return photo;
    }

    public void setPhoto (String photo)
    {
        this.photo = photo;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getAllowPush ()
    {
        return allowPush;
    }

    public void setAllowPush (String allowPush)
    {
        this.allowPush = allowPush;
    }

    public String getDevice_type ()
    {
        return device_type;
    }

    public void setDevice_type (String device_type)
    {
        this.device_type = device_type;
    }

    public String getAppleId ()
    {
        return appleId;
    }

    public void setAppleId (String appleId)
    {
        this.appleId = appleId;
    }

    public String getTwitterId ()
    {
        return twitterId;
    }

    public void setTwitterId (String twitterId)
    {
        this.twitterId = twitterId;
    }

    public String getPassword ()
    {
        return password;
    }

    public void setPassword (String password)
    {
        this.password = password;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getCountryCode ()
    {
        return countryCode;
    }

    public void setCountryCode (String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String getTotal_coins ()
    {
        return total_coins;
    }

    public void setTotal_coins (String total_coins)
    {
        this.total_coins = total_coins;
    }

    public String getDevice_token ()
    {
        return device_token;
    }

    public void setDevice_token (String device_token)
    {
        this.device_token = device_token;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getVerificateCode ()
    {
        return verificateCode;
    }

    public void setVerificateCode (String verificateCode)
    {
        this.verificateCode = verificateCode;
    }

    public String getDisabled ()
    {
        return disabled;
    }

    public void setDisabled (String disabled)
    {
        this.disabled = disabled;
    }

    public String getPaypal_id ()
    {
        return paypal_id;
    }

    public void setPaypal_id (String paypal_id)
    {
        this.paypal_id = paypal_id;
    }

    public String getTotalFollowers ()
    {
        return totalFollowers;
    }

    public void setTotalFollowers (String totalFollowers)
    {
        this.totalFollowers = totalFollowers;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [paypal_token = "+paypal_token+", paypal_verification = "+paypal_verification+", country = "+country+", facebookId = "+facebookId+", totalFollowing = "+totalFollowing+", verified = "+verified+", photo = "+photo+", description = "+description+", created_at = "+created_at+", allowPush = "+allowPush+", device_type = "+device_type+", appleId = "+appleId+", twitterId = "+twitterId+", password = "+password+", user_id = "+user_id+", countryCode = "+countryCode+", total_coins = "+total_coins+", device_token = "+device_token+", name = "+name+", verificateCode = "+verificateCode+", disabled = "+disabled+", paypal_id = "+paypal_id+", totalFollowers = "+totalFollowers+", email = "+email+"]";
    }
}

