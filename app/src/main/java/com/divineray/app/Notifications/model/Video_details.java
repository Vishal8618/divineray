package com.divineray.app.Notifications.model;

public class Video_details
{
    private String postHeight;

    private String postVideo;

    private String created;

    private String isLiked;

    private String totalLike;

    private String description;

    private String videoId;

    private String postWidth;

    private String title;

    private UserDetails userDetails;

    private String musicName;

    private String tags;

    private String postImage;

    private String totalComments;

    private String musicId;

    private String user_id;

    private String price;

    private String disable;

    private String totalViews;

    private String shareUrl;

    public String getPostHeight ()
    {
        return postHeight;
    }

    public void setPostHeight (String postHeight)
    {
        this.postHeight = postHeight;
    }

    public String getPostVideo ()
    {
        return postVideo;
    }

    public void setPostVideo (String postVideo)
    {
        this.postVideo = postVideo;
    }

    public String getCreated ()
    {
        return created;
    }

    public void setCreated (String created)
    {
        this.created = created;
    }

    public String getIsLiked ()
    {
        return isLiked;
    }

    public void setIsLiked (String isLiked)
    {
        this.isLiked = isLiked;
    }

    public String getTotalLike ()
    {
        return totalLike;
    }

    public void setTotalLike (String totalLike)
    {
        this.totalLike = totalLike;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getVideoId ()
    {
        return videoId;
    }

    public void setVideoId (String videoId)
    {
        this.videoId = videoId;
    }

    public String getPostWidth ()
    {
        return postWidth;
    }

    public void setPostWidth (String postWidth)
    {
        this.postWidth = postWidth;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public UserDetails getUserDetails ()
    {
        return userDetails;
    }

    public void setUserDetails (UserDetails userDetails)
    {
        this.userDetails = userDetails;
    }

    public String getMusicName ()
    {
        return musicName;
    }

    public void setMusicName (String musicName)
    {
        this.musicName = musicName;
    }

    public String getTags ()
    {
        return tags;
    }

    public void setTags (String tags)
    {
        this.tags = tags;
    }

    public String getPostImage ()
    {
        return postImage;
    }

    public void setPostImage (String postImage)
    {
        this.postImage = postImage;
    }

    public String getTotalComments ()
    {
        return totalComments;
    }

    public void setTotalComments (String totalComments)
    {
        this.totalComments = totalComments;
    }

    public String getMusicId ()
    {
        return musicId;
    }

    public void setMusicId (String musicId)
    {
        this.musicId = musicId;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getDisable ()
    {
        return disable;
    }

    public void setDisable (String disable)
    {
        this.disable = disable;
    }

    public String getTotalViews ()
    {
        return totalViews;
    }

    public void setTotalViews (String totalViews)
    {
        this.totalViews = totalViews;
    }

    public String getShareUrl ()
    {
        return shareUrl;
    }

    public void setShareUrl (String shareUrl)
    {
        this.shareUrl = shareUrl;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [postHeight = "+postHeight+", postVideo = "+postVideo+", created = "+created+", isLiked = "+isLiked+", totalLike = "+totalLike+", description = "+description+", videoId = "+videoId+", postWidth = "+postWidth+", title = "+title+", userDetails = "+userDetails+", musicName = "+musicName+", tags = "+tags+", postImage = "+postImage+", totalComments = "+totalComments+", musicId = "+musicId+", user_id = "+user_id+", price = "+price+", disable = "+disable+", totalViews = "+totalViews+", shareUrl = "+shareUrl+"]";
    }
}

