package com.divineray.app.Profile;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.divineray.app.R;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.model.ProfileGetVideoModel;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter{

Activity mActivity;
    ArrayList<ProfileGetVideoModel.Data> mArrayList = new ArrayList<>();


    public CustomAdapter(Activity activity, ArrayList<ProfileGetVideoModel.Data> mArrayList) {
        this.mActivity=activity;
        this.mArrayList=mArrayList;
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView==null){
            convertView=LayoutInflater.from(mActivity).inflate(R.layout.sub_profile_image,parent,false);
        }

        ProfileGetVideoModel.Data mModel = mArrayList.get(position);
        ImageView img=(ImageView)convertView.findViewById(R.id.icon);
        Glide.with(mActivity)
                .load(mModel.getPostImage())
                .into(img);


            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent(mActivity, ProfieVideoView.class);
                    intent.putExtra("videoId", String.valueOf(position));
                    String id=DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null);
                    intent.putExtra("profileUserId",id);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
                    mActivity.startActivity(intent);

                }
            });

        return convertView;
    }
    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


}