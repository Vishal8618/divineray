package com.divineray.app.Profile;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.divineray.app.R;
import com.divineray.app.model.FavModel;

import java.util.ArrayList;

public class CustomAdapterFav extends BaseAdapter{

    Activity mActivity;
    ArrayList<FavModel.FavVideos> mArrayList = new ArrayList<>();
    String profileUserId;


    public CustomAdapterFav(Activity activity, ArrayList<FavModel.FavVideos> mArrayList,String profileUserId) {
        this.mActivity=activity;
        this.mArrayList=mArrayList;
        this.profileUserId=profileUserId;
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView==null){
            convertView=LayoutInflater.from(mActivity).inflate(R.layout.sub_profile_image2,parent,false);
        }

        FavModel.FavVideos mModel = mArrayList.get(position);
        ImageView img=(ImageView)convertView.findViewById(R.id.icon2);
        Glide.with(mActivity)
                .load(mModel.getPostImage())
                .into(img);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(mActivity, ProfileUnfavVideoview.class);
                intent.putExtra("videoId", String.valueOf(position));
                intent.putExtra("profileUserId",profileUserId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(intent);

            }
        });

        return convertView;
    }
    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


}