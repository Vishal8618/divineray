package com.divineray.app.Profile;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.Utils.FileCompressor;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.DataModel;
import com.divineray.app.model.EditProfileStatusModel;
import com.divineray.app.model.GetProfileModel;
import com.divineray.app.model.NotificationModel;
import com.divineray.app.model.RootModel;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static android.graphics.BitmapFactory.decodeFile;

public class EditProfileActivity extends BaseActivity {
    String TAG = EditProfileActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = EditProfileActivity.this;

    @BindView(R.id.ed_profile_email)
    TextView ed_profile_email;
    @BindView(R.id.ed_profile_name)
    EditText ed_profile_name;
    @BindView(R.id.ed_lly_profile_image)
    FrameLayout ed_lly_profile_image;
    @BindView(R.id.ed_profile_password)
    EditText ed_profile_password;
    @BindView(R.id.ed_profile_description)
    EditText ed_profile_description;
    @BindView(R.id.ed_profile_savebutton)
    TextView ed_profile_savebutton;
    @BindView(R.id.ed_profilePic)
    ImageView ed_profilePic;
    @BindView(R.id.ed_profile_cancel)
    TextView ed_profile_cancel;
    @BindView(R.id.ed_profile_username)
    TextView ed_profile_username;
    @BindView(R.id.ed_profie_pass_lly)
    LinearLayout ed_profie_pass_lly;
    @BindView(R.id.ed_profile_email_ch)
    EditText ed_profile_email_ch;
    static final int REQUEST_IMAGE = 1;

    String imageFilePath;
    File imageUri;
     String imageUrl;
    Integer REQUEST_CAMERA=1, SELECT_FILE=0;
    Bitmap myBitmap;
    String encodedImage;
    static public Bitmap selectedImage;
    String pasW="";
    String email_put="";

    String base64Image ;
    Bitmap mBitmap = null;
    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;


    File mPhotoFile;
    FileCompressor mCompressor;
    String deviceToken;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 101;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        builder.detectFileUriExposure();
        setContentView(R.layout.activity_edit_profile);
        getUserProfileDetails();

        mCompressor = new FileCompressor(this);
        ButterKnife.bind(this);
        imageUrl=DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.PHOTO,null);
        deviceToken=DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.DEVICETOKEN,null);
    }
    @OnClick({R.id.ed_lly_profile_image,R.id.ed_profile_savebutton,R.id.ed_profile_cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ed_lly_profile_image:
                editProfileImage();
                break;
            case R.id.ed_profile_savebutton:
                editProfileSavebutton();
                break;
            case R.id.ed_profile_cancel:
                onBack();
                break;

        }}

    private void sendNotificationToUser(String token) {
        RootModel rootModel = new RootModel(token, new NotificationModel("Title", "Body"), new DataModel("Name", "30"));

        ApiInterface apiService =  ApiClient.getClient2().create(ApiInterface.class);
        retrofit2.Call<ResponseBody> responseBodyCall = apiService.sendNotification(rootModel);

        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                Log.e("Test","Successfully notification send by using retrofit.");
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
                Log.e("Test","Failed");
            }
        });
    }
    private void getUserProfileDetails() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetUserDetailsApi();
        }
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("lastId","");
        mMap.put("perPage","");
        mMap.put("profileUserId", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetUserDetailsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getProfileDetailsRequest(getAuthToken(),mParams()).enqueue(new Callback<GetProfileModel>() {
            @Override
            public void onResponse(Call<GetProfileModel> call, Response<GetProfileModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                GetProfileModel mGetDetailsModel = response.body();
                assert mGetDetailsModel != null;
                if (mGetDetailsModel.getStatus().equals(1)) {
                    //showToast(mActivity, m);
                    Log.d("**PROFILEIMAGE",""+mGetDetailsModel.getData().getPhoto());

                    String  imageurl= mGetDetailsModel.getData().getPhoto();
                        Log.d("error1",""+"error2"+imageurl+mGetDetailsModel);
//

                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.unknown_user)
                            .error(R.drawable.unknown_user)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();
                    if (imageurl!=null){
                        Glide.with(mActivity)
                                .load(imageurl)
                                .apply(options)
                                .into(ed_profilePic);}
                    else {
                        String img=DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.PHOTO,null);
                        Glide.with(mActivity)
                                .load(img)
                                .apply(options)
                                .into(ed_profilePic);

                   // ed_profilePic.setImageURI(Uri.parse(img));
                }
                    if (mGetDetailsModel.getData().getEmail()==null||mGetDetailsModel.getData().getEmail().equals("") ||mGetDetailsModel.getData().getEmail().equals("N/A")
                    )
                    {
                        email_put=mGetDetailsModel.getData().getEmail();
                        ed_profile_email.setVisibility(View.GONE);
                        ed_profile_email_ch.setVisibility(View.VISIBLE);
                    }
                    else{
                        ed_profile_email.setVisibility(View.VISIBLE);
                     email_put=mGetDetailsModel.getData().getEmail();
                        ed_profile_email_ch.setVisibility(View.GONE);
                        ed_profile_email.setText(mGetDetailsModel.getData().getEmail());

                    }

                    ed_profile_name.setText(mGetDetailsModel.getData().getName());

//                    if (mGetDetailsModel.getData().getPassword()!=null) {
//                        ed_profile_password.setText(mGetDetailsModel.getData().getPassword());
//
//                    }
//                    else  if(mGetDetailsModel.getData().getPassword().equals(""))
//                    {
//                        ed_profie_pass_lly.setVisibility(View.GONE);
//                    }
//                    else {
//                       ed_profie_pass_lly.setVisibility(View.GONE);
//                    }
//                    }
                    if (mGetDetailsModel.getData().getDescription()!=null && mGetDetailsModel.getData().getDescription().equals("N/A"))
                    {
                        ed_profile_description.setText("");
                    }
                    else {
                        ed_profile_description.setText(mGetDetailsModel.getData().getDescription());
                    }

                } else if (response.body().getStatus().equals(0)) {
                     showAlertDialog(mActivity, mGetDetailsModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void onBack() {
        finish();
    }
    //for camera
    private void editProfileImage() {

        final CharSequence[] items={"Camera","Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Add Image");

        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (items[i].equals("Camera")) {

                    if(checkAndRequestPermissions(mActivity)){
                        openCamera();
                    }


                } else if (items[i].equals("Gallery")) {
                    if(checkAndRequestPermissions(mActivity)) {

                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        //startActivityForResult(intent.createChooser(intent, "Select File"), SELECT_FILE);
                        startActivityForResult(intent, SELECT_FILE);
                    }
                }
            }
        });
        builder.show();

    }

    public static boolean checkAndRequestPermissions(final Activity context) {
        int ExtstorePermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (ExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                    .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(context, listPermissionsNeeded
                            .toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION,true);
        if (cameraIntent.resolveActivity(mActivity.getPackageManager())!=null)
        {
            File pictureFile = null;
            try {
                pictureFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(mActivity, "Photo can't be created", Toast.LENGTH_SHORT).show();
                return;
            }
            // Continue only if the File was successfully created
            if (pictureFile != null) {

                Uri photoURI = FileProvider.getUriForFile(mActivity,
                        mActivity.getPackageName()+".provider",
                        pictureFile);
                mPhotoFile=pictureFile;
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(cameraIntent,REQUEST_CAMERA );
            }
        }

    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = mActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".png",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        imageFilePath = image.getAbsolutePath();
        return image;
    }




    @Override
    public  void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode,data);

        if(resultCode== Activity.RESULT_OK){

            if(requestCode==REQUEST_CAMERA){

                try {
                    if (mPhotoFile!=null) {
                        Log.e("CompressBitmap",""+mPhotoFile);
                        File mPhotoFile2 = mCompressor.compressToFile(mPhotoFile);
                        Glide.with(mActivity)
                                .load(mPhotoFile2)
                                .apply(new RequestOptions().centerCrop()
                                        .circleCrop()
                                        .placeholder(R.drawable.unknown_user))
                                .into(ed_profilePic);



                        convertUrlToBase64(Uri.fromFile(mPhotoFile2));
                    }
                    else {
                        //  Toast.makeText(mActivity, ""+mPhotoFile, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }



            }else if(requestCode==SELECT_FILE){

                Uri selectedImageUri = data.getData();

                // signup_profilepic.setImageURI(selectedImageUri);
                Glide.with(mActivity).load(selectedImageUri).into(ed_profilePic);
                imageUrl=selectedImageUri.toString();
                //convertUrlToBase64(Uri.parse(imageUrl));
                CropImage.getPickImageResultUri(mActivity,data);
                if (CropImage.isReadExternalStoragePermissionsRequired(mActivity,selectedImageUri)) {
                    // request permissions and handle the result in onRequestPermissionsResult()

                } else {
                    // no permissions required or already grunted, can start crop image activity
                    startCropImageActivity(selectedImageUri);
                }

            }
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    try {
                        Log.e(TAG, "onActivityResult: " + result.getUri());
                        final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        selectedImage.compress(Bitmap.CompressFormat.PNG, 40, out);
                       ed_profilePic.setImageBitmap(selectedImage);
                        base64Image = encodeTobase64(selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    showToast(mActivity, "Cropping failed: " + result.getError());
                }
            }
        }
    }
    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(50, 50)
                .setRequestedSize(120, 120)
                .setMultiTouchEnabled(false)
                .start(mActivity);
    }

    public String convertUrlToBase64(Uri url)  {
        try {


            Log.e(TAG, "base_64_conversion_started" + base64Image);

            final InputStream imageStream = getContentResolver().openInputStream(url);

            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
            //  String path = MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), selectedImage, "Title", null);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            selectedImage.compress(Bitmap.CompressFormat.PNG, 5, baos);

            base64Image= encodeTobase64(selectedImage);
            Log.e(TAG, "base_64_conversion_done" + base64Image);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return base64Image;
    }
    private String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 5, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    private void editProfileEmail() {
    }

    private void editProfilePassword() {
    }

    private void editProfileDescription() {
    }

    private void editProfileSavebutton() {
ed_profile_savebutton.setClickable(false);
            if (isValidate()) {
                if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                ed_profile_savebutton.setClickable(true);
            } else {

               Log.e("Test",""+mParam());
                    String getPas=ed_profile_password.getText().toString();
                    if (getPas.equals("")||(getPas==null)) {
                        saveUserProfileDetailsApi();
                    //
                        }
                    else
                    {
                        if (getPas.length()<6)
                        {
                            showAlertDialog(mActivity,"Password length can't be less than 6");
                        }
                        else if (getPas.length() > 13) {
                            showAlertDialog(mActivity, getString(R.string.password_length_greater));

                        }
                        else {
                            saveUserProfileDetailsApi();
                        }

     ed_profile_savebutton.setClickable(true);
                    }
            }
            }
            else
            {
                ed_profile_savebutton.setClickable(true);
            }
        }

    private boolean isValidate() {
        boolean flag = true;

         if (ed_profile_name.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.string_profile_name));
            flag = false;
        }
         else if (ed_profile_name.getText().toString().length()<=2) {
             showAlertDialog(mActivity, getString(R.string.please_enter_max_chars));
             flag = false;
         }
       else if (ed_profile_email_ch.getText().toString().trim().length()>1&&!(isValidEmaillId(ed_profile_email_ch.getText().toString().trim()))) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        }
        else if (ed_profile_description.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.string_profile_description_length));
            flag = false;
        }else if (ed_profile_description.getText().toString().trim().length() <= 1) {
            showAlertDialog(mActivity, getString(R.string.string_profile_description));
            flag = false;
        }
         else if (ed_profile_description.getText().toString().trim().length() > 500) {
             showAlertDialog(mActivity, getString(R.string.string_profile_description_limit));
             flag = false;
         }

        return flag;

    }

//"user_id, email, photo, , description,password,
 //   country, countryCode,name"
    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        String countryCode = mActivity.getResources().getConfiguration().locale.getCountry();
        String country = mActivity.getResources().getConfiguration().locale.getDisplayCountry();
        Map<String, String> mMap = new HashMap<>();
        if (ed_profile_email.getText().equals("")||ed_profile_email==null)
        {
            mMap.put("email", ed_profile_email_ch.getText().toString().trim());

        }
        else {

            mMap.put("email", ed_profile_email.getText().toString().trim());
        }

        mMap.put("name",ed_profile_name.getText().toString().trim());

        mMap.put("photo", base64Image);
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.ID,null));
        mMap.put("country",country);
        mMap.put("countryCode",countryCode);
        mMap.put("description",ed_profile_description.getText().toString().trim());
        String pas="";
        String getPas=ed_profile_password.getText().toString();
        if (!getPas.equals("")||!(getPas==null)) {
            mMap.put("password", ed_profile_password.getText().toString());
        }
        else {
            mMap.put("password","");
        }
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void saveUserProfileDetailsApi() {

        showProgressDialog(mActivity);
       ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.saveProfileDetailsRequest(getAuthToken(),mParam()).enqueue(new Callback<EditProfileStatusModel>() {
            @Override
            public void onResponse(Call<EditProfileStatusModel> call, Response<EditProfileStatusModel> response) {
                dismissProgressDialog();
                sendNotificationToUser(deviceToken);
                ed_profile_savebutton.setClickable(true);
                Log.e(TAG, "RESPONSE_Edit" + response.body());
                EditProfileStatusModel mModel = response.body();
                if (mModel!=null){
                if (mModel.getStatus().equals(0)) {
                    showAlertDialog(mActivity, mModel.getMessage());
                } else  {
                    showToast(mActivity, mModel.getMessage());
                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.USERID, mModel.getLoginData().getUser_id());
                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.EMAIL, mModel.getLoginData().getEmail());
                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.DEVICETYPE, mModel.getLoginData().getDevice_type());
                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.DEVICETOKEN, mModel.getLoginData().getDevice_token());
                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.PHOTO, response.body().getLoginData().getPhoto());
                   finish();

                }}
                else {}
            }

            @Override
            public void onFailure(Call<EditProfileStatusModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }




}
