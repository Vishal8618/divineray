package com.divineray.app.Profile;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.divineray.app.Home.SelectedUserProfile;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.StringChange.StringFormatter;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.FollowFollowersModel;
import com.divineray.app.model.RemoveModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FollowFollowersadapter extends RecyclerView.Adapter<FollowFollowersadapter.ViewHolder> implements Filterable {
    ArrayList<FollowFollowersModel.DataF> mArrayList1=new ArrayList<>();
    Context context;
    String user_id;
    public Dialog progressDialog;
    TextView text_no_user;
    ArrayList<FollowFollowersModel.DataF> FilteredmArrayList =new ArrayList<>();

    public FollowFollowersadapter(Context context, ArrayList<FollowFollowersModel.DataF> mArrayList1, TextView text_no_user) {
        this.context=context;
        this.mArrayList1=mArrayList1;
        this.FilteredmArrayList=mArrayList1;
        this.text_no_user=text_no_user;
    }

    @NonNull
    @Override
    public FollowFollowersadapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.follow_unfollow_item, parent, false);
        return new FollowFollowersadapter.ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull FollowFollowersadapter.ViewHolder holder, int position) {
        FollowFollowersModel.DataF mModel = mArrayList1.get(position);

            Glide.with(context).
                    load(mModel.getPhoto())
                    .placeholder(context.getResources().getDrawable(R.drawable.unknown_user))
                    .into(holder.photo);
        if (!(mModel.getName().length()==0)) {
            holder.user_name.setText(StringFormatter.capitalizeWord(mModel.getName()));
        }
        else {
            holder.user_name.setText("Unknown");
        }
            user_id = mModel.getUser_id();
            if (mModel.getFollow().equals("0") && mModel.getUser_id().equals(
                    DivineRayPrefernces.readString(context, DivineRayPrefernces.USERID, null))) {
                holder.rly_follow_unfollowi_button.setVisibility(View.GONE);

            } else if (mModel.getFollow().equals("1") && mModel.getUser_id().equals(
                    DivineRayPrefernces.readString(context, DivineRayPrefernces.USERID, null))) {
                holder.rly_follow_unfollowi_button.setVisibility(View.GONE);

            } else if (mModel.getFollow().equals("1") && !mModel.getUser_id().equals(
                    DivineRayPrefernces.readString(context, DivineRayPrefernces.USERID, null))) {
                holder.rly_follow_unfollowi_button.setVisibility(View.VISIBLE);
                holder.follow_unfollowi_button_following.setVisibility(View.VISIBLE);
                holder.follow_unfollowi_button_follow.setVisibility(View.GONE);
                //holder.rly_follow_unfollowi_button.setVisibility(View.GONE);
            } else if (mModel.getFollow().equals("0") && mModel.getUser_id().equals(
                    DivineRayPrefernces.readString(context, DivineRayPrefernces.USERID, null))) {
                holder.rly_follow_unfollowi_button.setVisibility(View.VISIBLE);
                holder.follow_unfollowi_button_following.setVisibility(View.GONE);
                holder.follow_unfollowi_button_follow.setVisibility(View.VISIBLE);
            }


            holder.user_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, SelectedUserProfile.class);
                    intent.putExtra("profileUserId", mModel.getUser_id());
                    intent.putExtra("name", mModel.getName());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        holder.photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SelectedUserProfile.class);
                intent.putExtra("profileUserId", mModel.getUser_id());
                intent.putExtra("name", mModel.getName());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

            holder.follow_unfollowi_button_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String authtoken= DivineRayPrefernces.readString(context,DivineRayPrefernces.AUTHTOKEN,null);

                    //  Toast.makeText(context, "Follow"+mModel.getName()+""+mModel.getUser_id(), Toast.LENGTH_SHORT).show();
                 //   showProgressDialog(context);
                    ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
                    user_id = mModel.getUser_id();
                    mApiInterface1.followUnfollow(authtoken,mParams2()).enqueue(new Callback<RemoveModel>() {
                        @Override
                        public void onResponse(Call<RemoveModel> call, Response<RemoveModel> response) {
                         //   dismissProgressDialog();
                            Log.e("", "**RESPONSE**" + response.body());
                            RemoveModel mGetDetailsModel = response.body();

                            if (mGetDetailsModel.getStatus() == 1) {
                                // Toast.makeText(mActivity, ""+mGetDetailsModel.getMessage(), Toast.LENGTH_SHORT).show();
                                // Toast.makeText(context, "User Followed ", Toast.LENGTH_SHORT).show();
                                holder.follow_unfollowi_button_following.setVisibility(View.VISIBLE);
                                holder.follow_unfollowi_button_follow.setVisibility(View.GONE);

                            } else {
                                holder.follow_unfollowi_button_following.setVisibility(View.VISIBLE);
                                holder.follow_unfollowi_button_follow.setVisibility(View.GONE);
                                //  Toast.makeText(context, "Already Followed", Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<RemoveModel> call, Throwable t) {
                            dismissProgressDialog();
                            Log.e("", "**ERROR**" + t.getMessage());
                        }
                    });
                }
            });
        holder.follow_unfollowi_button_following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Toast.makeText(context, "Follow"+mModel.getName()+""+mModel.getUser_id(), Toast.LENGTH_SHORT).show();
               // showProgressDialog(context);
                ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
                user_id = mModel.getUser_id();
                String authtoken= DivineRayPrefernces.readString(context,DivineRayPrefernces.AUTHTOKEN,null);

                mApiInterface1.followUnfollow(authtoken,mParams3()).enqueue(new Callback<RemoveModel>() {
                    @Override
                    public void onResponse(Call<RemoveModel> call, Response<RemoveModel> response) {
                     //   dismissProgressDialog();
                        Log.e("", "**RESPONSE**" + response.body());
                        RemoveModel mGetDetailsModel = response.body();

                        if (mGetDetailsModel.getStatus() == 1) {
                            // Toast.makeText(mActivity, ""+mGetDetailsModel.getMessage(), Toast.LENGTH_SHORT).show();
                            // Toast.makeText(context, "User Followed ", Toast.LENGTH_SHORT).show();
                            holder.follow_unfollowi_button_following.setVisibility(View.GONE);
                            holder.follow_unfollowi_button_follow.setVisibility(View.VISIBLE);

                        } else {
                            holder.follow_unfollowi_button_following.setVisibility(View.GONE);
                            holder.follow_unfollowi_button_follow.setVisibility(View.VISIBLE);
                            //  Toast.makeText(context, "Already Followed", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<RemoveModel> call, Throwable t) {
                        dismissProgressDialog();
                        Log.e("", "**ERROR**" + t.getMessage());
                    }
                });
            }
        });



    }

    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id",DivineRayPrefernces.readString(context,DivineRayPrefernces.USERID,null));
        mMap.put("follow_id", user_id);
        mMap.put("followType", "1");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private Map<String, String> mParams3() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id",DivineRayPrefernces.readString(context,DivineRayPrefernces.USERID,null));
        mMap.put("follow_id", user_id);
        mMap.put("followType", "0");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog(Context context) {
        progressDialog = new Dialog(context);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();


    }

    @Override
    public int getItemCount() {
        return mArrayList1.size();
    }

    //filter method
    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mArrayList1=FilteredmArrayList;

                } else {
                    ArrayList<FollowFollowersModel.DataF> filteredList = new ArrayList<>();
                    for (FollowFollowersModel.DataF row : FilteredmArrayList) {


                        //change this to filter according to your case
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    mArrayList1 = (ArrayList<FollowFollowersModel.DataF>) filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values =mArrayList1;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mArrayList1 = (ArrayList) filterResults.values;
                if (mArrayList1.size()>0){
                    text_no_user.setVisibility(View.GONE);
                }
                else {

                    text_no_user.setVisibility(View.VISIBLE);
                    text_no_user.setText("No User found "+"'"+charSequence.toString()+"'");
                }
                notifyDataSetChanged();

            }
        };

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView photo;
        TextView user_name,user_id;
        TextView follow_unfollowi_button_follow,follow_unfollowi_button_following;
        RelativeLayout rly_follow_unfollowi_button;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            photo=itemView.findViewById(R.id.follow_unfollowi_photo);
            user_name=itemView.findViewById(R.id.follow_unfollowi_name);
            user_id=itemView.findViewById(R.id.follow_unfollowi_user_id);
            follow_unfollowi_button_follow=itemView.findViewById(R.id.follow_unfollowi_button_follow);
            follow_unfollowi_button_following=itemView.findViewById(R.id.follow_unfollowi_button_following);
            rly_follow_unfollowi_button=itemView.findViewById(R.id.follow_unfollowi_button);
        }
    }
}
