package com.divineray.app.Profile;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.FollowFollowersModel;
import com.divineray.app.model.RemoveModel;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonDetails extends BaseActivity {
    /*
      Getting the Current Class Name
     */
    String TAG = PersonDetails.this.getClass().getSimpleName();

    /*
      Current Activity Instance
     */
    Activity mActivity = PersonDetails.this;

    /*
      Widgets
      */

    @BindView(R.id.recycler_follow_unfollow)
    RecyclerView recyclerView;
    @BindView(R.id.follow_unfollow_backbutton)
    LinearLayout lly_backButton;
    @BindView(R.id.follow_unfollow_heading)
    TextView tx_heading;
    @BindView(R.id.follow_unfollow_tablayout)
    TabLayout tablayout;
    @BindView(R.id.person_detail_search)
    EditText ed_searchBar;
    @BindView(R.id.person_detail_search_f)
    EditText searchBar2;
    @BindView(R.id.swiperefresh_follow)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.text_no_user)
    TextView text_no_user;
    String profileUserId;
    String layoutType;
    String tabSwitchString;


    List<FollowFollowersModel.DataF> mArrayList1 = new ArrayList<>();
    List<FollowFollowersModel.DataF> mArrayList12 = new ArrayList<>();
    private FollowFollowersadapter followFollowersadapter;
    FollowFollowingsAdapter followFollowingsAdapter;
    private String  user_id="";

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_person_details);
        ButterKnife.bind(this);
        Intent intent=getIntent();

        String head_name=intent.getStringExtra("head_name");
        String followers=intent.getStringExtra("followers");
        String followings=intent.getStringExtra("followings");
        layoutType=intent.getStringExtra("layoutType");
         profileUserId=intent.getStringExtra("profileUserId");


        tabSwitchString=layoutType;
            if (layoutType.equals("followers")) {
                tx_heading.setText("Followers");
              //  executeFollowersApi();
                tabSwitchString = "followers";
                swiperefresh.setColorSchemeResources(R.color.colorBlack);
                swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {

                        executeFollowersApi();
                    }
                });


            } else if (layoutType.equals("followings")) {
                tx_heading.setText("Following");
                tabSwitchString = "followings";
            //    executeFollowingApi();
                ed_searchBar.setVisibility(View.GONE);
                searchBar2.setVisibility(View.VISIBLE);
                swiperefresh.setColorSchemeResources(R.color.colorBlack);
                swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        executeFollowingApi();
                    }
                });


        }
        getData();

        lly_backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        if (layoutType.equals("followings"))
//        {
//
//            tablayout.addTab(tablayout.newTab().setText("Followings"+"("+followings+")"));
//            tablayout.addTab(tablayout.newTab().setText("Followers"+"("+followers+")"));
//            tablayout.setTabGravity(TabLayout.GRAVITY_FILL);
//            tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//                @Override
//                public void onTabSelected(TabLayout.Tab tab) {
//                    switch (tab.getPosition()) {
//                        case 0:
//                            tabSwitchString="followings";
////                        codes related to the first tab
//                            ed_searchBar.setVisibility(View.GONE);
//                            searchBar2.setVisibility(View.VISIBLE);
//                            tabAllFollowings();
//                            swiperefresh.setColorSchemeResources(R.color.colorBlack);
//                            swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//                                @Override
//                                public void onRefresh() {
//                                   tabAllFollowings();
//                                }
//                            });
//
//                            break;
//                        case 1:
////                        codes related to the second tab
//                        tabSwitchString="followers";
//
//
//                        tabAllFollowers();
//                        ed_searchBar.setVisibility(View.VISIBLE);
//                        searchBar2.setVisibility(View.GONE);
//
//                            swiperefresh.setColorSchemeResources(R.color.colorBlack);
//                            swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//                                @Override
//                                public void onRefresh() {
//                                    tabAllFollowers();
//                                }
//                            });
//
//                            break;
//                    }
//                }
//
//                @Override
//                public void onTabUnselected(TabLayout.Tab tab) {
//
//                }
//
//                @Override
//                public void onTabReselected(TabLayout.Tab tab) {
//
//                }
//            });
//        }
//        else if (layoutType.equals("followers"))
//        {
//            tablayout.addTab(tablayout.newTab().setText("Followers"+"("+followers+")"));
//            tablayout.addTab(tablayout.newTab().setText("Followings"+"("+followings+")"));
//            tablayout.setTabGravity(TabLayout.GRAVITY_FILL);
//            tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//                @Override
//                public void onTabSelected(TabLayout.Tab tab) {
//                    switch (tab.getPosition()) {
//                        case 0:
//                            tabSwitchString="followers";
////                        codes related to the first tab
//
//                            tabAllFollowers();
//                            ed_searchBar.setVisibility(View.VISIBLE);
//                            searchBar2.setVisibility(View.GONE);
//                            break;
//                        case 1:
////                        codes related to the second tab
//                            tabSwitchString="followings";
//                            ed_searchBar.setVisibility(View.GONE);
//                            searchBar2.setVisibility(View.VISIBLE);
//                            tabAllFollowings();
//                            break;
//
//                    }
//                }
//
//                @Override
//                public void onTabUnselected(TabLayout.Tab tab) {
//
//                }
//
//                @Override
//                public void onTabReselected(TabLayout.Tab tab) {
//
//                }
//            });
//        }
//        else
//        {
//            tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//                @Override
//                public void onTabSelected(TabLayout.Tab tab) {
//                    switch (tab.getPosition()) {
//                        case 0:
////                        codes related to the first tab
//
//                            tabAllFollowers();
//                            ed_searchBar.setVisibility(View.VISIBLE);
//                            searchBar2.setVisibility(View.GONE);
//                            break;
//                        case 1:
////                        codes related to the second tab
//                            ed_searchBar.setVisibility(View.GONE);
//                            searchBar2.setVisibility(View.VISIBLE);
//                            tabAllFollowings();
//                            break;
//
//                    }
//                }
//
//                @Override
//                public void onTabUnselected(TabLayout.Tab tab) {
//
//                }
//
//                @Override
//                public void onTabReselected(TabLayout.Tab tab) {
//
//                }
//            });
//        }

        ed_searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String string=s.toString();


                try {
                       followFollowersadapter.getFilter().filter(string);

                } catch (Exception exception)
                {
                    exception.printStackTrace();
                }
               // followFollowingsAdapter.getFilter().filter(string);
            }
        });

        ed_searchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    closeKeyboard();
                    return true;
                }
                return false;
            }
        });
        searchBar2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    closeKeyboard();
                    return true;
                }
                return false;
            }
        });

        searchBar2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String string=s.toString();
                    try {
                        followFollowingsAdapter.getFilter().filter(string);

                    } catch (Exception exception)
                    {
                        exception.printStackTrace();
                    }



            }
        });
        searchBar2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    closeKeyboard();
                    return true;
                }
                return false;
            }
        });

    }

    private void getData() {
        swiperefresh.setRefreshing(true);
                if (searchBar2.getText().toString().equals("")&&ed_searchBar.getText().toString().equals(""))
        {

            if (tabSwitchString.equals("followers"))
            {
                tabAllFollowers();
            }
            else if (tabSwitchString.equals("followings")){
                tabAllFollowings();
            }
        }
    }

    private void closeKeyboard()
    {
        // this will give us the view
        // which is currently focus
        // in this layout
        View view = mActivity.getCurrentFocus();

        // if nothing is currently
        // focus then this will protect
        // the app from crash
        if (view != null) {

            // now assign the system
            // service to InputMethodManager
            InputMethodManager manager
                    = (InputMethodManager)
                    mActivity.getSystemService(
                            Context.INPUT_METHOD_SERVICE);
            manager
                    .hideSoftInputFromWindow(
                            view.getWindowToken(), 0);
        }
    }


    private void tabAllFollowings() {
        if (!isNetworkAvailable(mActivity)) {
            Toast.makeText(mActivity, "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
            executeFollowingApi();
        }
    }
    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("followType","2");
        mMap.put("perPage","100");
        mMap.put("profileUserId", profileUserId);
        mMap.put("lastUserId","0");
        mMap.put("search","");
        Log.e("**PARAM**", "**PARAM**" + mMap.toString()+":::"+getAuthToken());
        return mMap;
    }

    private void executeFollowingApi() {
       // showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getFollowUsers(getAuthToken(),mParams2()).enqueue(new Callback<FollowFollowersModel>() {
            @Override
            public void onResponse(Call<FollowFollowersModel> call, Response<FollowFollowersModel> response) {
                swiperefresh.setRefreshing(false);
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                FollowFollowersModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus()==1) {
                    mArrayList12 =response.body().getData();
                    if (mArrayList12!=null){
                        setFollowingAdapter();
                    }

                   // Toast.makeText(mActivity, "!:"+response.body().toString(), Toast.LENGTH_SHORT).show();

                } else {
                   //  Toast.makeText(mActivity, "0"+response.body().toString(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<FollowFollowersModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
              //  Toast.makeText(mActivity, "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setFollowingAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
       followFollowingsAdapter = new FollowFollowingsAdapter(mActivity, (ArrayList<FollowFollowersModel.DataF>) mArrayList12, text_no_user, new FollowFollowingsAdapter.OnItemClickListener() {
           @Override
           public void onItemClick(int positon, FollowFollowersModel.DataF item, View view) {
               if (view.getId() == R.id.follow_unfollowi_button_following) {
                   ShowfollowList(item);
               }
           }
       });
        recyclerView.setAdapter(followFollowingsAdapter);
    }
    private Map<String, String> mParams3() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id",DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("follow_id", user_id);
        mMap.put("followType", "0");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void ShowfollowList(FollowFollowersModel.DataF mModel) {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        user_id = mModel.getUser_id();
        mApiInterface1.followUnfollow(getAuthToken(),mParams3()).enqueue(new Callback<RemoveModel>() {
            @Override
            public void onResponse(Call<RemoveModel> call, Response<RemoveModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                RemoveModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {
                   Log.e("T","Success");

                   executeFollowingApi();
                    followFollowingsAdapter.notifyDataSetChanged();

                } else {

                    //  Toast.makeText(context, "Already Followed", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<RemoveModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void tabAllFollowers() {
        if (!isNetworkAvailable(mActivity)) {
            Toast.makeText(mActivity, "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
            executeFollowersApi();
        }
    }

    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("followType","3");
        mMap.put("perPage","100");
        mMap.put("profileUserId", profileUserId);
        mMap.put("lastUserId","0");
        mMap.put("search","");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeFollowersApi() {
       // showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getFollowUsers(getAuthToken(),mParams()).enqueue(new Callback<FollowFollowersModel>() {
            @Override
            public void onResponse(Call<FollowFollowersModel> call, Response<FollowFollowersModel> response) {
                swiperefresh.setRefreshing(false);
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                FollowFollowersModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus()==1) {
                 //   Toast.makeText(mActivity, "1 :"+response.body(), Toast.LENGTH_SHORT).show();
                    mArrayList1 =response.body().getData();
                    if (mArrayList1!=null){
                        setFollowersAdapter();
                    }

                } else {
                 //  Toast.makeText(mActivity, "0:"+response.body(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<FollowFollowersModel> call, Throwable t) {
                dismissProgressDialog();
                //Toast.makeText(mActivity, "Failure", Toast.LENGTH_SHORT).show();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });

    }

    private void setFollowersAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        followFollowersadapter = new FollowFollowersadapter(mActivity, (ArrayList<FollowFollowersModel.DataF>) mArrayList1,text_no_user);
        recyclerView.setAdapter(followFollowersadapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (searchBar2.getText().toString().equals("")&&ed_searchBar.getText().toString().equals(""))
//        {
//
//            if (tabSwitchString.equals("followers"))
//            {
//                tabAllFollowers();
//            }
//            else if (tabSwitchString.equals("followings")){
//                tabAllFollowings();
//            }
//        }
    }
}