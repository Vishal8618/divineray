package com.divineray.app.Profile;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.divineray.app.Notifications.GetAllNotifications;
import com.divineray.app.SelecteUserMore.AppointmentBooking;
import com.divineray.app.SelecteUserMore.GiftItem;
import com.divineray.app.SelecteUserMore.ProductPurchase;
import com.divineray.app.SelecteUserMore.ViddeoAudio;
import com.divineray.app.Settings.SettingActivity;
import com.divineray.app.Utils.ExpandableHeightGridView;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseFragment;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.DeepLinkModel;
import com.divineray.app.model.FavModel;
import com.divineray.app.model.GetProfileModel;
import com.divineray.app.model.ProfileGetVideoModel;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;


public class ProfileFragment extends BaseFragment {
    public static Context context;


    public Dialog progressDialog;
    TextView im_share;
    ImageView im_setttings;
   CircleImageView profile_image;
    TextView tx_name,tx_following,tx_followers,tx_description;
    ProgressBar p_bar;
    SwipeRefreshLayout swiperefresh;

    ExpandableHeightGridView simpleGrid;
    RelativeLayout rly_grid;
    static  public  TabLayout tabLayout;
    TabItem tab_all,tab_fav,tab_services;
    String currentPage="tab_all";
    ImageView imNotification;
    LinearLayout lly_follow_followers,lly_followings,lly_folllowers;

    List<ProfileGetVideoModel.Data> mQuotesArrayList = new ArrayList<>();
    List<FavModel.FavVideos> mQuotesArrayList2 = new ArrayList<>();
    LinearLayout lly_noposts,lly_moretab;

    @BindView(R.id.tx_appointment)
    TextView tx_appointment;
    @BindView(R.id.tx_audiovideo)
    TextView tx_audiovideo;
    @BindView(R.id.tx_productpurchase)
    TextView tx_productpurchase;
    @BindView(R.id.tx_giftitem)
    TextView tx_giftitem;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.profile_fragment, container, false);
        simpleGrid = view.findViewById(R.id.gridview);
        getUserProfileDetails();
        context = getApplicationContext();
        im_setttings=view.findViewById(R.id.profile_settings);
        tx_name=view.findViewById(R.id.profilef_name);
        tabLayout=view.findViewById(R.id.tabs_profilefrag);
        tx_description=view.findViewById(R.id.profilef_description);
        tx_followers=view.findViewById(R.id.profilef_followers);
        tx_following=view.findViewById(R.id.profilef_following);
        profile_image=view.findViewById(R.id.profilef_image);
        tab_all=view.findViewById(R.id.Tabitem_all);
        im_share=view.findViewById(R.id.sprofile_share);
        tab_fav=view.findViewById(R.id.Tabitem_favs);
        tab_services=view.findViewById(R.id.Tabitem_services);
        lly_follow_followers=view.findViewById(R.id.lly_follow_following);
        lly_folllowers=view.findViewById(R.id.lly_followers);
        lly_followings=view.findViewById(R.id.lly_followings);
        lly_noposts=view.findViewById(R.id.lly_noposts);
        rly_grid=view.findViewById(R.id.rely_gridy);
        lly_moretab=view.findViewById(R.id.lly_selected_user_Moretab);
        imNotification=view.findViewById(R.id.profile_notification);
        ButterKnife.bind(this,view);
        //lly_main=view.findViewById(R.id.lly_profile_main);
      //  lly_main.setMinimumHeight(1000);
        simpleGrid.setFastScrollEnabled(true);
       simpleGrid.setExpanded(true);
      rly_grid.setMinimumHeight(1000);
       tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        lly_moretab.setVisibility(View.GONE);
                        tabAllApi();
                        mQuotesArrayList2.clear();
                        currentPage="tab_all";
                        showProgressDialog(getActivity());
                        break;
                    case 1:
                        lly_moretab.setVisibility(View.GONE);
                        tabFavApi();
                        currentPage="tab_fav";
                        break;
                    case  2:
                        simpleGrid.setVisibility(View.GONE);
                        lly_noposts.setVisibility(View.GONE);
                        lly_moretab.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                tabServices();
                            }
                        },1000);

                        currentPage="tab_services";
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            //noinspection ConstantConditions
            TextView tv = (TextView)LayoutInflater.from(getContext()).inflate(R.layout.custom_tab_layout,null);
           // tv.setTypeface(Typeface);
            tabLayout.getTabAt(i).setCustomView(tv);
        }

       lly_followings.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               Intent intent=new Intent(getContext(), PersonDetails.class);
               intent.putExtra("head_name",tx_name.getText().toString().trim());
               intent.putExtra("followers",tx_followers.getText().toString().trim());
               intent.putExtra("followings",tx_following.getText().toString().trim());
               intent.putExtra("layoutType","followings");
               intent.putExtra("profileUserId",DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.USERID,null));
               startActivity(intent);
           }
       });

        lly_follow_followers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(getContext(), PersonDetails.class);
                intent.putExtra("head_name",tx_name.getText().toString().trim());
                intent.putExtra("followers",tx_followers.getText().toString().trim());
                intent.putExtra("followings",tx_following.getText().toString().trim());
                intent.putExtra("layoutType","followers");
                intent.putExtra("profileUserId",DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.USERID,null));
                startActivity(intent);
            }
        });






        im_setttings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                im_setttings.setClickable(false);
                Intent intent = new Intent(getContext(), SettingActivity.class);
                startActivity(intent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        im_setttings.setClickable(true);
                    }
                },1500);

            }
        });


        imNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imNotification.setClickable(false);
                Intent intent = new Intent(getContext(), GetAllNotifications.class);
                startActivity(intent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        imNotification.setClickable(true);
                    }
                },1500);

            }
        });
        im_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                im_share.setClickable(false);
                performDeeplink();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        im_share.setClickable(true);
                    }
                },1500);



            }
        });


        tx_audiovideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tx_audiovideo.setClickable(false);
                Intent intent1=new Intent(context, ViddeoAudio.class);
                intent1.putExtra("other_user_id",DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.USERID,null));
                intent1.putExtra("username",tx_name.getText().toString());
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        tx_audiovideo.setClickable(true);
                    }
                },1500);

            }
        });
        tx_productpurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tx_productpurchase.setClickable(false);
                Intent intent1=new Intent(context, ProductPurchase.class);
                intent1.putExtra("other_user_id",DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.USERID,null));
                intent1.putExtra("username",tx_name.getText().toString());

                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        tx_productpurchase.setClickable(true);
                    }
                },1500);


            }
        });
        tx_giftitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tx_giftitem.setClickable(false);
                Intent intent1=new Intent(context, GiftItem.class);
                intent1.putExtra("other_user_id",DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.USERID,null));
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        tx_giftitem.setClickable(true);
                    }
                },1500);




            }
        });
        tx_appointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {


                tx_appointment.setClickable(false);
                Intent intent1=new Intent(context, AppointmentBooking.class);
                intent1.putExtra("other_user_id",DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.USERID,null));
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        tx_appointment.setClickable(true);
                    }
                },1500);


            }
        });


        return  view;
    }

    private void tabServices() {

        simpleGrid.setVisibility(View.GONE);
        lly_noposts.setVisibility(View.GONE);
    }


    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
private void getUserProfileDetails() {
        if (!isNetworkAvailable(getActivity())) {
            Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
        executeGetUserDetailsApi();
        }
        }
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();


    }

    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
/*
 * Execute api
 * */
private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.USERID,null));
        mMap.put("lastId","");
        mMap.put("perPage","100");
        mMap.put("profileUserId", DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.USERID,null));
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
        }

private void executeGetUserDetailsApi() {
 //  showProgressDialog(getActivity());
    String authtoken= DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.AUTHTOKEN,null);

    ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
    mApiInterface.getProfileDetailsRequest(authtoken,mParams()).enqueue(new Callback<GetProfileModel>() {
        @Override
        public void onResponse(Call<GetProfileModel> call, Response<GetProfileModel> response) {
            dismissProgressDialog();
            Log.e("", "**RESPONSE**" + response.body());
            GetProfileModel mGetDetailsModel = response.body();
            //assert mGetDetailsModel != null;
            if (mGetDetailsModel.getStatus().equals(1)) {
                //showToast(mActivity, m);
                Log.d("**PROFILEIMAGE", "" + mGetDetailsModel.getData().getPhoto());

                String imageurl = mGetDetailsModel.getData().getPhoto();
                Log.d("error1", "" + "error2" + imageurl + mGetDetailsModel);
//

                RequestOptions options = new RequestOptions()
                        .placeholder(R.drawable.unknown_user)
                        .error(R.drawable.unknown_user)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform();
                if (imageurl != null&&getContext()!=null) {
                    try {
                        Glide.with(Objects.requireNonNull(getContext()))
                                .load(imageurl)
                                .apply(options)
                                .into(profile_image);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                } else if (mGetDetailsModel.getStatus()==0){
//                    String img = DivineRayPrefernces.readString(getContext(), DivineRayPrefernces.PHOTO, null);
//                    Glide.with(getContext())
//                            .load(img)
//                            .apply(options)
//                            .into(profile_image);

                }
                else
                {
                    showDialogLogOut(getActivity());
                }

                if (mGetDetailsModel.getData().getName()!=null||mGetDetailsModel.getData().getName().equals("")) {
                    tx_name.setText(mGetDetailsModel.getData().getName());
                }
                else {
                    tx_name.setText("Unknown");
                }
               // tx_name.setText(mGetDetailsModel.getData().getName());
                tx_followers.setText(mGetDetailsModel.getData().getTotalFollowers());
                tx_following.setText(mGetDetailsModel.getData().getTotalFollowing());

                String description="Description: ";
         SpannableString ss=new SpannableString(description+mGetDetailsModel.getData().getDescription());
           StyleSpan boldSpan=new StyleSpan(Typeface.BOLD);

           ss.setSpan(boldSpan,0,12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

           if (!mGetDetailsModel.getData().getDescription().equals("N/A"))
           {
               tx_description.setText(mGetDetailsModel.getData().getDescription());
           }
           else
           {
               tx_description.setText("");
           }





            } else if (response.body().getStatus().equals(0)) {
               // Toast.makeText(getActivity(), "" + mGetDetailsModel.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(Call<GetProfileModel> call, Throwable t) {
            dismissProgressDialog();
            Log.e("", "**ERROR**" + t.getMessage());
        }
    });
   // tabAllApi();

}
    private Map<String, String> mParams3() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("profileUserId",DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.USERID,null));
        mMap.put("type", "1");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void performDeeplink() {
        String authtoken= DivineRayPrefernces.readString(context,DivineRayPrefernces.AUTHTOKEN,null);

        showProgressDialog(getActivity());
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getDeepLInk(authtoken,mParams3()).enqueue(new Callback<DeepLinkModel>() {
            @Override
            public void onResponse(Call<DeepLinkModel> call, Response<DeepLinkModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                DeepLinkModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {

                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                    shareIntent.putExtra(Intent.EXTRA_TEXT, mGetDetailsModel.getShareUrl());
                    startActivity(Intent.createChooser(shareIntent, getString(R.string.app_name)));

                } else {
                    Toast.makeText(getContext(), mGetDetailsModel.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<DeepLinkModel> call, Throwable t) {
                dismissProgressDialog();

                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        if (currentPage.equals("tab_all"))
        {
            lly_moretab.setVisibility(View.GONE);
          tabAllApi();
        }
        else if (currentPage.equals("tab_fav"))
        {
            lly_moretab.setVisibility(View.GONE);
           tabFavApi();
        }
        else if (currentPage.equals("tab_services"))
        {

            tabServices();
        }

        executeGetUserDetailsApi();

    }

    public  void tabAllApi(){
        String authtoken= DivineRayPrefernces.readString(context,DivineRayPrefernces.AUTHTOKEN,null);

    ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
    mApiInterface1.getProfileVideos(authtoken,mParams()).enqueue(new Callback<ProfileGetVideoModel>() {
        @Override
        public void onResponse(Call<ProfileGetVideoModel> call, Response<ProfileGetVideoModel> response) {
            dismissProgressDialog();

            Log.e("", "**RESPONSE**" + response.body());
            ProfileGetVideoModel mGetDetailsModel = response.body();

            if (mGetDetailsModel.getStatus() == 1) {
                simpleGrid.setVisibility(View.VISIBLE);
                lly_noposts.setVisibility(View.GONE);
                mQuotesArrayList =response.body().getData();
                if (mQuotesArrayList!=null){
                    setProfileAdapter();
                }

            } else {
                lly_noposts.setVisibility(View.VISIBLE);
                simpleGrid.setVisibility(View.GONE);
             //   Toast.makeText(getActivity(), "Error Getting daa", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public void onFailure(Call<ProfileGetVideoModel> call, Throwable t) {
            dismissProgressDialog();
            Log.e("", "**ERROR**" + t.getMessage());
        }
    });

}

    public  void tabFavApi(){
        String authtoken= DivineRayPrefernces.readString(context,DivineRayPrefernces.AUTHTOKEN,null);

        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getFavVideos(authtoken,mParams()).enqueue(new Callback<FavModel>() {
            @Override
            public void onResponse(Call<FavModel> call, Response<FavModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                FavModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {
                    simpleGrid.setVisibility(View.VISIBLE);
                    lly_noposts.setVisibility(View.GONE);
                    mQuotesArrayList2 =response.body().getFavVideos();
                    if (mQuotesArrayList2!=null){
                        setProfileAdapter2();
                    }

                } else {
                    simpleGrid.setVisibility(View.GONE);
                    lly_noposts.setVisibility(View.VISIBLE);

                }

            }

            @Override
            public void onFailure(Call<FavModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });

    }

    private void setProfileAdapter() {
        CustomAdapter customAdapter = new CustomAdapter(getActivity(), (ArrayList<ProfileGetVideoModel.Data>) mQuotesArrayList);
        simpleGrid.setAdapter(customAdapter);


    }
    private void setProfileAdapter2() {
    String profileUserId=DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.USERID,null);


        CustomAdapterFav customAdapter = new CustomAdapterFav(getActivity(), (ArrayList<FavModel.FavVideos>) mQuotesArrayList2,profileUserId);
        simpleGrid.setAdapter(customAdapter);
    }



}