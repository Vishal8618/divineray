package com.divineray.app.Profile;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.divineray.app.Home.BottomSheetDialogH;
import com.divineray.app.Home.DownloadUtil;
import com.divineray.app.Home.Fragment_Data_Send;
import com.divineray.app.Home.HomeFragment;
import com.divineray.app.Home.SelectedUserOwnProfile;
import com.divineray.app.Home.SelectedUserProfile;
import com.divineray.app.R;
import com.divineray.app.Report.ReportVideo;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Search.HashtagItemView;
import com.divineray.app.Utils.Constants;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.Utils.Functions;
import com.divineray.app.Utils.ShareUtils;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.HomeModel;
import com.divineray.app.model.RemoveModel;
import com.divineray.app.model.SearchModelget;
import com.divineray.app.model.SearchModeltagged;
import com.divineray.app.model.VideofavModel;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.downloader.request.DownloadRequest;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class VideoView2 extends BaseActivity implements Player.EventListener , Fragment_Data_Send {
    /**
     * Getting the Current Class Name
     */
    String TAG = VideoView2.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = VideoView2.this;
    public  static int newcount=0;


    ArrayList<SearchModeltagged.Data> data_list;
    ArrayList<SearchModelget.Datan> data_list2;
    int currentPage = -1;
    LinearLayoutManager layoutManager;
    VideoAdapter2 adapter;
    boolean is_user_stop_video = false;
    String videoID;
    int lastPosition=0;
    Dialog progressDialog;
    String tag,profileUserId,user_newProfileid;
    SearchModeltagged mGetDetailsModel;

    int viewVideoId;

    @BindView(R.id.p_bar_vidView2)
    ProgressBar p_bar;
    @BindView(R.id.recylerview_vidView2)
    RecyclerView recyclerView;
    @BindView(R.id.swiperefresh_vidView2)
    SwipeRefreshLayout swiperefresh;
    String musicId;
    String Music__id;
    String tag__Name;
    int video_count=0;


//    public VideoView2(int comment_counnt, SearchF_view_count searchF_view_count) {
//        this.video_count=comment_counnt;
//        this.searchF_view_count=searchF_view_count;
//    }
    String position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_video_view2);
        ButterKnife.bind(this);
        Intent intent=getIntent();
        viewVideoId= Integer.parseInt(intent.getStringExtra("videoId"));
        tag=intent.getStringExtra("tagName");
        musicId=intent.getStringExtra("musicId");
        position=intent.getStringExtra("position");
        if (tag.equals(""))
        {
            Music__id="2";
            tag__Name="";


        }
        else
        {
            Music__id="0";
            tag__Name=tag;


        }

        layoutManager=new LinearLayoutManager(mActivity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);
        // tag=intent.getStringExtra("tagName");
        SnapHelper snapHelper =  new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);



        // this is the scroll listener of recycler view which will tell the current item number
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);


            }
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //here we find the current item number
                final int scrollOffset = recyclerView.computeVerticalScrollOffset();
                final int height = recyclerView.getHeight();
                int page_no=scrollOffset / height;

                if(page_no!=currentPage ){
                    currentPage=page_no;

                    Release_Privious_Player();
                    Set_Player(currentPage);

                }
            }
        });

        swiperefresh.setProgressViewOffset(false, 0, 200);

        swiperefresh.setColorSchemeResources(R.color.black);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage=-1;
                getData();
            }
        });
        getData();
    }


    // when we swipe for another video this will relaese the privious player
    SimpleExoPlayer privious_player;
    public void Release_Privious_Player(){
        if(privious_player!=null) {
            privious_player.removeListener(this);
            privious_player.release();
        }
    }

    // this will call when swipe for another video and
    // this function will set the player to the current video
    public void Set_Player(final int currentPage){

        final SearchModeltagged.Data item= data_list.get(currentPage);

        if (data_list.size()>2) {
            // Top
            if (lastPosition > currentPage) {

                if (currentPage > 2) {
                    Log.e("Ted", "1st if");
                    performN(currentPage);

                }

            } else if (lastPosition < currentPage) {//Bottom

                if (currentPage < data_list.size() - 2) {
                    Log.e("Ted", "2nd if");
                    perform(currentPage);
                } else {
                }
            } else if (currentPage == 0) {
                Log.e("Ted", "3rd if");
                perform(currentPage);

            }
            lastPosition = currentPage;
        }
        View layout=layoutManager.findViewByPosition(currentPage);
        layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);

        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
        final SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(mActivity, trackSelector);
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(mActivity,
                Util.getUserAgent(mActivity, "DivineRay"));
        CacheDataSourceFactory cacheDataSourceFactory= new CacheDataSourceFactory(DownloadUtil.getCache(getApplicationContext()),dataSourceFactory);
        MediaSource videoSource = new ExtractorMediaSource.Factory(cacheDataSourceFactory)
                .createMediaSource(Uri.parse(item.getPostVideo()));


        Log.d("resp",item.getPostVideo());


        player.prepare(videoSource);


        player.setRepeatMode(Player.REPEAT_MODE_ALL);

        player.addListener((Player.EventListener) this);
        videoID=item.getVideoId();
        ViewIncrease(currentPage,item);






        final PlayerView playerView=layout.findViewById(R.id.playerview);
        //playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);
        float checkVal=Float.parseFloat(item.getPostHeight());
        float checkVal2=Float.parseFloat(item.getPostWidth());
        float val2=checkVal-checkVal2;
        if (checkVal==checkVal2)
        {

            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
            player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            playerView.setPlayer(player);
        }
        else if (checkVal2>checkVal||val2<200)
        {

            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
            player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            playerView.setPlayer(player);
        }
        else if (checkVal2<checkVal) {

            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
            player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            playerView.setPlayer(player);
        }



        player.setPlayWhenReady(true);

        privious_player=player;
        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {

                    case Player.STATE_BUFFERING:
                       // layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);
                        break;
                    case Player.STATE_IDLE:
                        layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);

                        break;
                    case Player.STATE_READY:
                        layout.findViewById(R.id.frame_thumbnail).setVisibility(View.GONE);
                        break;

                    default:
                        break;
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });




        // final RelativeLayout mainlayout = layout.findViewById(R.id.mainlayout);
        playerView.setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(mActivity, new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                    super.onFling(e1, e2, velocityX, velocityY);
                    float deltaX = e1.getX() - e2.getX();
                    float deltaXAbs = Math.abs(deltaX);
                    // Only when swipe distance between minimal and maximal distance value then we treat it as effective swipe
                    if((deltaXAbs > 100) && (deltaXAbs < 1000)) {
                        if(deltaX > 0)
                        {
                            //  OpenProfile(item,true);
                        }
                    }


                    return true;
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    super.onSingleTapUp(e);
                    if(!player.getPlayWhenReady()){
                        is_user_stop_video=false;
                        privious_player.setPlayWhenReady(true);
                    }else{
                        is_user_stop_video=true;
                        privious_player.setPlayWhenReady(false);
                    }


                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                      Show_video_option(item);

                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {

                    if(!player.getPlayWhenReady()){
                        is_user_stop_video=false;
                        privious_player.setPlayWhenReady(true);
                    }


//                    if(Variables.sharedPreferences.getBoolean(Variables.islogin,false)) {
//                        Show_heart_on_DoubleTap(item, mainlayout, e);
//                        Like_Video(currentPage, item);
//                    }else {
//                        Toast.makeText(context, "Please Login into app", Toast.LENGTH_SHORT).show();
//                    }
                    return super.onDoubleTap(e);

                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });





//        LinearLayout soundimage = (LinearLayout)layout.findViewById(R.id.linear_sound_h);
//        Animation sound_animation = AnimationUtils.loadAnimation(context,R.anim.d_clockwise_rotation);
//        soundimage.startAnimation(sound_animation);










    }
    // this will call when go to the home tab From other tab.
    // this is very importent when for video play and pause when the focus is changes
    boolean is_visible_to_user;


    private void Show_video_option(final SearchModeltagged.Data home_get_set) {

        final CharSequence[] options = { "Report Video","Save Video","Cancel" };

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mActivity,R.style.AlertDialogCustom);

        builder.setTitle(null);

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Report Video"))

                {
                    Intent intent=new Intent(mActivity, ReportVideo.class);
                    intent.putExtra("videoId",home_get_set.getVideoId());
                    startActivity(intent);
                    dialog.dismiss();

                }
                else if (options[item].equals("Save Video"))

                {
                    if(Functions.Checkstoragepermision(mActivity))
                        Save_Video(home_get_set);

                }

                else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }

            }

        });

        builder.show();

    }
    public void Save_Video(final SearchModeltagged.Data item) {
        String path = Constants.app_folder3;
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdir();
        }

        String newF = getMD5EncryptedString(item.getPostVideo()) + ".mp4";
        String path2 = Constants.app_folder3 + newF;
        File dir2 = new File(path2);
        if (dir2.exists()) {
            Toast.makeText(mActivity, "Video already downloaded", Toast.LENGTH_SHORT).show();
        } else {
            Functions.Show_determinent_loader(mActivity, false, false);
            PRDownloader.initialize(getApplicationContext());
            DownloadRequest prDownloader = PRDownloader.download(item.getPostVideo(), Constants.app_folder3, newF)
                    .build()
                    .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                        @Override
                        public void onStartOrResume() {

                        }
                    })
                    .setOnPauseListener(new OnPauseListener() {
                        @Override
                        public void onPause() {

                        }
                    })
                    .setOnCancelListener(new OnCancelListener() {
                        @Override
                        public void onCancel() {

                        }
                    })
                    .setOnProgressListener(new OnProgressListener() {
                        @Override
                        public void onProgress(Progress progress) {

                            int prog = (int) ((progress.currentBytes * 100) / progress.totalBytes);
                            Functions.Show_loading_progress(prog / 2);

                        }
                    });


            prDownloader.start(new OnDownloadListener() {
                @Override
                public void onDownloadComplete() {
                    Functions.cancel_determinent_loader();
                    Toast.makeText(mActivity, "Video stored to" + Constants.app_folder3, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(Error error) {

                    Toast.makeText(mActivity, "Error", Toast.LENGTH_SHORT).show();
                    Functions.cancel_determinent_loader();
                }


            });
        }
    }

    public  String getMD5EncryptedString(String encTarget){
        MessageDigest mdEnc = null;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Exception while encrypting to md5");
            e.printStackTrace();
        } // Encryption algorithm
        mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
        String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
        while ( md5.length() < 32 ) {
            md5 = "0"+md5;
        }
        return md5;
    }

    private void getData() {


        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeApi();
        }

    }
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("lastId","");
        mMap.put("perPage","100");

        if(Music__id.equals("2"))
        {
            mMap.put("tagString","" );
        }
        else{
            mMap.put("tagString",tag );
        }
        mMap.put("musicId",musicId);
        mMap.put("isMusicData",Music__id);
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void executeApi() {
        showProgressDialog(mActivity);

        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.searchTag(getAuthToken(),mParams()).enqueue(new Callback<SearchModeltagged>() {
            @Override
            public void onResponse(Call<SearchModeltagged> call, Response<SearchModeltagged> response) {
                swiperefresh.setRefreshing(false);
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                mGetDetailsModel = response.body();

                    data_list = (ArrayList<SearchModeltagged.Data>) response.body().getData();
                    if (data_list!=null){
                       // setProfileAdapter();
                        Set_Adapter();
                    }

            }

            @Override
            public void onFailure(Call<SearchModeltagged> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });

    }


    private void Set_Adapter() {
        Collections.swap(data_list,0,viewVideoId);
        adapter = new VideoAdapter2(mActivity,profileUserId, data_list,mGetDetailsModel, new VideoAdapter2.OnItemClickListener() {
            @Override
            public void onItemClick(int positon, SearchModeltagged.Data item, View view) {
                switch (view.getId()) {
                    case R.id.shome_profile:
                        onPause();
                        OpenProfile(item,false);
                        break;

                    case R.id.home_title:
                      //  onPause();
                     //   OpenProfile(item,false);
                        break;
                    case R.id.home_like_imagee:
                        // Toast.makeText(context, "Work", Toast.LENGTH_SHORT).show();

                        videoID=item.getVideoId();
                        Like_Video(positon, item,currentPage);


                        break;

                    case R.id.home_dislike_imagee:
                        videoID=item.getVideoId();
                        //Toast.makeText(mActivity, "diskil", Toast.LENGTH_SHORT).show();
                        DisLike_Video(positon, item);
                        break;

                    case R.id.home_comments_image_new:
                        OpenComment(item);
                        break;

                    case R.id.home_share_link_h:
                        ShareOpen(item);

                        break;
                    case R.id.linear_sound_h:
                        OpenMuscicVid(item);

                        break;

                    case R.id.delete_video_button:
                        DeleteVideo(positon,item);
                        break;


                }

            }
        });

        adapter.setHasStableIds(true);
        recyclerView.setAdapter(adapter);


    }


    private void DeleteVideo(int position,SearchModeltagged.Data item) {
        data_list.remove(position);
        data_list.add(position,item);
        adapter.notifyDataSetChanged();
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.removeVideo(getAuthToken(),mParamsD()).enqueue(new Callback<RemoveModel>() {
            @Override
            public void onResponse(Call<RemoveModel> call, Response<RemoveModel> response) {
                dismissProgressDialog();
                finish();

            }

            @Override
            public void onFailure(Call<RemoveModel> call, Throwable t) {

                Log.e("", "**ERROR**" + t.getMessage());
            }
        });




    }

    private Map<String, String> mParamsD() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("videoId",videoID);
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void OpenMuscicVid(SearchModeltagged.Data item) {
        Intent intent=new Intent(mActivity, HashtagItemView.class);
        intent.putExtra("musicId",item.getMusicId());
        intent.putExtra("tagName","");
        startActivity(intent);
    }

//    private void ShareOpen(SearchModeltagged.Data item) {
//
//        try {
//            Intent shareIntent = new Intent(Intent.ACTION_SEND);
//            shareIntent.setType("text/plain");
//            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "DivineRay");
//            String shareMessage= "\nSee this Video\n";
//            shareMessage = shareMessage + item.getShareUrl();
//            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
//            startActivity(Intent.createChooser(shareIntent, "choose one"));
//        } catch(Exception e) {
//            //e.toString();
//        }
//    }
 private void ShareOpen(SearchModeltagged.Data item) {

        if (check_permissions()) {
            File dir = Environment.getExternalStorageDirectory();
            File dcim = new File(dir.getAbsolutePath() + "/Download/DivineRayVideo/" + videoID + ".mp4");

            if (dcim.exists()) {
                setVideoLinkData(Uri.fromFile(dcim), item, mActivity);
            } else {
                showProgressDialog(mActivity);
                downloadFileNew(item.getPostVideo(), item.getVideoId(), item);
            }
        }

    }

    private void setVideoLinkData(Uri fromFile, SearchModeltagged.Data item, Context context) {

        showBottomSheetDilaog(fromFile,item,context);
    }

    private void showBottomSheetDilaog(Uri fromFile, SearchModeltagged.Data item, Context context) {

        View view = LayoutInflater.from(mActivity).inflate(R.layout.share_dialog_layout, null);
        view.setBackgroundColor(Color.TRANSPARENT);
        view.setBackgroundTintMode(PorterDuff.Mode.CLEAR);
        view.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(mActivity);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.show();

        //fetching ids
        LinearLayout llyTwitter = view.findViewById(R.id.llyTwitter);
        LinearLayout llyInsta = view.findViewById(R.id.llyInsta);
        LinearLayout llyFb = view.findViewById(R.id.llyFb);
        LinearLayout llyWhatsapp = view.findViewById(R.id.llyWhatsapp);


        //Click Listeners
        llyTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                ShareUtils.shareTwitter(mActivity,getString(R.string.check_on_divineray),item.getBranchShareUrl(),"","");
            }
        });
        llyFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                ShareUtils.shareFacebook(mActivity, getString(R.string.check_on_divineray),item.getBranchShareUrl());
            }
        });
        llyWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                ShareUtils.shareWhatsAppData(fromFile,item.getBranchShareUrl(),mActivity);
            }
        });
        llyInsta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                ShareUtils.shareInsta(mActivity,fromFile,item.getBranchShareUrl());
            }
        });

    }


    @SuppressLint("StaticFieldLeak")
    public void downloadFileNew(final String uRl, final String idValue, final SearchModeltagged.Data homeModel) {
        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... strings) {
                File direct = new File(Environment.getExternalStorageDirectory() + "/DivineRayVideo");

                if (!direct.exists()) {
                    direct.mkdirs();
                }

                DownloadManager mgr = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                Uri downloadUri = Uri.parse(uRl);
                String filename2 = "";
                filename2 = idValue + "" + ".mp4";
                DownloadManager.Request request = new DownloadManager.Request(downloadUri);

                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                        .setAllowedOverRoaming(false).setTitle("Demo")
                        .setDescription("Something useful. No, really.")
                        .setVisibleInDownloadsUi(false)
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/DivineRayVideo/" + filename2);

                mgr.enqueue(request);


                return null;
            }

            @SuppressLint("WrongThread")
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        File dir = Environment.getExternalStorageDirectory();
                        File dcim = new File(dir.getAbsolutePath() + "/Download/DivineRayVideo/" + idValue + ".mp4");

                        if (dcim.exists()) {
                        }
                        Uri uriPath = Uri.fromFile(dcim);
                        dismissProgressDialog();
                        setVideoLinkData(uriPath, homeModel, mActivity);
                    }
                }, 10000);
            }
        }.execute();
    }

    private void OpenComment(SearchModeltagged.Data item) {
        int value=Integer.parseInt(item.getTotalComments())-1;
        int comment_counnt=value;

        Fragment_Data_Send fragment_data_send=this;

        String comment_userid=item.getUser_id();
        String comment_videoId=item.getVideoId();

        // fun();
        Bundle args=new Bundle();
        args.putString("b_comment_userid",comment_userid);
        args.putString("b_comment_videoId",comment_videoId);


        BottomSheetDialogH bottomSheet = new BottomSheetDialogH(comment_counnt,fragment_data_send);
        bottomSheet.setArguments(args);
        bottomSheet.show(getSupportFragmentManager(), "BottomSheet");



    }
    private void perform(int currentPage) {


        for (int i=currentPage;i<=currentPage+2;i++) {
            DefaultTrackSelector trackSelectorq = new DefaultTrackSelector();

            SimpleExoPlayer playerq = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelectorq);
            final SearchModeltagged.Data itemw = data_list.get(i);
            DefaultDataSourceFactory dataSourceFactoryq = new DefaultDataSourceFactory(getApplicationContext(),
                    Util.getUserAgent(getApplicationContext(), "DivineRay"));
            CacheDataSourceFactory cacheDataSourceFactoryq = new CacheDataSourceFactory(DownloadUtil.getCache(getApplicationContext()), dataSourceFactoryq, CacheDataSource.FLAG_BLOCK_ON_CACHE | CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);
            MediaSource videoSourceq = new ExtractorMediaSource.Factory(cacheDataSourceFactoryq)
                    .createMediaSource(Uri.parse(itemw.getPostVideo()));
            Log.e("Testq", "" + i + "  vg:" + itemw.getPostVideo());
            playerq.prepare(videoSourceq);
            playerq.addListener(new Player.EventListener() {


                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    switch (playbackState) {


                        case Player.STATE_READY:

                            playerq.release();
                            break;

                        default:
                            //  layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);
                            break;
                    }
                }
            });
        }

    }

    private void performN(int currentPage) {

        for (int i = currentPage - 3; i <= currentPage - 1; i++) {
            DefaultTrackSelector trackSelectorq = new DefaultTrackSelector();

            SimpleExoPlayer playerq = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelectorq);
            final SearchModeltagged.Data itemw = data_list.get(i);
            DefaultDataSourceFactory dataSourceFactoryq = new DefaultDataSourceFactory(getApplicationContext(),
                    Util.getUserAgent(getApplicationContext(), "DivineRay"));
            CacheDataSourceFactory cacheDataSourceFactoryq = new CacheDataSourceFactory(DownloadUtil.getCache(getApplicationContext()), dataSourceFactoryq, CacheDataSource.FLAG_BLOCK_ON_CACHE | CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);
            MediaSource videoSourceq = new ExtractorMediaSource.Factory(cacheDataSourceFactoryq)
                    .createMediaSource(Uri.parse(itemw.getPostVideo()));
            Log.e("Testq", "" + i + "  vg:" + itemw.getPostVideo());
            playerq.prepare(videoSourceq);
            playerq.addListener(new Player.EventListener() {


                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    switch (playbackState) {


                        case Player.STATE_READY:

                            playerq.release();
                            break;

                        default:
                            //  layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);
                            break;
                    }
                }
            });
        }

    }
        private void DisLike_Video(int positon, SearchModeltagged.Data item) {

        String action=item.getIsLiked();

        if(action.equals("1")){
            action="0";
            item.setTotalLike(String.valueOf(Integer.parseInt(item.getTotalLike()) -1));
        }else {
            action="1";
            item.setTotalLike(String.valueOf(Integer.parseInt(item.getTotalLike()) +1));
        }


        data_list.remove(positon);
        item.setIsLiked(action);
        data_list.add(positon,item);
        adapter.notifyDataSetChanged();
        showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.addFavVideo(getAuthToken(),mParams2()).enqueue(new Callback<VideofavModel>() {
            @Override
            public void onResponse(Call<VideofavModel> call, Response<VideofavModel> response) {
                dismissProgressDialog();
                //   Toast.makeText(mContext, "islike"+isView, Toast.LENGTH_SHORT).show();
                Log.e("", "**RESPONSE**" + response.body());
                VideofavModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {

                } else {


                    //Toast.makeText(mActivity, "Error Getting daa", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<VideofavModel> call, Throwable t) {

                Log.e("", "**ERROR**" + t.getMessage());
            }
        });

    }


    /*
     * Execute api
     * */
    private Map<String, String> mParamsVi() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("videoId",videoID);
        mMap.put("isView","1");
        mMap.put("indexCall","" );
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    void ViewIncrease(int positon, SearchModeltagged.Data item) {

        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.addFavVideo(getAuthToken(),mParamsVi()).enqueue(new Callback<VideofavModel>() {
            @Override
            public void onResponse(Call<VideofavModel> call, Response<VideofavModel> response) {
                item.setTotalViews(String.valueOf(Integer.parseInt(item.getTotalViews()) +1));
                data_list.remove(positon);
                data_list.add(positon,item);
                adapter.notifyDataSetChanged();

                Log.e("", "**RESPONSE**" + response.body());
                VideofavModel mGetDetailsModel = response.body();


                if (mGetDetailsModel.getStatus() == 1) {

                } else {


                    //Toast.makeText(mActivity, "Error Getting daa", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<VideofavModel> call, Throwable t) {

                Log.e("", "**ERROR**" + t.getMessage());
            }
        });

    }



    /*
     * Execute api
     * */
    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("videoId",videoID);
        mMap.put("isView","0");
        mMap.put("indexCall","" );
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void Like_Video(int positon, SearchModeltagged.Data item,int currentPage) {
        String action=item.getIsLiked();

        if(action.equals("1")){
            action="0";
            item.setTotalLike(String.valueOf(Integer.parseInt(item.getTotalLike()) -1));
        }else {
            action="1";
            item.setTotalLike(String.valueOf(Integer.parseInt(item.getTotalLike()) +1));
        }


        data_list.remove(positon);
        item.setIsLiked(action);
        data_list.add(positon,item);
        adapter.notifyDataSetChanged();
        //  Toast.makeText(context, "Working", Toast.LENGTH_SHORT).show();

        showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.addFavVideo(getAuthToken(),mParams2()).enqueue(new Callback<VideofavModel>() {
            @Override
            public void onResponse(Call<VideofavModel> call, Response<VideofavModel> response) {

                VideofavModel mGetDetailsModel = response.body();
                if (mGetDetailsModel.getStatus() == 1) {
                    dismissProgressDialog();
                    Log.e("", "**RESPONSE**" + response.body());

                    //  Toast.makeText(mContext, "islike"+isView+"fc:  "+video_id+"Use  "+DivineRayPrefernces.readString(mContext,DivineRayPrefernces.USERID,null), Toast.LENGTH_SHORT).show();
                } else {

                    Log.e("Data","error");
                }

            }

            @Override
            public void onFailure(Call<VideofavModel> call, Throwable t) {

                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }
    public boolean is_fragment_exits(){
        FragmentManager fm = getSupportFragmentManager();
        if(fm.getBackStackEntryCount()==0){
            return false;
        }else {
            return true;
        }

    }

    // this is lifecyle of the Activity which is importent for play,pause video or relaese the player
    @Override
    public void onResume() {
        super.onResume();
        if(privious_player!=null ){
            // Toast.makeText(context, "Start Play1", Toast.LENGTH_SHORT).show();
            privious_player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

            privious_player.setPlayWhenReady(true);
        }
        AudioManager ama = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

// Request audio focus for playback
        int result = ama.requestAudioFocus(focusChangeListener,
// Use the music stream.
                AudioManager.STREAM_MUSIC,
// Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);


        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
       // other app had stopped playing song now , so u can do u stuff now .
        }
    }

    private AudioManager.OnAudioFocusChangeListener focusChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                public void onAudioFocusChange(int focusChange) {
                    try {
                        MediaPlayer mediaPlayerBackground = new MediaPlayer();
                        AudioManager am = (AudioManager) Objects.requireNonNull(getApplicationContext()).getSystemService(Context.AUDIO_SERVICE);
                        switch (focusChange) {

                            case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK):
                                // Lower the volume while ducking.
                                mediaPlayerBackground.setVolume(0.2f, 0.2f);
                                break;
                            case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT):
                                mediaPlayerBackground.stop();
                                break;

                            case (AudioManager.AUDIOFOCUS_LOSS):
                                mediaPlayerBackground.stop();

                                break;

                            case (AudioManager.AUDIOFOCUS_GAIN):
                                // Return the volume to normal and resume if paused.
                                mediaPlayerBackground.setVolume(1f, 1f);
                                mediaPlayerBackground.start();
                                break;
                            default:
                                break;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

    @Override
    public void onPause() {
        super.onPause();
        if(privious_player!=null){
            privious_player.setPlayWhenReady(false);
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if(privious_player!=null){
            privious_player.setPlayWhenReady(false);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(privious_player!=null){
            privious_player.release();
        }
    }


    // this will open the profile of user which have uploaded the currenlty running video\


    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if(playbackState==Player.STATE_BUFFERING){
            p_bar.setVisibility(View.VISIBLE);
        }
        else if(playbackState==Player.STATE_READY){
            p_bar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    private void OpenProfile(SearchModeltagged.Data item, boolean from_right_to_left) {
        String new_profileId= DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null);
        if (item.getUser_id().equals(new_profileId)) {
            Intent intent = new Intent(mActivity, SelectedUserOwnProfile.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

        }
        else {
            Intent intent = new Intent(mActivity, SelectedUserProfile.class);
            intent.putExtra("profileUserId", item.getUser_id());
            intent.putExtra("followingsb", item.getUserDetails().getTotalFollowing());
            intent.putExtra("followersb", item.getUserDetails().getTotalFollowers());
            intent.putExtra("photo", item.getUserDetails().getPhoto());
            intent.putExtra("name", item.getUserDetails().getName());
            intent.putExtra("descriptionb", item.getUserDetails().getDescription());
            intent.putExtra("followornot", item.getUserDetails().getFollow());
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }


    @Override
    public void onDataSent(String yourData) {
        int comment_count =Integer.parseInt(yourData);
        SearchModeltagged.Data item=data_list.get(currentPage);
        item.setTotalComments(String.valueOf(comment_count));
        data_list.remove(currentPage);
        data_list.add(currentPage,item);
        adapter.notifyDataSetChanged();

    }
    // we need 4 permission during creating an video so we will get that permission
    // before start the video recording
    public boolean check_permissions() {

        String[] PERMISSIONS = new String[0];

        PERMISSIONS = new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.CAMERA
        };

        if (!hasPermissions(mActivity, PERMISSIONS)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS, 2);
            }
        }else if (hasPermissions(mActivity, PERMISSIONS)) {
//setUpCamera();
            return  true;
        }
        else {
            return true;
        }



        return false;
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }

            }
        }
        return true;


    }
}
