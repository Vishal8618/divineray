package com.divineray.app.Report;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.SelecteUserMore.RecyclerviewAppointmentadapter;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.AppointmentBookingModel;
import com.divineray.app.model.HomeModel;
import com.divineray.app.model.ReportAbuseModel;
import com.divineray.app.model.ReportVideoModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportVideo extends BaseActivity {
    Activity mActivity=ReportVideo.this;
    @BindView(R.id.recycler_report)
    RecyclerView recyclerView;
    @BindView(R.id.report_back)
    LinearLayout lly_report;
    @BindView(R.id.llinear)
    LinearLayout lly;
    @BindView(R.id.Reportsumbit)
    TextView tx_report;
    String videoId;
    String other_user_id;
    ArrayList<ReportVideoModel.Data> mArrayList = new ArrayList<>();

    ReportVideoapter adapter;
    RadioButton radioButton;
    RadioGroup.LayoutParams rl;
    RadioGroup radioGroup;
    int radId=-1;
    String videoIdn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_report_video);
        ButterKnife.bind(this);
        Intent intent =getIntent();
        tx_report.setVisibility(View.GONE);
        if (intent!=null)
        {
            videoIdn=intent.getStringExtra("videoId");
        }
        radioGroup=new RadioGroup(mActivity);
        radioGroup.setOrientation(RadioGroup.VERTICAL);
        radioGroup.removeAllViews();

        fetchApi();
        //fetchApi();

        lly_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tx_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    radId = radioGroup.getCheckedRadioButtonId();
                   //  Toast.makeText(mActivity, ""+radId+"", Toast.LENGTH_SHORT).show();
                if (!(radId==-1)) {
                    executeReportApi();
                }
                else
                {
                    showAlertDialog(mActivity,"Please select atleast one option");
                }
            }
        });
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("reasonId",String.valueOf(radId));
        mMap.put("reportedId", videoIdn);
        mMap.put("reportType", "2");
        mMap.put("reportedBy", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeReportApi() {
        showProgressDialog(mActivity);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.reportVideosubmit(getAuthToken(),mParams2()).enqueue(new Callback<ReportAbuseModel>() {
            @Override
            public void onResponse(Call<ReportAbuseModel> call, Response<ReportAbuseModel> response) {
                dismissProgressDialog();

                Log.e("", "**RESPONSE**" + response.body());
                ReportAbuseModel mModel = response.body();
                //assert mGetDetailsModel != null;
                assert mModel != null;
                if (mModel.getStatus()==1) {

                    Toast.makeText(mActivity, ""+mModel.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();

                } else if (response.body().getStatus()==0) {
                    Log.e("Data","0");
                    Toast.makeText(mActivity, ""+mModel.getMessage(), Toast.LENGTH_SHORT).show();
                    // Toast.makeText(getActivity(), "" + mGetDetailsModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ReportAbuseModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });

    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void fetchApi() {
        showProgressDialog(mActivity);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.reportVideo(getAuthToken(),mParams()).enqueue(new Callback<ReportVideoModel>() {
            @Override
            public void onResponse(Call<ReportVideoModel> call, Response<ReportVideoModel> response) {
                dismissProgressDialog();

                Log.e("Report", "**RESPONSE**" + response.body().toString());
                ReportVideoModel mModel = response.body();
                //assert mGetDetailsModel != null;
                assert mModel != null;
                if (mModel.getStatus()==1) {

                    mArrayList.clear();
                    mArrayList = (ArrayList<ReportVideoModel.Data>) response.body().getData();
                    if (mArrayList!=null){
////
int j=1;
//                        for (int i=0;i<=mArrayList.size();i++)
//                        {
                        for (int i=0;i<mArrayList.size();i++)
                        {

                            ReportVideoModel.Data fmodel=mArrayList.get(i) ;
                            Log.e("Testyop",""+fmodel.getReportReasons());
                            radioButton=new RadioButton(mActivity);
                              radioButton.setText(fmodel.getReportReasons());
                              radioButton.setId(j);
                              j++;
                            rl=new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT,RadioGroup.LayoutParams.MATCH_PARENT);
                            rl.setMargins(15, 15, 15, 15);
                            radioButton.setTextSize(15);
                            radioButton.setPadding(4,4,4,4);
                            radioGroup.addView(radioButton,rl);


                        }
                        lly.addView(radioGroup);
                        tx_report.setVisibility(View.VISIBLE);


                    }

                } else if (response.body().getStatus()==0) {
                   Log.e("Data","0");
                    // Toast.makeText(getActivity(), "" + mGetDetailsModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ReportVideoModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onRestart() {
        super.onRestart();


    }
}