package com.divineray.app.Report;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.divineray.app.R;
import com.divineray.app.SelecteUserMore.RecyclerviewAppointmentadapter;
import com.divineray.app.model.AppointmentBookingModel;
import com.divineray.app.model.ReportVideoModel;

import java.util.ArrayList;

public class ReportVideoapter  extends RecyclerView.Adapter<ReportVideoapter.ViewHolder> {
    Context context;
    ArrayList<ReportVideoModel.Data> mArrayList;
    int selectedOption=0;
    RadioGroup rgp,rf;
    private int selectedIndex = -1;


    public ReportVideoapter(Context mActivity, ArrayList<ReportVideoModel.Data> mArrayList, RadioGroup rgp) {
        this.context=mActivity;
        this.mArrayList=mArrayList;
        this.rgp=rgp;

    }

    @NonNull
    @Override
    public ReportVideoapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_video_item, parent, false);
        return new ReportVideoapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportVideoapter.ViewHolder holder, int position) {
        ReportVideoModel.Data  mModel1 = mArrayList.get(position);

//
        holder.rg.removeAllViews();
           RadioButton btn= new RadioButton(context);
           btn.setText(mModel1.getReportReasons());
           btn.setSelected(false);

            holder.rg.addView(btn);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, ""+position, Toast.LENGTH_SHORT).show();
                    btn.setSelected(true);
                }
            });

     //  rf.addView(btn);
       // rgp.addView(btn);
//        if (position == selectedIndex) {
//            btn.setSelected ( true );
//        }
//        else {
//            btn.setSelected ( false );
//        }
//
//       }

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
       // RadioButton rd1,rd2,rd3,rd4,rd5,rd6;
        RadioGroup rg;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
           // rd1=itemView.findViewById(R.id.radio1);
            rg=itemView.findViewById(R.id.radioGroup);



        }
    }
}
