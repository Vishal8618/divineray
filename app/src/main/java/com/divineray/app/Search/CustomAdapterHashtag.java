package com.divineray.app.Search;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.divineray.app.R;
import com.divineray.app.Profile.VideoView2;
import com.divineray.app.model.SearchModeltagged;

import java.util.ArrayList;

class CustomAdapterHashtag extends BaseAdapter {
    Activity mActivity;
    ArrayList<SearchModeltagged.Data> mArrayList = new ArrayList<>();
    String tag;


    public CustomAdapterHashtag(Activity activity, ArrayList<SearchModeltagged.Data> mArrayList, String tag) {
        this.mActivity=activity;
        this.mArrayList=mArrayList;
        this.tag=tag;
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView==null){
            convertView=LayoutInflater.from(mActivity).inflate(R.layout.sub_profile_image,parent,false);
        }

        SearchModeltagged.Data mModel = mArrayList.get(position);
        ImageView img=(ImageView)convertView.findViewById(R.id.icon);
        Glide.with(mActivity)
                .load(mModel.getPostImage())
                .into(img);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent=new Intent(mActivity, VideoView2.class);
                intent.putExtra("videoId", String.valueOf(position));
                intent.putExtra("tagName",tag);
                intent.putExtra("musicId",mModel.getMusicId());
             mActivity.startActivity(intent);


            }
        });

        return convertView;
    }
    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}
