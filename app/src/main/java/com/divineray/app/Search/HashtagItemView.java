package com.divineray.app.Search;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.divineray.app.Add.AddFragment;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.Video_Recording.Video_Recorder_A;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.SearchModeltagged;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HashtagItemView extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = HashtagItemView.this.getClass().getSimpleName();
    /*
    Current Activity Instance
   */
    Activity mActivity =HashtagItemView.this;
    /**
     * Current Activity Instance
     */

    TextView  hash_description;

    TextView  hash_view;

    TextView  hash_tagName;
    TextView tx_already_added;


    GridView simpleGrid;
    FloatingActionButton floatbutton;


    LinearLayout hashtag_back_lly;
    int logos[] = { R.drawable.img2,R.drawable.imgg,R.drawable.imggg,
            R.drawable.img2,R.drawable.imgg};

    private String tag,musicId,photo,views,description,videoId,postVideo,isLiked,comments,likes,title,shareUrl,totalLikes,name,user_photo;
    TextView hashtag_addtofav;

    List<SearchModeltagged.Data> mQuotesArrayList = new ArrayList<>();
    AddFragment addFragment;
    String profileUserId;
    String Music__id;
    String tag__Name="s";
    String tagD;
    /*
     * Widgets
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_hashtag_item_view);
        ButterKnife.bind(this);
        hash_tagName = findViewById(R.id.hash_tagname);
        hashtag_back_lly = findViewById(R.id.hashtag_back);
        floatbutton = findViewById(R.id.floatButton);

        simpleGrid = findViewById(R.id.gridview_hashtag);

        Intent intent = getIntent();
        tag = intent.getStringExtra("tagName");
        tagD=intent.getStringExtra("tagD");
        views = intent.getStringExtra("tagViews");
        description = intent.getStringExtra("tagDescription");
        photo = intent.getStringExtra("videoPhoto");
        videoId = intent.getStringExtra("videoId");
        postVideo = intent.getStringExtra("postVideo");
        isLiked = intent.getStringExtra("isLiked");
        comments = intent.getStringExtra("comments");
        likes = intent.getStringExtra("likes");
        title = intent.getStringExtra("title");
        shareUrl = intent.getStringExtra("shareUrl");
        isLiked = intent.getStringExtra("isLiked");
        name = intent.getStringExtra("name");
        user_photo = intent.getStringExtra("user_photo");

        musicId = intent.getStringExtra("musicId");

        profileUserId = intent.getStringExtra("profileUserId");

        hashtag_back_lly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (tag.equals(""))
        {
            Music__id="2";
            tag__Name="";


        }
        else
        {
            Music__id="0";
            tag__Name=tag;


        }
     getVideos();

        floatbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(mActivity, Video_Recorder_A.class);
                startActivity(intent1);
                finish();
                overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);

            }
        });

//        hash_view.setText(views+" views");

        if (tagD!=null) {
            String result = tagD.replaceAll("[-+.^:,#$]","");
            String message2=result;
            message2 = Character.toUpperCase(message2.charAt(0)) + message2.substring(1);
            hash_tagName.setText("#"+message2);
        }


    }


    private void getVideos() {
        if (!isNetworkAvailable(mActivity)) {
            Toast.makeText(mActivity, "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
            executeApi();
        }
    }

    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("lastId","");
        mMap.put("perPage","100");
        if(Music__id.equals("2"))
        {
            mMap.put("tagString","" );
        }
        else{
            mMap.put("tagString",tag );
        }
        mMap.put("musicId",musicId);
        mMap.put("isMusicData",Music__id);
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void executeApi() {

        showProgressDialog(mActivity);

        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);

        mApiInterface1.searchTag(getAuthToken(),mParams()).enqueue(new Callback<SearchModeltagged>() {
            @Override
            public void onResponse(Call<SearchModeltagged> call, Response<SearchModeltagged> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                SearchModeltagged mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {
                    mQuotesArrayList = response.body().getData();
                    if (mQuotesArrayList != null) {
                        setProfileAdapter();
                    }

                } else {
                   // Toast.makeText(mActivity, "Error Getting daa", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SearchModeltagged> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());

            }
        });

    }

    /*
     * Execute api
     * */
    private Map<String, String> mParamsP() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("lastId","");
        mMap.put("perPage","100");
        mMap.put("profileUserId", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    public  void tabAllApi() {
        Toast.makeText(mActivity, "TabAll", Toast.LENGTH_SHORT).show();
showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getProfileMusicVideos(getAuthToken(),mParamsP()).enqueue(new Callback<SearchModeltagged>() {
            @Override
            public void onResponse(Call<SearchModeltagged> call, Response<SearchModeltagged> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                SearchModeltagged mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {
                    mQuotesArrayList = response.body().getData();
                    if (mQuotesArrayList != null) {
                        setProfileAdapter();
                    }

                } else {
                   Log.e("Data","error frtch");
                }

            }

            @Override
            public void onFailure(Call<SearchModeltagged> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setProfileAdapter() {


        CustomAdapterHashtag customAdapter = new CustomAdapterHashtag(mActivity, (ArrayList<SearchModeltagged.Data>) mQuotesArrayList,tag);
        simpleGrid.setAdapter(customAdapter);
    }
}