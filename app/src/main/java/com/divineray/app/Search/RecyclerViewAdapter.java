package com.divineray.app.Search;

import android.content.Context;
import android.content.Intent;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.divineray.app.R;
import com.divineray.app.model.ProfileGetVideoModel;
import com.divineray.app.model.SearchModelget;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RecyclerViewAdapter  extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> implements Filterable {
    public String video_tag_title;
    private Context mContext;
    static  public String fixed_tag_title="";
    ArrayList<SearchModelget.Datan> mArrayList = new ArrayList<>();
    TextView tx_no_search;
    ShimmerFrameLayout mShimmerViewContainer;
    ArrayList<SearchModelget.Datan> FilteredmArrayList =new ArrayList<>();


    public RecyclerViewAdapter(Context context, ArrayList<SearchModelget.Datan> mArrayList, TextView text_no_search, ShimmerFrameLayout mShimmerViewContainer) {
        this.mContext=context;
        this.mArrayList=mArrayList;
        this.FilteredmArrayList=mArrayList;
        this.tx_no_search=text_no_search;
        this.mShimmerViewContainer=mShimmerViewContainer;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_item, parent, false);
        return new RecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter.ViewHolder holder, int position) {
      //  holder.tag.setImageResource(mImageUrls.get(position).getImgId());
        SearchModelget.Datan mModel = mArrayList.get(position);




                String message=mModel.getType();
                message = Character.toUpperCase(message.charAt(0)) + message.substring(1);
                holder.tx_tagType.setText(message);
                String tagString=mModel.getTagTitle();
                String result = tagString.replaceAll("[-+.^:,#$]","");

                if (result!=null) {
                    try {
                        String message2 = result;
                        message2 = Character.toUpperCase(message2.charAt(0)) + message2.substring(1);
                        holder.tx_tagtitle.setText(message2);
                    }
                    catch (StringIndexOutOfBoundsException e)
                    {
                        e.printStackTrace();
                    }

                }


                holder.tx_tagtotalVideos.setText(mModel.getTotalVideosViews());

                fixed_tag_title=mModel.getTagTitle();

                Log.d("**RESPONSE",""+mModel);
                //  Toast.makeText(mContext, ""+mModel.getTagVideos(), Toast.LENGTH_SHORT).show();
                LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
                layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                holder.recyclerViewHorizontal.setLayoutManager(layoutManager);
                video_tag_title=mModel.getTagTitle();
                RecyclerViewAdapter2 horizontalAdapter = new RecyclerViewAdapter2((ArrayList<SearchModelget.TagVideos>) mModel.getTagVideos(), mContext,video_tag_title,holder.tx_tagtotalVideos,mModel.getTotalVideosViews(),position);
                holder.recyclerViewHorizontal.setAdapter(horizontalAdapter);
        mShimmerViewContainer.setVisibility(View.GONE);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(mContext,HashtagItemView.class);
                        intent.putExtra("tagName",mModel.getTagTitle());
                        intent.putExtra("tagViews",mModel.getTotalVideosViews());
                        intent.putExtra("musicId","");
                        intent.putExtra("tagD",mModel.getTagTitle());
                        intent.putExtra("tagDescription",mModel.getTagDesc());
                        intent.putExtra("isLiked","2");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    }
                });

    }

    @Override
    public int getItemCount() {

        return mArrayList== null ? 0 : mArrayList.size();
    }




    //filter method
    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mArrayList=FilteredmArrayList;

                } else {
                    ArrayList<SearchModelget.Datan> filteredList = new ArrayList<>();
                    for (SearchModelget.Datan row : FilteredmArrayList) {


                        //change this to filter according to your case
                        if (row.getTagTitle().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    mArrayList = (ArrayList<SearchModelget.Datan>) filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values =mArrayList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mArrayList = (ArrayList) filterResults.values;
                if (mArrayList.size()>0){
                    tx_no_search.setVisibility(View.GONE);
                }
                else {

                    tx_no_search.setVisibility(View.VISIBLE);
                    tx_no_search.setText("No Results found for "+"'"+charSequence.toString()+"'");
                }
                notifyDataSetChanged();

            }
        };

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tx_tagtitle,tx_tagType;
        TextView tx_tagtotalVideos;
        RecyclerView recyclerViewHorizontal;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
           tx_tagtitle=itemView.findViewById(R.id.search_tagTitle);
           tx_tagtotalVideos=itemView.findViewById(R.id.search_tagTotalVideos_);
           tx_tagType=itemView.findViewById(R.id.search_tagType);
           recyclerViewHorizontal=itemView.findViewById(R.id.search_item1_recycler);
        }
    }
}
