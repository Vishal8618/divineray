package com.divineray.app.Search;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.divineray.app.R;
import com.divineray.app.Profile.VideoView2;
import com.divineray.app.model.SearchModelget;

import java.util.ArrayList;

public class RecyclerViewAdapter2  extends RecyclerView.Adapter<RecyclerViewAdapter2.ViewHolder> {
        String vide_tagTitle;
    private Context mContext;
   ArrayList<SearchModelget.TagVideos> mArrayList = new ArrayList<>();
   TextView tx_tagTotalVideos;
   int video_count=0;
   String total_videos;
   int position;

    public RecyclerViewAdapter2(ArrayList<SearchModelget.TagVideos> tagVideos, Context mContext, String video_tag_title, TextView tx_tagtotalVideos, String totalVideosViews, int position) {
this.mArrayList=tagVideos;
this.mContext=mContext;
this.vide_tagTitle=video_tag_title;
this.tx_tagTotalVideos=tx_tagtotalVideos;
this.total_videos=totalVideosViews;
this.position=position;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter2.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter2.ViewHolder holder, int position) {

        SearchModelget.TagVideos mModel=mArrayList.get(position);

        Glide.with(mContext).load(mModel.getPostImage()).into(holder.imageView);
        int val= Integer.parseInt(total_videos);
        tx_tagTotalVideos.setText(String.valueOf(video_count+val));
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(mContext, VideoView2.class);
                intent.putExtra("videoId", String.valueOf(holder.getAdapterPosition()));
                intent.putExtra("tagName",vide_tagTitle);
                intent.putExtra("position",String.valueOf(position));
                mContext.startActivity(intent);

                }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.search_icon);

        }
    }
}