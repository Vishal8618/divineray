package com.divineray.app.Search;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.divineray.app.Agora.ConstantsA;
import com.divineray.app.Agora.activities.BaseActivity;
import com.divineray.app.Chat.SearchTabScreen;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseFragment;
import com.divineray.app.activities.HomeActivity;
import com.divineray.app.activities.LiveStreaming1Activity;
import com.divineray.app.activities.LiveStreaming2Activity;
import com.divineray.app.adapters.LiveuserScreenAdapter;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.LiveUsersModel;
import com.divineray.app.model.SearchModelget;
import com.divineray.app.model.StatusModel;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.agora.rtc.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends BaseFragment {
    RecyclerView simpleGrid;
    List<SearchModelget.Datan> mQuotesArrayList = new ArrayList<>();

    public Dialog progressDialog;


    @BindView(R.id.search_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.search_searchtab_button)
    TextView tx_searchtab;
    @BindView(R.id.search_searchedittext)
    EditText editText_search;
    @BindView(R.id.text_no_result_search)
    TextView text_no_search;
    @BindView(R.id.p_bar_search)
    ProgressBar p_bar;
    @BindView(R.id.swiperefresh_search)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.shimmer_view_container)
    public ShimmerFrameLayout mShimmerViewContainer;
    @BindView(R.id.llySearchScreen)
    LinearLayout llySearchScreen;
    @BindView(R.id.recyclerLiveUsers)
    RecyclerView recyclerViewLiveUsers;

    @BindView(R.id.llySVideo)
    LinearLayout llySVideo;
    @BindView(R.id.llySLiveUsers)
    LinearLayout llySLiveUsers;
    @BindView(R.id.imSVideo)
    ImageView imSVideo;
    @BindView(R.id.imSLiveUsers)
    ImageView imSLiveUsers;
    @BindView(R.id.txSLiveUsers)
    TextView txSLiveUsers;
    @BindView(R.id.txSVideo)
    TextView txSVideo;

    @BindView(R.id.swiperefresh_liveusers)
    SwipeRefreshLayout swiperefresh_liveusers;

    RecyclerViewAdapter adapter;

    boolean isSearchVideoScreen=true;

    ArrayList<String> mArraylist=new ArrayList<String>();
    ArrayList<LiveUsersModel.Data> mArraylistLiveUsers=new ArrayList<>();
    LiveuserScreenAdapter adapter1;
    String showLiveUsers="false";


    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate( R.layout.search_fragment, container, false);
        ButterKnife.bind(this,view);
        config().setVideoDimenIndex(4);
        performSearchVideoScreenCheck();
        setDataLiveUsers();
        swiperefresh.setProgressViewOffset(false, 0, 200);
        swiperefresh.setColorSchemeResources(R.color.black);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
            getDetails();
            }
        });


        swiperefresh_liveusers.setProgressViewOffset(false, 0, 200);

        swiperefresh_liveusers.setColorSchemeResources(R.color.black);
        swiperefresh_liveusers.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setDataLiveUsers();
            }
        });

        getDetails();

        editText_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String string = editable.toString();
                if (mQuotesArrayList.size()>1) {
                    adapter.getFilter().filter(string);
                }
                else {}
            }
        });
        editText_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                   closeKeyboard();
                    return true;
                }
                return false;
            }
        });
        Bundle mArgs=getArguments();
        if (mArgs!=null) {
            showLiveUsers = mArgs.getString("showLiveUsers");

            if (showLiveUsers.equals("true")) {
                isSearchVideoScreen=false;
                performSearchVideoScreenCheck();
            }
       }

        return  view;
    }


    @OnClick({R.id.llySVideo,R.id.llySLiveUsers})
    public void onViewCLicked(View view)
    {
        switch (view.getId())
        {
            case R.id.llySVideo:
                isSearchVideoScreen=true;
                performSearchVideoScreenCheck();
                break;
            case R.id.llySLiveUsers:
                isSearchVideoScreen=false;
                performSearchVideoScreenCheck();
                break;
        }
    }



    private void performSearchVideoScreenCheck() {
        if (isSearchVideoScreen)
        {
            llySVideo.setBackgroundResource(R.drawable.bg_btn_blue);
            imSVideo.setBackgroundResource(R.drawable.ic_play_white);
            txSVideo.setTextColor(getResources().getColor(R.color.colorWhite));
            llySLiveUsers.setBackgroundResource(R.drawable.bg_et2);
            imSLiveUsers.setBackgroundResource(R.drawable.ic_broadcast_black);
            txSLiveUsers.setTextColor(getResources().getColor(R.color.black));

            recyclerViewLiveUsers.setVisibility(View.GONE);
            swiperefresh.setVisibility(View.VISIBLE);
            swiperefresh_liveusers.setVisibility(View.GONE);
            llySearchScreen.setVisibility(View.VISIBLE);

        }
        else {
            llySVideo.setBackgroundResource(R.drawable.bg_et2);
            imSVideo.setBackgroundResource(R.drawable.ic_play_black);
            txSVideo.setTextColor(getResources().getColor(R.color.black));
            llySLiveUsers.setBackgroundResource(R.drawable.bg_btn_blue);
            imSLiveUsers.setBackgroundResource(R.drawable.ic_broadcast_white);
            txSLiveUsers.setTextColor(getResources().getColor(R.color.colorWhite));

            llySearchScreen.setVisibility(View.GONE);
            recyclerViewLiveUsers.setVisibility(View.VISIBLE);
            swiperefresh_liveusers.setVisibility(View.VISIBLE);
            swiperefresh.setVisibility(View.GONE);

        }
    }

    private void closeKeyboard()
    {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            manager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onPause() {
       super.onPause();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (editText_search.getText().toString().equals(""))
        {
            executeDetailsApi();
            Log.e("T","if");
        }
        else
        {

            Log.e("T","else");
        }
        closeKeyboard();



    }

    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    private void getDetails() {
        if (!isNetworkAvailable(getActivity())) {
            Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
            executeDetailsApi();
        }
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.USERID,null));
        mMap.put("lastId","");
        mMap.put("perPage","100");
        mMap.put("tagString","");
        mMap.put("musicId","");
        mMap.put("isMusicData","0");
        Log.e("Vall", "**PARAM**"+ DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.USERID,null) + mMap.toString());
        return mMap;
    }
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null&&!progressDialog.isShowing())
            progressDialog.show();


    }
    private void executeDetailsApi() {
        String authtoken= DivineRayPrefernces.readString(getContext(),DivineRayPrefernces.AUTHTOKEN,null);

       // showProgressDialog(getActivity());
        p_bar.setVisibility(View.VISIBLE);
        swiperefresh.setVisibility(View.VISIBLE);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getSearchData(authtoken,mParams()).enqueue(new Callback<SearchModelget>() {
            @Override
            public void onResponse(Call<SearchModelget> call, Response<SearchModelget> response) {
                swiperefresh.setRefreshing(false);
                //dismissProgressDialog();
                p_bar.setVisibility(View.GONE);
                Log.e("kk", "**RESPONSE**" + response.body());
                if (response.body()!=null) {
                    SearchModelget mGetDetailsModel = response.body();

                    assert mGetDetailsModel != null;
                    if (mGetDetailsModel.getStatus() == 1) {
                        mQuotesArrayList = response.body().getData();
                        if (mQuotesArrayList != null) {
                            setSearchAdapter();
                        }

                    } else if (mGetDetailsModel.getStatus() == 0) {
                        Log.e("Data", "error fetch");
                    } else {

                        showDialogLogOut(getActivity());
                    }
                }

            }

            @Override
            public void onFailure(Call<SearchModelget> call, Throwable t) {
                dismissProgressDialog();
                p_bar.setVisibility(View.GONE);
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setSearchAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
         adapter = new RecyclerViewAdapter(getContext(), (ArrayList<SearchModelget.Datan>) mQuotesArrayList,text_no_search,mShimmerViewContainer);
        recyclerView.setAdapter(adapter);
    }


    public  void  setDataLiveUsers()
    {
        if (isNetworkAvailable((Activity) getContext())) {
            executeLiveUsersApi();
        }
        else
        {
            swiperefresh_liveusers.setRefreshing(false);
            showToast((Activity) getContext(), getResources().getString(R.string.internet_conection));
        }
    }

    private void performLiveClick() {
        if (isNetworkAvailable(getContext())) {
            startActivity(new Intent(getContext(), LiveStreaming1Activity.class));
            getActivity().overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        } else
        {
            showAlertDialog2((Activity) getContext(),getResources().getString(R.string.internet_conection) );
        }
    }





    void executeLiveUsersApi()
    {
       String authtoken= DivineRayPrefernces.readString(getActivity(),DivineRayPrefernces.AUTHTOKEN,null);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.showLiveUsers(authtoken).enqueue(new Callback<LiveUsersModel>() {
            @Override
            public void onResponse(Call<LiveUsersModel> call, Response<LiveUsersModel> response) {
                swiperefresh_liveusers.setRefreshing(false);
                LiveUsersModel mModel = response.body();
                mArraylistLiveUsers.clear();
                if (mModel!=null) {
                   if (mModel.getStatus() == 1) {
                        mArraylistLiveUsers.addAll(mModel.getData());
                    }

                }
                setLiveUsersAdapter(mArraylistLiveUsers);
            }

            @Override
            public void onFailure(Call<LiveUsersModel> call, Throwable t) {
                swiperefresh_liveusers.setRefreshing(false);;
                dismissProgressDialog();
                          }
        });


    }

    private void setLiveUsersAdapter(ArrayList<LiveUsersModel.Data> mArraylistLiveUsers) {

        int numofColumns=3;
        recyclerViewLiveUsers.setLayoutManager(new GridLayoutManager(getContext(), numofColumns));
        adapter1= new LiveuserScreenAdapter(mArraylistLiveUsers, getContext(), new LiveuserScreenAdapter.onItemClickClickListener() {
            @Override
            public void onItemClick(int position, ArrayList<LiveUsersModel.Data> mArrayList, View view) {
                switch (view.getId())
                {
                    case R.id.cdGoLive:
                        performLiveClick();
                        break;
                    case R.id.imProfilePic:
                        performJoinLiveUser(mArrayList,position);
                        break;
                    case R.id.txUsername:
                        performJoinLiveUser(mArrayList,position);
                        break;

                }
            }
        });
        recyclerViewLiveUsers.setAdapter(adapter1);

    }

    private void performJoinLiveUser(ArrayList<LiveUsersModel.Data> mArrayList, int position) {
        if (isNetworkAvailable(getContext())) {
            position = position - 1;
            DivineRayPrefernces.writeString(getContext(), DivineRayPrefernces.STREAM_USERID, mArrayList.get(position).getUser_id());
            DivineRayPrefernces.writeString(getContext(), DivineRayPrefernces.STREAM_ROOMID, mArrayList.get(position).getStream_room());
            DivineRayPrefernces.writeString(getContext(), DivineRayPrefernces.STREAM_USERNAME, mArrayList.get(position).getName());
            DivineRayPrefernces.writeString(getContext(), DivineRayPrefernces.STREAM_ID, mArrayList.get(position).getStream_id());
            DivineRayPrefernces.writeString(getContext(), DivineRayPrefernces.STREAM_USERPHOTO, mArrayList.get(position).getPhoto());
            DivineRayPrefernces.writeString(getContext(), DivineRayPrefernces.COMMENT_DISABLE, mArrayList.get(position).getDisable_comment());
            DivineRayPrefernces.writeString(getContext(), DivineRayPrefernces.LIKE_DISABLE, mArrayList.get(position).getDisable_like());
            gotoRoleActivity(mArrayList.get(position).getStream_room(), mArrayList.get(position).getUser_id());
        }
        else
        {
            showAlertDialog2((Activity) getContext(),getResources().getString(R.string.internet_conection));
        }
   }


    public void gotoRoleActivity(String stream_room, String userID) {
        int mil= (int) (System.currentTimeMillis()/1000L);
        // Intent intent = new Intent(mActivity, LiveStreaming2Activity.class);
        String room = String.valueOf(mil);
      //  config().setChannelName(stream_room);
        ConstantsA.channelNameA=stream_room;
       // Toast.makeText(getContext(), ""+userID, Toast.LENGTH_SHORT).show();
      //  config().setUid(getUserID());
        ConstantsA.useridA=getUserID();
        onJoinAsAudience();
       // onJoinAsBroadcaster();
    }
    public void onJoinAsBroadcaster() {
        gotoLiveActivity(Constants.CLIENT_ROLE_BROADCASTER);
    }
    public void onJoinAsAudience() {
        gotoLiveActivity(Constants.CLIENT_ROLE_AUDIENCE);
    }

    private void gotoLiveActivity(int role) {
        Intent intent = new Intent();
        intent.putExtra(ConstantsA.KEY_CLIENT_ROLE, role);
        intent.setClass(getContext(), LiveStreaming2Activity.class);
        startActivity(intent);
    }

}