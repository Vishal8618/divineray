package com.divineray.app.SelecteUserMore;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.AppointmentBookingModel;
import com.divineray.app.model.Requestappointmentmodel;
//import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
//import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.google.android.material.bottomsheet.BottomSheetDialog;



import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AppointmentBooking extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    Activity mActivity = AppointmentBooking.this;

    @BindView(R.id.appointmentbooking_backbutton)
    ImageView im_back;

    @BindView(R.id.lly_appointment_nothing)
    LinearLayout lly_appointment_nothing;
    @BindView(R.id.appointmentbooking_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.p_bar_appointmentbooking_)
    ProgressBar p_bar;
    @BindView(R.id.lly_appointment_new)
    LinearLayout lly_appointmentnew;
    @BindView(R.id.appointment_time)
    TextView tx_appointmenttime;
//    @BindView(R.id.appointmentbooking_swiperefresh)
//    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.lly_appointment_booking)
    LinearLayout tx_appointmentbook;
    @BindView(R.id.appoint_result_text)
    TextView tx_result;
    @BindView(R.id.txDetails)
    TextView txDetails;
    String durationVal="1";
    int mYear,mMonth,mDay;

   LinearLayout duration1,duration2,duration3,duration4,duration5,duration6,durationcancel;

    List<AppointmentBookingModel.Data> mArrayList = new ArrayList<>();
    String other_user_id="";
    RecyclerviewAppointmentadapter adapter;


    SimpleDateFormat simpleDateFormat,simpleTimeFormat,simpleDateOnlyFormat;
    String cd="Date & Time";
    Date s;
    String stringDate="";
   String stringTime="";
   String stringDate2="";
    BottomSheetDialog bottomSheetDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_appointment_booking);
        ButterKnife.bind(this);
        String text = "<font color=#E84558 size=14><b>Note: </b></font> <font color=#C4000000>The suffix </font> <font color=#C4000000><b>\"Psychic\" </b></font>  <font color=#C4000000> stands for - Psychic readings, and </font>" + "<font color=#C4000000><b>\"Spiritual\" </b> </font><font color=#C4000000> stands for - Spiritual healing </font>";
        txDetails.setText(Html.fromHtml(text));

        this.simpleDateFormat = new SimpleDateFormat("EEE d MMM HH:mm", Locale.getDefault());
        this.simpleTimeFormat = new SimpleDateFormat("hh:mm aa", Locale.getDefault());

        this.simpleDateOnlyFormat = new SimpleDateFormat("EEE d MMM", Locale.getDefault());

        Intent intent =getIntent();
        if (intent!=null)
        {
            other_user_id=intent.getStringExtra("other_user_id");
        }

       // executeApi();
        lly_appointmentnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenDialog();
            }
        });



        im_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//
        Date currentDate = new Date(System.currentTimeMillis()-1000);
        SimpleDateFormat DateFor11 = new SimpleDateFormat("yyyy-M-d");
        SimpleDateFormat DateFor12 = new SimpleDateFormat("d MMMM, yyyy");

        stringDate = DateFor11.format(currentDate);
        stringDate2 = DateFor12.format(currentDate);
                executeApi();
//            }
//        });




        tx_appointmentbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(AppointmentBooking.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int yeare,
                                                  int monthOfYeare, int dayOfMonthr) {
                                int year = view.getYear();
                                int month = view.getMonth();
                                int day = view.getDayOfMonth();

                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, month, day);
//
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-M-d");
                                SimpleDateFormat DateFor12 = new SimpleDateFormat("d MMMM, yyyy");
                                 stringDate = format.format(calendar.getTime());
                                stringDate2=DateFor12.format(calendar.getTime());

                           executeApi2();
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis()-1000);
                datePickerDialog.show();


            }


        });
    }



    /*
     * Execute api
     * */
    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("RequestAppointmentid",other_user_id);
        mMap.put("appointment_date",stringDate);
        mMap.put("appointment_time",stringTime);
        mMap.put("appointment_duration",durationVal);
        Log.e("Datak", "" + mMap.toString());
        return mMap;
    }

    private void executeBookingApi() {

        showProgressDialog(mActivity);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.bookAppointment(getAuthToken(),mParams2()).enqueue(new Callback<Requestappointmentmodel>() {
            @Override
            public void onResponse(Call<Requestappointmentmodel> call, Response<Requestappointmentmodel> response) {
                dismissProgressDialog();

                Log.e("", "**RESPONSE**" + response.body());
                Requestappointmentmodel mModel = response.body();
                //assert mGetDetailsModel != null;
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                  showAlertDialog(mActivity,mModel.getMessage());

                } else if (response.body().getStatus().equals("0")) {
                    showAlertDialog(mActivity,mModel.getMessage());
                    // Toast.makeText(getActivity(), "" + mGetDetailsModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Requestappointmentmodel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void OpenDialog() {
     //   Toast.makeText(mActivity, "You have selected duartion: "+durationVal, Toast.LENGTH_SHORT).show();
            Calendar calendar = Calendar.getInstance(Locale.getDefault());

            calendar.set(Calendar.HOUR_OF_DAY, Calendar.HOUR);
            calendar.set(Calendar.MINUTE, Calendar.MINUTE);

            final Date defaultDate = calendar.getTime();
            Date currentDate = new Date(System.currentTimeMillis()-1000);


//             Sin= new SingleDateAndTimePickerDialog.Builder(AppointmentBooking.this)
//                    .setTimeZone(TimeZone.getDefault())
//                    .bottomSheet()
//                    .minutesStep(1000)
//                    .minDateRange(new Date(System.currentTimeMillis()))
//                    .defaultDate(new Date(System.currentTimeMillis()))
//                    .titleTextColor(Color.RED)
//                    .backgroundColor(Color.WHITE)
//                    .mainColor(Color.RED)
//
//                    .displayMinutes(false)
//                    .displayHours(false)
//                    .displayDays(true)
//                    // .displayMonth(true)
//                    //.displayYears(true)
//
//                    .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
//                        @Override
//                        public void onDisplayed(SingleDateAndTimePicker picker) {
//
//                        }
//                    })
//                    .title("  Date   ")
//                    .listener(new SingleDateAndTimePickerDialog.Listener() {
//                        @Override
//                        public void onDateSelected(Date date) {
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                            cd=simpleDateFormat.format(date);
//                             s=date;
//
//                            SimpleDateFormat DateFor1 = new SimpleDateFormat("h:mm a");
//
//                            stringDate = DateFor1.format(s);
//                            stringDate2=DateFor1.format(s);
//                            tx_appointmenttime.setText(stringDate);
//
//                            SimpleDateFormat DateFor11 = new SimpleDateFormat("yyyy-M-d");
//                            SimpleDateFormat DateFor2 = new SimpleDateFormat("h:mm a");
//                            SimpleDateFormat DateFor12 = new SimpleDateFormat("d MMMM, yyyy");
//                            stringDate = DateFor11.format(s);
//                            stringDate2=DateFor12.format(s);
//                            stringTime = DateFor2.format(s);
//                            executeApi();
//
//                          //  tx_appointmentbook.setText("Submit");
//                                }
//                            },2000);
//                        }
//                    });
//
//            singleBuilder.display();

        }
    


    public void showDatePickerDialog(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("profileUserId",other_user_id);
        mMap.put("appointment_date",stringDate);
        mMap.put("timezone", String.valueOf(TimeZone.getDefault().getID()));
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }



    /*
     * Execute api
     * */
    private Map<String, String> mParams0() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("profileUserId",other_user_id);
        mMap.put("appointment_date","");
        mMap.put("timezone", String.valueOf(TimeZone.getDefault().getID()));
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeApi() {

        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getAllAppointment(getAuthToken(),mParams0()).enqueue(new Callback<AppointmentBookingModel>() {
            @Override
            public void onResponse(Call<AppointmentBookingModel> call, Response<AppointmentBookingModel> response) {
                dismissProgressDialog();
                //swipeRefreshLayout.setRefreshing(false);
                Log.e("", "**RESPONSE**" + response.body());
                AppointmentBookingModel mModel = response.body();
                //assert mGetDetailsModel != null;
                assert mModel != null;
                try {
                    if (mModel.getStatus()==1) {
                        recyclerView.setVisibility(View.VISIBLE);
                        lly_appointment_nothing.setVisibility(View.GONE);
                        mArrayList =response.body().getData();
                        Log.e("Tesr",""+response.body().getData().toString());
                        if (mArrayList!=null){
                            setAdapter();
                        }

                    } else if (response.body().getStatus()==0) {
                        recyclerView.setVisibility(View.GONE);
                        lly_appointment_nothing.setVisibility(View.VISIBLE);
                        tx_result.setText(response.body().getMessage());

                        // Toast.makeText(getActivity(), "" + mGetDetailsModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    showAlertDialog(mActivity,"Server error");
                }


            }

            @Override
            public void onFailure(Call<AppointmentBookingModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }


    private void executeApi2() {
        showProgressDialog(mActivity);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getAllAppointment(getAuthToken(),mParams()).enqueue(new Callback<AppointmentBookingModel>() {
            @Override
            public void onResponse(Call<AppointmentBookingModel> call, Response<AppointmentBookingModel> response) {
                dismissProgressDialog();
                //swipeRefreshLayout.setRefreshing(false);
                Log.e("", "**RESPONSE**" + response.body());
                AppointmentBookingModel mModel = response.body();
                //assert mGetDetailsModel != null;
                assert mModel != null;
                if (mModel.getStatus()==1) {
                    recyclerView.setVisibility(View.VISIBLE);
                    lly_appointment_nothing.setVisibility(View.GONE);
                    mArrayList =response.body().getData();
                    Log.e("Tesr",""+response.body().getData().toString());
                    if (mArrayList!=null){
                        setAdapter();
                    }

                } else if (response.body().getStatus()==0) {
                    recyclerView.setVisibility(View.GONE);
                    lly_appointment_nothing.setVisibility(View.VISIBLE);
                    tx_result.setText(stringDate2+" has no booking slots, Please choose another date!");

                    // Toast.makeText(getActivity(), "" + mGetDetailsModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AppointmentBookingModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }
    private void setAdapter() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerviewAppointmentadapter(mActivity, (ArrayList<AppointmentBookingModel.Data>) mArrayList,stringDate,other_user_id,stringDate2);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
        Date date2 = new Date(year, month, dayOfMonth-1);
        String dayOfWeek = simpledateformat.format(date2);
        String date = "month/day/year: " + month + "/" + dayOfMonth + "/" + year;
        Log.e("Check",date+" day: "+dayOfWeek);
    }



}