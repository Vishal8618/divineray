package com.divineray.app.SelecteUserMore;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.divineray.app.Utils.ExpandableHeightGridView;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.GetAllGiftModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GiftItem extends BaseActivity {
    Activity mActivity = GiftItem.this;
    @BindView(R.id.giftitem_backbutton)
    ImageView giftitem_backbutton;
    @BindView(R.id.girftitem_gridView)
    ExpandableHeightGridView simpleGrid;
    @BindView(R.id.lly_grid_gift)
    LinearLayout lly_grid;
    List<GetAllGiftModel.Data> mArrayList = new ArrayList<>();
    String other_user_id="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTransparentStatusBarOnly(this);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_gift_item);
        ButterKnife.bind(this);
        Intent intent =getIntent();
        if (intent!=null)
        {
            other_user_id=intent.getStringExtra("other_user_id");
        }
        simpleGrid.setFastScrollEnabled(true);
        simpleGrid.setExpanded(true);
        executeApi();
    }



    @OnClick({R.id.giftitem_backbutton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.giftitem_backbutton:
                back();
                break;
        }}

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("lastId","");
        mMap.put("perPage","100");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeApi() {
        showProgressDialog(mActivity);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getAllGifts(getAuthToken(),mParams()).enqueue(new Callback<GetAllGiftModel>() {
            @Override
            public void onResponse(Call<GetAllGiftModel> call, Response<GetAllGiftModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                GetAllGiftModel mModel = response.body();
                //assert mGetDetailsModel != null;
                if( mModel != null){
                if (mModel.getStatus()==1) {
Log.e("Testr","1");
                    mArrayList =response.body().getData();
                    if (mArrayList!=null){
                        setAdapter();
                    }
//                    if (mArrayList.size()<12)
//                    {
//                        lly_grid.setMinimumHeight(800);
//                    }
                } else if (response.body().getStatus()==0) {
                    Log.e("Testr","1");
                    // Toast.makeText(getActivity(), "" + mGetDetailsModel.getMessage(), Toast.LENGTH_SHORT).show();
                }}
            }

            @Override
            public void onFailure(Call<GetAllGiftModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Testr","fail");
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setAdapter() {
        GiftItemAdapter adapter = new GiftItemAdapter(mActivity,other_user_id, mArrayList);
        simpleGrid.setAdapter(adapter);
    }


    private void back() {
        finish();
    }
}