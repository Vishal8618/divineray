package com.divineray.app.SelecteUserMore;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.divineray.app.BuyCoin.BuyCoins;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.ShareItem.ShareItemActivity;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.BuyCoinTotalModel;
import com.divineray.app.model.GetAllGiftModel;
import com.divineray.app.model.ShareItemModel;
import com.ligl.android.widget.iosdialog.IOSDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GiftItemAdapter extends BaseAdapter {
    Activity mActivity;
    ArrayList<GetAllGiftModel.Data> mArrayList = new ArrayList<>();
    TextView tx_bucointotal;
    String other_user_id,gift_id;
    Dialog progressDialog;

    public GiftItemAdapter(Activity mActivity, String other_user_id, List<GetAllGiftModel.Data> mArrayList) {
        this.mActivity=mActivity;
        this.other_user_id=other_user_id;
        this.mArrayList= (ArrayList<GetAllGiftModel.Data>) mArrayList;
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mActivity).inflate(R.layout.share_gift_item, parent, false);
        }

        GetAllGiftModel.Data mModel = mArrayList.get(position);
        ImageView img = (ImageView) convertView.findViewById(R.id.bucoin_item_image);
        TextView tx_item_coin=convertView.findViewById(R.id.bucoin_item_coin);
        TextView tx_item_price=convertView.findViewById(R.id.bucoin_item_price);
        TextView tx_buycoin_item=convertView.findViewById(R.id.bucoin_item_buy);

        Glide.with(mActivity).load(mModel.getImage()).placeholder(R.drawable.bitcoin_blackbg).into(img);
        String strScore=mModel.getCoins();
        String coin=strScore.replaceFirst(".*?(\\d+).*", "$1");
        tx_item_coin.setText(coin+" Coins");
        tx_item_price.setVisibility(View.GONE);
        tx_buycoin_item.setText("Share Gift");


        tx_buycoin_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                gift_id=mModel.getId();
              //  executeApi();
            funDelalertDialog();
            }
        });
 return convertView;

    }

    void  funDelalertDialog()
    {
        new IOSDialog.Builder(mActivity)
                .setMessage("Are you sure want to share gift ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        executeApi();
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("shareTo",other_user_id);
        mMap.put("gift_id",gift_id);

        return mMap;
    }

    private void executeApi() {
        String authtoken= DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.AUTHTOKEN,null);

        Log.e("Check", "**PARAM**" + mParams().toString());
        showProgressDialog(mActivity);
       ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.shareGift(authtoken,mParams()).enqueue(new Callback<ShareItemModel>() {
            @Override
            public void onResponse(Call<ShareItemModel> call, Response<ShareItemModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                ShareItemModel mModel = response.body();
                //assert mGetDetailsModel != null;
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    showAlertDialog(mActivity,mModel.getMessage());
                } else if (response.body().getStatus().equals("0")) {
                    showAlertDialog(mActivity,mModel.getMessage());
               }
                else
                {
                    showAlertDialog22(mActivity,mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ShareItemModel> call, Throwable t) {
                dismissProgressDialog();
              showAlertDialog(mActivity,t.getMessage());
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }


    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Context mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();


    }
    public void showAlertDialog22(Context mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mActivity, BuyCoins.class);
                mActivity.startActivity(intent);
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
