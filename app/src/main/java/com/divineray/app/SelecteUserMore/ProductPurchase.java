package com.divineray.app.SelecteUserMore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.divineray.app.Chat.ChatActivity;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Search.RecyclerViewAdapter;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.activities.LoginActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.FavModel;
import com.divineray.app.model.ProductListingModel;
import com.divineray.app.model.RoomModel;
import com.divineray.app.model.SearchModelget;
import com.ligl.android.widget.iosdialog.IOSDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductPurchase extends BaseActivity {

    @BindView(R.id.im_productpurchase_back)
    ImageView im_productpurchase_back;
    @BindView(R.id.recycler_productPurchase)
    RecyclerView recycler_productPurchase;
    @BindView(R.id.lly_products_nothing)
    LinearLayout lly_products_nothing;

    ProductPurchaseAdapter productPurchaseAdapter;
    Activity mActivity = ProductPurchase.this;
    List<ProductListingModel.Data> mArrayList = new ArrayList<>();
    String other_user_id,profilename;
    String message="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTransparentStatusBarOnly(this);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_product_purchase);
        ButterKnife.bind(this);
        Intent intent =getIntent();
        if (intent!=null)
        {
            other_user_id=intent.getStringExtra("other_user_id");
            profilename=intent.getStringExtra("username");

        }
        if (!isNetworkAvailable(mActivity)) {
            Toast.makeText(mActivity, "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
            executeApi();
        }

    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("profileUserId",other_user_id );
        mMap.put("lastId","");
        mMap.put("perPage","100");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void executeApi() {

        showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getProductPurchase(getAuthToken(),mParams()).enqueue(new Callback<ProductListingModel>() {
            @Override
            public void onResponse(Call<ProductListingModel> call, Response<ProductListingModel> response) {
                dismissProgressDialog();
                Log.e("CC", "**RESPONSE**" + response.body());
                ProductListingModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {
                    mArrayList =response.body().getData();
                    if (mArrayList!=null){
                        setAdapter();
                    }

                } else {
                    lly_products_nothing.setVisibility(View.VISIBLE);
                    recycler_productPurchase.setVisibility(View.GONE);
                    //Toast.makeText(getActivity(), "Error Getting daa", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ProductListingModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setAdapter() {
        Collections.sort( mArrayList, new Comparator<ProductListingModel.Data>() {
            @Override
            public int compare(ProductListingModel.Data o1, ProductListingModel.Data o2) {
                int a=Integer.parseInt(o2.getCreateDate());
                int b=Integer.parseInt(o1.getCreateDate());

                return Integer.compare(a,b);
            }

        } );
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_productPurchase.setLayoutManager(layoutManager);
        productPurchaseAdapter = new ProductPurchaseAdapter(mActivity, other_user_id,(ArrayList<ProductListingModel.Data>) mArrayList,new ProductPurchaseAdapter.OnItemClickListener()
        {

            @Override
            public void onItemClick(int positon, ProductListingModel.Data item, View view) {
                if (view.getId()==R.id.product_purchase_item_buy)
                {
                    funDelalertDialog(item);
                }
            }

        });
        recycler_productPurchase.setAdapter(productPurchaseAdapter);
    }
    void  funDelalertDialog(ProductListingModel.Data item)
    {
        new IOSDialog.Builder(mActivity)
                .setMessage("Are you sure want to purchase this product ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                       String message1="I want to buy this product - "+item.getProductTitle();

                        RoomApiFetch(message1);

                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                }).show();
    }

    @OnClick(R.id.im_productpurchase_back)
    public  void onViewClicked(View view)
    {
        switch (view.getId()) {
            case R.id.im_productpurchase_back:
                onBackPress();
        }
    }




    /*
     * Execute api
     * */
    private Map<String, String> mParamsR() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("chatUserId", other_user_id);
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void RoomApiFetch(String message2) {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.checkRoom(getAuthToken(),mParamsR()).enqueue(new Callback<RoomModel>() {
            @Override
            public void onResponse(Call<RoomModel> call, Response<RoomModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                RoomModel mGetDetailsModel = response.body();
                if (mGetDetailsModel != null) {
                    if (mGetDetailsModel.getStatus() == 1) {
                        String roomId = mGetDetailsModel.getData().get(0).getRoomId();
                        String chatUserId = mGetDetailsModel.getData().get(0).getChatUserId();
                        // Toast.makeText(mActivity, ""+, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(mActivity, ChatActivity.class);
                        intent.putExtra("roomId", roomId);
                        intent.putExtra("message", message2);
                        intent.putExtra("chatUserId", chatUserId);
                        intent.putExtra("chatuserName", profilename);
                        startActivity(intent);

                    } else {
                        Toast.makeText(mActivity, "Already Followed", Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<RoomModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }
    private void onBackPress() {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}