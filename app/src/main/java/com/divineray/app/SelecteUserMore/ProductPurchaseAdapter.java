package com.divineray.app.SelecteUserMore;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.divineray.app.Home.VideoAdapter;
import com.divineray.app.R;
import com.divineray.app.Search.RecyclerViewAdapter;
import com.divineray.app.StringChange.StringFormatter;
import com.divineray.app.model.HomeModel;
import com.divineray.app.model.ProductListingModel;
import com.divineray.app.model.SearchModelget;

import java.util.ArrayList;

public class ProductPurchaseAdapter  extends RecyclerView.Adapter<ProductPurchaseAdapter.ViewHolder> {
    Context context;
    ArrayList<ProductListingModel.Data> mArrayList=new ArrayList<>();
    private ProductPurchaseAdapter.OnItemClickListener listener;
    public interface OnItemClickListener {
        void onItemClick(int positon,ProductListingModel.Data item, View view);
    }

    public ProductPurchaseAdapter(Context context, String other_user_id, ArrayList<ProductListingModel.Data> mArrayList, ProductPurchaseAdapter.OnItemClickListener listener) {
        this.context=context;
        this.mArrayList=mArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ProductPurchaseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_purchase_item, parent, false);
        return new ProductPurchaseAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductPurchaseAdapter.ViewHolder holder, int position) {
        ProductListingModel.Data mModel = mArrayList.get(position);
        holder.bind(position,mModel,listener);
        Glide.with(context).load(mModel.getProductImage()).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.imageView);

        holder.tx_productName.setText(StringFormatter.capitalizeWord(mModel.getProductTitle()));
        String input=mModel.getProductDescription();
        String output = input.substring(0, 1).toUpperCase() + input.substring(1);
        holder.tx_productDescription.setText(output);

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView tx_productName,tx_productDescription,button_productbuy;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.product_purchase_item_image);
            tx_productName=itemView.findViewById(R.id.product_purchase_item_title);
            tx_productDescription=itemView.findViewById(R.id.product_purchase_item_description);
            button_productbuy=itemView.findViewById(R.id.product_purchase_item_buy);
        }

        public void bind(int position, ProductListingModel.Data mModel, OnItemClickListener listener) {
            button_productbuy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });
        }
    }
}
