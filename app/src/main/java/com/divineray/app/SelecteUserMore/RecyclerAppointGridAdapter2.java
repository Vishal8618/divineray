package com.divineray.app.SelecteUserMore;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.divineray.app.BuyCoin.BuyCoins;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.Utils.ExpandableHeightGridView;
import com.divineray.app.fonts.TextViewMedium;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.AppointmentBookingModel;
import com.divineray.app.model.GetAllGiftModel;
import com.divineray.app.model.Requestappointmentmodel;
import com.ligl.android.widget.iosdialog.IOSDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerAppointGridAdapter2 extends BaseAdapter {
    ArrayList<AppointmentBookingModel.Dataf> mArrayList;
    Context mContext;

    String stringDate;
    String slot_id;
    String other_user_id;
    String name, email;
    Dialog progressDialog;

    public RecyclerAppointGridAdapter2(ArrayList<AppointmentBookingModel.Dataf> data2, Context mContext, String dateTime, String other_user_id) {
        this.mArrayList=data2;
        this.mContext=mContext;

        this.stringDate=dateTime;
        this.other_user_id=other_user_id;
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.appointment_booking_item2, parent, false);
        }

        AppointmentBookingModel.Dataf mModel=mArrayList.get(position);

        ImageView img= (ImageView) convertView.findViewById(R.id.appointment_item_image);
        TextView tx_item_coin = convertView.findViewById(R.id.appointment_item_coin);
        TextView tx_item_price = convertView.findViewById(R.id.appointment_timing);
        TextView tx_buycoin_item = convertView.findViewById(R.id.appointment_item_buy);
        TextView tx_timeZone = convertView.findViewById(R.id.appointment_timezone);
        LinearLayout llyAppointmType=convertView.findViewById(R.id.llyAppointmType);
        TextView txType=convertView.findViewById(R.id.txType);
        llyAppointmType.setVisibility(View.VISIBLE);
        if (mModel.getTitle()!=null&&!mModel.getTitle().equals(""))
        {
            llyAppointmType.setVisibility(View.VISIBLE);
            if (mModel.getTitle().length()>20) {
                txType.setText(mModel.getTitle().substring(0,15)+"...");
            }
            else
            {
                txType.setText(mModel.getTitle());
            }
        }
        else
        {

        if (mModel.getAdvisory()!=null&& !mModel.getAdvisory().equals(""))
        {
            llyAppointmType.setVisibility(View.VISIBLE);
            if (mModel.getAdvisory().toLowerCase().equals("p"))
            {
                txType.setText(R.string.physchic);
            }
            else if (mModel.getAdvisory().toLowerCase().equals("s"))
            {
                txType.setText(R.string.spritiual);
            }
            else
            {
                txType.setText(mModel.getAdvisory());
            }
        }
        else
        {
            llyAppointmType.setVisibility(View.GONE);
        }



            //  llyAppointmType.setVisibility(View.GONE);

        }
        llyAppointmType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mModel.getTitle().length()>20) {
                    showAlertDialog3(mContext, mModel.getTitle());
                }
            }
        });
       // if (mModel.getAdvisory()!=null&&mModel.getAdvisory().length()>0) {
        tx_timeZone.setText(mModel.getTimeZone());

        Glide.with(mContext).load(R.drawable.bg_appoint).into(img);
        String strScore = mModel.getAppointmentRate();
        String coin = strScore.replaceFirst(".*?(\\d+).*", "$1");
        tx_item_coin.setText(strScore + " "+ "Coins");
        tx_item_price.setText(mModel.getStartTime() +"\n"+ mModel.getEndTime());
        tx_buycoin_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                slot_id=mModel.getId();
               funDelalertDialog();
            }
        });
//        Toast.makeText(mContext, ""+mModel.toString(), Toast.LENGTH_SHORT).show();

        return convertView;

    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mContext,DivineRayPrefernces.USERID,null));
        mMap.put("slot_id",slot_id);
        mMap.put("RequestAppointmentid",other_user_id);
        mMap.put("appointment_date",stringDate);
        mMap.put("name",name);
        mMap.put("email",email);
        Log.e("Datak", "" + mMap.toString());
        return mMap;
    }

    private void executeBookingApi() {
        String authtoken= DivineRayPrefernces.readString(mContext,DivineRayPrefernces.AUTHTOKEN,null);

        showProgressDialog(mContext);
        Log.e("Datay",""+mParams2());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.bookAppointment(authtoken,mParams2()).enqueue(new Callback<Requestappointmentmodel>() {
            @Override
            public void onResponse(Call<Requestappointmentmodel> call, Response<Requestappointmentmodel> response) {
                dismissProgressDialog();

                Log.e("", "**RESPONSE**" + response.body());
                Requestappointmentmodel mModel = response.body();
                //assert mGetDetailsModel != null;
                if (mModel.getStatus()!=null)
                {
                if (mModel.getStatus().equals("1")) {
                    showAlertDialog(mContext,mModel.getMessage());
                } else if (response.body().getStatus().equals("0")) {
                    showAlertDialog(mContext,mModel.getMessage());
                    // Toast.makeText(getActivity(), "" + mGetDetailsModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else  {
                    showAlertDialog22(mContext,mModel.getMessage());
                    // Toast.makeText(getActivity(), "" + mGetDetailsModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
                
                }
            }

            @Override
            public void onFailure(Call<Requestappointmentmodel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void showAlertDialog2(Context mContext, String message) {
    }

    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog(Context mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();


    }
    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
    /*
     *
     *  Alert Dialog
     * */
    public void showAlertDialog(Context mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     *
     * Alert Dialog2
     * */
    public void showAlertDialog22(Context mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext, BuyCoins.class);
                mContext.startActivity(intent);
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


    /*
     *
     * Alert Dialog2
     * */
    public void showAlertDialog3(Context mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert2);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }





    void  funDelalertDialog()
    {
        new IOSDialog.Builder(mContext)
                .setTitle("Divine Ray")
                .setMessage(R.string.message)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       showAlertDialogCustom((Activity) mContext);
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                }).show();
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialogCustom(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert_appointment);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        EditText editUserNameET = alertDialog.findViewById(R.id.editUserNameET);
        EditText editUserEmailET = alertDialog.findViewById(R.id.editUserEmailET);
        EditText editUserConfrimEmailET = alertDialog.findViewById(R.id.editUserConfirmEmailET);
        TextView txCancel = alertDialog.findViewById(R.id.txCancel);
        TextView txSubmit = alertDialog.findViewById(R.id.txSubmit);

        txCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        txSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editUserNameET.getText().toString().trim().equals("")) {
                    showToast(mActivity, mActivity.getString(R.string.please_enter__name));
                }
                else if (editUserEmailET.getText().toString().trim().equals("")) {
                    showToast(mActivity, mActivity.getString(R.string.please_enter_email_address));
                } else if (!isValidEmaillId(editUserEmailET.getText().toString().trim())) {
                    showToast(mActivity, mActivity.getString(R.string.please_enter_valid_email_address));

                }
                else if (editUserConfrimEmailET.getText().toString().trim().equals("")) {
                    showToast(mActivity, "Please confirm you email address");
                } else if (!isValidEmaillId(editUserConfrimEmailET.getText().toString().trim())) {
                    showToast(mActivity, mActivity.getString(R.string.please_enter_valid_email_address));

                }
                else if (!editUserEmailET.getText().toString().matches(editUserConfrimEmailET.getText().toString())) {
                    showToast(mActivity, "Email address not matched");

                }
                else
                {
                    name=editUserNameET.getText().toString().trim();
                    email=editUserEmailET.getText().toString().trim();
                    executeBookingApi();
                    alertDialog.dismiss();
                }
            }
        });




        alertDialog.show();
    }

    public boolean isValidEmaillId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

        public  void showToast(Activity activity, String messsage)
        {
            Toast.makeText(activity, messsage, Toast.LENGTH_SHORT).show();
        }

}
