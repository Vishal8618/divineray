package com.divineray.app.SelecteUserMore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.divineray.app.R;
import com.divineray.app.Search.RecyclerViewAdapter;
import com.divineray.app.Search.RecyclerViewAdapter2;
import com.divineray.app.Utils.ExpandableHeightGridView;
import com.divineray.app.model.AppointmentBookingModel;
import com.divineray.app.model.SearchModelget;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RecyclerviewAppointmentadapter  extends RecyclerView.Adapter<RecyclerviewAppointmentadapter.ViewHolder> {

    Context mContext;
    ArrayList<AppointmentBookingModel.Data> mArrayList;
    String dateTime,dateTime2;
    String other_user_id;



    public RecyclerviewAppointmentadapter(Context context, ArrayList<AppointmentBookingModel.Data> mArrayList, String stringDate, String other_user_id, String stringDate2) {
        this.mContext=context;
        this.mArrayList=mArrayList;


        this.dateTime=stringDate;
        this.other_user_id=other_user_id;
        this.dateTime2=stringDate2;
    }



    @NonNull
    @Override
    public RecyclerviewAppointmentadapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.appointmentbook_item1, parent, false);
        return new RecyclerviewAppointmentadapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerviewAppointmentadapter.ViewHolder holder, int position) {
        AppointmentBookingModel.Data  mModel = mArrayList.get(position);
        holder.tx_tagtitle.setText(mModel.getTitle());

//        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
//        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//        holder.recyclerViewHorizontal.setLayoutManager(layoutManager);
//        RecyclerviewAppointmentadapter2 horizontalAdapter = new RecyclerviewAppointmentadapter2((ArrayList<AppointmentBookingModel.Dataf>) mModel.getData2(), mContext,dateTime,other_user_id);
//        holder.recyclerViewHorizontal.setAdapter(horizontalAdapter);
        DateFormat originalFormat = new SimpleDateFormat("EEEE dd MMM, yyyy");
        DateFormat targetFormat = new SimpleDateFormat("yyyy-M-d");
        Date date = null;
        try {
           date = originalFormat.parse(mModel.getTitle());

        } catch (ParseException e) {
            e.printStackTrace();

        }
        String formattedDate = targetFormat.format(date);

        //  @SuppressLint("SimpleDateFormat") String  newDate = new SimpleDateFormat("yyyy-M-d").format(mMod2222el.getTitle());

        holder.simpleGrid.setFastScrollEnabled(true);
        holder.simpleGrid.setExpanded(true);
        RecyclerAppointGridAdapter2 adapter = new  RecyclerAppointGridAdapter2((ArrayList<AppointmentBookingModel.Dataf>) mModel.getData2(), mContext,formattedDate,other_user_id);
        holder.simpleGrid.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return mArrayList== null ? 0 : mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tx_tagtitle;
        RecyclerView recyclerViewHorizontal;
        ExpandableHeightGridView simpleGrid;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tx_tagtitle=itemView.findViewById(R.id.appointmentbooking_item_headtext);
            recyclerViewHorizontal=itemView.findViewById(R.id.appointmentbooking_recycler2);
            simpleGrid=itemView.findViewById(R.id.appointment_gridView);
        }
    }
}
