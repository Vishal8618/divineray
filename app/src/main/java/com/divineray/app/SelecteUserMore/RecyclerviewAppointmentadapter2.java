package com.divineray.app.SelecteUserMore;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Search.RecyclerViewAdapter2;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.Utils.ExpandableHeightGridView;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.AppointmentBookingModel;
import com.divineray.app.model.Requestappointmentmodel;
import com.ligl.android.widget.iosdialog.IOSDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerviewAppointmentadapter2 extends RecyclerView.Adapter<RecyclerviewAppointmentadapter2.ViewHolder> {

    ArrayList<AppointmentBookingModel.Dataf> mArrayList;
    Context mContext;

    String stringDate;
    String slot_id;
    String other_user_id;
    Dialog progressDialog;
    String email,name;

    public RecyclerviewAppointmentadapter2(ArrayList<AppointmentBookingModel.Dataf> data2, Context mContext,  String dateTime, String other_user_id) {

        this.mArrayList=data2;
        this.mContext=mContext;

        this.stringDate=dateTime;
        this.other_user_id=other_user_id;
    }

    @NonNull
    @Override
    public RecyclerviewAppointmentadapter2.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.appointment_booking_item, parent, false);
        return new RecyclerviewAppointmentadapter2.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerviewAppointmentadapter2.ViewHolder holder, int position) {
        AppointmentBookingModel.Dataf mModel=mArrayList.get(position);
        Glide.with(mContext).load(R.drawable.bg_appoint).into(holder.img);
        String strScore = mModel.getAppointmentRate();
        String coin = strScore.replaceFirst(".*?(\\d+).*", "$1");
        holder.tx_item_coin.setText(coin + " "+ "Coins");
        holder.tx_item_price.setText(mModel.getStartTime() +"\n"+ mModel.getEndTime());
        holder.tx_buycoin_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                slot_id=mModel.getId();
            funDelalertDialog();
            }
        });

    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mContext,DivineRayPrefernces.USERID,null));
        mMap.put("slot_id",slot_id);
        mMap.put("RequestAppointmentid",other_user_id);
        mMap.put("appointment_date",stringDate);
        mMap.put("name",name);
        mMap.put("email",email);
        mMap.put("appointment_date",stringDate);
        Log.e("Datak", "" + mMap.toString());
        return mMap;
    }

    private void executeBookingApi() {
        String authtoken= DivineRayPrefernces.readString(mContext,DivineRayPrefernces.AUTHTOKEN,null);

        showProgressDialog(mContext);
Log.e("Datay",""+mParams2());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.bookAppointment(authtoken,mParams2()).enqueue(new Callback<Requestappointmentmodel>() {
            @Override
            public void onResponse(Call<Requestappointmentmodel> call, Response<Requestappointmentmodel> response) {
                dismissProgressDialog();

                Log.e("", "**RESPONSE**" + response.body());
                Requestappointmentmodel mModel = response.body();
                //assert mGetDetailsModel != null;
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    showAlertDialog(mContext,mModel.getMessage());
                } else if (response.body().getStatus().equals("0")) {
                    showAlertDialog(mContext,mModel.getMessage());
                    // Toast.makeText(getActivity(), "" + mGetDetailsModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Requestappointmentmodel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }
    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog(Context mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();


    }
    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Context mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    void  funDelalertDialog()
    {
        new IOSDialog.Builder(mContext)
                .setMessage("Are you sure, want to book appointment ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       showAlertDialogCustom((Activity) mContext);
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                }).show();
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        ExpandableHeightGridView simpleGrid;
        TextView tx_item_coin,tx_item_price,tx_buycoin_item;
        public ViewHolder(@NonNull View convertView) {
            super(convertView);

           img= (ImageView) convertView.findViewById(R.id.appointment_item_image);
             tx_item_coin = convertView.findViewById(R.id.appointment_item_coin);
            tx_item_price = convertView.findViewById(R.id.appointment_timing);
           tx_buycoin_item = convertView.findViewById(R.id.appointment_item_buy);
            simpleGrid = convertView.findViewById(R.id.appointment_gridView);
        }
    }
    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialogCustom(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert_appointment);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        EditText editUserNameET = alertDialog.findViewById(R.id.editUserNameET);
        EditText editUserEmailET = alertDialog.findViewById(R.id.editUserEmailET);
        TextView txCancel = alertDialog.findViewById(R.id.txCancel);
        TextView txSubmit = alertDialog.findViewById(R.id.txSubmit);

        txCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        txSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editUserNameET.getText().toString().trim().equals("")) {
                    showAlertDialog(mActivity, mActivity.getString(R.string.please_enter__name));
                }
               else if (editUserEmailET.getText().toString().trim().equals("")) {
                    showAlertDialog(mActivity, mActivity.getString(R.string.please_enter_email_address));
                } else if (!isValidEmaillId(editUserEmailET.getText().toString().trim())) {
                    showAlertDialog(mActivity, mActivity.getString(R.string.please_enter_valid_email_address));

                }
               else
                {
                    name=editUserNameET.getText().toString().trim();
                    email=editUserEmailET.getText().toString().trim();
                    executeBookingApi();
                    alertDialog.dismiss();
                }
            }
        });




        alertDialog.show();
    }

    public boolean isValidEmaillId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

}
