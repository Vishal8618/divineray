package com.divineray.app.SelecteUserMore;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.divineray.app.BuyCoin.BuyCoins;
import com.divineray.app.Chat.ChatActivity;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.CheckModel;
import com.divineray.app.model.RoomModel;
import com.divineray.app.model.VideoAudioModel;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViddeoAudio extends BaseActivity  implements Player.EventListener {
    Activity mActivity = ViddeoAudio.this;
    RecyclerView rcv_video_audio;

    @BindView(R.id.p_bar_video_audio)
    ProgressBar p_bar;
    @BindView(R.id.swiperefresh_video_audio)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.viddaudio_backbutton)
    ImageView im_back;
    @BindView(R.id.lly_videoaudio_nothing)
    LinearLayout lly_nothing;
    @BindView(R.id.video_audio_recycler)
    RecyclerView recyclerView;
    String downloadUrl;
    boolean check = true;
    boolean is_visible_to_user;
    SimpleExoPlayer player;
    boolean is_user_stop_video = false;
    ArrayList<VideoAudioModel.Data> data_list;
    Thread thread;
    View previous_view;
    static boolean active = false;
    LinearLayoutManager layoutManager;
    VideoAudioAdaper adapter;
    String mYoutubeLink,newLink;
    String other_user_id,profilename;
    String mediaId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_viddeo_audio);
        ButterKnife.bind(this);
        Intent intent =getIntent();
        if (intent!=null)
        {
            other_user_id=intent.getStringExtra("other_user_id");
            profilename=intent.getStringExtra("username");

        }
        //  recyclerView.setHasFixedSize(true);
        im_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // player.setPlayWhenReady(false);
                finish();
            }
        });

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                doApiCall();
            }
        });

        doApiCall();

        /**
         * add scroll listener while user reach in bottom load more will call
         */

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //    adapter.BackP_videoAudio();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(this, DivineRayPrefernces.USERID, null));
        mMap.put("profileUserId",other_user_id );
        Log.e("aa", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void doApiCall() {

        showProgressDialog(mActivity);
        final ArrayList<VideoAudioModel.Data> items = new ArrayList<>();
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mApiInterface.getVideoAudio(getAuthToken(),mParams()).enqueue(new Callback<VideoAudioModel>() {
            @Override
            public void onResponse(Call<VideoAudioModel> call, Response<VideoAudioModel> response) {
                swiperefresh.setRefreshing(false);
                final ArrayList<VideoAudioModel.Data> items = new ArrayList<>();
                Log.e("", "**RESPONSE**" + response.body());
                dismissProgressDialog();


                VideoAudioModel mHomeModel = response.body();

      if (response.body().getStatus()==1) {
                    lly_nothing.setVisibility(View.GONE);
                    data_list = (ArrayList<VideoAudioModel.Data>) response.body().getData();
                    setAdapter();



                } else if (response.body().getStatus()==0){
                    Log.e("Test", "" + response.body().getMessage());
                    lly_nothing.setVisibility(View.VISIBLE);

                }


                Log.e("", "**RESPONSE**" + response.body());
            }


            @Override
            public void onFailure(Call<VideoAudioModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("test","failure");
                // Toast.makeText(getActivity(), "Errror"+t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });

    }
    public static String extractYoutubeVideoId(String ytUrl) {

        String vId = null;

        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(ytUrl);

        if(matcher.find()){
            vId= matcher.group();
        }
        return vId;
    }
    private void setAdapter() {
        layoutManager = new LinearLayoutManager(ViddeoAudio.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new VideoAudioAdaper(getApplicationContext(), data_list, new VideoAudioAdaper.OnItemClickListener() {
            @Override
            public void onItemClick(int positon, VideoAudioModel.Data item, View view) {
                if (view.getId() == R.id.viddeoaudio_play||view.getId()==R.id.video_audio_buy_button) {
                    if (item.getIsPurchased().equals("1")) {
                        View layout = layoutManager.findViewByPosition(positon);
                        YouTubePlayerView youTubePlayerView = layout.findViewById(R.id.youtubePlayerView);
                        layout.findViewById(R.id.viddeoaudio_play).setVisibility(View.GONE);
                        layout.findViewById(R.id.video_audio_thumbnail).setVisibility(View.GONE);
                        layout.findViewById(R.id.exo_pause).setVisibility(View.GONE);

                        //      mYoutubeLink=item.getMedia();
                        if (!item.getMedia_link().equals("") && !(item.getMedia_link() == null)) {
                            StopPlaying();
                            layout.findViewById(R.id.viddeoaudio_play).setVisibility(View.VISIBLE);
                            Intent webIntent = new Intent(Intent.ACTION_VIEW,
                                    Uri.parse(item.getMedia_link()));
                            try {
                                startActivity(webIntent);
                            } catch (ActivityNotFoundException ex) {
                            }
                        } else {

                            if (thread != null && !thread.isAlive()) {

                                StopPlaying();

                                playaudio(view, item, positon);
                            } else if (thread == null) {
                                StopPlaying();
                                playaudio(view, item, positon);
                            }
                            Log.e("Testg", "Initial:" + mYoutubeLink);

                        }
                    }
                    else {
                        mediaId=item.getId();
                       funDelalertDialog(positon);
                    }
                }
//               else if (view.getId()==R.id.video_audio_buy_button){
//                    RoomApiFetch();
//                }
            }
        });
        recyclerView.setAdapter(adapter);
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParamsR() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("chatUserId", other_user_id);
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void RoomApiFetch() {
        String authtoken= DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.AUTHTOKEN,null);

        showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.checkRoom(authtoken,mParamsR()).enqueue(new Callback<RoomModel>() {
            @Override
            public void onResponse(Call<RoomModel> call, Response<RoomModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());
                RoomModel mGetDetailsModel = response.body();
                String roomId=mGetDetailsModel.getData().get(0).getRoomId();
                String chatUserId=mGetDetailsModel.getData().get(0).getChatUserId();
                if (mGetDetailsModel.getStatus() == 1) {
                    // Toast.makeText(mActivity, ""+, Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(mActivity, ChatActivity.class);
                    //  Toast.makeText(mActivity, ""+chatUserId, Toast.LENGTH_SHORT).show();
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("roomId",roomId);

                    intent.putExtra("chatUserId",chatUserId);
                    intent.putExtra("chatuserName",profilename);
                    startActivity(intent);

                } else {
                    Toast.makeText(mActivity, "Already Followed", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<RoomModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }
    public void StopPlaying() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.removeListener(this);
            player.release();
        }


    }


    private void extracting(String url) {
        Log.e("Test","3"+url);
    }

    public void playaudio(View view, final VideoAudioModel.Data item, int positon) {


        previous_view = view;
        View view2 = LayoutInflater.from(mActivity).inflate(R.layout.video_audio_item, null);
        View layout = layoutManager.findViewByPosition(positon);
        layout.findViewById(R.id.prbar_videoaudio).setVisibility(View.VISIBLE);
        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
        player = ExoPlayerFactory.newSimpleInstance(mActivity, trackSelector);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(mActivity,
                Util.getUserAgent(mActivity, "DivineRay"));
String val=item.getMedia();

        if (val.contains("http"))
        {
            val.replace("http","https");
        }
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(val));
        player.prepare(videoSource);
        player.addListener((Player.EventListener) this);

        TextView textView = layout.findViewById(R.id.video_audio_description);

        final PlayerView playerView = layout.findViewById(R.id.video_audio_playerview);
        // playerView.setKeepContentOnPlayerReset(true);
        playerView.setPlayer(player);

        player.setPlayWhenReady(true);

       // layout.findViewById(R.id.prbar_videoaudio).setVisibility(View.VISIBLE);
        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }


            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {

                    case Player.STATE_BUFFERING:

                       // layout.findViewById(R.id.video_audio_playerview).setVisibility(View.GONE);
                        layout.findViewById(R.id.prbar_videoaudio).setVisibility(View.VISIBLE);
                        break;
                    case Player.STATE_IDLE:
                        layout.findViewById(R.id.video_audio_thumbnail).setVisibility(View.VISIBLE);
                        layout.findViewById(R.id.viddeoaudio_play).setVisibility(View.VISIBLE);
                        break;

                    case Player.STATE_READY:

                        layout.findViewById(R.id.video_audio_playerview).setVisibility(View.VISIBLE);
                        layout.findViewById(R.id.video_audio_thumbnail).setVisibility(View.GONE);
                        layout.findViewById(R.id.prbar_videoaudio).setVisibility(View.GONE);
                        break;
                    case Player.STATE_ENDED:
                        layout.findViewById(R.id.video_audio_playerview).setVisibility(View.VISIBLE);
                        layout.findViewById(R.id.video_audio_thumbnail).setVisibility(View.INVISIBLE);
                    default:
                        //  layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }

        });



    }




    @Override
    public void onStop() {
        super.onStop();
        active=false;



        if(player!=null){
            player.setPlayWhenReady(false);
            player.removeListener(this);
            player.release();
        }



    }
    void  funDelalertDialog(int positon)
    {
        new IOSDialog.Builder(this)
                .setTitle("Divine Ray")
                .setMessage(R.string.videoAudioInsufficient)
                .setPositiveButton("Buy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        doMediaBuyApi(positon);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                }).show();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(this, DivineRayPrefernces.USERID, null));
        mMap.put("id",mediaId );
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void doMediaBuyApi(int positon) {
        String authtoken= DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.AUTHTOKEN,null);

        showProgressDialog(mActivity);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.buyMedia(authtoken,mParams2()).enqueue(new Callback<CheckModel>() {
            @Override
            public void onResponse(Call<CheckModel> call, Response<CheckModel> response) {
                dismissProgressDialog();
                CheckModel mModel = response.body();

                if (mModel.getStatus()==1) {
                    showAlertDialog(mActivity,mModel.getMessage());
                    View layout=layoutManager.findViewByPosition(positon);
                    layout.findViewById(R.id.video_audio_buy_button).setVisibility(View.GONE);
                    data_list.get(positon).setIsPurchased("1");
                    adapter.notifyDataSetChanged();
                }
                else if (mModel.getStatus()==0){
                   showAlertDialog(mActivity,mModel.getMessage());

                }
                else
                {
                showAlertDialog22(mActivity,mModel.getMessage());
                }



            }


            @Override
            public void onFailure(Call<CheckModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("test","failure");
                // Toast.makeText(getActivity(), "Errror"+t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });

    }
    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog22(Context mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), BuyCoins.class);
                startActivity(intent);
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

}