package com.divineray.app.SelecteUserMore;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.divineray.app.R;
import com.divineray.app.StringChange.StringFormatter;
import com.divineray.app.model.VideoAudioModel;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;

import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoAudioAdaper  extends RecyclerView.Adapter<VideoAudioAdaper.CustomViewHolder > {
Context context;
    ArrayList<VideoAudioModel.Data> data_list;
    boolean is_user_stop_video=false;
    private VideoAudioAdaper.OnItemClickListener listener;
    SimpleExoPlayer player;
    String formattedDate="";

    DisplayMetrics displayMetrics = new DisplayMetrics();

    public interface OnItemClickListener {
        void onItemClick(int positon, VideoAudioModel.Data item, View view);
    }
    public VideoAudioAdaper(Context context, ArrayList<VideoAudioModel.Data> data_list, OnItemClickListener listener) {
       this.context=context;
        this.data_list=data_list;
        this.listener = (VideoAudioAdaper.OnItemClickListener) listener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_audio_item,null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        VideoAudioAdaper.CustomViewHolder viewHolder = new VideoAudioAdaper.CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        final VideoAudioModel.Data item= data_list.get(position);
        try {
            Log.e("test",item.toString());
            String strScore=item.getCoins();
            String coin=strScore.replaceFirst(".*?(\\d+).*", "$1");
           //  holder.video_audio_coins.setText(String.valueOf(coin)+" Coins");
           holder.video_audio_coins.setText(item.getCoins());
            holder.bind(position,item,listener);
            holder.video_audio_time.setVisibility(View.GONE);
            holder.video_audio_time.setText(item.getMediaDuration());
            if (item.getIsPurchased()!=null&&item.getIsPurchased().equals("1"))
            {
                holder.video_audio_buybutton.setVisibility(View.GONE);
            }
            else {
                holder.video_audio_buybutton.setVisibility(View.VISIBLE);
            }
           // .setText(item.getPrice());
            String val=item.getThumbImage();
            if (val.contains("http"))
            {
                val.replace("http","https");
            }
            Log.e("newCheck",val);
            //String newval=val.replace("http","https");
           Glide.with(context).load(val).into(holder.video_audio_thumbnail);
            holder.video_audio_title.setText(StringFormatter.capitalizeWord(item.getTitle()));
           String input=item.getDescription();
           String output = input.substring(0, 1).toUpperCase() + input.substring(1);
            holder.video_audio_description.setText(output);
            holder.video_audio_thumbnail.setVisibility(View.VISIBLE);


          //  context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int width = displayMetrics.widthPixels;










        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public void StopPlaying() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.removeListener((Player.EventListener) context);
            player.release();
        }



    }
public  void BackP_videoAudio()
{
    if (player!=null) {
        player.setPlayWhenReady(false);
        player.release();
    }


    }
    void timeChange(long coment_time)
    {
        Date date = new java.util.Date(coment_time * 1000L);

// the format of your date
        //SimpleDateFormat sdf = new java.text.SimpleDateFormat("MM-dd"+","+" hh:mm a ");
        SimpleDateFormat sdf = new java.text.SimpleDateFormat(" hh:mm");
// give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(TimeZone.getDefault());

        formattedDate = sdf.format(date);
    }

    @Override
    public int getItemCount() {

        return data_list.size();
    }




    public class CustomViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.video_audio_title)
        TextView video_audio_title;
        @BindView(R.id.video_audio_buy_button)
        TextView video_audio_buybutton;
        @BindView(R.id.video_audio_coins)
        TextView video_audio_coins;
        @BindView(R.id.video_audio_description)
        TextView video_audio_description;
        @BindView(R.id.video_audio_thumbnail)
        ImageView video_audio_thumbnail;
        @BindView(R.id.viddeoaudio_play)
        ImageView viddeoaudio_play;
        @BindView(R.id.youtubePlayerView)
        YouTubePlayerView youTubePlayerView;

        @BindView(R.id.video_audio_time)
        TextView video_audio_time;
        @BindView(R.id.video_audio_playerview)
        PlayerView video_audio_playerView;
        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        public void bind(int position, VideoAudioModel.Data item, OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,item,v);
                }
            });
            viddeoaudio_play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,item,v);
                }
            });
            video_audio_buybutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,item,v);
                }
            });

        }
    }
}
