package com.divineray.app.Settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.divineray.app.R;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactUs extends BaseActivity {
    Activity mActivity=ContactUs.this;
    @BindView(R.id.web1)
    WebView webView;
    @BindView(R.id.txHeading)
    TextView txHeading;
    @BindView(R.id.lly_backbutton)
    LinearLayout lly_backbutton;

    String pageType;
    String pageText="";
    String val;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        Intent intent=getIntent();
        pageType=intent.getStringExtra("pageType");
        if (intent.getStringExtra("pageText")!=null)
        {
          pageText= intent.getStringExtra("pageText");
          txHeading.setText(pageText);
        }
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
       //webView.setWebViewClient(new WebViewClient());
       /// Toast.makeText(mActivity, ""+val, Toast.LENGTH_SHORT).show();


        if (pageType.equals("privacypolicy"))
        {
            webView.loadUrl("https://divineray.io/PrivacyAndPolicy.html");
        }
        else if (pageType.equals("terms"))
        {
            webView.loadUrl("https://divineray.io/TermsOfService.html");
        }

        else if (pageType.equals("my_store"))
        {

            val = DivineRayPrefernces.readString(mActivity, DivineRayPrefernces.SECURITYTOKEN, null);
            Log.e("link",val);
            val=val.replace("https","http");
            webView.loadUrl(val);
            finish();
        }
        else if (pageType.equals("aboutus"))
        {
            webView.loadUrl("https://divineray.io");
        }
        else
        {
            webView.loadUrl("https://divineray.io");
        }

    }

    @OnClick({R.id.lly_backbutton})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case  R.id.lly_backbutton:
                performCLick();
                break;

        }
    }

    private void performCLick() {
        finish();
    }
}