package com.divineray.app.Settings;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.divineray.app.BuyCoin.BuyCoins;
import com.divineray.app.Notifications.GetAllNotifications;
import com.divineray.app.Profile.EditProfileActivity;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.activities.LoginActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.LogoutModel;
import com.divineray.app.model.NottificationModel;
import com.facebook.login.LoginManager;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.suke.widget.SwitchButton;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.luongvo.widget.iosswitchview.SwitchView;

public class SettingActivity extends BaseActivity {
    String TAG = SettingActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = SettingActivity.this;

    @BindView(R.id.lly_profile_edit)
    LinearLayout lly_profile_edit;
    @BindView(R.id.profile_setting_back_s)
    LinearLayout profile_setting_back;
    @BindView(R.id.settings_logout)
    TextView settings_logout;
    @BindView(R.id.lly_buycoins_setting)
    LinearLayout lly_buycoins_setting;
    @BindView(R.id.settings_privacypolicy)
    LinearLayout lly_privacypolicy;
    @BindView(R.id.settings_aboutus)
    LinearLayout lly_aboutus;
    @BindView(R.id.settings_cotactus)
    LinearLayout lly_contactus;
    @BindView(R.id.settings_termsofuse)
    LinearLayout lly_termsofuse;
    @BindView(R.id.settings_mystore)
    LinearLayout lly_store;
    @BindView(R.id.imNotification)
    ImageView imNotification;
    @BindView(R.id.notificationsSB)
    SwitchButton switchView;
    @BindView(R.id.tx_notification)
    TextView tx_notification;
    @BindView(R.id.llySwitch)
    LinearLayout llySwitch;
    int allowNotification;
    String strDeviceToken="";
    String val;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        val = DivineRayPrefernces.readString(mActivity, DivineRayPrefernces.PUSHNOTIFICATION, null);
       if (val!=null&&val.equals("1"))
        {
            //llySwitch.performClick();
            switchView.performClick();
          //switchView.performLongClick();
        }
        else {
            switchView.setChecked(false);
        }

        lly_profile_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lly_profile_edit.setClickable(false);
                Intent intent=new Intent(mActivity, EditProfileActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        lly_profile_edit.setClickable(true);
                    }
                },1500);



            }
        });



        profile_setting_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profile_setting_back.setClickable(false);
               finish();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        profile_setting_back.setClickable(true);

                    }
                },1500);

            }
        });
        settings_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settings_logout.setClickable(false);
                if (!isNetworkAvailable(mActivity)) {
                    showAlertDialog(mActivity ,getString(R.string.internet_connection_error));
                } else {


                    funDelalertDialog();
                }
            }
        });

        imNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imNotification.setClickable(false);
                Intent intent = new Intent(mActivity, GetAllNotifications.class);
                startActivity(intent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        imNotification.setClickable(true);
                    }
                },1500);

            }
        });
        switchView.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if (isChecked)
                {
                    allowNotification=1;
                    executePush();
                }
                else
                {
                    allowNotification=0;
                    executePush();
                }
            }
        });


        lly_buycoins_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lly_buycoins_setting.setClickable(false);
                Intent intent=new Intent(mActivity, BuyCoins.class);
                startActivity(intent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lly_buycoins_setting.setClickable(true);

                    }
                },1500);


            }
        });
        tx_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tx_notification.setClickable(true);
                Intent intent=new Intent(mActivity, GetAllNotifications.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        tx_notification.setClickable(true);

                    }
                },1500);

            }
        });
lly_aboutus.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        lly_aboutus.setClickable(true);
        Intent intent=new Intent(mActivity, ContactUs.class);
        intent.putExtra("pageType","aboutus");
        intent.putExtra("pageText","About Us");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                lly_aboutus.setClickable(true);

            }
        },1500);

    }
});
        lly_store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lly_store.setClickable(true);
                Intent intent=new Intent(mActivity, ContactUs.class);
                intent.putExtra("pageType","my_store");
                intent.putExtra("pageText","My Store");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        lly_store.setClickable(true);

                    }
                },1500);

            }
        });

        lly_privacypolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lly_privacypolicy.setClickable(true);
                Intent intent=new Intent(mActivity, ContactUs.class);
                intent.putExtra("pageType","privacypolicy");
                intent.putExtra("pageText","Privacy Policy");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        lly_privacypolicy.setClickable(true);
                    }
                },1500);

            }
        });

        lly_termsofuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lly_termsofuse.setClickable(false);
                Intent intent=new Intent(mActivity, ContactUs.class);
                intent.putExtra("pageType","terms");
                intent.putExtra("pageText","Terms of Use");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        lly_termsofuse.setClickable(true);
                    }
                },1500);

            }
        });

        lly_contactus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lly_contactus.setClickable(false);
                Intent intent=new Intent(mActivity, ContactUs.class);
                intent.putExtra("pageType","default");
                intent.putExtra("pageText","Contact Us");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        lly_contactus.setClickable(true);
                    }
                },1500);

            }
        });



    }



    /*
     * Execute api
     * */
    private Map<String, String> mParam0() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id",  DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("allowPush", String.valueOf(allowNotification));
        mMap.put("device_type",DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.DEVICETYPE,null));
        mMap.put("device_token",DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.DEVICETOKEN,null));

        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void executePush() {

        // showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.pushNotification(getAuthToken(),mParam0()).enqueue(new Callback<NottificationModel>() {
            @Override
            public void onResponse(Call<NottificationModel> call, Response<NottificationModel> response) {
                DivineRayPrefernces.writeString(mActivity,DivineRayPrefernces.PUSHNOTIFICATION, String.valueOf(allowNotification));
                Log.e(TAG, "base_64_api_success" + response.body().toString());
                Log.e("teste",""+mParam0());
                dismissProgressDialog();
                NottificationModel mModel = response.body();
                // Toast.makeText(mActivity, ""+response.body(), Toast.LENGTH_SHORT).show();

                if (mModel.getStatus()==1) {

                    //showAlertDialog(mActivity, mModel.getMessage());


                } else  {
                 //   showAlertDialog(mActivity, mModel.getMessage());

                }

            }

            @Override
            public void onFailure(Call<NottificationModel> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(mActivity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

           }





    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    void  funDelalertDialog()
    {

        new IOSDialog.Builder(this)
                .setMessage("Are you sure you want to log out ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            allowNotification=2;
                            executePush();

                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                        finally {
                            executeLogoutApi();
                        }


                        dialogInterface.dismiss();
                        settings_logout.setClickable(true);
                        LoginManager.getInstance().logOut();
                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                        settings_logout.setClickable(true);
                    }
                }).show();
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity, DivineRayPrefernces.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLogoutApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.logoutRequest(getAuthToken(),mParam()).enqueue(new Callback<LogoutModel>() {
            @Override
            public void onResponse(Call<LogoutModel> call, Response<LogoutModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                LogoutModel mModel = response.body();
                if (mModel.getStatus().equals(1)) {
                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.LOGOUT_ID, mModel.getLogoutId());
                    SharedPreferences preferences = DivineRayPrefernces.getPreferences(Objects.requireNonNull(mActivity));
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                    editor.apply();
                    mActivity.onBackPressed();

                    Intent mIntent = new Intent(mActivity, LoginActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<LogoutModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        val = DivineRayPrefernces.readString(mActivity, DivineRayPrefernces.PUSHNOTIFICATION, null);

//        if (val!=null&&val.equals("1"))
//        {
//            switchView.performClick();
//            switchView.setChecked(true);
//        }
//        else {
//            switchView.setChecked(false);
//        }

    }
}