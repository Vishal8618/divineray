package com.divineray.app.ShareItem;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.GetAllUserModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShareItemActivity extends BaseActivity {
    List<GetAllUserModel.Data> mArrayList = new ArrayList<>();
    //private SearchViewModel mViewModel;
    public Dialog progressDialog;
    RecyclerView recyclerView;
    SearchView searchView;


    @BindView(R.id.search_toolbar)
    Toolbar search_toolbar;
    @BindView(R.id.text_no_result_shareitemtab)
    TextView text_no_result;
    @BindView(R.id.swiperefresh_shareitemtab)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.p_bar_shareitemtab)
    ProgressBar p_bar;
    String search_name="";

    EditText ed_searchBar;
    TextView search_button;

    ShareItemAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_share_item);
        setSupportActionBar((Toolbar) search_toolbar);
        ButterKnife.bind(this);
        recyclerView = findViewById(R.id.shareitemtab_recycler);


        ed_searchBar = search_toolbar.findViewById(R.id.search_text);
        search_button=search_toolbar.findViewById(R.id.searchtab_search_button);

        swiperefresh.setColorSchemeResources(R.color.black);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getDetails();
            }
        });

        getDetails();


        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_name=ed_searchBar.getText().toString().trim();
                getDetails();
            }
        });

        ed_searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                adapter.getFilter().filter(string);
            }
        });
    }


    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    private void getDetails() {
        if (!isNetworkAvailable(this)) {
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
            executeDetailsApi();
        }
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(this,DivineRayPrefernces.USERID,null));
        mMap.put("search",search_name);
        mMap.put("perPage","10000");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeDetailsApi() {
        showProgressDialog(this);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getUserList(getAuthToken(),mParams()).enqueue(new Callback<GetAllUserModel>() {
            @Override
            public void onResponse(Call<GetAllUserModel> call, Response<GetAllUserModel> response) {
                swiperefresh.setRefreshing(false);
                dismissProgressDialog();
                Log.e("Test","success"+response.body().toString());
                Log.e("", "**RESPONSE**" + response.body());
                GetAllUserModel mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {
                    mArrayList =response.body().getData();
                    if (mArrayList!=null){
                        setSearchAdapter();
                    }

                } else {
                    text_no_result.setVisibility(View.VISIBLE);
                    text_no_result.setText("No Results found for "+"'"+ed_searchBar.getText().toString()+"'");
                }
            }

            @Override
            public void onFailure(Call<GetAllUserModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Test","Failure"+t.getMessage());
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setSearchAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ShareItemAdapter(this, (ArrayList<GetAllUserModel.Data>) mArrayList,text_no_result);
        recyclerView.setAdapter(adapter);
    }
}