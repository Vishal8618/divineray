package com.divineray.app.ShareItem;



import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.divineray.app.Chat.ChatActivity;
import com.divineray.app.R;
import com.divineray.app.model.GetAllUserModel;
import com.divineray.app.model.SearchModelget;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.divineray.app.Search.RecyclerViewAdapter.fixed_tag_title;

public class ShareItemAdapter  extends RecyclerView.Adapter<ShareItemAdapter.ViewHolder> implements Filterable {
    public static String video_tag_title;
    private Context mContext;
    ArrayList<GetAllUserModel.Data> mArrayList = new ArrayList<>();
    ArrayList<GetAllUserModel.Data> FilteredmArrayList =new ArrayList<>();
    TextView text_no_result;
    private final int limit = 10;

    public ShareItemAdapter(Context context, ArrayList<GetAllUserModel.Data> mArrayList, TextView text_no_result) {
        this.mContext=context;
        this.mArrayList=mArrayList;
        this.FilteredmArrayList=mArrayList;
        this.text_no_result=text_no_result;
    }

    @NonNull
    @Override
    public ShareItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.share_item, parent, false);
        return new ShareItemAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShareItemAdapter.ViewHolder holder, int position) {
        //  holder.tag.setImageResource(mImageUrls.get(position).getImgId());
        GetAllUserModel.Data mModel = mArrayList.get(position);
        Glide.with(mContext).load(mModel.getPhoto()).placeholder(R.drawable.unknown_user).into(holder.photo);
        if(mModel.getName().equals("")||mModel.getName()==null)
        {
            holder.tx_username.setText("Unknown");
        }
        else {
            holder.tx_username.setText(mModel.getName());
        }

        holder.tx_lastmessage.setText("DivineRay");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext,ChatActivity.class);
                intent.putExtra("roomId",mModel.getRoomId());
                intent.putExtra("chatUserId",mModel.getUser_id());
                // Toast.makeText(mContext, ""+mModel.getUser_id(), Toast.LENGTH_SHORT).show();
                if (!(mModel.getName().length()==0))
                {
                    intent.putExtra("chatuserName",mModel.getName());

                }
                else {
                    intent.putExtra("chatuserName","Unknown");

                }

                mContext.startActivity(intent);

            }
        });
    }
    //filter method
    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mArrayList=FilteredmArrayList;

                } else {
                    ArrayList<GetAllUserModel.Data> filteredList = new ArrayList<>();
                    for (GetAllUserModel.Data row : FilteredmArrayList) {


                        //change this to filter according to your case
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    mArrayList = (ArrayList<GetAllUserModel.Data>) filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values =mArrayList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mArrayList = (ArrayList) filterResults.values;
                if (mArrayList.size()>0){
                    text_no_result.setVisibility(View.GONE);
                }
                else {

//                    text_no_result.setVisibility(View.VISIBLE);
//                    text_no_result.setText("No Results found for "+"'"+charSequence.toString()+"'");
                }
                notifyDataSetChanged();

            }
        };

    }
    @Override
    public int getItemCount()
    {
        if(mArrayList.size() > limit){
            return limit;
        }
        else
        {
            return mArrayList.size();
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView photo;
        TextView tx_username,tx_lastmessage;
        LinearLayout lly_share;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            photo=itemView.findViewById(R.id.chatf_photo);
            tx_username=itemView.findViewById(R.id.chatf_username);
            tx_lastmessage=itemView.findViewById(R.id.chatf_usermessage);
            lly_share=itemView.findViewById(R.id.share);

        }
    }
}
