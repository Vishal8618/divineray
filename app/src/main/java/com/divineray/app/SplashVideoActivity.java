package com.divineray.app;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.divineray.app.Home.DownloadUtil;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.Constants;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.activities.HomeActivity;
import com.divineray.app.activities.LoginActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.HomeModel;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashVideoActivity extends BaseActivity implements Player.EventListener {

    Activity mActivity=SplashVideoActivity.this;

    @BindView(R.id.playerview)
    PlayerView playerView;
    @BindView(R.id.imClose)
    ImageView imClose;
    @BindView(R.id.logo)
    ImageView imLogo;
    @BindView(R.id.lly)
    LinearLayout lly;
    @BindView(R.id.progressBarWeb)
    ProgressBar progressBar;

    SimpleExoPlayer privious_player;
    boolean is_user_stop_video = false;

    HomeModel mHomeModel;
    SimpleExoPlayer player;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Hide_navigation();
        getSupportActionBar().hide();
        setContentView(R.layout.activity_splash_video);
        ButterKnife.bind(this);
        executeAPI();
        //showCloseButton();


    }


    @Override
    protected void onSaveInstanceState(@NonNull @NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.imClose,R.id.lly})
    public  void  onViewClicked(View view)
    {
        switch (view.getId())
        {
            case  R.id.imClose:
                final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
                imClose.startAnimation(myAnim);
                performSwitch();
                break;
            case  R.id.lly:
               performSwitch();
                break;
        }
    }



    private void executeAPI() {
        if (isNetworkAvailable(mActivity)) {
            executeDataApi();
        }else
        {
            performSwitch();
        }
    }

    private void setPlayer(String promotionVideo) {
        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
       player = ExoPlayerFactory.newSimpleInstance(mActivity, trackSelector);
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(mActivity,
                Util.getUserAgent(mActivity, "DivineRay"));
        CacheDataSourceFactory cacheDataSourceFactory= new CacheDataSourceFactory(DownloadUtil.getCache(mActivity),dataSourceFactory);
        MediaSource videoSource = new ExtractorMediaSource.Factory(cacheDataSourceFactory)
                .createMediaSource(Uri.parse(promotionVideo));
        player.addListener(this);
        player.prepare(videoSource);



        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
        playerView.setPlayer(player);

        lly.setVisibility(View.VISIBLE);
        player.setPlayWhenReady(true);

        privious_player=player;
        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {

                    case Player.STATE_BUFFERING:
                        imLogo.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        break;
                    case Player.STATE_IDLE:
                        imLogo.setVisibility(View.GONE);
                       break;

                    case Player.STATE_READY:
                        imLogo.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        break;
                    case Player.STATE_ENDED:
                        startActivity(new Intent(mActivity, HomeActivity.class));
                        finish();
                        break;


                    default:
                        //  layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });

        SimpleExoPlayer finalPlayer = player;

        playerView.setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(mActivity, new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                    super.onFling(e1, e2, velocityX, velocityY);
                    float deltaX = e1.getX() - e2.getX();
                    float deltaXAbs = Math.abs(deltaX);
                    // Only when swipe distance between minimal and maximal distance value then we treat it as effective swipe
                    if((deltaXAbs > 100) && (deltaXAbs < 1000)) {
                        if(deltaX > 0)
                        {
                            //  OpenProfile(item,true);
                        }
                    }


                    return true;
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    super.onSingleTapUp(e);
                    if(!finalPlayer.getPlayWhenReady()){
                        is_user_stop_video=false;
                        privious_player.setPlayWhenReady(true);
                    }else{
                        is_user_stop_video=true;
                        privious_player.setPlayWhenReady(false);
                    }


                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);


                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {

                    if(!finalPlayer.getPlayWhenReady()){
                        is_user_stop_video=false;
                        privious_player.setPlayWhenReady(true);
                    }

                    return super.onDoubleTap(e);

                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });



    }

    private void performSwitch() {
        onPause();
        if (privious_player!=null)
        {
            privious_player.setPlayWhenReady(false);
            privious_player.stop();


        }

        startActivity(new Intent(mActivity, HomeActivity.class));
        finish();
    }


    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity, DivineRayPrefernces.ID, null));
        mMap.put("lastId", "");
        mMap.put("perPage", "100");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    void executeDataApi() {
        String authtoken= DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.AUTHTOKEN,null);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getVideos(authtoken,mParams()).enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                dismissProgressDialog();
                Log.e("", "**RESPONSE**" + response.body());

//                if (response.body().promotionVideo != null) {
//                   Glide.with(mActivity).load(response.body().promotionVideo).into(imLogo);
//                    setPlayer(response.body().promotionVideo);
//
//                }


                Log.e("", "**RESPONSE**" + response.body());
            }

            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (privious_player!=null)
        {
        privious_player.setPlayWhenReady(false);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

            super.onPause();
            if (privious_player!=null)
            {

                privious_player.setPlayWhenReady(true);
            }
        }



//    private void showCloseButton() {
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//
//            }
//        }, 3000);
//    }

}