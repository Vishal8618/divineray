package com.divineray.app.StringChange;

public class StringFormatter {
    public static String capitalizeWord(String str){
        String words[]=null;
//        if(str!=null && str.equals(""))
//        {
         words=str.split("\\s");
        //}

        String capitalizeWord="";
        if (words.length>0) {
            for (String w : words) {
                String first = w.substring(0, 1);
                String afterfirst = w.substring(1);
                capitalizeWord += first.toUpperCase() + afterfirst + " ";
            }
        }
        else {
            capitalizeWord=str;
        }
        return capitalizeWord.trim();
    }
}
