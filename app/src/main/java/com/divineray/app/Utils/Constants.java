package com.divineray.app.Utils;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.divineray.app.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.agora.rtc.video.BeautyOptions;
import io.agora.rtc.video.VideoEncoderConfiguration;

public class Constants {
    public static final String SERVER_KEY = "AAAARHf1Uow:APA91bEkmIeMI9OT8IJfF1DHtLeglf3T29MEe9exeUY_luouE_80FaKYr9sWwf5A7rv-7tlCWqJwbojU_BfJ3TfDSuAfUZMfxaqU1mQG4Vf1VNdQmpjjxEFkahXp2cdGYArl-dl0DDbH";/*
     * Constants @params
     * */
    public static String device="android";

    public static int screen_width;
    public static int screen_height;
    public static final String TAG = "Utility";
    //Working Socket URL
    public static final String SocketURL = "https://jaohar-uk.herokuapp.com";


    public  static boolean endStreamDone=false;

    public static String SelectedAudio_MP3 ="SelectedAudio.mp3";
    public static String REMOVE_ADD_AUDIO_TO_VIDEO = "-y,-i,%s,-i,%s,-c:v,copy,-map,0:v:0,-map,1:a:0,-c:a,copy,%s";

    public static  String Redirection_type="";
    public static  String Redirection_value="";
    public static String SelectedAudio_AAC ="SelectedAudio.aac";
    public static String root=  Environment.getExternalStorageDirectory().toString() + "/DivineRayVideo/";
    public static String root0=  Environment.getExternalStorageDirectory().toString() + "/DivineRayVideo/"+"DivineVideoCreator/";
    public static String rootD=  Environment.getExternalStorageDirectory().toString() + "/DivineRayVideo/"+"DivineVideoCreator/";
    public static String root2=  Environment.getExternalStorageDirectory().toString() + "/DivineRay";

    public static int max_recording_duration=60000;
    public static int recording_duration=60000;

    public static String outputfile=root0 +"output.mp4";
    public static String outputfile2=root + "output2.mp4";
    public static String output_filter_file=root0+"output-filtered.mp4";

    public static String gallery_trimed_video=root0 +"gallery_trimed_video.mp4";
    public static String gallery_resize_video=root0 +"gallery_resize_video.mp4";

    public static String app_folder=root+"DivineRay/";
    public static String app_folder2=root+".VideoThumbs/";
    public static String app_folder3=root2+"/VideosD/";
    public static String app_folder2Old=root+"VideoThumbs/";
    public static SharedPreferences sharedPreferences;
    public static String pref_name="pref_name";
    public static String u_id="u_id";
    public static String u_name="u_name";
    public static String u_pic="u_pic";
    public static String device_token="device_token";

    public static Dialog determinant_dialog;
    public static ProgressBar determinant_progress;
    public static String musicid="";
    public static String root1=  Environment.getExternalStorageDirectory().toString() + "/";
    public final static int Pick_video_from_gallery=791;
    public static String vid_desc="";
    public static String vid_tags="";
    public static String vid_width="";
    public static String vid_height="";

    public static void Show_determinent_loader(Context context, boolean outside_touch, boolean cancleable) {

        determinant_dialog = new Dialog(context);
        determinant_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        determinant_dialog.setContentView(R.layout.item_determinant_progress_layout);
        determinant_dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.d_round_white_background));

        determinant_progress=determinant_dialog.findViewById(R.id.pbar);

        if(!outside_touch)
            determinant_dialog.setCanceledOnTouchOutside(false);

        if(!cancleable)
            determinant_dialog.setCancelable(false);

        determinant_dialog.show();

    }

    public static void Show_loading_progress(int progress){
        if(determinant_progress!=null ){
            determinant_progress.setProgress(progress);

        }
    }


    public static void cancel_determinent_loader(){
        if(determinant_dialog!=null){
            determinant_progress=null;
            determinant_dialog.cancel();
        }
    }
    public static String tag="divine_";

    public static String Selected_sound_id="null";


    public static String gif_firstpart="https://media.giphy.com/media/";



    public static String user_id;
    public final static int permission_write_data=788;
    public final static int permission_Recording_audio=790;



    public static String generateFilename(String prifix) {
        return prifix +"_"+ String.valueOf(((new Date().getTime())%(1000000000))%1000);
    }

    //AGORA
    private static final int BEAUTY_EFFECT_DEFAULT_CONTRAST = BeautyOptions.LIGHTENING_CONTRAST_NORMAL;
    private static final float BEAUTY_EFFECT_DEFAULT_LIGHTNESS = 0.7f;
    private static final float BEAUTY_EFFECT_DEFAULT_SMOOTHNESS = 0.5f;
    private static final float BEAUTY_EFFECT_DEFAULT_REDNESS = 0.1f;

    public static final BeautyOptions DEFAULT_BEAUTY_OPTIONS = new BeautyOptions(
            BEAUTY_EFFECT_DEFAULT_CONTRAST,
            BEAUTY_EFFECT_DEFAULT_LIGHTNESS,
            BEAUTY_EFFECT_DEFAULT_SMOOTHNESS,
            BEAUTY_EFFECT_DEFAULT_REDNESS);

    public static VideoEncoderConfiguration.VideoDimensions[] VIDEO_DIMENSIONS = new VideoEncoderConfiguration.VideoDimensions[]{
            VideoEncoderConfiguration.VD_320x240,
            VideoEncoderConfiguration.VD_480x360,
            VideoEncoderConfiguration.VD_640x360,
            VideoEncoderConfiguration.VD_640x480,
            new VideoEncoderConfiguration.VideoDimensions(960, 540),
            VideoEncoderConfiguration.VD_1280x720
    };

    public static int[] VIDEO_MIRROR_MODES = new int[]{
            io.agora.rtc.Constants.VIDEO_MIRROR_MODE_AUTO,
            io.agora.rtc.Constants.VIDEO_MIRROR_MODE_ENABLED,
            io.agora.rtc.Constants.VIDEO_MIRROR_MODE_DISABLED,
    };

    public static final String PREF_NAME = "io.agora.openlive";
    public static final int DEFAULT_PROFILE_IDX = 2;
    public static final String PREF_RESOLUTION_IDX = "pref_profile_index";
    public static final String PREF_ENABLE_STATS = "pref_enable_stats";
    public static final String PREF_MIRROR_LOCAL = "pref_mirror_local";
    public static final String PREF_MIRROR_REMOTE = "pref_mirror_remote";
    public static final String PREF_MIRROR_ENCODE = "pref_mirror_encode";

    public static final String KEY_CLIENT_ROLE = "key_client_role";

}