package com.divineray.app.Utils;

import android.app.Application;
import android.content.SharedPreferences;


import com.divineray.app.Agora.rtc.AgoraEventHandler;
import com.divineray.app.Agora.rtc.EngineConfig;
import com.divineray.app.Agora.rtc.EventHandler;
import com.divineray.app.Agora.stats.StatsManager;
import com.divineray.app.Agora.utils.FileUtil;
import com.divineray.app.Agora.utils.PrefManager;
import com.divineray.app.R;

import java.net.URISyntaxException;

import io.agora.rtc.RtcEngine;
import io.socket.client.IO;
import io.socket.client.Socket;

public class DivineRayAppplication extends Application {
    /**
     * Getting the Current Class Name
     */
    public static final String TAG = DivineRayAppplication.class.getSimpleName();
    /**
     * Initialize the Applications Instance
     */
    private static DivineRayAppplication mInstance;

    //Agora
    private RtcEngine mRtcEngine;
    private EngineConfig mGlobalConfig = new EngineConfig();
    private AgoraEventHandler mHandler = new AgoraEventHandler();
    private StatsManager mStatsManager = new StatsManager();


    public static final int CONNECTION_TIMEOUT = 50000 * 1000;//120 Seconds    private RequestQueue mRequestQueue;


    public static synchronized DivineRayAppplication getInstance()
    {
        return mInstance;
    }
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(Constants.SocketURL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Required initialization logic here!
        mInstance = this;
        try {
            mRtcEngine = RtcEngine.create(getApplicationContext(), getString(R.string.private_app_id), mHandler);
            mRtcEngine.setLogFile(FileUtil.initializeLogFile(this));
        } catch (Exception e) {
            e.printStackTrace();
        }

      //  initConfig();
    }

    private void initConfig() {
        SharedPreferences pref = PrefManager.getPreferences(getApplicationContext());
        mGlobalConfig.setVideoDimenIndex(pref.getInt(
                Constants.PREF_RESOLUTION_IDX, Constants.DEFAULT_PROFILE_IDX));

        boolean showStats = pref.getBoolean(Constants.PREF_ENABLE_STATS, false);
        mGlobalConfig.setIfShowVideoStats(showStats);
        mStatsManager.enableStats(showStats);

        mGlobalConfig.setMirrorLocalIndex(pref.getInt(Constants.PREF_MIRROR_LOCAL, 0));
        mGlobalConfig.setMirrorRemoteIndex(pref.getInt(Constants.PREF_MIRROR_REMOTE, 0));
        mGlobalConfig.setMirrorEncodeIndex(pref.getInt(Constants.PREF_MIRROR_ENCODE, 0));
    }

    public EngineConfig engineConfig() {
        return mGlobalConfig;
    }

    public RtcEngine rtcEngine() {
        return mRtcEngine;
    }

    public StatsManager statsManager() {
        return mStatsManager;
    }

    public void registerEventHandler(EventHandler handler) {
        mHandler.addHandler(handler);
    }

    public void removeEventHandler(EventHandler handler) {
        mHandler.removeHandler(handler);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        RtcEngine.destroy();
    }

}
