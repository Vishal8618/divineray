package com.divineray.app.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class DivineRayPrefernces {
    /***************
     * Define SharedPrefrances Keys & MODE
     ****************/
    public static final String PREF_NAME = "App_PREF";
    public static final int MODE = Context.MODE_PRIVATE;

    /*
     * Keys
     * */
    public static final String ID = "user_id";
    public static final String USERID="user_id";
    public static final String ISLOGIN = "is_login";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String NAME = "name";
    public static final String DEVICETYPE = "device_type";
    public static final String DEVICETOKEN = "device_token";
    public static final String CREATED_AT = "created_at";
    public static final String DISABLED = "disabled";
    public static final String FACEBOOK_ID = "facebookId";
    public static final String TWITTER_ID = "twitterId";
    public static final String ACTIVITY_FLAG = "activity_flag";
    public static final String PHOTO = "photo";
    public static final String SECURITYTOKEN = "securitytoken";
    public static final String AUTHTOKEN = "authtoken";
    public static final String STATUS = "status";
    public static final String LOGOUT_ID = "logout_id";
    public static final String ROOM_ID = "room_id" ;
    public static final String FCM_TOKEN ="" ;
    public static final String PUSHNOTIFICATION ="" ;
    public static final String SPLASHVIDEOLINK ="" ;


    //Video upload
    public static final String  URI ="" ;
    public static final String DESC ="" ;
    public static final String TAGV ="" ;
    public static final String VIDEOWIDTH ="" ;
    public static final String VIDEOHEIGHT ="" ;

    //Live Stream
    public static final String STREAM_ROOMID="stream_roomid";
    public static final  String STREAM_USERNAME="stream_username";
    public static final  String STREAM_USERID="stream_userid";
    public static final  String STREAM_ID="stream_id";
    public static final  String STREAM_USERPHOTO="stream_userphoto";
    public static final String COMMENT_DISABLE ="comment_disable" ;
    public static final String LIKE_DISABLE ="like_disable" ;


    /*
     * Write the Boolean Value
     * */
    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).apply();
    }

    /*
     * Read the Boolean Value
     * */
    public static boolean readBoolean(Context context, String key, boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    /*
     * Write the Integer Value
     * */
    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).apply();

    }

    /*
     * Read the Interger Value
     * */
    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    /*
     * Write the String Value
     * */
    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).apply();

    }

    /*
     * Read the String Value
     * */
    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    /*
     * Write the Float Value
     * */
    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).apply();
    }

    /*
     * Read the Float Value
     * */
    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    /*
     * Write the Long Value
     * */
    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).apply();
    }

    /*
     * Read the Long Value
     * */
    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }

    /*
     * Return the SharedPreferences
     * with
     * @Prefreces Name & Prefreces = MODE
     * */
    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    /*
     * Return the SharedPreferences Editor
     * */
    public static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

}
