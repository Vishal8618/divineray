package com.divineray.app.Utils;

public class DivineRaySingleton {


    private static DivineRaySingleton ourInstance;

    private DivineRaySingleton() { }





    public static DivineRaySingleton getInstance() {
        if (ourInstance == null) {
            ourInstance = new DivineRaySingleton();
        }
        return ourInstance;
    }

   
}
