package com.divineray.app.VideoPick;


public interface OnSelectStateListener<T> {
    void OnSelectStateChanged(boolean state, T file);
}
