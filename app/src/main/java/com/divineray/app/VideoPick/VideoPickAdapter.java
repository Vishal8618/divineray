package com.divineray.app.VideoPick;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.coremedia.iso.boxes.Container;
import com.coremedia.iso.boxes.MovieHeaderBox;
import com.divineray.app.R;
import com.divineray.app.Utils.Constants;
import com.divineray.app.Utils.FileUtils;
import com.divineray.app.Utils.Functions;
import com.divineray.app.VideoPickFilter.Util;
import com.divineray.app.VideoPickFilter.entity.VideoFile;
import com.divineray.app.Video_Recording.GallerySelectedVideo.GallerySelectedVideo_A;
import com.divineray.app.Video_Recording.Video_Recorder_A;
import com.divineray.app.model.HomeModel;
import com.googlecode.mp4parser.FileDataSourceImpl;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.CroppedTrack;
import com.googlecode.mp4parser.util.Matrix;
import com.googlecode.mp4parser.util.Path;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.WritableByteChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import static android.os.Environment.DIRECTORY_DCIM;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.divineray.app.VideoPickFilter.Constant.REQUEST_CODE_TAKE_VIDEO;

public class VideoPickAdapter extends BaseAdapter<VideoFile, VideoPickAdapter.VideoPickViewHolder> {
    private boolean isNeedCamera;
    private int mMaxNumber;
    private int mCurrentNumber = 0;
    public String mVideoPath;
    String val;

    public VideoPickAdapter(Context ctx, boolean needCamera, int max,String val) {
        this(ctx, new ArrayList<VideoFile>(), needCamera, max);
        this.val=val;
    }


    public VideoPickAdapter(Context ctx, ArrayList<VideoFile> list, boolean needCamera, int max) {
        super(ctx, list);
        isNeedCamera = needCamera;
        mMaxNumber = max;
    }

    @Override
    public VideoPickAdapter.VideoPickViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.vw_layout_item_video_pick, parent, false);
        ViewGroup.LayoutParams params = itemView.getLayoutParams();
        if (params != null) {
            WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            int width = wm.getDefaultDisplay().getWidth();
            params.height = width / VideoPickActivity.COLUMN_NUMBER;
        }
        return new VideoPickViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final VideoPickViewHolder holder, int position) {
        if (!isNeedCamera) {

            holder.mIvCamera.setVisibility(View.INVISIBLE);
            holder.mIvThumbnail.setVisibility(View.VISIBLE);
            holder.mCbx.setVisibility(View.INVISIBLE);
            holder.mDurationLayout.setVisibility(View.VISIBLE);

            final VideoFile file;
            if (isNeedCamera) {
                file = mList.get(position - 1);
            } else {
                file = mList.get(position);
            }

            RequestOptions options = new RequestOptions();
            Glide.with(mContext)
                    .load(file.getPath())
                    .apply(options.centerCrop())
                    .transition(withCrossFade())
                    .transition(new DrawableTransitionOptions().crossFade(500))
                    .into(holder.mIvThumbnail);

            if (file.isSelected()) {
                holder.mCbx.setSelected(true);
                holder.mShadow.setVisibility(View.VISIBLE);
            } else {
                holder.mCbx.setSelected(false);
                holder.mShadow.setVisibility(View.INVISIBLE);
            }

//            holder.mCbx.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (!v.isSelected() && isUpToMax()) {
//                        ToastUtil.getInstance(mContext).showToast(R.string.vw_up_to_max);
//                        return;
//                    }
//
//                    if (v.isSelected()) {
//                        holder.mShadow.setVisibility(View.INVISIBLE);
//                        holder.mCbx.setSelected(false);
//                        mCurrentNumber--;
//                    } else {
//                        holder.mShadow.setVisibility(View.VISIBLE);
//                        holder.mCbx.setSelected(true);
//                        mCurrentNumber++;
//                    }
//
//                    int index = isNeedCamera ? holder.getAdapterPosition() - 1 : holder.getAdapterPosition();
//                    mList.get(index).setSelected(holder.mCbx.isSelected());
//
//                    if (mListener != null) {
//                        mListener.OnSelectStateChanged(holder.mCbx.isSelected(), mList.get(index));
//                    }
//                }
//            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  Toast.makeText(mContext, "Selected", Toast.LENGTH_SHORT).show();



                 //   try {
//                        Intent intent = new Intent(Intent.ACTION_VIEW);
//                        Uri uri;
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                            File f = new File(file.getPath());
//                            uri = FileProvider.getUriForFile(mContext, mContext.getApplicationContext().getPackageName() + ".provider", f);
//                        }else{
//                            uri = Uri.parse("file://" + file.getPath());
//                        }
                    MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                    Bitmap bmp = null;
                    try {
                        retriever.setDataSource(file.getPath());
                        bmp = retriever.getFrameAtTime();
                        int videoHeight=bmp.getHeight();
                        int videoWidth=bmp.getWidth();

                        Log.d("resp",""+videoWidth+"---"+videoHeight);

                    }
                    catch (Exception e){

                    }


                    if (file.getDuration() < 60000) {
                        File output = new File(Constants.gallery_resize_video);

                        if(output.exists()){
                            output.delete();
                            //   Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT).show();
                        }


                            Chnage_Video_size(file.getPath(), Constants.gallery_resize_video);

                        } else {
                        File output = new File(Constants.gallery_resize_video);

                        if(output.exists()){
                            output.delete();
                            //   Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT).show();
                        }
                            try {
                                startTrim(new File(file.getPath()), new File(Constants.gallery_resize_video), 0, 58200);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

//                    intent.setDataAndType(uri, "video/mp4");
//                    if (Util.detectIntent(mContext, intent)) {
//                        mContext.startActivity(intent);
//                    } else {
//                        Toast.makeText(mContext, "Try Again", Toast.LENGTH_SHORT).show();
//                    }
            }
            });

            holder.mDuration.setText(Util.getDurationString(file.getDuration()));
        }
    }


    public void Chnage_Video_size(String src_path,String destination_path){

        Functions.Show_indeterminent_loader(mContext,false,false);
        File source = new File(src_path);
        File destination = new File(destination_path);
        try {
            if (source.exists()) {

                InputStream in = new FileInputStream(source);
                OutputStream out = new FileOutputStream(destination);

                byte[] buf = new byte[1024];
                int len;

                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }

                in.close();
                out.close();

                Intent intent = new Intent(mContext, GallerySelectedVideo_A.class);
                intent.putExtra("video_path", Constants.gallery_resize_video);
                intent.putExtra("sound_name",val);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
                Functions.cancel_indeterminent_loader();
            } else {
                Toast.makeText(mContext, "Failed to get video from Device", Toast.LENGTH_SHORT).show();
                Functions.cancel_indeterminent_loader();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public  void startTrim(final File src, final File dst, final int startMs, final int endMs) throws IOException {

        new AsyncTask<String,Void,String>() {
            @Override
            protected String doInBackground(String... strings) {
                try {

                    FileDataSourceImpl file = new FileDataSourceImpl(src);
                    Movie movie = MovieCreator.build(file);
                    List<Track> tracks = movie.getTracks();
                    movie.setTracks(new LinkedList<Track>());
                    double startTime = startMs / 1000;
                    double endTime = endMs / 1000;
                    boolean timeCorrected = false;

                    for (Track track : tracks) {
                        if (track.getSyncSamples() != null && track.getSyncSamples().length > 0) {
                            if (timeCorrected) {
                                throw new RuntimeException("The startTime has already been corrected by another track with SyncSample. Not Supported.");
                            }
                            startTime = Functions.correctTimeToSyncSample(track, startTime, false);
                            endTime = Functions.correctTimeToSyncSample(track, endTime, true);
                            timeCorrected = true;
                        }
                    }
                    for (Track track : tracks) {
                        long currentSample = 0;
                        double currentTime = 0;
                        long startSample = -1;
                        long endSample = -1;

                        for (int i = 0; i < track.getSampleDurations().length; i++) {
                            if (currentTime <= startTime) {
                                startSample = currentSample;
                            }
                            if (currentTime <= endTime) {
                                endSample = currentSample;
                            } else {
                                break;
                            }
                            currentTime += (double) track.getSampleDurations()[i] / (double) track.getTrackMetaData().getTimescale();
                            currentSample++;
                        }
                        movie.addTrack(new CroppedTrack(track, startSample, endSample));
                    }

                    Container out = new DefaultMp4Builder().build(movie);
                    MovieHeaderBox mvhd = Path.getPath(out, "moov/mvhd");
                    mvhd.setMatrix(Matrix.ROTATE_180);
                    if (!dst.exists()) {
                        dst.createNewFile();
                    }
                    FileOutputStream fos = new FileOutputStream(dst);
                    WritableByteChannel fc = fos.getChannel();
                    try {
                        out.writeContainer(fc);
                    } finally {
                        fc.close();
                        fos.close();
                        file.close();
                    }

                    file.close();
                    return "Ok";
                }catch (IOException e){

                    return "error";
                }

            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Functions.Show_indeterminent_loader(mContext,true,true);
            }

            @Override
            protected void onPostExecute(String result) {
                if(result.equals("error")){
                    Toast.makeText(mContext, "Try Again", Toast.LENGTH_SHORT).show();
                }else {
                    Functions.cancel_indeterminent_loader();
                    Chnage_Video_size(Constants.gallery_trimed_video, Constants.gallery_resize_video);
                }
            }


        }.execute();

    }
    @Override
    public int getItemCount() {
        return isNeedCamera ? mList.size() + 1 : mList.size();
    }
    public long getfileduration(Uri uri) {
        try {

            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(mContext, uri);
            String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            final int file_duration = Integer.parseInt(durationStr);

            return file_duration;
        }
        catch (Exception e){

        }
        return 0;
    }


    class VideoPickViewHolder extends RecyclerView.ViewHolder {
        private ImageView mIvCamera;
        private ImageView mIvThumbnail;
        private View mShadow;
        private ImageView mCbx;
        private TextView mDuration;
        private RelativeLayout mDurationLayout;

        public VideoPickViewHolder(View itemView) {
            super(itemView);
            mIvCamera = (ImageView) itemView.findViewById(R.id.iv_camera);
            mIvThumbnail = (ImageView) itemView.findViewById(R.id.iv_thumbnail);
            mShadow = itemView.findViewById(R.id.shadow);
            mCbx = (ImageView) itemView.findViewById(R.id.cbx);
            mDuration = (TextView) itemView.findViewById(R.id.txt_duration);
            mDurationLayout = (RelativeLayout) itemView.findViewById(R.id.layout_duration);
        }
    }

    public boolean isUpToMax() {
        return mCurrentNumber >= mMaxNumber;
    }

    public void setCurrentNumber(int number) {
        mCurrentNumber = number;
    }
}

