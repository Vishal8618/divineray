package com.divineray.app.VideoPickFilter;


/**
 * Created by Vincent Woo
 * Date: 2016/10/14
 * Time: 17:20
 */

public class Constant {
    public static final String MAX_NUMBER = "MaxNumber";


    public static final int REQUEST_CODE_PICK_VIDEO = 0x200;
    public static final String RESULT_PICK_VIDEO = "ResultPickVideo";
    public static final int REQUEST_CODE_TAKE_VIDEO = 0x201;

}

