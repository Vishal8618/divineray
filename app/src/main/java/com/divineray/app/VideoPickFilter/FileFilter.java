package com.divineray.app.VideoPickFilter;


import androidx.fragment.app.FragmentActivity;

import com.divineray.app.VideoPickFilter.callback.FileLoaderCallbacks;
import com.divineray.app.VideoPickFilter.callback.FilterResultCallback;
import com.divineray.app.VideoPickFilter.entity.VideoFile;

import static android.media.tv.TvTrackInfo.TYPE_VIDEO;

/**
 * Created by Vincent Woo
 * Date: 2016/10/11
 * Time: 10:19
 */

public class FileFilter {

    public static void getVideos(FragmentActivity activity, FilterResultCallback<VideoFile> callback){
        activity.getSupportLoaderManager().initLoader(1, null,
                new FileLoaderCallbacks(activity, callback, TYPE_VIDEO));
    }


}
