package com.divineray.app.Video_Recording.GallerySelectedVideo;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.GestureDetector;

import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.coremedia.iso.boxes.Container;
import com.daasuu.gpuv.composer.GPUMp4Composer;
import com.divineray.app.Add.AddPostActivityS;
import com.divineray.app.Utils.Functions;
import com.divineray.app.R;
import com.divineray.app.Video_Recording.Merge_Video_Audio;
import com.divineray.app.Video_Recording.PreviewGalleryVideoA;
import com.divineray.app.Video_Recording.Preview_Video_A;
import com.divineray.app.Video_Recording.SoundList_Main_A;
import com.divineray.app.Utils.Constants;
import com.divineray.app.Video_Recording.Video_Recorder_A;
import com.divineray.app.activities.BaseActivity;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.googlecode.mp4parser.authoring.Movie;

import com.ligl.android.widget.iosdialog.IOSDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.divineray.app.Video_Recording.Video_Recorder_A.Sounds_list_Request_code;
import static com.divineray.app.Video_Recording.Video_Recorder_A.chronometer;


public class GallerySelectedVideo_A extends BaseActivity implements View.OnClickListener, Player.EventListener {
    String path;
    TextView add_sound_txt;
    LinearLayout changeSoundLayout, removeSoundLayout, cancelLayout;
    BottomSheetDialog bottomSheetDialog;
    SimpleExoPlayer video_player;
    MediaPlayer audio;
    boolean is_user_stop_video = false;
    Movie[] inMovies=new Movie[]{};
    ArrayList<String> video_list=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_gallery_selected_video);


        Intent intent=getIntent();
        if(intent!=null){
          path=intent.getStringExtra("video_path");
          //  path=Constants.gallery_resize_video;
            String val= intent.getStringExtra("sound_name");
           // Toast.makeText(this, ""+val, Toast.LENGTH_SHORT).show();
            if (val.equals("Add Sound"))
            {
                    File output = new File(Constants.root+"DivineRay/"+Constants.SelectedAudio_AAC);

                    if(output.exists()){
                        output.delete();
                     //   Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT).show();
                    }

                }
        }

        findViewById(R.id.Goback).setOnClickListener(this);

        add_sound_txt=findViewById(R.id.add_sound_txt);
        add_sound_txt.setOnClickListener(this);

        findViewById(R.id.next_btn_gallery).setOnClickListener(this);

        Set_Player();
        if(intent.hasExtra("sound_name")){


            add_sound_txt.setText(intent.getStringExtra("sound_name"));
            Constants.Selected_sound_id=intent.getStringExtra("sound_id");
            PreparedAudio();

        }

    }

    // this will call when swipe for another video and
    // this function will set the player to the current video

    public void Set_Player(){

        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
        video_player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "DivineRay"));

        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(path));

        video_player.prepare(videoSource);

        video_player.setRepeatMode(Player.REPEAT_MODE_OFF);
        video_player.addListener(this);



        final PlayerView playerView=findViewById(R.id.playerview_gal);

        File someFile = new File(path);
        String videfile = "Vid"+someFile;
        getVideoWidthOrHeight(someFile, videfile);
        playerView.setPlayer(video_player);
        playerView.setKeepContentOnPlayerReset(true);
        // Get the video width
        int mVideoWidth = getVideoWidthOrHeight(someFile, "width");
// Get the video height
        int mVideoHeight = getVideoWidthOrHeight(someFile, "height");
        float checkVal=Float.parseFloat(String.valueOf(mVideoHeight));

        playerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });


        video_player.setPlayWhenReady(true);

        playerView.setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {



                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    super.onSingleTapUp(e);
                    if(!video_player.getPlayWhenReady()){
                        is_user_stop_video=false;
                        video_player.setPlayWhenReady(true);
                        if(audio!=null) {
                            audio.start();
                        }
                    }else{
                        if(audio!=null) {
                            audio.pause();
                        }
                        is_user_stop_video=true;
                        video_player.setPlayWhenReady(false);

                    }


                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    //  Show_video_option(item);

                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {

                    if(!video_player.getPlayWhenReady()){
                        is_user_stop_video=false;
                        video_player.setPlayWhenReady(true);
                        if(audio!=null) {
                            audio.start();
                        }
                    }

                    return super.onDoubleTap(e);

                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });


    }

    public int getVideoWidthOrHeight(File file, String widthOrHeight) {
        MediaMetadataRetriever retriever = null;
        Bitmap bmp = null;
        FileInputStream inputStream = null;
        int mWidthHeight = 0;
        try {
            retriever = new  MediaMetadataRetriever();
            inputStream = new FileInputStream(file.getAbsolutePath());
            retriever.setDataSource(inputStream.getFD());
            bmp = retriever.getFrameAtTime();
            if (widthOrHeight.equals("width")){
                mWidthHeight = bmp.getWidth();
            }else {
                mWidthHeight = bmp.getHeight();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally{
            if (retriever != null){
                retriever.release();
            }if (inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return mWidthHeight;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.Goback:
                showDialogd();
               break;


            case R.id.next_btn_gallery:
                //findViewById(R.id.next_btn).setClickable(false);
                if(video_player!=null) {
                    video_player.setPlayWhenReady(false);
                }
                if(audio!=null) {
                    audio.pause();
                }
                showProgressDialog(this);
               if (audio==null)
               {
                   Go_To_preview_Activity();
               }
               else {
                   Merge_withAudio();
               }
                break;
            case R.id.add_sound_txt:
               // createBottomSheetDialog();
                if (add_sound_txt.getText().equals("Add Sound")||add_sound_txt.getText()==null||add_sound_txt.getText().equals("")) {
                    Intent intent2 = new Intent(this, SoundList_Main_A.class);
                    startActivityForResult(intent2, Sounds_list_Request_code);
                    overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top);

                }
                else if (!add_sound_txt.getText().equals("Add Sound")){

                    bottomSheetDialog.show();

                }
                break;



        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==Sounds_list_Request_code){
            if(data!=null){

                if(data.getStringExtra("isSelected").equals("yes")){
                    add_sound_txt.setText(data.getStringExtra("sound_name"));
                   Constants.Selected_sound_id=data.getStringExtra("sound_id");

                    PreparedAudio();
                }

            }

        }
    }


    // this will play the sound with the video when we select the audio

    public  void PreparedAudio(){
//        video_player.setVolume(0);
//
//        File file=new File(Constants.app_folder+ Constants.SelectedAudio_AAC);
//        if(file.exists()) {
//            audio = new MediaPlayer();
//            try {
//                audio.setDataSource(Constants.app_folder+ Constants.SelectedAudio_AAC);
//                audio.prepare();
//                audio.setLooping(true);
//
//
//                video_player.seekTo(0);
//                video_player.setPlayWhenReady(true);
//                audio.start();
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }

        File file=new File(Constants.app_folder+Constants.SelectedAudio_AAC);
        if(file.exists()) {
            audio = new MediaPlayer();
            try {
                video_player.setVolume(0);
                audio.setDataSource(Constants.app_folder + Constants.SelectedAudio_AAC);
                audio.setLooping(true);
                audio.prepare();
                video_player.seekTo(0);
                video_player.setPlayWhenReady(true);
                audio.start();
            } catch (IOException e) {
                e.printStackTrace();
            }

            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(this, Uri.fromFile(file));
        }
    }




  //  }


    @Override
    public void onBackPressed() {

        showDialogd();
    }

    // this will add the select audio with the video
    public void Merge_withAudio(){

        String audio_file;
        audio_file =Constants.app_folder+Constants.SelectedAudio_AAC;
        String video =path;
        String finaloutput = Constants.root +"output2.mp4";
        File file=new File(finaloutput);
        if (file.exists())
        {
            file.delete();
        }
        Log.e("Test","11");
        Merge_Video_Audio2 merge_video_audio=new Merge_Video_Audio2(getApplicationContext());
        merge_video_audio.doInBackground(audio_file,video,finaloutput);

    }



    public void Go_To_preview_Activity(){
        //Functions.Show_determinent_loader(GallerySelectedVideo_A.this,false,false);
       // Save_Video(path,Constants.output_filter_file);

        new VideoCompressAsyncTask(GallerySelectedVideo_A.this).doInBackground(path,Constants.output_filter_file);

    }

    class VideoCompressAsyncTask extends AsyncTask<String, String, String> {

        Context mContext;

        public VideoCompressAsyncTask(Context context){
            mContext = context;
            Functions.Show_determinent_loader(context,false,false);
        }



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... paths) {

            Thread thread = new Thread(runnable);
            thread.start();

            return  null;

        }


        @Override
        protected void onPostExecute(String compressedFilePath) {
            super.onPostExecute(compressedFilePath);
//GotopostScreen();
        }
    }

    public Runnable runnable =new Runnable() {
        @Override
        public void run() {

            try {


                new GPUMp4Composer(path, Constants.output_filter_file)
                        .videoBitrate((int) (0.95 *4* 500 * 900))
                        .listener(new GPUMp4Composer.Listener() {
                            @Override
                            public void onProgress(double progress) {
                               Functions.Show_loading_progress((int)(progress*100));
                                // MediaController.getInstance().convertVideo(tempFile.getPath());


                            }

                            @Override
                            public void onCompleted() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //  Toast.makeText(GallerySelectedVideo_A.this, "completed", Toast.LENGTH_SHORT).show();
                                        Functions.cancel_determinent_loader();

                                        //  Go_To_preview_Activity();
                                        Intent intent =new Intent(GallerySelectedVideo_A.this, AddPostActivityS.class);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                                        finish();

                                    }
                                });


                            }
                            @Override
                            public void onCanceled() {
                                Log.e("Testi","cancel");
                                Log.d("resp", "onCanceled");
                            }

                            @Override
                            public void onFailed(Exception exception) {
                                Log.e("Testi","fail");
                                Log.d("resp",exception.toString());

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {

                                            Functions.cancel_determinent_loader();
                                            File dir = new File(Constants.output_filter_file);
                                            try {
                                                    if (dir.exists())
                                                    {
                                                    dir.delete();
                                                    Intent intent =new Intent(GallerySelectedVideo_A.this, AddPostActivityS.class);
                                                    startActivity(intent);
                                                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                                                    finish();
                                                    System.out.println("Directory is not created");
                                                }
                                                    else
                                                    {
                                                        Intent intent =new Intent(GallerySelectedVideo_A.this, AddPostActivityS.class);
                                                        startActivity(intent);
                                                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                                                        finish();
                                                    }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                         //   Toast.makeText(GallerySelectedVideo_A.this, "Try Again", Toast.LENGTH_SHORT).show();
                                        }catch (Exception e){

                                        }
                                    }
                                });

                            }
                        })
                        .start();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }


        }};



    @Override
    protected void onResume() {
        super.onResume();
        File dir = new File(Environment.getExternalStorageDirectory() + "/DivineRayVideo/"+"DivineVideoCreator/");
        try {
            if (!dir.exists()) {

                System.out.println("Directory created");
                dir.mkdir();
            } else {

                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        dismissProgressDialog();
        if(video_player!=null){
            if (audio!=null) {
                audio.start();
                audio.setLooping(true);
               //PreparedAudio();
            }
            video_player.seekTo(0);
            video_player.setPlayWhenReady(true);
        }

    }



    @Override
    public void onStop() {
        super.onStop();
        try{
            if(video_player!=null){
                video_player.setPlayWhenReady(false);
            }
            if(audio!=null){
                audio.pause();
            }
        }catch (Exception e){

        }
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        if(video_player!=null){
            video_player.release();
        }

        if(audio!=null){
            audio.pause();
            audio.release();
        }
    }


    // Bottom all the function and the Call back listener of the Expo player
    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }


    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }


    @Override
    public void onLoadingChanged(boolean isLoading) {

    }


    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

        if(playbackState== Player.STATE_ENDED){

            video_player.seekTo(0);
            video_player.setPlayWhenReady(true);

            if(audio!=null){
                audio.seekTo(0);
                audio.start();
            }
        }

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

        Toast.makeText(this, "Repeat mode change", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }


    @Override
    public void onSeekProcessed() {

        Log.d("resp","smmdsmd");
    }



    // this will hide the bottom mobile navigation controll
    public void Hide_navigation(){

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        // This work only for android 4.4+
        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {

            getWindow().getDecorView().setSystemUiVisibility(flags);

            // Code below is to handle presses of Volume up or Volume down.
            // Without this, after pressing volume buttons, the navigation bar will
            // show up and won't hide
            final View decorView = getWindow().getDecorView();
            decorView
                    .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
                    {

                        @Override
                        public void onSystemUiVisibilityChange(int visibility)
                        {
                            if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                            {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
        }

    }


    @SuppressLint("NewApi")
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && hasFocus)
        {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
    // we need 4 permission during creating an video so we will get that permission
    // before start the video recording
    public boolean check_permissions() {

        String[] PERMISSIONS = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.CAMERA
        };

        if (!hasPermissions(getApplicationContext(), PERMISSIONS)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS, 2);
            }
        }else if (hasPermissions(getApplicationContext(), PERMISSIONS)) {
//setUpCamera();
        }
        else {
            return true;
        }



        return false;
    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }

            }
        }
        return true;


    }

    private void showDialogd() {
        new IOSDialog.Builder(GallerySelectedVideo_A.this)
                .setMessage("Want to create new video ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int ik) {
                        dismissProgressDialog();
                        finish();
                        overridePendingTransition(R.anim.in_from_top,R.anim.out_from_bottom);


                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                }).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }
}