package com.divineray.app.Video_Recording.GallerySelectedVideo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.arthenica.mobileffmpeg.FFmpeg;
import com.divineray.app.Video_Recording.PreviewGalleryVideoA;
import com.divineray.app.Utils.Constants;

import com.googlecode.mp4parser.authoring.Track;

import java.io.File;

public class Merge_Video_Audio2 extends AsyncTask<String, String, String> {

    ProgressDialog progressDialog;
    Context context;

    String audio, video, output;
    Track crop_track;

    FFmpeg fFmpeg;


    public Merge_Video_Audio2(Context context) {
        this.context = context;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait...");

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }


    @Override
    public String doInBackground(String... strings) {

        try {
            progressDialog.show();
        } catch (Exception ignored) {

        }
        audio = strings[0];
        video = strings[1];
        output = strings[2];

        Thread thread = new Thread(runnable);
        thread.start();
        return null;
    }


    public Runnable runnable = new Runnable() {
        @Override
        public void run() {

            Load_FFmpeg();
        }

    };

    private void Load_FFmpeg() {
        if (fFmpeg == null) {
        Log.e("testop","1"+video+" hh: "+audio+" out:"+output);
          // String[] command = {"-i", video, "-i", audio, "-c:v", "copy", "-c:a", "aac", "-map", "0:v:0", "-map", "1:a:0", "-shortest", output};
           String[] command0 = { "-i",video,"-stream_loop","-1","-i",audio,"-map","0:v","-map","1:a","-c:v","copy","-shortest", output};

           executeCommand(command0);
            fFmpeg = null;


        }

    }

    public void executeCommand(final String[] command) {
        FFmpeg.execute(command);
        Go_To_preview_Activity();
    }


    public void Go_To_preview_Activity() {
        Intent intent = new Intent(context, PreviewGalleryVideoA.class);
        intent.putExtra("path", Constants.root +"output2.mp4");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}

