package com.divineray.app.Video_Recording;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.arthenica.mobileffmpeg.FFmpeg;

import com.divineray.app.Utils.Constants;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import it.federicoboschini.androidresourcefileprovider.BuildConfig;


import static com.divineray.app.Utils.Constants.REMOVE_ADD_AUDIO_TO_VIDEO;

public class Merge_Video_Audio extends AsyncTask<String, String, String> {

    ProgressDialog progressDialog;
    Context context;
    String audio, video, output;
    FFmpeg fFmpeg;


    public Merge_Video_Audio(Context context) {
        this.context = context;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait...");

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }


    @Override
    public String doInBackground(String... strings) {

        try {
            progressDialog.show();

        } catch (Exception ignored) {

        }
        audio = strings[0];
        video = strings[1];
        output = strings[2];

        Log.e("resp", audio + "----" + video + "-----" + output);




        Thread thread = new Thread(runnable);
        thread.start();


        return null;
    }



    public Runnable runnable =new Runnable() {
        @Override
        public void run() {
                Load_FFmpeg();
        }

    };


                private void Load_FFmpeg()  {

        if (fFmpeg== null) {
           Log.e("Testl","null");
             String[] command0 = { "-i",video,"-stream_loop","-1","-i",audio,"-map","0:v","-map","1:a","-c:v","copy","-shortest", output};
           //   String[] command = {"-i", video, "-i", audio, "-c:v", "copy", "-c:a", "aac", "-map", "0:v:0", "-map", "1:a:0", "-shortest", output};

          executeCommand(command0);




                }
else {
    Log.e("Testl","Not null");
        }



        }




    public void executeCommand(final String[] command) {
        fFmpeg.execute(command);
        Go_To_preview_Activity();
    }






    public void Go_To_preview_Activity() {
        Intent intent = new Intent(context, Preview_Video_A.class);
        intent.putExtra("path", Constants.outputfile2);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }





}

