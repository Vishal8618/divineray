package com.divineray.app.Video_Recording

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.divineray.app.R
import com.divineray.app.Utils.Constants
import com.divineray.app.Video_Recording.GallerySelectedVideo.GallerySelectedVideo_A
import life.knowledge4.videotrimmer.K4LVideoTrimmer
import life.knowledge4.videotrimmer.interfaces.OnTrimVideoListener
import java.io.*

class NewVideoTrimActivity  : AppCompatActivity(), android.view.View.OnClickListener {
    var timeLine: K4LVideoTrimmer? = null
    var butonSave: Button? = null
     lateinit var tx60sec: TextView
     lateinit var tx180sec: TextView

    /**
     * set Activity
     */
    var mActivity: Activity = this
    var sound_name:String=""
    var tx60secSelected: kotlin.Boolean = true
    var tx180secSeleted: kotlin.Boolean = false
    var vidDuration=60
    var strFilePath:String=""

    /**
     * set Activity TAG
     */
    var TAG: String = this.javaClass.getSimpleName()

    companion object{
        public lateinit var newTrimActivity:NewVideoTrimActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_video_trimmer)
        val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        builder.detectFileUriExposure()
        newTrimActivity=this;
        getIntentData()
        tx60sec!!.setOnClickListener(this)
        tx180sec!!.setOnClickListener(this)
        setUpViews()

    }


    //Clicks
    @SuppressLint("WrongConstant")
    override fun onClick(v: android.view.View?) {
        when (v!!.getId()) {
            R.id.tx60sec -> performVideo60SecInc()
            R.id.tx180sec -> performVideo180SecInc()
        }
    }

    fun performVideo60SecInc()
    {
        if (!tx60secSelected) {
            tx180secSeleted = false
            tx60secSelected = true
            tx60sec!!.setTextColor(ContextCompat.getColor(this, R.color.vid_red_color))
            tx180sec!!.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
            vidDuration=60
            setUpViews()
        }
    }

    fun performVideo180SecInc()
    {
        if (!tx180secSeleted) {
        tx180secSeleted = true
        tx60secSelected = false
        tx180sec.setTextColor(ContextCompat.getColor(this, R.color.vid_red_color))
        tx60sec.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
        vidDuration=180
        setUpViews()
    }

    }

    fun getIntentData()
    {

        timeLine = findViewById<View>(R.id.timeLine) as K4LVideoTrimmer
        tx60sec =findViewById<View>(R.id.tx60sec) as TextView
        tx180sec =findViewById<View>(R.id.tx180sec) as TextView
        /*Retrive Data from Privious Activity*/if (intent != null) {
        if (intent.getStringExtra("video_path") != null) {
            sound_name= intent.getStringExtra("sound_name")!!
            strFilePath = intent.getStringExtra("video_path")!!

        }
    }
    }

    private fun setUpViews() {
        if (timeLine != null) {
            timeLine!!.setVideoURI(Uri.parse(strFilePath))
        }
        timeLine!!.setMaxDuration(vidDuration)

        timeLine!!.setOnTrimVideoListener(object : OnTrimVideoListener {
            override fun getResult(uri: Uri) {
                val strURI = uri.toString()
                Log.e("test", "URITEST====$strURI")

                //var returnIntent = Intent(this, VideoReviewActivity::class.java)
                val source: File = File(uri.path)
                val destination: File = File(Constants.gallery_resize_video)
                // try {
                if (source.exists()) {
                    var `in`: InputStream? = null
                    try {
                        `in` = FileInputStream(source)
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                    }
                    if (destination.exists())
                    {
                        destination.delete()
                        destination.createNewFile()
                    }
                    var out: OutputStream? = null
                    try {
                        out = FileOutputStream(destination)
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                    }

                    val buf = ByteArray(1024)
                    var len = 0

                    while (true) {
                        try {
                            if (`in`!!.read(buf).also { len = it } <= 0) break
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        try {
                            out!!.write(buf, 0, len)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }

                    try {
                        `in`!!.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    try {
                        out!!.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    setData(destination.absolutePath)

                } else
                {
                    intent.getStringExtra("video_path")?.let { setData(it) }


                }
            }
            override fun cancelAction() {
                finish()
            }
        })
    }

    fun setData(uri: String)
    {
        val intent = Intent(this, GallerySelectedVideo_A::class.java)
        intent.putExtra("video_path", uri)

        intent.putExtra("sound_name", Video_Recorder_A.add_sound_txt.text.toString())
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish();

    }

}