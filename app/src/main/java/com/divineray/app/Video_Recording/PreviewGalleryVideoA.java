package com.divineray.app.Video_Recording;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.daasuu.gpuv.composer.FillMode;
import com.daasuu.gpuv.composer.GPUMp4Composer;
import com.daasuu.gpuv.player.GPUPlayerView;
import com.daasuu.mp4compose.composer.Mp4Composer;
import com.divineray.app.Add.AddPostActivityS;

import com.divineray.app.R;
import com.divineray.app.Utils.Constants;
import com.divineray.app.Utils.Functions;

import com.divineray.app.Video_Recording.GallerySelectedVideo.GallerySelectedVideo_A;
import com.divineray.app.activities.BaseActivity;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class PreviewGalleryVideoA extends BaseActivity implements Player.EventListener {
    String video_url;

    GPUPlayerView gpuPlayerView;
    PlayerView playerView;

    public static int  select_postion=0;


    RecyclerView recylerview;
    SimpleExoPlayer player;
    boolean is_user_stop_video = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_preview_gallery_video);
        select_postion = 0;

        video_url = Constants.root + "output2.mp4";
        playerView = findViewById(R.id.playerview_vid_g);


        findViewById(R.id.Goback_g).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  finish();
                DeleteFileK();
                finish();
                player.setVolume(0);
                player.setPlayWhenReady(false);
                overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
                //overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);

            }
        });


        findViewById(R.id.next_btn_g).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_permissions()) {
                    File f = new File(Constants.output_filter_file);
                    if (f.exists()) {
                        f.delete();
                    }
                }
                player.setPlayWhenReady(false);

              //  new VideoCompressAsyncTask(PreviewGalleryVideoA.this).doInBackground(Constants.outputfile2, Constants.output_filter_file);
                if (check_permissions()) {
                    File dir = new File(Constants.output_filter_file);
                    try {
                        if (!dir.exists()) {

                            System.out.println("Directory created");
                            //dir.mkdir();
                        } else {
                                dir.delete();
                            System.out.println("Directory is not created");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }}

                new VideoCompressAsyncTask(PreviewGalleryVideoA.this).doInBackground(Constants.outputfile2, Constants.output_filter_file);
//                new Mp4Composer(Constants.outputfile2, Constants.output_filter_file)
//                      //  .size(720, 1280)
//                       // .videoBitrate((int) (0.4 *4  * 500 * 900))
//                        //.filter(glFilter)
//                        //.fillMode(FillMode.CUSTOM)
//                       // .customFillMode(fillModeCustomItem)
//                        .listener(new Mp4Composer.Listener() {
//                            @Override
//                            public void onProgress(double progress) {
//                                Functions.Show_loading_progress((int)(progress*100));
//                            }
//
//                            @Override
//                            public void onCompleted() {
//
//                                Functions.cancel_determinent_loader();
//
//                                GotopostScreen();
//
//
//                            }
//
//                            @Override
//                            public void onCanceled() {
//                                Functions.cancel_determinent_loader();
//
//                                Toast.makeText(PreviewGalleryVideoA.this, "Try again", Toast.LENGTH_SHORT).show();
//
//                            }
//
//                            @Override
//                            public void onCurrentWrittenVideoTime(long timeUs) {
//
//                            }
//
//                            @Override
//                            public void onFailed(Exception exception) {
//                              //  Log.d(TAG, "onFailed()");
//                            }
//                        })
//                        .start();









                 // Save_Video(Constants.outputfile2, Environment.getExternalStorageDirectory().toString()+"/" + new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date()) + ".mp4");

//                Functions.Show_determinent_loader(PreviewGalleryVideoA.this,false,false);
//                VideoCompress.compressVideoMedium(Constants.outputfile2, Constants.output_filter_file, new VideoCompress.CompressListener() {
//                    @Override
//                    public void onStart() {
//                        //Start Compress
//                        Log.e("Testr","Start");
//                    }
//
//                    @Override
//                    public void onSuccess() {
//                        Log.e("Testr","Finish");
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                Functions.cancel_determinent_loader();
//                               // Save_Video(Constants.output_filter_file,Constants.output_filter_file);
//                                GotopostScreen();
//
//
//                            }
//                        });
//
//                        //Finish successfully
//
//                    }
//
//                    @Override
//                    public void onFail() {
//                        Log.e("Testr","Fail");
//                        //Failed
//                    }
//
//                    @Override
//                    public void onProgress(float percent) {
//                        //Progress
//                        Functions.Show_loading_progress((int)(percent));
//                        Log.e("Testr","progress"+percent);
//                    }
//                });
//              //  task.cancel(true);
//                player.setPlayWhenReady(false);
            }
        });


        Set_Player(video_url);


    }
    // this function will set the player to the current video

    public void Set_Player(String path){


        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "DivineRay"));

        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(path));


        player.prepare(videoSource);
        player.setRepeatMode(Player.REPEAT_MODE_ALL);
        player.addListener(this);




        playerView.setPlayer(player);
        playerView.setKeepContentOnPlayerReset(true);
        File someFile = new File(path);
        String videfile = "Vid"+someFile;
        getVideoWidthOrHeight(someFile, videfile);
        // Get the video width
        int mVideoWidth = getVideoWidthOrHeight(someFile, "width");
// Get the video height
        int mVideoHeight = getVideoWidthOrHeight(someFile, "height");
        float checkVal=Float.parseFloat(String.valueOf(mVideoHeight));
        //  Toast.makeText(this, ""+mVideoWidth, Toast.LENGTH_SHORT).show();
//        float videoVal= (float) 400.00;
//        if (!(checkVal <videoVal)) {
//            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
//            player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
//        }
        player.setPlayWhenReady(true);


        playerView.setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {



                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    super.onSingleTapUp(e);
                    if(!player.getPlayWhenReady()){
                        is_user_stop_video=false;
                        player.setPlayWhenReady(true);
                    }else{
                        is_user_stop_video=true;
                        player.setPlayWhenReady(false);
                    }


                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    //  Show_video_option(item);

                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {

                    if(!player.getPlayWhenReady()){
                        is_user_stop_video=false;
                        player.setPlayWhenReady(true);
                    }

                    return super.onDoubleTap(e);

                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });

    }

    public int getVideoWidthOrHeight(File file, String widthOrHeight) {
        MediaMetadataRetriever retriever = null;
        Bitmap bmp = null;
        FileInputStream inputStream = null;
        int mWidthHeight = 0;
        try {
            retriever = new  MediaMetadataRetriever();
            inputStream = new FileInputStream(file.getAbsolutePath());
            retriever.setDataSource(inputStream.getFD());
            bmp = retriever.getFrameAtTime();
            if (widthOrHeight.equals("width")){
                mWidthHeight = bmp.getWidth();
            }else {
                mWidthHeight = bmp.getHeight();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally{
            if (retriever != null){
                retriever.release();
            }if (inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return mWidthHeight;
    }


    // this is lifecyle of the Activity which is importent for play,pause video or relaese the player
    @Override
    protected void onStop() {
        super.onStop();

        if(player!=null){
            player.setPlayWhenReady(false);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        if(player!=null){
            player.setPlayWhenReady(true);
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if(player!=null){
            player.setPlayWhenReady(true);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(player!=null){
            player.removeListener(PreviewGalleryVideoA.this);
            player.release();
            player=null;
        }
    }




    // this function will add the filter to video and save that same video for post the video in post video screen
    public void Save_Video(String srcMp4Path, String destMp4Path){

        Functions.Show_determinent_loader(this,false,false);

        new GPUMp4Composer(srcMp4Path, destMp4Path)
               // .size(500, 885)
       // 0.25 *4.5  * 500 * 900
                .videoBitrate((int) (0.4 *4  * 500 * 900))
                .listener(new GPUMp4Composer.Listener() {
                    @Override
                    public void onProgress(double progress) {

                        Log.d("resp",""+(int) (progress*100));
                        Functions.Show_loading_progress((int)(progress*100));
                        // MediaController.getInstance().convertVideo(tempFile.getPath());


                    }

                    @Override
                    public void onCompleted() {
                        File dir3=new File(Constants.gallery_resize_video);

                        // video_path= Constants.output_filter_file;
                        if (dir3.exists()) {
                            dir3.delete();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Functions.cancel_determinent_loader();

                                GotopostScreen();


                            }
                        });


                    }

                    @Override
                    public void onCanceled() {
                        Log.d("resp", "onCanceled");
                    }

                    @Override
                    public void onFailed(Exception exception) {

                        Log.d("resp",exception.toString());

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {

                                    Functions.cancel_determinent_loader();
                                    File dir = new File(Constants.output_filter_file);
                                    try {
                                        if (dir.exists())
                                        {
                                            dir.delete();
                                            Intent intent =new Intent(PreviewGalleryVideoA.this, AddPostActivityS.class);
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                                            finish();
                                            System.out.println("Directory is not created");
                                        }
                                        else
                                        {
                                            Intent intent =new Intent(PreviewGalleryVideoA.this, AddPostActivityS.class);
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                                            finish();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    //   Toast.makeText(GallerySelectedVideo_A.this, "Try Again", Toast.LENGTH_SHORT).show();
                                }catch (Exception e){

                                }
                            }
                        });

                    }
                })
                .start();
    }





    public void GotopostScreen(){

        Intent intent =new Intent(PreviewGalleryVideoA.this, AddPostActivityS.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

    }



    // Bottom all the function and the Call back listener of the Expo player
    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }



    @Override
    public void onBackPressed() {
        DeleteFileK();
        finish();
        Video_Recorder_A.DeleteFile();

        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);

    }


    // this will delete all the video parts that is create during priviously created video

    public void DeleteFileK(){

        File output = new File(Constants.outputfile);
        File output2 = new File(Constants.outputfile2);
        File output_filter_file = new File(Constants.output_filter_file);

        if(output.exists()){
            output.delete();
        }
        if(output2.exists()){

            output2.delete();
        }
        if(output_filter_file.exists()){
            output_filter_file.delete();
        }



    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }

            }
        }
        return true;


    }

    public boolean check_permissions() {

        String[] PERMISSIONS = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.CAMERA
        };

        if (!hasPermissions(getApplicationContext(), PERMISSIONS)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS, 2);
            }
        }else if (hasPermissions(getApplicationContext(), PERMISSIONS)) {
            Log.e("Test","Already have permissions");
        }
        else {
            return true;
        }
        return false;
    }


    class VideoCompressAsyncTask extends AsyncTask<String, String, String> {

        Context mContext;

        public VideoCompressAsyncTask(Context context){
            mContext = context;
            Functions.Show_determinent_loader(context,false,false);
        }



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... paths) {




            Thread thread = new Thread(runnable);
            thread.start();

            return  null;

        }


        @Override
        protected void onPostExecute(String compressedFilePath) {
            super.onPostExecute(compressedFilePath);
//GotopostScreen();
        }
    }
    public Runnable runnable =new Runnable() {
        @Override
        public void run() {




                new GPUMp4Composer(Constants.outputfile2, Constants.output_filter_file)
                       // .videoBitrate((int) (0.4 *4  * 500 * 900))
                        .listener(new GPUMp4Composer.Listener() {
                            @Override
                            public void onProgress(double progress) {

                                Log.d("resp",""+(int) (progress*100));
                                Functions.Show_loading_progress((int)(progress*100));
                                // MediaController.getInstance().convertVideo(tempFile.getPath());


                            }

                            @Override
                            public void onCompleted() {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        Functions.cancel_determinent_loader();

                                        GotopostScreen();


                                    }
                                });


                            }

                            @Override
                            public void onCanceled() {
                                Log.d("resp", "onCanceled");
                            }

                            @Override
                            public void onFailed(Exception exception) {

                                Log.d("resp",exception.toString());

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                       // try {
                                        File dir = new File(Constants.output_filter_file);
                                        try {
                                            if (dir.exists()) {

                                                dir.delete();
                                                GotopostScreen();
                                                System.out.println("Directory is not created");
                                            }
                                            else
                                            {
                                                GotopostScreen();
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                            //Functions.cancel_determinent_loader();


//                                            Toast.makeText(PreviewGalleryVideoA.this, "Try Again", Toast.LENGTH_SHORT).show();
//                                        }catch (Exception e){
//
//                                        }
                                    }
                                });

                            }
                        })
                        .start();



        }};
}