package com.divineray.app.Video_Recording;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;


import com.daasuu.gpuv.camerarecorder.CameraThread;
import com.daasuu.gpuv.camerarecorder.LensFacing;
import com.daasuu.gpuv.composer.GPUMp4Composer;
import com.daasuu.gpuv.player.GPUPlayerView;
import com.divineray.app.Add.AddPostActivityS;

import com.divineray.app.R;
import com.divineray.app.Utils.Constants;
import com.divineray.app.Utils.Functions;

import com.divineray.app.activities.BaseActivity;

import com.divineray.app.activities.HomeActivity;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.ligl.android.widget.iosdialog.IOSDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.divineray.app.Video_Recording.Video_Recorder_A.add_sound_txt;
import static com.divineray.app.Video_Recording.Video_Recorder_A.chronArray;
import static com.divineray.app.Video_Recording.Video_Recorder_A.chronometer;
import static com.divineray.app.Video_Recording.Video_Recorder_A.done_btn;
import static com.divineray.app.Video_Recording.Video_Recorder_A.lly_delete;
import static com.divineray.app.Video_Recording.Video_Recorder_A.number;
import static com.divineray.app.Video_Recording.Video_Recorder_A.pauseOffset;
import static com.divineray.app.Video_Recording.Video_Recorder_A.sec_passed;
import static com.divineray.app.Video_Recording.Video_Recorder_A.tx_holdforvideo;
import static com.divineray.app.Video_Recording.Video_Recorder_A.uploadLayout;
import static com.divineray.app.Video_Recording.Video_Recorder_A.vidDeleteNumber;
import static com.divineray.app.Video_Recording.Video_Recorder_A.video_progress;
import static com.divineray.app.Video_Recording.Video_Recorder_A.videopaths;

public class Preview_Video_A  extends BaseActivity implements Player.EventListener {
    String video_url;

    GPUPlayerView gpuPlayerView;
    PlayerView playerView;

    public static int  select_postion=0;


    RecyclerView recylerview;
    String publicFile;
    boolean is_user_stop_video = false;
    String inputfile,outputfile;
    int height,width;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_preview_video);
        select_postion=0;

        video_url= Constants.outputfile2;
        playerView=findViewById(R.id.playerview_vid);
        Intent intent=getIntent();
        String sec_passedd=intent.getStringExtra("sec_passed");
        ArrayList<String> chronArrayo=intent.getStringArrayListExtra("chronArray");



        findViewById(R.id.Goback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent=new Intent(Preview_Video_A.this, Video_Recorder_A.class);
//                intent.putExtra("St","1");
//                startActivity(intent);
//                sec_passed= Integer.parseInt(sec_passedd);
//                chronArray=chronArrayo;

               showDialogd();


                overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
                //overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);

            }
        });


        findViewById(R.id.next_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player != null) {
                    player.setPlayWhenReady(false);
                }
                //   Functions.Show_determinent_loader(Preview_Video_A.this,false,false);
                if (check_permissions()) {
                    File f = new File(Constants.output_filter_file);
                    if (f.exists()) {
                        f.delete();
                    }
                }

                    File f2 = new File(Constants.root0);
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                height = displayMetrics.heightPixels;
                width = displayMetrics.widthPixels;
                    //  SiliCompressor.with(Preview_Video_A.this).compress(imagePath, destinationDirectory, true);
                   // Save_Video(Constants.outputfile2, Constants.output_filter_file);
                       new VideoCompressAsyncTask(Preview_Video_A.this).doInBackground(Constants.outputfile2,Constants.output_filter_file);
                    player.setPlayWhenReady(false);

//                VideoCompress.compressVideoMedium(Constants.outputfile2, Constants.output_filter_file, new VideoCompress.CompressListener() {
//                    @Override
//                    public void onStart() {
//                        //Start Compress
//                    }
//
//                    @Override
//                    public void onSuccess() {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                Functions.cancel_determinent_loader();
//                               // Save_Video(Constants.output_filter_file,Constants.output_filter_file);
//                                GotopostScreen();
//
//
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void onFail() {
//                        //Failed
//                    }
//
//                    @Override
//                    public void onProgress(float percent) {
//                        //Progress
//                        Log.e("Val",""+percent);
//                        Functions.Show_loading_progress((int)(percent));
//                    }
//                });


//                VideoCompressor.start(Constants.outputfile2,Constants.output_filter_file, new CompressionListener() {
//                    @Override
//                    public void onStart() {
//                        // Compression start
//                    }
//
//                    @Override
//                    public void onSuccess() {
//                        // On Compression success
//                        GotopostScreen();
//                    }
//
//                    @Override
//                    public void onFailure(String failureMessage) {
//                        // On Failure
//                    }
//
//                    @Override
//                    public void onProgress(float v) {
//                        // Update UI with progress value
//                        runOnUiThread(new Runnable() {
//                            public void run() {
//
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void onCancelled() {
//                        // On Cancelled
//                    }
//                }, VideoQuality.MEDIUM, false, false);

                }

        });


        Set_Player(video_url);


        recylerview=findViewById(R.id.recylerview_pre);


    }
    // this function will set the player to the current video
    SimpleExoPlayer player;
    public void Set_Player(String path){


        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "DivineRay"));

        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(path));


        player.prepare(videoSource);
        player.setRepeatMode(Player.REPEAT_MODE_ALL);
        player.addListener(this);




        playerView.setPlayer(player);
        playerView.setKeepContentOnPlayerReset(true);
        File someFile = new File(path);
        String videfile = "Vid"+someFile;
        getVideoWidthOrHeight(someFile, videfile);
        // Get the video width
        int mVideoWidth = getVideoWidthOrHeight(someFile, "width");
// Get the video height
        int mVideoHeight = getVideoWidthOrHeight(someFile, "height");
        float checkVal=Float.parseFloat(String.valueOf(mVideoHeight));
      //  Toast.makeText(this, ""+mVideoWidth, Toast.LENGTH_SHORT).show();
        float videoVal= (float) 400.00;
//       if (!(checkVal <videoVal)) {
//           playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
//            player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
//      }
//        player.setPlayWhenReady(true);
//
//        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
//        player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);


        playerView.setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {



                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    super.onSingleTapUp(e);
                    if(!player.getPlayWhenReady()){
                        is_user_stop_video=false;
                        player.setPlayWhenReady(true);
                    }else{
                        is_user_stop_video=true;
                        player.setPlayWhenReady(false);
                    }


                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                  //  Show_video_option(item);

                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {

                    if(!player.getPlayWhenReady()){
                        is_user_stop_video=false;
                        player.setPlayWhenReady(true);
                    }

                    return super.onDoubleTap(e);

                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });

    }

    public int getVideoWidthOrHeight(File file, String widthOrHeight) {
        MediaMetadataRetriever retriever = null;
        Bitmap bmp = null;
        FileInputStream inputStream = null;
        int mWidthHeight = 0;
        try {
            retriever = new  MediaMetadataRetriever();
            inputStream = new FileInputStream(file.getAbsolutePath());
            retriever.setDataSource(inputStream.getFD());
            bmp = retriever.getFrameAtTime();
            if (widthOrHeight.equals("width")){
                mWidthHeight = bmp.getWidth();
            }else {
                mWidthHeight = bmp.getHeight();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally{
            if (retriever != null){
                retriever.release();
            }if (inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return mWidthHeight;
    }


    // this is lifecyle of the Activity which is importent for play,pause video or relaese the player
    @Override
    protected void onStop() {
        super.onStop();

        if(player!=null){
            player.setPlayWhenReady(false);
        }


    }

    @Override
    protected void onStart() {
        super.onStart();

        if(player!=null){
            player.setPlayWhenReady(true);
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if(player!=null){
            player.setPlayWhenReady(true);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(player!=null){
            player.removeListener(Preview_Video_A.this);
            player.release();
            player=null;
        }

    }




    // this function will add the filter to video and save that same video for post the video in post video screen
    public void Save_Video(String srcMp4Path, final String destMp4Path){

        Functions.Show_determinent_loader(this,false,false);

       new GPUMp4Composer(srcMp4Path, destMp4Path)
                //.size(540, 960)
                .videoBitrate((int) (0.47 * 8 * 540 * 960))
                .listener(new GPUMp4Composer.Listener() {
                    @Override
                    public void onProgress(double progress) {

                        Log.d("resp",""+(int) (progress*100));
                        Functions.Show_loading_progress((int)(progress*100));
                       // MediaController.getInstance().convertVideo(tempFile.getPath());


                    }

                    @Override
                    public void onCompleted() {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Functions.cancel_determinent_loader();

                                GotopostScreen();


                            }
                        });


                    }

                    @Override
                    public void onCanceled() {
                        Log.d("resp", "onCanceled");
                    }

                    @Override
                    public void onFailed(Exception exception) {

                        Log.d("resp",exception.toString());

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {

                                    Functions.cancel_determinent_loader();
                                    File dir = new File(Constants.output_filter_file);
                                    try {
                                        if (dir.exists()) {
                                            dir.delete();
                                            GotopostScreen();
                                            System.out.println("Directory is not created");
                                        }
                                        else
                                        {
                                            GotopostScreen();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    //Toast.makeText(Preview_Video_A.this, "Try Again", Toast.LENGTH_SHORT).show();
                                }catch (Exception e){

                                }
                            }
                        });

                    }
                })
                .start();
    }




    public void GotopostScreen(){

        Intent intent =new Intent(Preview_Video_A.this, AddPostActivityS.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

    }



    // Bottom all the function and the Call back listener of the Expo player
    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }



    @Override
    public void onBackPressed() {
showDialogd();

        //Intent intent=new Intent(this, Camera.class);
//        Intent intent=new Intent(Preview_Video_A.this, Video_Recorder_A.class);
//
//        startActivity(intent);
    //    showDialogd();


        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);

    }

    private void showDialogd() {
        new IOSDialog.Builder(Preview_Video_A.this)
                .setMessage("Want to create new video ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int ik) {
                     //   DeleteFileP();
                      //  finish();
                        File file = new File(Constants.rootD);
                        String[] files;
                        files = file.list();
                        for (int i=0; i<files.length; i++) {
                            File myFile = new File(file, files[i]);
                            myFile.delete();
                        }
                        video_progress.resetComplete();
                        chronometer.setVisibility(View.GONE);
                        tx_holdforvideo.setVisibility(View.VISIBLE);
                        chronArray.clear();
                        videopaths.clear();
                        vidDeleteNumber=0;
                        number=0;
                        lly_delete.setVisibility(View.GONE);
                        uploadLayout.setVisibility(View.VISIBLE);
                        pauseOffset = 0;
                        sec_passed=0;

                        int color = Color.WHITE;
                        add_sound_txt.setClickable(true);
                        add_sound_txt.setTextColor(color);
                        done_btn.setVisibility(View.GONE);
                        chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
                        chronometer.stop();
                        sec_passed=0;
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                }).show();
    }


    // this will delete all the video parts that is create during priviously created video

    public void DeleteFileP(){
        File output = new File(Constants.rootD);



        if(output.exists()){
                output.delete();

        }



    }
    class VideoCompressAsyncTask extends AsyncTask<String, String, String> {

        Context mContext;

        public VideoCompressAsyncTask(Context context){
            mContext = context;
            Functions.Show_determinent_loader(context,false,false);
        }



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... paths) {

            inputfile=paths[0];
             outputfile=paths[0];


            Thread thread = new Thread(runnable);
            thread.start();
            String filePath = null;
//            File file = null;
//            file=new File(paths[1]);
//           publicFile= getFilename(file);
//
//            Log.e("Teste",""+getFilename(file));
//            try {
//
//              SiliCompressor.with(mContext).compressVideo(paths[0], paths[1],544,960,350000);
//setFileName("output-filtered");
//            } catch (URISyntaxException e) {
//                e.printStackTrace();
//            }
//
//            return  filePath;
            return  null;

        }


        @Override
        protected void onPostExecute(String compressedFilePath) {
            super.onPostExecute(compressedFilePath);
//GotopostScreen();
        }
    }



    public Runnable runnable =new Runnable() {
        @Override
        public void run() {


try {

    Log.e("Datan","h: "+height+" w: "+width);
    new GPUMp4Composer(Constants.outputfile2, Constants.output_filter_file)
            //.size(width, height)
            .videoBitrate((int) (0.47 * 8 * 540 * 960))
            .listener(new GPUMp4Composer.Listener() {
                @Override
                public void onProgress(double progress) {

                    Log.d("resp",""+(int) (progress*100));
                    Functions.Show_loading_progress((int)(progress*100));
                    // MediaController.getInstance().convertVideo(tempFile.getPath());


                }

                @Override
                public void onCompleted() {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Functions.cancel_determinent_loader();

                            GotopostScreen();


                        }
                    });


                }

                @Override
                public void onCanceled() {
                    Log.d("resp", "onCanceled");
                }

                @Override
                public void onFailed(Exception exception) {

                    Log.d("resp",exception.toString());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                Functions.cancel_determinent_loader();

                                Toast.makeText(Preview_Video_A.this, "Try Again", Toast.LENGTH_SHORT).show();
                            }catch (Exception e){

                            }
                        }
                    });

                }
            })
            .start();
}
catch (Exception e)
{
    e.printStackTrace();
}


        }};
            private String getFilename( File file) {

        String ext = ".mp4";
        //get extension
        /*if (Pattern.matches("^[.][p][n][g]", filename)){
            ext = ".png";
        }*/

        return ("/VIDEO_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ext);

    }
    private void setFileName(String text) {
        String currentFileName = publicFile;
        currentFileName = currentFileName.substring(1);
        Log.i("Testrq","Current file name"+ currentFileName);

        File directory = new File(Constants.root);
        File from      = new File(directory, currentFileName);
        File to        = new File(directory, text.trim() + ".mp4");
        from.renameTo(to);
        Log.e("Testrq","Directory is"+ directory.toString());

        Log.e("Testrq","From path is"+ from.toString());
        Log.e("Testrq","To path is"+ to.toString());
    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }

            }
        }
        return true;


    }

    public boolean check_permissions() {

        String[] PERMISSIONS = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.CAMERA
        };

        if (!hasPermissions(getApplicationContext(), PERMISSIONS)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS, 2);
            }
        }else if (hasPermissions(getApplicationContext(), PERMISSIONS)) {
            Log.e("Test","Already have permissions");
        }
        else {
            return true;
        }
        return false;
}}