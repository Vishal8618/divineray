package com.divineray.app.Video_Recording;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.divineray.app.Home.ItemAdapter;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.Constants;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.BaseActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.BottomSheetVidModel;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.downloader.request.DownloadRequest;
import com.gmail.samehadar.iosdialog.IOSDialog;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SoundList_Main_A extends BaseActivity implements  Player.EventListener{

    /**
     * Current Activity Instance
     */
    Activity mActivity = SoundList_Main_A.this;

    ImageView bottomsheet_closebutton;
    RecyclerView recyclerView_bottom;
    ItemAdapter mAdapter;
    EditText ed_commenttext;
    LinearLayout lly_addcommnet;
    TextView tx_bottom_head;
    Dialog progressDialog;
    VideoSoundItem videoSoundItem;

    IOSDialog iosDialog;
    DownloadRequest prDownloader;
    static boolean active = false;
    public static String running_sound_id;
    SwipeRefreshLayout swiperefresh;
    View previous_view;
    Thread thread;
    VideoSoundItem adapter;
    SimpleExoPlayer player;
    String previous_url = "none";


    ArrayList<BottomSheetVidModel.DataVi> mArrayList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTransparentStatusBarOnly(this);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_sound_list__main_);
        running_sound_id = "none";
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        builder.detectFileUriExposure();
        iosDialog = new IOSDialog.Builder(this)
                .setCancelable(false)
                .setSpinnerClockwise(true)
                .setSpinnerColor(Color.WHITE)
                .setBackgroundColor(Color.BLUE)
                .setMessageContentGravity(Gravity.END)
                .build();


        PRDownloader.initialize(this);

        bottomsheet_closebutton = findViewById(R.id.bottomsheet_vid_closebutton);
        recyclerView_bottom = findViewById(R.id.recyclerView_bottomSheet_vid);
        executeDataApi();
        swiperefresh=findViewById(R.id.swiperefresh);
        swiperefresh.setColorSchemeResources(R.color.colorBlack);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                previous_url = "none";
                StopPlaying();
                executeDataApi();
            }
        });

        executeDataApi();
        bottomsheet_closebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.finish();
                mActivity.overridePendingTransition(R.anim.in_from_top, R.anim.out_from_bottom);
            }
        });
    }

    private Map<String, String> mParam2s() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(this, DivineRayPrefernces.USERID, null));
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeDataApi() {

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getAllMusic(getAuthToken(),mParam2s()).enqueue(new Callback<BottomSheetVidModel>() {
            @Override
            public void onResponse(Call<BottomSheetVidModel> call, Response<BottomSheetVidModel> response) {
                swiperefresh.setRefreshing(false);
                //  Toast.makeText(getContext(), "hit", Toast.LENGTH_SHORT).show();
                Log.e("", "RESPONSE1" + response.body());
                BottomSheetVidModel mHomeModel = response.body();



                if (response.body().getStatus() == 0) {
                    // Toast.makeText(getContext(), "Sataus 0"+mArrayList, Toast.LENGTH_SHORT).show();
                    Log.e("", "RESPONSE12" + response.body());
                }
                else
                {
                    mArrayList = (ArrayList<BottomSheetVidModel.DataVi>) response.body().getData();

                    Set_adapter();}

            }

            @Override
            public void onFailure(Call<BottomSheetVidModel> call, Throwable t) {
            }
        });
    }

    public void Set_adapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView_bottom.setLayoutManager(layoutManager);
        adapter=new VideoSoundItem(this, mArrayList, new VideoSoundItem.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int postion, BottomSheetVidModel.DataVi item) {
                Log.d("resp", item.getMusicFile());

                if (view.getId() == R.id.done_pp) {
                    //Toast.makeText(this, "Hello", Toast.LENGTH_SHORT).show();
                    StopPlaying();
                    Down_load_mp3(item.getMusicId(), item.getMusicName(), item.getMusicFile());

                }
                else {
                   // Toast.makeText(getContext(),"you", Toast.LENGTH_SHORT).show();
                    if (thread != null && !thread.isAlive()) {
                        StopPlaying();
                        playaudio(view, item);
                    } else if (thread == null) {
                        StopPlaying();
                        playaudio(view, item);
                    }
                }

            }
        });
        recyclerView_bottom.setAdapter(adapter);



    }


    public void StopPlaying() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.removeListener(this);
            player.release();
        }

        show_Stop_state();

    }

    public void Down_load_mp3(final String id,final String sound_name, String url){
        iosDialog.show();
        prDownloader= PRDownloader.download(url, Constants.app_folder, Constants.SelectedAudio_AAC)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {

                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {

                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {

                    }
                });

        prDownloader.start(new OnDownloadListener() {
            @Override
            public void onDownloadComplete() {
             //   progressDialog.dismiss();
                iosDialog.dismiss();
                Intent output = new Intent();
                output.putExtra("isSelected","yes");
                output.putExtra("sound_name",sound_name);
                output.putExtra("sound_id",id);
                mActivity.setResult(RESULT_OK, output);
                mActivity.finish();
                mActivity.overridePendingTransition(R.anim.in_from_top, R.anim.out_from_bottom);
                Constants.musicid=id;
              //  Toast.makeText(mActivity, "Hello 2", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onError(Error error) {
                progressDialog.dismiss();
            }
        });

    }


    public void playaudio(View view, final BottomSheetVidModel.DataVi item){
        previous_view=view;

        if(previous_url.equals(item.getMusicFile())){

            previous_url="none";
            running_sound_id="none";
        }else {

            previous_url=item.getMusicFile();
            running_sound_id=item.getMusicId();

            DefaultTrackSelector trackSelector = new DefaultTrackSelector();
            player = ExoPlayerFactory.newSimpleInstance(mActivity, trackSelector);

            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(mActivity,
                    Util.getUserAgent(mActivity, "TikTok"));

            MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                    .createMediaSource(Uri.parse(item.getMusicFile()));


            player.prepare(videoSource);
            player.addListener((Player.EventListener) this);


            player.setPlayWhenReady(true);



        }

    }



    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

        if(playbackState==Player.STATE_BUFFERING){
            Show_loading_state();
        }
        else if(playbackState==Player.STATE_READY){
            Show_Run_State();
        }else if(playbackState==Player.STATE_ENDED){
            show_Stop_state();
        }

    }

    public void Show_loading_state(){
        previous_view.findViewById(R.id.play_btn).setVisibility(View.GONE);
        previous_view.findViewById(R.id.loading_progress).setVisibility(View.VISIBLE);
    }

    @Override
    public void onStop() {
        super.onStop();
        active=false;

        running_sound_id="null";

        if(player!=null){
            player.setPlayWhenReady(false);
            player.removeListener(this);
            player.release();
        }

        show_Stop_state();

    }
    public void Show_Run_State(){

        if (previous_view != null) {
            previous_view.findViewById(R.id.loading_progress).setVisibility(View.GONE);
            previous_view.findViewById(R.id.pause_btn).setVisibility(View.VISIBLE);
            previous_view.findViewById(R.id.done_pp).setVisibility(View.VISIBLE);

        }

    }



    public void show_Stop_state(){

        if (previous_view != null) {
            previous_view.findViewById(R.id.play_btn).setVisibility(View.VISIBLE);
            previous_view.findViewById(R.id.loading_progress).setVisibility(View.GONE);
            previous_view.findViewById(R.id.pause_btn).setVisibility(View.GONE);
            previous_view.findViewById(R.id.done_pp).setVisibility(View.VISIBLE);

        }

        running_sound_id="none";

    }
    }
