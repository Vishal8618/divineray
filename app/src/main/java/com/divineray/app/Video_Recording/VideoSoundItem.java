package com.divineray.app.Video_Recording;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.divineray.app.Home.ItemAdapter;
import com.divineray.app.R;
import com.divineray.app.StringChange.StringFormatter;
import com.divineray.app.model.BottomSheetVidModel;
import com.divineray.app.model.GetCommentsModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class VideoSoundItem extends RecyclerView.Adapter<VideoSoundItem.ViewHolder>  {
    Context context;
    ArrayList<BottomSheetVidModel.DataVi> mArrayList=new ArrayList<>();
    public interface OnItemClickListener {
        void onItemClick(View view,int postion,  BottomSheetVidModel.DataVi item) ;
    }

    public VideoSoundItem.OnItemClickListener listener;


    public VideoSoundItem(Context context, ArrayList<BottomSheetVidModel.DataVi> mArrayList,VideoSoundItem.OnItemClickListener listener) {
        this.context=context;
        this.mArrayList=mArrayList;
        this.listener=listener;
    }

    @NonNull
    @Override
    public VideoSoundItem.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VideoSoundItem.ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bottomsheet_vid_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VideoSoundItem.ViewHolder holder, int position) {
        BottomSheetVidModel.DataVi mModel = mArrayList.get(position);
        holder.setIsRecyclable(false);


        BottomSheetVidModel.DataVi item=mArrayList.get(position);
        SnapHelper snapHelper =  new LinearSnapHelper();
//        snapHelper.findSnapView(gridLayoutManager);
//        snapHelper.attachToRecyclerView(holder.recyclerView);
       // Toast.makeText(context, ""+mModel.toString(), Toast.LENGTH_SHORT).show();
        holder.bind(position, mArrayList.get(position), listener);

        holder.musicCat.setText(StringFormatter.capitalizeWord(mModel.getMusicName()));
        holder.musicName.setText(StringFormatter.capitalizeWord(mModel.getMusicCat()));
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_unknown_music)
                .error(R.drawable.ic_unknown_music)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();

        Glide.with(context).load(mModel.getMusicImage()).apply(options).into(holder.imageView);

    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imageView;
        TextView musicCat,musicName;
        TextView done;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.bottom_vid_image);
            musicCat=itemView.findViewById(R.id.bottom_vid_musictype);
            musicName=itemView.findViewById(R.id.bottom_vid_musicname);
            done=itemView.findViewById(R.id.done_pp);

        }

        public void bind(final int position, final BottomSheetVidModel.DataVi dataVi,  final VideoSoundItem.OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(v,position,dataVi);
                }
            });

            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(v,position,dataVi);
                }
            });
        }
    }


    }

