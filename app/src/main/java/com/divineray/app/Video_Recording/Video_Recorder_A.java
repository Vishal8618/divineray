package com.divineray.app.Video_Recording;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.os.SystemClock;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.arthenica.mobileffmpeg.FFmpeg;

import com.coremedia.iso.boxes.MovieHeaderBox;
import com.daasuu.gpuv.camerarecorder.CameraThread;
import com.daasuu.gpuv.camerarecorder.GPUCameraRecorder;
import com.daasuu.gpuv.camerarecorder.LensFacing;
import com.daasuu.gpuv.player.GPUPlayerView;

import com.divineray.app.Utils.FileUtils;
import com.divineray.app.Utils.Functions;
import com.divineray.app.activities.HomeActivity;
import com.divineray.app.R;
import com.divineray.app.SegmentProgress.ProgressBarListener;
import com.divineray.app.SegmentProgress.SegmentedProgressBar;
import com.divineray.app.Utils.Constants;
import com.divineray.app.activities.BaseActivity;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.googlecode.mp4parser.BasicContainer;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.googlecode.mp4parser.util.Matrix;
import com.googlecode.mp4parser.util.Path;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.ligl.android.widget.iosdialog.IOSSheetDialog;

import com.otaliastudios.transcoder.Transcoder;
import com.otaliastudios.transcoder.TranscoderListener;
import com.otaliastudios.transcoder.common.TrackType;
import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitError;
import com.wonderkiln.camerakit.CameraKitEvent;
import com.wonderkiln.camerakit.CameraKitEventListener;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.io.Writer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import life.knowledge4.videotrimmer.K4LVideoTrimmer;

@SuppressLint("StaticFieldLeak")
public class Video_Recorder_A extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.record_image)
    ImageView record_image;
    @BindView(R.id.flash_camera)
    ImageButton flash_btn;
    @BindView(R.id.camera_options)
    LinearLayout camera_options;
    @BindView(R.id.rotate_camera)
    ImageButton rotate_camera;
    @BindView(R.id.video_record_filter)
    ImageView filter_image;
    @BindView(R.id.recylerview_record)
    RecyclerView recyclerView_v;
    @BindView(R.id.camera_filter)
    ImageView im_camerafilter;
    @BindView(R.id.wrap_view)
    FrameLayout frameLayout;
    @BindView(R.id.filterLayout)
    LinearLayout filter_layout;
    @BindView(R.id.sound_lly)
    LinearLayout souns_layout;
    @BindView(R.id.vid_delete)
    ImageView im_delete;
    @BindView(R.id.camera)
    CameraView cameraView;
    @BindView(R.id.tx60sec)
    TextView tx60sec;
    @BindView(R.id.tx180sec)
    TextView tx180sec;

    //Static Views
    public static SegmentedProgressBar video_progress;
    static TextView add_sound_txt;
    static TextView done_btn;
    public static LinearLayout uploadLayout;
    public static LinearLayout lly_delete;
    public static Chronometer chronometer;
    public static TextView tx_holdforvideo;

    public static ArrayList<String> videopaths = new ArrayList<>();
    public static ArrayList<String> chronArray = new ArrayList<>();
    ArrayList<String> video_list = new ArrayList<>();

    boolean is_recording = false;
    boolean is_flash_on = false;
    boolean tx60secSelected=true;
    boolean tx180secSeleted=false;
    int flashonornot;
    int duratio_meas = 0;
    LensFacing facing;
    public static int Sounds_list_Request_code = 1;
    public int select_postion = 0;
    static public int sec_passed;
    static public int number;
    static public int uid;
    static long pauseOffset;
    MediaPlayer audio;
    static int vidDeleteNumber;
    boolean running;
    LinearLayout ll;



    @SuppressLint("ServiceCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Hide_navigation();
        setContentView(R.layout.activity_video_recorder);
        ButterKnife.bind(this);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        builder.detectFileUriExposure();
        check_permissions();
        makeDir();
        delPrevFile();

        //Static view id fetch
        StaticIdFetch();


        //Camera Record related
        Constants.Selected_sound_id = getString(R.string.null0);
        Constants.recording_duration = Constants.max_recording_duration;
        ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        int memoryClass = am.getMemoryClass();
        Log.e("Memory", "memoryClass:" + memoryClass);
        cameraView.addCameraKitListener(new CameraKitEventListener() {
            @Override
            public void onEvent(CameraKitEvent cameraKitEvent) {
            }

            @Override
            public void onError(CameraKitError cameraKitError) {
            }

            @Override
            public void onImage(CameraKitImage cameraKitImage) {
            }

            @Override
            public void onVideo(CameraKitVideo cameraKitVideo) {

            }
        });
        cameraView.start();
        makeDir();

        //Apply click linstener
        lly_delete.setVisibility(View.GONE);
        recyclerView_v = findViewById(R.id.recylerview_record);
        filter_image.setOnClickListener(this);
        frameLayout.setOnClickListener(this);
        lly_delete.setOnClickListener(this);
        record_image.setOnClickListener(this);
        tx60sec.setOnClickListener(this);
        tx180sec.setOnClickListener(this);
        findViewById(R.id.upload_layouti).setOnClickListener(this);
        done_btn.setEnabled(false);
        done_btn.setOnClickListener(this);
        rotate_camera.setOnClickListener(this);
        flash_btn.setOnClickListener(this);
        findViewById(R.id.Goback).setOnClickListener(this);
        add_sound_txt.setOnClickListener(this);


        //Must func
        VideoPause0();
        recyclerView_v.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        Intent intent = getIntent();
        if (intent.hasExtra("sound_name")) {
            add_sound_txt.setText(intent.getStringExtra("sound_name"));
            Constants.Selected_sound_id = intent.getStringExtra("sound_id");
            PreparedAudio();

        }
        cameraView.setCropOutput(false);
        initlize_Video_progress();
        RecordPause();
    }

    private void delPrevFile() {
        try {
            File output_filter_file = new File(Constants.output_filter_file);
            if(output_filter_file.exists())
            {
                output_filter_file.delete();
            }
            DeleteFile();
            deleteVideo();
        }
        catch ( Exception e) {
            e.printStackTrace();
        }
    }


    //Clicks
    @SuppressLint("WrongConstant")
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.rotate_camera:
                RotateCamera();
                break;
            case  R.id.upload_layouti:
                UploadLayoutClick();
                break;
            case R.id.done_next_btn:
                DoneBtnClick();
                break;
            case R.id.record_image:
                Start_or_Stop_Recording();
                break;
            case R.id.flash_camera:
                CameraFlashClick();
                break;
            case R.id.Goback:
                onBackPressed();
                break;
            case R.id.delete_layout:
                lly_delete.setClickable(false);
                funDelalertDialog();
                break;
            case R.id.video_record_filter:
                recyclerView_v.setVisibility(View.GONE);
                break;
            case R.id.frame_layout:
                frameLayoutClick();
                break;
            case R.id.add_sound_txt:
                AddSoundTextClick();
                break;
            case R.id.tx60sec:
                if (!is_recording) {
                    performVideo60SecInc();
                }
                break;
            case R.id.tx180sec:
                if (!is_recording) {
                    performVideo180SecInc();
                }
                break;

        }
    }


    /*
    CLICKS FUNC
     */

    //Rotating Camera
    private void RotateCamera() {
        cameraView.toggleFacing();
        if (facing == LensFacing.BACK) {
            facing = LensFacing.FRONT;
            if (is_flash_on) {
                flash_btn.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_on));
            }
        } else {
            flash_btn.setImageDrawable(getResources().getDrawable(R.drawable.ic__flash_off));
            facing = LensFacing.BACK;
        }
    }

    //upload video from gallery
    private void UploadLayoutClick() {
        lly_delete.setVisibility(View.GONE);
        try {
            DeleteFile();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        Pick_video_from_gallery();

    }

    //Video completed, switch to other activity
    private void DoneBtnClick() {
        if (tx60secSelected) {
            Constants.recording_duration = 60000;
        }
        else {
            Constants.recording_duration = 180000;
        }
        video_progress.pause();
        chronometer.stop();
        cameraView.setFlash(CameraKit.Constants.FLASH_OFF);
        is_recording = true;
        is_flash_on = true;
        // flashonornot=1;
        flash_btn.setImageDrawable(getResources().getDrawable(R.drawable.ic__flash_off));
        Start_or_Stop_Recording();
        showProgressDialog(this);
        done_btn.setClickable(false);
        try {
            append2();
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgressDialog();
            showAlertDialog2(Video_Recorder_A.this,getString(R.string.try_again_new));
        }
    }

    //camera Flash on/off
    private void CameraFlashClick() {
        if (is_flash_on) {
            is_flash_on = false;
            flashonornot = 0;
            flash_btn.setImageDrawable(getResources().getDrawable(R.drawable.ic__flash_off));
            cameraView.setFlash(CameraKit.Constants.FLASH_OFF);
        } else {
            flashonornot = 1;
            is_flash_on = true;
            flash_btn.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_on));
            cameraView.setFlash(CameraKit.Constants.FLASH_TORCH);
        }


    }

    //theme(hidden)
    private void frameLayoutClick() {
        recyclerView_v.setVisibility(View.GONE);
        flash_btn.setVisibility(View.VISIBLE);
        rotate_camera.setVisibility(View.VISIBLE);
        souns_layout.setVisibility(View.VISIBLE);
    }

    //Add sound click
    private void AddSoundTextClick() {
        if (add_sound_txt.getText().equals(getString(R.string.add_sound)) || add_sound_txt.getText() == null || add_sound_txt.getText().equals("")) {
            Intent intent = new Intent(this, SoundList_Main_A.class);
            startActivityForResult(intent, Sounds_list_Request_code);
            overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top);

        } else if (!add_sound_txt.getText().equals(getString(R.string.add_sound))) {
            IOSSheetDialog.SheetItem[] items = new IOSSheetDialog.SheetItem[2];
            items[0] = new IOSSheetDialog.SheetItem(getString(R.string.chnage_sound), IOSSheetDialog.SheetItem.BLUE);
            items[1] = new IOSSheetDialog.SheetItem(getString(R.string.remove_sound), IOSSheetDialog.SheetItem.RED);
            new IOSSheetDialog.Builder(Video_Recorder_A.this)
                    .setData(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (i == 0) {
                                Intent intent = new Intent(Video_Recorder_A.this, SoundList_Main_A.class);
                                startActivityForResult(intent, Sounds_list_Request_code);
                                overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top);
                            } else if (i == 1) {
                                File file = new File(Constants.app_folder + Constants.SelectedAudio_AAC);
                                if (file.exists()) {
                                    audio = new MediaPlayer();
                                    try {
                                        audio.setDataSource(Constants.app_folder + Constants.SelectedAudio_AAC);
                                        audio.release();
                                        audio = null;
                                        file.delete();
                                        Constants.musicid = "";
                                        Constants.SelectedAudio_AAC = getString(R.string.add_sound);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                                add_sound_txt.setText(getString(R.string.add_sound));
                                if (tx60secSelected) {
                                    Constants.recording_duration = 60000;
                                }
                                else {
                                    Constants.recording_duration = 180000;
                                }
                                Constants.musicid = "";
                                initlize_Video_progress();
                            }
                        }
                    }).show();
        }
    }


    //For increasing vid sec to 60 sec
    private void performVideo60SecInc() {
        if (!tx60secSelected) {
            tx180secSeleted = false;
            tx60secSelected = true;
            if (number > 0)
            {
                diallog60_180Sec();
            }
            else
            {
                Constants.recording_duration = 60000;
                Constants.max_recording_duration = 60000;
                tx60sec.setTextColor(ContextCompat.getColor(this, R.color.vid_red_color));
                tx180sec.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
                initlize_Video_progress();
            }

        }

    }


    //For increasing vid sec to 180 sec
    private void performVideo180SecInc() {
        if (!tx180secSeleted)
        {
            tx180secSeleted = true;
            tx60secSelected = false;
            if (number > 0)
            {
                diallog60_180Sec();
            }
            else
            {
                Constants.recording_duration = 180000;
                Constants.max_recording_duration = 180000;
                tx180sec.setTextColor(ContextCompat.getColor(this, R.color.vid_red_color));
                tx60sec.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
                initlize_Video_progress();
            }

        }
    }
    /*
    ID FETCH & STORAGE MADE
     */

    private void StaticIdFetch() {
        video_progress=findViewById(R.id.video_progress);
        add_sound_txt=findViewById(R.id.add_sound_txt);
        done_btn=findViewById(R.id.done_next_btn);
        uploadLayout=findViewById(R.id.upload_layouti);
        lly_delete=findViewById(R.id.delete_layout);
        chronometer=findViewById(R.id.chronometer);
        tx_holdforvideo=findViewById(R.id.camera_holforvideo);
    }



    private void makeDir() {

        if (check_permissions()) {
            File dir = new File(Environment.getExternalStorageDirectory() + "/DivineRayVideo/" + "DivineVideoCreator/");
            try {
                if (!dir.exists()) {
                    System.out.println("Directory created");
                    dir.mkdir();
                } else {
                    System.out.println("Directory is not created");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } }
        else
        {
            File dir = new File(Environment.getExternalStorageDirectory() + "/DivineRayVideo/" + "DivineVideoCreator/");
            try {
                if (!dir.exists()) {
                    System.out.println("Directory created");
                    dir.mkdir();
                } else {
                    System.out.println("Directory is not created");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }



    private void VideoPause0() {
        //for Audio Pause
        AudioManager ama = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        // Request audio focus for playback
        int result = ama.requestAudioFocus(focusChangeListener,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);


        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            // other app had stopped playing song now , so u can do u stuff now .
        }
    }

    private void RecordPause() {
        PhoneStateListener phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    //Incoming call: Pause music
                    is_recording = true;
                    is_flash_on = true;
                    flash_btn.setImageDrawable(getResources().getDrawable(R.drawable.ic__flash_off));


                    Start_or_Stop_Recording();
                } else if (state == TelephonyManager.CALL_STATE_IDLE) {
                    //Not in call: Play music
                } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    //A call is dialing, active or on hold
                }
                super.onCallStateChanged(state, incomingNumber);
            }
        };
        TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if (mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }

    }

    private final AudioManager.OnAudioFocusChangeListener focusChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                public void onAudioFocusChange(int focusChange) {
                    try {
                        MediaPlayer mediaPlayerBackground = new MediaPlayer();
                        AudioManager am = (AudioManager) Objects.requireNonNull(getApplicationContext()).getSystemService(Context.AUDIO_SERVICE);
                        switch (focusChange) {

                            case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK):
                                // Lower the volume while ducking.
                                mediaPlayerBackground.setVolume(0.2f, 0.2f);
                                break;
                            case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT):
                                mediaPlayerBackground.stop();
                                break;

                            case (AudioManager.AUDIOFOCUS_LOSS):
                                mediaPlayerBackground.stop();

                                break;

                            case (AudioManager.AUDIOFOCUS_GAIN):
                                // Return the volume to normal and resume if paused.
                                mediaPlayerBackground.setVolume(1f, 1f);
                                mediaPlayerBackground.start();
                                break;
                            default:
                                break;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };



    /*
    VIDEO RECORD FUNC
     */


    public void initlize_Video_progress() {

        Log.e("Test", "duration" + Constants.recording_duration);
        video_progress = findViewById(R.id.video_progress);
        video_progress.enableAutoProgressView(Constants.recording_duration);
        video_progress.setDividerColor(Color.BLACK);
        video_progress.setProgresBarColor(Color.BLACK);
        video_progress.setProgressColor(Color.BLUE);
        video_progress.setDividerEnabled(true);
        video_progress.setDividerWidth(4);
        int rColor = Color.parseColor("#e84558");
        video_progress.setShader(new int[]{Color.WHITE, Color.WHITE, Color.WHITE});

        video_progress.SetListener(new ProgressBarListener() {
            @Override
            public void TimeinMill(long mills) {
                sec_passed = (int) (mills / 1000);

                if (sec_passed > (Constants.recording_duration / 1000 - 1)) {
                    Start_or_Stop_Recording();

                } else {
                    lly_delete.setVisibility(View.GONE);
                }
                if (sec_passed - 1 >= 1) {
                    Log.e("Test", "2");
                    done_btn.setVisibility(View.VISIBLE);
                    done_btn.setEnabled(true);
                }


            }
        });
    }

    private void deleteVideo() {
        File file = new File(Constants.root0 + "myvideo" + (vidDeleteNumber) + ".mp4");
        if (file.exists()) {
            file.delete();
            vidDeleteNumber = vidDeleteNumber - 1;
            number = number - 1;

        }
        if(number==0||vidDeleteNumber==0)
        {
            rotate_camera.setVisibility(View.VISIBLE);
        }
        if (vidDeleteNumber >= 1) {

            video_progress.enableAutoProgressView(Constants.recording_duration);
            video_progress.reset();
            String value = chronArray.get(chronArray.size() - 2);
            sec_passed = Integer.parseInt(value) / 1000;
            chronometer.stop();
            pauseOffset = Long.parseLong(value);
            chronArray.remove(chronArray.size() - 1);
            chronometer.setBase(SystemClock.elapsedRealtime() - Long.parseLong(value));
            chronometer.stop();
            record_image.setClickable(true);

        } else {
            lly_delete.setVisibility(View.GONE);
            video_progress.reset();
            video_progress.resetComplete();
            chronometer.setVisibility(View.GONE);
            tx_holdforvideo.setVisibility(View.VISIBLE);
            uploadLayout.setVisibility(View.VISIBLE);
            pauseOffset = 0;
            uid = 0;
            vidDeleteNumber = 0;
            number = 0;
            chronArray.clear();
            int color = Color.WHITE;
            add_sound_txt.setClickable(true);
            add_sound_txt.setTextColor(color);
            done_btn.setVisibility(View.GONE);
            chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
            chronometer.stop();
            record_image.setClickable(true);
            sec_passed = 0;

        }


    }



    // if the Recording is stop then it we start the recording
    // and if the mobile is recording the video then it will stop the recording
    public void Start_or_Stop_Recording() {
        makeDir();
        if (!is_recording && sec_passed < ((Constants.recording_duration / 1000 - 1))) {
            VideoPause0();
            Log.e("Work", "1" + duratio_meas);
            done_btn.setClickable(false);
            tx_holdforvideo.setVisibility(View.GONE);
            chronometer.setVisibility(View.VISIBLE);
            number = number + 1;
            rotate_camera.setVisibility(View.GONE);
            filter_layout.setVisibility(View.GONE);
            lly_delete.setVisibility(View.GONE);
            if (!running) {
                chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
                chronometer.start();
                running = true;
            }
            Log.e("Testc", "T: " + pauseOffset);
            uploadLayout.setVisibility(View.GONE);
            is_recording = true;
            vidDeleteNumber = number;
            File file = new File(Constants.root0 + "myvideo" + (number) + ".mp4");
            videopaths.add(Constants.root0 + "myvideo" + (number) + ".mp4");
            cameraView.captureVideo(file);
            if (audio != null)
                audio.start();
            video_progress.resume();
            done_btn.setEnabled(false);
            record_image.setImageDrawable(getResources().getDrawable(R.drawable.camera_pause));
            camera_options.setVisibility(View.GONE);
            int color = Color.LTGRAY;
            add_sound_txt.setClickable(false);
            add_sound_txt.setTextColor(color);
        } else if (is_recording) {
            if (running) {
                chronometer.stop();
                pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();
                String ChronValue1 = String.valueOf(pauseOffset);
                chronArray.add(ChronValue1);
                running = false;
            }
            is_recording = false;
            video_progress.pause();
            video_progress.addDivider();
            if (audio != null)
                audio.pause();
            cameraView.stopVideo();
            lly_delete.setVisibility(View.VISIBLE);
            if (sec_passed >= (Constants.recording_duration / 1000)) {
                Log.e("Test", "4" + sec_passed + "S:" + Constants.recording_duration / 1000);
                try {
                    done_btn.setVisibility(View.VISIBLE);
                    done_btn.setEnabled(true);
                    cameraView.setFlash(CameraKit.Constants.FLASH_OFF);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
            record_image.setImageDrawable(getResources().getDrawable(R.drawable.camera_start));
            done_btn.setClickable(true);
            camera_options.setVisibility(View.VISIBLE);
            lly_delete.setVisibility(View.VISIBLE);
            if (sec_passed > (Constants.recording_duration / 1000 - 1)) {
                lly_delete.setVisibility(View.VISIBLE);
                chronometer.stop();
            }
            uploadLayout.setVisibility(View.GONE);
        } else if (sec_passed > (Constants.recording_duration / 1000 - 1)) {
            lly_delete.setVisibility(View.VISIBLE);
            chronometer.stop();
        }
    }


    private boolean append2() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<String> dd;
                ArrayList<String> video_list = new ArrayList<>();
                for (int i = 0; i < videopaths.size(); i++) {
                    File file = new File(videopaths.get(i));
                    if (file.exists()) {
                        try {
                            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                            retriever.setDataSource(Video_Recorder_A.this, Uri.fromFile(file));
                            String hasVideo = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_HAS_VIDEO);
                            boolean isVideo = "yes".equals(hasVideo);

                            if (isVideo && file.length() > 3000) {
                                Log.e("resp", videopaths.get(i));
                                video_list.add(videopaths.get(i));

                            }
                        } catch (Exception e) {
                            Log.d(Constants.tag, e.toString());
                            showAlertDialog2(Video_Recorder_A.this,"Try again in some time !");
                        }
                    }
                }
                Log.e("trans","start");
                try {

                    Movie[] inMovies = new Movie[video_list.size()];

                    for (int i = 0; i < video_list.size(); i++) {
                        MovieCreator mvd=new MovieCreator();
                        inMovies[i] = mvd.build(video_list.get(i));

                    }


                    List<Track> videoTracks = new LinkedList<Track>();
                    List<Track> audioTracks = new LinkedList<Track>();
                    for (Movie m : inMovies) {
                        for (Track t : m.getTracks()) {
                            if (t.getHandler().equals("soun")) {
                                audioTracks.add(t);
                            }
                            if (t.getHandler().equals("vide")) {
                                videoTracks.add(t);
                            }
                        }
                    }
                    Movie result = new Movie();
                    if (audioTracks.size() > 0) {
                        result.addTrack(new AppendTrack(audioTracks.toArray(new Track[audioTracks.size()])));
                    }
                    if (videoTracks.size() > 0) {
                        result.addTrack(new AppendTrack(videoTracks.toArray(new Track[videoTracks.size()])));
                    }
                    String outputFilePath = null;
                    if (audio != null) {
                        outputFilePath = Constants.outputfile;
                    } else {
                        outputFilePath = Constants.outputfile2;
                    }


                    BasicContainer out = (BasicContainer) new DefaultMp4Builder().build(result);
                    MovieHeaderBox mvhd = Path.getPath(out, "moov/mvhd");

                    @SuppressWarnings("resource")
                    FileChannel fc = new RandomAccessFile(new File(outputFilePath), "rw").getChannel();
                    out.writeContainer(fc);
                    fc.close();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            if (audio != null)
                                Merge_withAudio();
                            else {
                                Go_To_preview_Activity();
                            }
                        }
                    });
                } catch (Exception e) {

                }
            }
        }).start();


        return true;
    }

    // Function to remove duplicates from an ArrayList
    public static <T> ArrayList<T> removeDuplicates(ArrayList<T> list) {
        // Create a new ArrayList
        ArrayList<T> newList = new ArrayList<T>();
        // Traverse through the first list
        for (T element : list) {
            // If this element is not present in newList
            // then add it
            if (!newList.contains(element)) {
                newList.add(element);
            }
        }
        return newList;
    }

    // this will add the select audio with the video
    public void Merge_withAudio() {
        String audio_file;
        audio_file = Constants.app_folder + Constants.SelectedAudio_AAC;
        String video = Constants.root0 + "output.mp4";
        String finaloutput = Constants.outputfile2;
        File file = new File(finaloutput);
        if (file.exists()) {
            file.delete();
        }
        Log.e("Test", "11");
        Merge_Video_Audio merge_video_audio = new Merge_Video_Audio(getApplicationContext());
        merge_video_audio.doInBackground(audio_file, video, finaloutput);

    }


    public void Pick_video_from_gallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        intent.setType("video/*");
        startActivityForResult(intent, Constants.Pick_video_from_gallery);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {

            if (cameraView != null) {
                cameraView.stopVideo();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        chronometer.stop();
        pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();
        running=false;
        if (tx60secSelected) {
            Constants.recording_duration = 60000;
        }
        else {
            Constants.recording_duration = 180000;
        }
        video_progress.pause();
        cameraView.setFlash(CameraKit.Constants.FLASH_OFF);
        String    ChronValue1= String.valueOf(pauseOffset);
        chronArray.add(ChronValue1);
        is_recording=true;
        is_flash_on=true;
        flashonornot=1;
        flash_btn.setImageDrawable(getResources().getDrawable(R.drawable.ic__flash_off));
        Start_or_Stop_Recording();


    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Sounds_list_Request_code) {
                if (data != null) {

                    if (data.getStringExtra("isSelected").equals("yes")) {
                        add_sound_txt.setText(data.getStringExtra("sound_name"));
                        Constants.Selected_sound_id = data.getStringExtra("sound_id");
                        PreparedAudio();
                    }

                }

            } else if (requestCode == Constants.Pick_video_from_gallery) {
                Uri uri = data.getData();
                File video_file = null;
                try {
                    video_file = FileUtils.getFileFromUri(this, uri);
                } catch (Exception e) {
                    e.printStackTrace();
                }



                try {

                    Chnage_Video_size(video_file.getAbsolutePath(), Constants.gallery_resize_video);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public long getfileduration(Uri uri) {
        try {

            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(this, uri);
            String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            final int file_duration = Integer.parseInt(durationStr);

            return file_duration;
        }
        catch (Exception e){

        }
        return 0;
    }


    public void Chnage_Video_size(String src_path,String destination_path){
        check_permissions();
        Functions.Show_indeterminent_loader(this,false,false);
        File source = new File(src_path);
        File destination = new File(destination_path);
        // try {
        if (source.exists()) {

            InputStream in = null;
            try {
                in = new FileInputStream(source);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            OutputStream out = null;
            try {
                out = new FileOutputStream(destination);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            byte[] buf = new byte[1024];
            int len = 0;

            while (true) {
                try {
                    if (!((len = in.read(buf)) > 0)) break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    out.write(buf, 0, len);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Intent intent=new Intent(Video_Recorder_A.this,NewVideoTrimActivity.class);
            intent.putExtra("video_path", Constants.gallery_resize_video);
            intent.putExtra("sound_name",add_sound_txt.getText().toString());
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            Functions.cancel_indeterminent_loader();
        } else {
            Toast.makeText(Video_Recorder_A.this, "Failed to get video from Device", Toast.LENGTH_SHORT).show();
            Functions.cancel_indeterminent_loader();
        }

    }

    @SuppressLint("StaticFieldLeak")
    public  void startTrim(final File src, final File dst, final int startMs, final int endMs) throws IOException {
        check_permissions();

        try {
            new AsyncTask<String,Void,String>() {
                @Override
                protected String doInBackground(String... strings) {
                    String[] complexCommand = { "-y", "-i", src.getAbsolutePath(),"-ss", "" + startMs / 1000, "-t", "" + (endMs - startMs) / 1000, "-c","copy", dst.getAbsolutePath()};
                    FFmpeg.execute(complexCommand);

                    return "Ok";
                }

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    Functions.Show_indeterminent_loader(Video_Recorder_A.this,true,true);
                }







                @Override
                protected void onPostExecute(String result) {
                    if(result.equals("error")){
                        Functions.cancel_indeterminent_loader();
                        Toast.makeText(Video_Recorder_A.this, "Try Again", Toast.LENGTH_SHORT).show();
                    }else {
                        Functions.cancel_indeterminent_loader();
                        Chnage_Video_size(Constants.gallery_trimed_video, Constants.gallery_resize_video);
                    }
                }


            }.execute();
        }
        catch (Exception e)
        {
            e.printStackTrace();


        }


    }



    // this will play the sound with the video when we select the audio

    public  void PreparedAudio(){
        File file=new File(Constants.app_folder+Constants.SelectedAudio_AAC);
        if(file.exists()) {
            audio = new MediaPlayer();
            try {

                audio.setDataSource(Constants.app_folder + Constants.SelectedAudio_AAC);
                audio.setLooping(true);
                audio.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }

            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(this, Uri.fromFile(file));
            String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            initlize_Video_progress();
            //     }

        }


    }


    @Override
    protected void onResume() {
        super.onResume();
        if(add_sound_txt.getText().toString().trim().equals("Add Sound"))
        {
            Constants.musicid="";
        }
        if(check_permissions()) {
            if (cameraView != null) {
                cameraView.start();
            }
        }
        if (number==0)
        {
            lly_delete.setVisibility(View.GONE);
            uploadLayout.setVisibility(View.VISIBLE);
        }
        File dir = new File(Environment.getExternalStorageDirectory() + "/DivineRayVideo/"+"DivineVideoCreator/");
        try {
            if (!dir.exists()) {

                System.out.println("Directory created");
                dir.mkdir();
                //cameraView.start();
            } else {
                //   cameraView.start();
                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (tx60secSelected) {
            Constants.recording_duration = 60000;
        }
        else {
            Constants.recording_duration = 180000;
        }
        video_list.clear();


        check_permissions();
        //    setUpCamera();

        dismissProgressDialog();


    }




    @Override
    protected void onDestroy() {
        super.onDestroy();


        DeleteFile();
        try {

            if (audio != null) {
                audio.stop();
                audio.reset();
                audio.release();
            }
            //   GPUCameraRecorder.stop();
            // cameraView.stop();
            cameraView.setFlash(CameraKit.Constants.FLASH_OFF);
        }catch (Exception e){

        }
    }




    @Override
    public void onBackPressed() {

        dismissProgressDialog();
        new IOSDialog.Builder(Video_Recorder_A.this)
                .setMessage(getString(R.string.want_to_exit))
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                        cameraView.destroyDrawingCache();
                        cameraView.stop();
                        select_postion=0;
                        DeleteFile();
                        File file = new File(Constants.root0  + "myvideo"+(delete_count)+".mp4");
                        if(file.exists()){
                            file.delete();
                            DeleteFile();
                        }
                        video_progress.resetComplete();
                        Intent intent=new Intent(Video_Recorder_A.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                }).show();


    }


    void  funDelalertDialog()
    {

        IOSDialog.Builder builder=new IOSDialog.Builder(Video_Recorder_A.this)
                .setMessage(getString(R.string.delete_previous_clip))
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteVideo();
                        dialogInterface.dismiss();
                        lly_delete.setClickable(true);
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                        lly_delete.setClickable(true);
                    }
                });
        builder.setCancelable(false);
        builder.show();


    }


    public void Go_To_preview_Activity(){
        Intent intent =new Intent(this,Preview_Video_A.class);
        intent.putExtra("sec_passed",String.valueOf(sec_passed));
        intent.putExtra("chronArray",chronArray);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }


    // this will delete all the video parts that is create during priviously created video
    static int delete_count=0;
    public static void DeleteFile(){
        delete_count++;
        File file = new File(Constants.root0  + "myvideo"+(delete_count)+".mp4");
        if(file.exists()){
            file.delete();
            DeleteFile();

        }
        File galley_filter_file = new File(Constants.gallery_resize_video);

        if(galley_filter_file.exists()){
            galley_filter_file.delete();
        }
        File galley_trim_video = new File(Constants.gallery_trimed_video);

        if(galley_trim_video.exists()){
            galley_trim_video.delete();
        }
        File output = new File(Constants.outputfile);
        File output2 = new File(Constants.outputfile2);

        if(output.exists()){
            output.delete();
        }
        if(output2.exists()){

            output2.delete();
        }

        video_progress.resetComplete();
        chronometer.setVisibility(View.GONE);
        tx_holdforvideo.setVisibility(View.VISIBLE);

        lly_delete.setVisibility(View.GONE);
        uploadLayout.setVisibility(View.VISIBLE);
        pauseOffset = 0;
        uid = 0;
        vidDeleteNumber = 0;
        number = 0;

        int color = Color.WHITE;
        add_sound_txt.setClickable(true);
        add_sound_txt.setTextColor(color);
        done_btn.setVisibility(View.GONE);
        chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
        chronometer.stop();
        sec_passed=0;


    }




    @SuppressLint("NewApi")
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && hasFocus)
        {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
    // we need 4 permission during creating an video so we will get that permission
    // before start the video recording
    public boolean check_permissions() {

        String[] PERMISSIONS = new String[0];

        PERMISSIONS = new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.CAMERA
        };

        if (!hasPermissions(getApplicationContext(), PERMISSIONS)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS, 2);
            }
        }else if (hasPermissions(getApplicationContext(), PERMISSIONS)) {
        }
        else {
            return true;
        }



        return false;
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }

            }
        }
        return true;


    }




    public void showAlertDialog2(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finish();
            }
        });
        alertDialog.show();
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        chronometer.setBase(SystemClock.elapsedRealtime());
        rotate_camera.setVisibility(View.VISIBLE);
    }


    //Dialog for  Sec
    public  void diallog60_180Sec()
    {
        new IOSDialog.Builder(Video_Recorder_A.this)
                .setMessage(getString(R.string.video_already_rec))
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        DelData();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private void DelData() {
        if (tx180secSeleted) {
            Constants.recording_duration = 180000;
            Constants.max_recording_duration = 180000;
            tx180sec.setTextColor(ContextCompat.getColor(this, R.color.vid_red_color));
            tx60sec.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        }else {
            Constants.recording_duration = 60000;
            Constants.max_recording_duration = 60000;
            tx60sec.setTextColor(ContextCompat.getColor(this, R.color.vid_red_color));
            tx180sec.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        }
        initlize_Video_progress();
        if (number>0) {
            DeleteFile();
            File file = new File(Constants.root0);
            if (file.exists()) {
                file.delete();
            }
            if (audio != null && audio.isPlaying()) {
                audio.pause();
                audio.seekTo(0);
                audio.start();
            }
            makeDir();
        }
    }
}
