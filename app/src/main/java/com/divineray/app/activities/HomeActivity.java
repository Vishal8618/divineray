package com.divineray.app.activities;


import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.ActionMode;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;


import com.divineray.app.Add.AddFragment;
import com.divineray.app.Chat.ChatFragment;
import com.divineray.app.Connection.ConnectionDetector;
import com.divineray.app.Home.HomeBaseFragment;
import com.divineray.app.Home.HomeFragment;
import com.divineray.app.Home.SelectedUserProfile;
import com.divineray.app.Profile.ProfileFragment;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Search.SearchFragment;
import com.divineray.app.Utils.Constants;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.Video_Recording.Video_Recorder_A;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.LogoutModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.appupdate.AppUpdateOptions;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeActivity extends BaseActivity {
    int REQUEST_CODE=11;
    private ConnectionDetector detector;
    long mBackPressed;
    /*
      Getting the Current Class Name
     */
    String TAG = HomeActivity.this.getClass().getSimpleName();

    /*
      Current Activity Instance
     */
    Activity mActivity = HomeActivity.this;

    String pageType;
    String menuFragment,datatype,roomid,name,userid,searchFragment;

     BroadcastReceiver mReciever;



    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {


        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;


            switch (item.getItemId()) {
                case R.id.ic_home:


                    selectedFragment = new HomeBaseFragment();

                    break;

                case R.id.ic_chat:

                    selectedFragment = new ChatFragment();

                    break;


                case R.id.ic_add:


                    selectedFragment =AddFragment.newInstance();

                        File dir = new File(Environment.getExternalStorageDirectory() + "/DivineRayVideo/" + "DivineVideoCreator/");
                        try {
                            if (!dir.exists()) {

                                System.out.println("Directory created");
                                dir.mkdir();
                            } else {

                                System.out.println("Directory is not created");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                            Intent intent = new Intent(mActivity, Video_Recorder_A.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            mActivity.overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top);





                    break;

                case R.id.ic_search:
                    selectedFragment = SearchFragment.newInstance();
                    break;
                case R.id.ic_profile:

                    selectedFragment = ProfileFragment.newInstance();

                    break;
            }

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.commit();

            return true;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        getSupportActionBar().hide();

        getWindow().setStatusBarColor(Color.TRANSPARENT);
    // this lines ensure only the status-bar to become transparent without affecting the nav-bar
    getWindow().getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR|View.SYSTEM_UI_FLAG_LAYOUT_STABLE|View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);


    setContentView( R.layout.activity_home );

        File dir = new File(Environment.getExternalStorageDirectory() + "/DivineRayVideo/" + "VideoThumbs/");
        try {
            if (dir.exists()) {

                dir.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        Intent intent=getIntent();
        if (intent!=null) {
             menuFragment = intent.getStringExtra("menuFragment");
            datatype  = getIntent().getStringExtra("Datatype");
            userid  = getIntent().getStringExtra("chatUserId");
            roomid  = getIntent().getStringExtra("roomId");
            name  = getIntent().getStringExtra("name");
            searchFragment=getIntent().getStringExtra("searchFragment");
        }

        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(HomeActivity.this);

// Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

// Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // This example applies an immediate update. To apply a flexible update
                    // instead, pass in AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                // Request the update.
            try {
                appUpdateManager.startUpdateFlowForResult(
                        // Pass the intent that is returned by 'getAppUpdateInfo()'.
                        appUpdateInfo,
                        // The current activity making the update request.
                        this,
                        // Or pass 'AppUpdateType.FLEXIBLE' to newBuilder() for
                        // flexible updates.
                        AppUpdateOptions.newBuilder(AppUpdateType.IMMEDIATE)
                                .setAllowAssetPackDeletion(true)
                                .build(),
                        // Include a request code to later monitor this update request.
                        REQUEST_CODE);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                try {
                    appUpdateManager.startUpdateFlowForResult(
                            // Pass the intent that is returned by 'getAppUpdateInfo()'.
                            appUpdateInfo,
                            // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                            AppUpdateType.IMMEDIATE,
                            // The current activity making the update request.
                            this,
                            // Include a request code to later monitor this update request.
                            REQUEST_CODE);
                } catch (IntentSender.SendIntentException sendIntentException) {
                    sendIntentException.printStackTrace();
                }
            }


            }
        });
        // If menuFragment is defined, then this activity was launched with a fragment selection

       try {
           if (menuFragment != null) {

               // Here we can decide what do to -- perhaps load other parameters from the intent extras such as IDs, etc
               if (menuFragment.equals("favoritesMenuItem")) {

                   FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                   ChatFragment myFragment = new ChatFragment();



                   transaction.replace( R.id.frame_layout, myFragment );
                   transaction.commit();

                   BottomNavigationView navigation = (BottomNavigationView) findViewById( R.id.navigation );
                   navigation.setOnNavigationItemSelectedListener( mOnNavigationItemSelectedListener );
                   navigation.setSelectedItemId(R.id.ic_chat);

               }
               else if (menuFragment.equals("searchMenuItem"))
               {


                   BottomNavigationView navigation = (BottomNavigationView) findViewById( R.id.navigation );
                   navigation.setOnNavigationItemSelectedListener( mOnNavigationItemSelectedListener );
                   navigation.setSelectedItemId(R.id.ic_search);
                   Bundle args=new Bundle();
                   args.putString("showLiveUsers","true");
                   FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                   SearchFragment myFragment = new SearchFragment();
                   myFragment.setArguments(args);
                   transaction.replace( R.id.frame_layout, myFragment );
                   transaction.commit();
               }

           } else {
               // Activity was not launched with a menuFragment selected -- continue as if this activity was opened from a launcher (for example)
               if (check_permissions()) {
                   FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                   transaction.replace(R.id.frame_layout, HomeFragment.newInstance());
                   transaction.commit();

               }
           }
       }
       catch (Exception e)
       {
           e.printStackTrace();
       }

        detector = new ConnectionDetector(mActivity);

        // check Internet
        if (detector.isInternetAvailable())
        {
            Log.e("e", "Internet Present");
        }
        else
        {
            Log.e("e", "No Internet");
            this.registerReceiver(this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
//        Intent intent=getIntent();
//        pageType=intent.getStringExtra("pageType");



//        if (check_permissions())
//        {
//            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//            transaction.replace( R.id.frame_layout, HomeFragment.newInstance() );
//            transaction.commit();
//            }



        BottomNavigationView navigation = (BottomNavigationView) findViewById( R.id.navigation );
        navigation.setOnNavigationItemSelectedListener( mOnNavigationItemSelectedListener );


    }


    // we need 4 permission during creating an video so we will get that permission
    // before start the video recording
    public boolean check_permissions() {

        String[] PERMISSIONS = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.CAMERA
        };

        if (!hasPermissions(getApplicationContext(), PERMISSIONS)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS, 2);
            }
        }else if (hasPermissions(getApplicationContext(), PERMISSIONS)) {

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, new HomeBaseFragment());
            transaction.commit();
        }
        else {
            return true;
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, new HomeBaseFragment());
        transaction.commit();


        return false;
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }

            }
        }
        return true;


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
        finish();
        }


    @Override
    protected void onRestart() {
        super.onRestart();
        Branch branch = Branch.getInstance(getApplicationContext());
        // Branch init
        branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    Log.e("BRANCH SDK", referringParams.toString());
                    // Retrieve deeplink keys from 'referringParams' and evaluate the values to determine where to route the user
                    // Check '+clicked_branch_link' before deciding whether to use your Branch routing logic
                    initDeepLinkSignIn(referringParams);
                } else {
                    Log.e("BRANCH SDK", error.getMessage());

                }
            }
        }, this.getIntent().getData(), this);

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

    }

    protected void onResume()
    {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter("com.divineray.app.Homeactivity");
        mActivity.registerReceiver(mConnReceiver, intentFilter);
        Branch branch = Branch.getInstance(getApplicationContext());
        // Branch init
        branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    Log.e("BRANCH SDK", referringParams.toString());
                   initDeepLinkSignIn(referringParams);
                } else {
                    Log.e("BRANCH SDK", error.getMessage());

                }
            }
        }, this.getIntent().getData(), this);


    }

    private void initDeepLinkSignIn(JSONObject referringParams) {
        if (referringParams.has("profileUserId") ) {
            //shoWDialog("show")

            try {
                switch (referringParams.getString("type")) {

                    case  "1" :
                        Log.e("perform",""+referringParams.getString("profileUserId"));
                        String val=referringParams.getString("profileUserId");
                        if (IsUserLogin()) {
                            // if (!val.equals(getUserID())) {
                            try {
                                startActivity(new Intent(mActivity, HomeActivity.class));
                                finish();
                                Intent intent = new Intent(this, SelectedUserProfile.class);
                                intent.putExtra("profileUserId", val);
                                startActivity(intent);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        break;
                    case "2" :
                        String val0 = referringParams.getString("profileUserId");
                        if (IsUserLogin()) {
                            try {
                                startActivity(new Intent(mActivity, HomeActivity.class));
                                Constants.Redirection_type=referringParams.getString("type");
                                Constants.Redirection_value=referringParams.getString("videoId");
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    default:


                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            // Log.e("fawfafwqfqsssw===",referringParams.getString("institutionUID"))

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        mActivity.unregisterReceiver(mConnReceiver);
    }

    private BroadcastReceiver mConnReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context context, Intent intent)
        {
            boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            String reason = intent.getStringExtra(ConnectivityManager.EXTRA_REASON);
            boolean isFailover = intent.getBooleanExtra(ConnectivityManager.EXTRA_IS_FAILOVER, false);

            NetworkInfo currentNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            NetworkInfo otherNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_OTHER_NETWORK_INFO);
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

            if (isNetworkAvailable(mActivity))
            {
                finish();
                startActivity(getIntent());

            }

        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                //Toast.makeText(mActivity, "Update Cancelled", Toast.LENGTH_SHORT).show();
                // If the update is cancelled or fails,
                // you can request to start the update again.
                Log.e(TAG,"Update Cancelled");
            }
            else
            {
            //    executeLogoutApi();
            }
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity, DivineRayPrefernces.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLogoutApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.logoutRequest(getAuthToken(),mParam()).enqueue(new Callback<LogoutModel>() {
            @Override
            public void onResponse(Call<LogoutModel> call, Response<LogoutModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                LogoutModel mModel = response.body();
                if (mModel.getStatus().equals(1)) {
                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.LOGOUT_ID, mModel.getLogoutId());
                    SharedPreferences preferences = DivineRayPrefernces.getPreferences(Objects.requireNonNull(mActivity));
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                    editor.apply();
                    mActivity.onBackPressed();

                    Intent mIntent = new Intent(mActivity, LoginActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<LogoutModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
    
}

