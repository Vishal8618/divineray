package com.divineray.app.activities;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daasuu.gpuv.camerarecorder.LensFacing;
import com.divineray.app.Agora.ConstantsA;
import com.divineray.app.Agora.activities.BaseActivity;
import com.divineray.app.Agora.activities.SettingsActivity;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.GoingLiveModel;
import com.divineray.app.model.SignUpModel;
import com.wonderkiln.camerakit.CameraKitError;
import com.wonderkiln.camerakit.CameraKitEvent;
import com.wonderkiln.camerakit.CameraKitEventListener;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.agora.rtc.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LiveStreaming1Activity extends BaseActivity {

    /*
   Getting the Current Class Name
  */
    String TAG = LiveStreaming1Activity.this.getClass().getSimpleName();

    Activity mActivity=LiveStreaming1Activity.this;

    @BindView(R.id.camera)
    CameraView cameraView;
    @BindView(R.id.imClose)
    ImageView imClose;
    @BindView(R.id.imCameraSwitch)
    ImageView imCameraSwitch;
    @BindView(R.id.imGoLive)
    ImageView txGoLive;
    @BindView(R.id.imSettingk)
    ImageView imSetting;

    LensFacing facing;
    protected LensFacing lensFacing = LensFacing.BACK;

    // Permission request code of any integer value
    private static final int PERMISSION_REQ_CODE = 1 << 4;

    private String[] PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @SuppressLint("ServiceCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
       // Hide_navigation();
        setContentView(R.layout.activity_live_streaming1);
        ButterKnife.bind(this);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
        check_permissions();
        //for high quality streaming 1080*720p
        config().setVideoDimenIndex(5);
    }



    @OnClick({R.id.imClose, R.id.imCameraSwitch,R.id.imGoLive,R.id.imSettingk})
    public  void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.imClose:
                onBackPressed();
                break;
            case R.id.imCameraSwitch:
                performSwitch();
                break;
            case R.id.imGoLive:
                performActivitySwitch();
                break;
            case R.id.imSettingk:
                performSettingsSwitch();
                break;

        }

    }

    private void performSettingsSwitch() {
        Intent i = new Intent(mActivity, SettingsActivity.class);
        startActivity(i);
    }

    private void performActivitySwitch() {
        txGoLive.setClickable(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
             txGoLive.setClickable(true);
            }
        },1500);
        checkPermission();
    }

    private void performSwitch() {
        cameraView.toggleFacing();
        if (facing == LensFacing.BACK) {
            facing = LensFacing.FRONT;


        } else {


            facing = LensFacing.BACK;
        }

    }

    @SuppressLint("ServiceCast")
    public  void performCamera() {
        try {
            cameraView.addCameraKitListener(new CameraKitEventListener() {
                @Override
                public void onEvent(CameraKitEvent cameraKitEvent) {
                }

                @Override
                public void onError(CameraKitError cameraKitError) {
                }

                @Override
                public void onImage(CameraKitImage cameraKitImage) {
                }

                @Override
                public void onVideo(CameraKitVideo cameraKitVideo) {

                }
            });

          //  cameraView.setCropOutput(false);
            cameraView.start();
        } catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    //Permission for live streaming
    private void checkPermission() {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per)) {
                granted = false;
                break;
            }
        }

        if (granted) {
            resetLayoutAndForward();
        } else {
            requestPermissions();
        }
    }

    private boolean permissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults) {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }

            if (granted) {
                resetLayoutAndForward();
            } else {
                toastNeedPermissions();
            }
        }
    }


    private void toastNeedPermissions() {
        Toast.makeText(this, R.string.need_permissions, Toast.LENGTH_LONG).show();
    }

    private void resetLayoutAndForward() {

        executeApi();

    }



    public void gotoRoleActivity(String stream_room, String userID) {
        ConstantsA.channelNameA=stream_room;
        int uid = (int) (System.currentTimeMillis() / 1000L);
        ConstantsA.useridA=String.valueOf(uid);
        onJoinAsBroadcaster();
    }

    public void onJoinAsBroadcaster() {
        gotoLiveActivity(Constants.CLIENT_ROLE_BROADCASTER);
    }

    public void onJoinAsAudience() {
        gotoLiveActivity(Constants.CLIENT_ROLE_AUDIENCE);
    }

    private void gotoLiveActivity(int role) {
        cameraView.stop();
        Intent intent = new Intent();
        intent.putExtra(ConstantsA.KEY_CLIENT_ROLE, role);
        intent.setClass(getApplicationContext(), LiveStreaming2Activity.class);
        startActivity(intent);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        performCamera();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeLiveStream(getAuthToken(),mParam()).enqueue(new Callback<GoingLiveModel>() {
            @Override
            public void onResponse(Call<GoingLiveModel> call, Response<GoingLiveModel> response) {
                dismissProgressDialog();
                GoingLiveModel mModel = response.body();
                Log.e(TAG, "LiveSream1" + mModel.getStreamDetail().getStream_id());
                if (mModel.getStatus()==0) {
                    showAlertDialog2(mActivity, mModel.getMessage());
                } else
                {
                    ConstantsA.StramID=mModel.getStreamDetail().getStream_id();
                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.STREAM_USERID, getUserID());
                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.STREAM_ROOMID,mModel.getStreamDetail().getStream_room());
                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.STREAM_USERNAME, getUserName());
                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.STREAM_ID, mModel.getStreamDetail().getStream_id());
                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.STREAM_USERPHOTO, getProfilePic());
                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.COMMENT_DISABLE, "0");
                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.LIKE_DISABLE, "0");
                    gotoRoleActivity(mModel.getStreamDetail().getStream_room(),getUserID());
                } }

            @Override
            public void onFailure(Call<GoingLiveModel> call, Throwable t) {
                dismissProgressDialog();
                showAlertDialog2(mActivity,t.getMessage());
            }
        });
    }

    // we need 4 permission during creating an video so we will get that permission
    // before start the video recording
    public boolean check_permissions() {

        String[] PERMISSIONS = new String[0];

        PERMISSIONS = new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.CAMERA
        };

        if (!hasPermissions(getApplicationContext(), PERMISSIONS)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS, 2);
            }
        }else if (hasPermissions(getApplicationContext(), PERMISSIONS)) {
            performCamera();
        }
        else {
            return true;
        }



        return false;
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }

            }
        }
        return true;


    }


    @Override
    public void onBackPressed() {
        cameraView.destroyDrawingCache();
        cameraView.stop();
        finish();
           }

}