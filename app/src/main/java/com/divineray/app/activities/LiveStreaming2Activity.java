package com.divineray.app.activities;

import androidx.core.app.ShareCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.divineray.app.Agora.ConstantsA;
import com.divineray.app.Agora.activities.RtcBaseActivity;
import com.divineray.app.Agora.stats.LocalStatsData;
import com.divineray.app.Agora.stats.RemoteStatsData;
import com.divineray.app.Agora.stats.StatsData;
import com.divineray.app.Agora.ui.VideoGridContainer;
import com.divineray.app.Animation.Direction;
import com.divineray.app.Animation.ZeroGravityAnimation;
import com.divineray.app.Utils.DivineRayAppplication;
import com.divineray.app.Home.SelectedUserProfile;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.adapters.LiveStream1Adapter;
import com.divineray.app.adapters.LiveVieweradapter;
import com.divineray.app.adapters.RequestUsersadapter;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.CloudRecordStartModel;
import com.divineray.app.model.EndStream2Model;
import com.divineray.app.model.EndStreamModel;
import com.divineray.app.model.GoingLiveModel;
import com.divineray.app.model.LiveViewersModel;
import com.divineray.app.model.StatusModel;
import com.divineray.app.model.StreamListModel;
import com.divineray.app.model.StreamMessageModel;
import com.divineray.app.model.ViewCounModel;
import com.divineray.app.model.cloudre.CloudRecordingResponse;
import com.divineray.app.service.Restarter;
import com.divineray.app.service.YourService;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.video.VideoEncoderConfiguration;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LiveStreaming2Activity extends RtcBaseActivity {

    String TAG = LiveStreaming2Activity.this.getClass().getSimpleName();


    Activity mActivity = LiveStreaming2Activity.this;

    @BindView(R.id.imClose)
    ImageView imClose;
    @BindView(R.id.live_btn_switch_camera)
    ImageView live_btn_switch_camera;
    @BindView(R.id.live_btn_beautification)
    ImageView beautyBtn;
    @BindView(R.id.live_btn_mute_audio)
    ImageView mMuteAudioBtn;
    @BindView(R.id.live_btn_mute_video)
    ImageView mMuteVideoBtn;
    @BindView(R.id.live_video_grid_layout)
    VideoGridContainer mVideoGridContainer;
    @BindView(R.id.txPeople)
    TextView txPeople;
    @BindView(R.id.imProfilePic)
    ImageView imProfilePic;
    @BindView(R.id.txName)
    TextView txName;
    @BindView(R.id.chat_recycler_view)
    RecyclerView recyclerViewChat;
    @BindView(R.id.imLike)
    ImageView imLike;
    @BindView(R.id.imSend)
    ImageView imSend;
    @BindView(R.id.imFinalUser)
    ImageView imFinalUser;
    @BindView(R.id.edComment)
    EditText edComment;
    @BindView(R.id.viewRequestStream)
    ImageView viewRequestStream;
    @BindView(R.id.llyExit)
    LinearLayout llyExit;
    @BindView(R.id.llyShare)
    LinearLayout llyShare;
    @BindView(R.id.txExt)
    TextView txExt;
    @BindView(R.id.imProfilePicStream)
    ImageView imProfilePicStream;
    @BindView(R.id.imProfilePic2)
    ImageView imProfilePic2;
    @BindView(R.id.imCommentLikeDis)
    ImageView imCommentLikeDis;
    @BindView(R.id.dummyLLyTx)
    LinearLayout dummyLLyTx;
    @BindView(R.id.llyMainComment)
    LinearLayout llyMainComment;

    static int peopleCount = 0;
    int totalCount = 0;
    int viewCount = 1;
    String toServerUnicodeEncoded = "";
    String toServer;
    SharedPreferences prefv;
    private Socket mSocket;
    private LiveStream1Adapter adapter;
    boolean pushEndStremCheck = false;
    String checkJoin = "1";
    String streamLink = "";

    private VideoEncoderConfiguration.VideoDimensions mVideoDimension;
    ArrayList<StreamMessageModel.Data> mArrayList = new ArrayList<>();
    ArrayList<StreamListModel.Joinuserdetails> mArrayListJoinedUsers = new ArrayList<>();
    ArrayList<LiveViewersModel.DataL> mArrayListViewers = new ArrayList<>();

    String typeAppRejKick = "";
    boolean disableComment = false;
    String commentEnabled = "1";
    String likeEnabled = "1";


    RequestUsersadapter requestUsersadapter;
    LiveVieweradapter liveUsersadapter;
    RecyclerView recyclerViewLiveUsers;
    RecyclerView recyclerViewLiveViewers;
    String reqUserid = "";
    int roleSeeData;


    Intent mServiceIntent;
    private YourService mYourService;
    boolean onDestroyMethodCall = false;


    @Override
    protected void onResume() {
        super.onResume();
        DivineRayAppplication app = (DivineRayAppplication) getApplication();
        mSocket = app.getSocket();
        mSocket.emit("ConncetedChat", getStreamRoomID());

        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("newMessage", onNewMessage);
        mSocket.on("leaveChat", onUserLeft);
        mSocket.on("online", onUserJoined);
        mSocket.connect();
        executeChatDetailsApi();
        if (rtcEngine() != null) {
            if (mMuteVideoBtn.isActivated()) {
                rtcEngine().enableVideo();
            }
        }
    }


    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("Test", "connect");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //   Toast.makeText(mActivity, "Connect", Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("Test", "disconnect");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("Test", "Error connecting");
                }
            });
        }
    };

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject mJsonObject = new JSONObject(args[1].toString());
                        String type = mJsonObject.getString("type");
                        //Reject live stream
                        if (type.equals("3")) {
                            String userid = mJsonObject.getString("userid");
                            if (userid.equals(getUserID())) {
                                StreamMessageModel.Data mModel = new StreamMessageModel.Data();
                                mModel.setName(getUserName());
                                mModel.setPhoto(getProfilePic());
                                mModel.setType("3");

                                if (mArrayList.size() == 0) {
                                    mArrayList.add(mModel);
                                    setAdapter();
                                } else {
                                    mArrayList.add(mModel);
                                    adapter.notifyDataSetChanged();
                                    if (mArrayList.size() > 1) {
                                        recyclerViewChat.scrollToPosition(mArrayList.size() - 1);

                                    }
                                }
                            }
                        }
                        //Kick user joined
                        else if (type.equals("4")) {
                            String userid = mJsonObject.getString("userid");
                            if (userid.equals(getUserID())) {
                                StreamMessageModel.Data mModel = new StreamMessageModel.Data();
                                mModel.setName(getUserName());
                                mModel.setPhoto(getProfilePic());
                                mModel.setType("4");

                                if (mArrayList.size() == 0) {
                                    mArrayList.add(mModel);
                                    setAdapter();
                                } else {
                                    mArrayList.add(mModel);
                                    adapter.notifyDataSetChanged();
                                    if (mArrayList.size() > 1) {
                                        recyclerViewChat.scrollToPosition(mArrayList.size() - 1);

                                    }
                                }
                            }
                            getHostLiveData();
                        }
                        //Stream ended by host
                        else if (type.equals("5")) {
                            Glide.with(mActivity).load(R.drawable.bg_live_stream).into(imFinalUser);
                            closeKeyboard(mActivity);
                            pushEndStremCheck = true;
                            imProfilePicStream.setVisibility(View.VISIBLE);
                            imFinalUser.setVisibility(View.VISIBLE);
                            llyExit.setVisibility(View.VISIBLE);
                            getHostLiveData();

                        }
                        //Some user left with api
                        else if (type.equals("6")) {
                            if (!disableComment) {
                                String name = mJsonObject.getString("userName");
                                String photo = mJsonObject.getString("photo");
                                String message = mJsonObject.getString("message");
                                StreamMessageModel.Data mModel = new StreamMessageModel.Data();
                                mModel.setName(name);
                                mModel.setPhoto(photo);
                                mModel.setComment(message);
                                mModel.setType("9");

                                if (mArrayList.size() == 0) {
                                    mArrayList.add(mModel);
                                    setAdapter();
                                } else {
                                    mArrayList.add(mModel);
                                    adapter.notifyDataSetChanged();
                                    if (mModel != null) {
                                        recyclerViewChat.scrollToPosition(mArrayList.size() - 1);

                                    }
                                }
                            }
                            getHostLiveData();
                        }
                        //user joined/left the chat for count updation
                        else if (type.equals("7")) {
                            String count = mJsonObject.getString("count");
                            txPeople.setText(count);

                        }
                        //user joined with name text in chat listing
                        else if (type.equals("8")) {
                            if (!disableComment) {
                                String name = mJsonObject.getString("name");
                                String photo = mJsonObject.getString("photo");
                                StreamMessageModel.Data mModel = new StreamMessageModel.Data();
                                mModel.setName(name);
                                mModel.setPhoto(photo);
                                mModel.setType("8");

                                if (mArrayList.size() == 0) {
                                    mArrayList.add(mModel);
                                    setAdapter();
                                } else {
                                    mArrayList.add(mModel);
                                    adapter.notifyDataSetChanged();
                                    if (mModel != null) {
                                        recyclerViewChat.scrollToPosition(mArrayList.size() - 1);

                                    }
                                }
                            }
                        }
                        //When Join stream requests received,show red dot on new list
                        else if (type.equals("9")) {
                            showHideRequestCount();
                        }
                        //When other user liking the stream, it willl show heart animation
                        else if (type.equals("10")) {
                            flyObject(R.drawable.heart_new_grey, 1500, Direction.BOTTOM, Direction.TOP, 0.12f);

                        }
                        //Accepting the request to join stream for host
                        else if (type.equals("11")) {
                            String user_id = mJsonObject.getString("userid");
                            if (user_id.equals(getUserID())) {
                                int role = 1;
                                showorHideButtons(role);
                                mMuteVideoBtn.setActivated(true);
                                startBroadcast();
                                checkJoin = "1";
                            }

                        }
                        //When user have been rejected to join stream/ or kick
                        else if (type.equals("13")) {
                            String user_id = mJsonObject.getString("userid");
                            if (user_id.equals(getUserID())) {
                                stopBroadcast();
                                int role = 2;
                                showorHideButtons(role);
                                checkJoin = "2";
                            }
                        }
                        //Disable like for viewers
                        else if (type.equals("14")) {
                            String user_id = mJsonObject.getString("userid");
                            String enabled = mJsonObject.getString("enable");
                            if (!user_id.equals(getUserID())) {
                                if (enabled.equals("0")) {
                                    Likedisable();
                                } else {
                                    Likeeanable();
                                }
                            }
                        }
                        //Disable comment for viewers
                        else if (type.equals("15")) {

                            disableComment = true;
                            String user_id = mJsonObject.getString("userid");
                            String enabled = mJsonObject.getString("enable");
                            if (!user_id.equals(getUserID())) {
                                if (enabled.equals("0")) {
                                    Commentdisable();
                                } else {
                                    CommentEnable();
                                }
                            }
                        }
                        //Disable comment for viewers
                        else if (type.equals("16")) {
                            //if (getStreamID().equals(getUserID())) {
                            getHostLiveData();
                            ///p}
                        }
                        //user name copunt for new user joining the stream
                        else if (type.equals("100")) {
                            Log.e("Test", "100 case");
                        }
                        //set header data
                        else if (type.equals("101")) {
                            liveStremHeadset();
                        }
                        //updation of comments
                        else {
                            Log.e("tesD", mJsonObject.toString());
                            StreamMessageModel.Data model = new Gson().fromJson(mJsonObject.toString(), StreamMessageModel.Data.class);
                            if (mArrayList.size() == 0) {
                                executeChatDetailsApi();
                            }
                            mArrayList.add(model);
                            adapter.notifyDataSetChanged();
                            if (model != null) {
                                recyclerViewChat.scrollToPosition(mArrayList.size() - 1);
                            }

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };

    private void Commentdisable() {
        llyMainComment.setVisibility(View.GONE);
        dummyLLyTx.setVisibility(View.VISIBLE);
        imSend.setVisibility(View.GONE);

    }


    private void Likedisable() {
        imLike.setVisibility(View.GONE);
    }

    private void Likeeanable() {
        imLike.setVisibility(View.VISIBLE);

    }


    private Emitter.Listener endStream = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.e("Test", "end stream");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.e("Test", "endStrea," + args[1].toString());
                        JSONObject mJsonObject = new JSONObject(args[1].toString());
                        Log.e("tesD", mJsonObject.toString());
                        EndStreamModel model = new Gson().fromJson(mJsonObject.toString(), EndStreamModel.class);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };

    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.e("Test", "User joined");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject mJsonData = null;
                    try {
                        mJsonData = new JSONObject(String.valueOf(args[1]));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };

    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("Test", "User Left");
                }
            });
        }
    };

    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    boolean isTyping = (boolean) args[1];
                    Log.e("Test", "User Typing");
                }
            });
        }
    };

    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("Test", "user stop typing");
                }
            });
        }
    };

    private Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {

        }
    };


    @SuppressLint("ServiceCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_live_streaming2);
        ButterKnife.bind(this);
        cloudRecordStart();
        txName.setText(getStreamUserName());
        getHostLiveData();
        initUI();
        initData();
        getDetails();
        executeInitialData();

    }


    @Override
    public void onPause() {
        super.onPause();
        if (rtcEngine() != null && mMuteVideoBtn.isActivated()) {
            rtcEngine().disableVideo();
            mMuteAudioBtn.setActivated(false);
        }

    }


    //Click handlers
    @OnClick({R.id.imClose, R.id.imSend, R.id.imProfilePic, R.id.txName, R.id.imLike, R.id.txExt, R.id.live_btn_mute_video, R.id.imCommentLikeDis, R.id.viewRequestStream,R.id.llyShare})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imClose:
                finishLiveStream();
                break;
            case R.id.imSend:
                funsendMessage();
                break;
            case R.id.imProfilePic:
                roomBottomSheetOpen();
                break;
            case R.id.txName:
                roomBottomSheetOpen();
                break;
            case R.id.imLike:
                performLikeAnimation();
                break;
            case R.id.txExt:
                searchScreenExitWithoutPush();
                break;
            case R.id.live_btn_mute_video:
                onMuteVideoClicked(view);
                break;
            case R.id.imCommentLikeDis:
                commentLikeBottomSheetOpen();
                break;
            case R.id.viewRequestStream:
                roomBottomSheetOpen();
                break;
            case R.id.llyShare:
                exceuteShareStreamApi();
                break;
        }
    }

    private void shareStreamLink() {
        ShareCompat.IntentBuilder.from(mActivity)
                .setType("text/plain")
                .setChooserTitle(R.string.share_url)
                .setText(getString(R.string.check_my_stream) + streamLink + getString(R.string.regards) + getStreamUserName())
                .startChooser();
    }

    //Initial Data
    private void executeInitialData() {
        int role = getIntent().getIntExtra(
                ConstantsA.KEY_CLIENT_ROLE,
                Constants.CLIENT_ROLE_AUDIENCE);
        roleSeeData = role;
        checkJoin = String.valueOf(role);
        int typeVal = 8;
        if (role == 2) {
            executeViewCountApi(typeVal);
            imCommentLikeDis.setVisibility(View.GONE);
        }
        showorHideButtons(role);
        performCommentLikeCheck();
        mYourService = new YourService();
        mServiceIntent = new Intent(this, mYourService.getClass());
        if (role == 1) {
            com.divineray.app.Utils.Constants.endStreamDone = false;
            if (!isMyServiceRunning(mYourService.getClass())) {
                startService(mServiceIntent);
            }
        }
    }


    //Cloud Record start
    private void cloudRecordStart() {
        int role = getIntent().getIntExtra(
                ConstantsA.KEY_CLIENT_ROLE,
                Constants.CLIENT_ROLE_AUDIENCE);
        boolean isBroadcaster = (role == Constants.CLIENT_ROLE_BROADCASTER);

        if (isBroadcaster) {
            ConstantsA.useridA = getUserID();
            RtcInit();
            startBroadcast();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    executeCloudRecordApi();

                }
            }, 1000);

        }
    }

    //get Live data
    private void getHostLiveData() {
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getHostLiveDetails(getAuthToken(), mParams()).enqueue(new Callback<StreamListModel>() {
            @Override
            public void onResponse(Call<StreamListModel> call, Response<StreamListModel> response) {
                StreamListModel mGetDetailsModel = response.body();
                if (mGetDetailsModel != null) {
                    if (mGetDetailsModel.getStreamDetail().get(0).getStream_end_time() != null &&
                            !mGetDetailsModel.getStreamDetail().get(0).getStream_end_time().equals("")) {

                        Glide.with(mActivity).load(R.drawable.bg_live_stream).into(imFinalUser);
                        imProfilePicStream.setVisibility(View.VISIBLE);
                        imFinalUser.setVisibility(View.VISIBLE);
                        llyExit.setVisibility(View.VISIBLE);

                    }
                    txName.setText(getStreamUserName());
                    imProfilePic2.setVisibility(View.GONE);
                    if (mGetDetailsModel.getJoinuserdetails() != null) {
                        txName.setText(getStreamUserName());
                        imProfilePic2.setVisibility(View.GONE);
                        try {
                            if (mGetDetailsModel.getJoinuserdetails() != null) {
                                RequestOptions options = new RequestOptions()
                                        .placeholder(R.drawable.unknown_user)
                                        .error(R.drawable.unknown_user)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .priority(Priority.HIGH)
                                        .dontAnimate()
                                        .dontTransform();
                                if (mGetDetailsModel.getJoinuserdetails().get(0).getStatus().equals("1")) {
                                    Glide.with(mActivity)
                                            .load(mGetDetailsModel.getJoinuserdetails().get(0).getPhoto())
                                            .apply(options)
                                            .into(imProfilePic2);
                                    imProfilePic2.setVisibility(View.VISIBLE);
                                    if (mGetDetailsModel.getJoinuserdetails().size() == 1) {
                                        txName.setText(getStreamUserName() + " and " + mGetDetailsModel.getJoinuserdetails().get(0).getName());
                                    }
                                    if (mGetDetailsModel.getJoinuserdetails().size() > 1) {
                                        txName.setText(getStreamUserName() + " and " + mGetDetailsModel.getJoinuserdetails().size() + " others");
                                    }
                                } else {
                                    txName.setText(getStreamUserName());
                                    imProfilePic2.setVisibility(View.GONE);
                                }
                            } else {
                                txName.setText(getStreamUserName());
                                imProfilePic2.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        txName.setText(getStreamUserName());
                        imProfilePic2.setVisibility(View.GONE);
                    }

                }

            }

            @Override
            public void onFailure(Call<StreamListModel> call, Throwable t) {
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }


    //Like comment check
    private void performCommentLikeCheck() {
        if (getDisableComment().equals("1")) {
            disableComment = true;
            Commentdisable();
        } else {
            disableComment = false;
            CommentEnable();
        }
        if (getDisableLike().equals("1")) {
            Likedisable();
        } else {
            LikeEnable();
        }
    }

    private void LikeEnable() {
        imLike.setVisibility(View.VISIBLE);
    }

    //Clicks
    private void CommentEnable() {
        llyMainComment.setVisibility(View.VISIBLE);
        dummyLLyTx.setVisibility(View.GONE);
        imSend.setVisibility(View.VISIBLE);
    }


    private void funsendMessage() {
        if (edComment.getText().toString().trim().length() > 0) {
            sendComment();
        } else {
            showToast(getResources().getString(R.string.type_something));
        }
    }

    private void performReuestBottomSheetClick() {
        txName.setClickable(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                txName.setClickable(true);
            }
        }, 1500);
        imProfilePic.setClickable(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                imProfilePic.setClickable(true);
            }
        }, 1500);
    }

    private void showHideRequestCount() {
        int role = getIntent().getIntExtra(
                ConstantsA.KEY_CLIENT_ROLE,
                Constants.CLIENT_ROLE_AUDIENCE);
        if (role == 1) {
            viewRequestStream.setVisibility(View.VISIBLE);
        } else {
            viewRequestStream.setVisibility(View.GONE);
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParamsb() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        mMap.put("stream_id", getStreamID());
        mMap.put("stream_room", getStreamRoomID());
        mMap.put("is_view", String.valueOf(viewCount));
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeViewCountApi(int typeVal) {
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getViewCounts(getAuthToken(), mParamsb()).enqueue(new Callback<ViewCounModel>() {
            @Override
            public void onResponse(Call<ViewCounModel> call, Response<ViewCounModel> response) {
                ViewCounModel mGetDetailsModel = response.body();
                if (mGetDetailsModel != null) {
                    if (mGetDetailsModel.getStatus() == 1) {
                        txPeople.setText(mGetDetailsModel.getTotalView());
                        totalCount = Integer.parseInt(mGetDetailsModel.getTotalView());
                        performLoginScreenCount(totalCount, mGetDetailsModel, typeVal);
                    } else {

                        Log.e("Data", "0");
                    }
                }
            }

            @Override
            public void onFailure(Call<ViewCounModel> call, Throwable t) {
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void performLoginScreenCount(int totalCountn, ViewCounModel mGetDetailsModel, int typeVal) {


        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put("count", String.valueOf(totalCountn));
            jsonObject.put("type", "7");
            Log.e("checkio", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("newMessage", getStreamRoomID(), jsonObject, "hdhd");
        performJoinUsernameCount(mGetDetailsModel, typeVal);

    }

    private void performJoinUsernameCount(ViewCounModel mGetDetailsModel, int typeVal) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put("name", mGetDetailsModel.getName());
            jsonObject.put("photo", mGetDetailsModel.getPhoto());
            jsonObject.put("message", getString(R.string.joined));
            jsonObject.put("type", String.valueOf(typeVal));
            Log.e("checkio", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("newMessage", getStreamRoomID(), jsonObject, "hdhd");
    }


    private void showorHideButtons(int role) {
        if (role == 2) {
            live_btn_switch_camera.setVisibility(View.GONE);
            beautyBtn.setVisibility(View.GONE);
            mMuteAudioBtn.setVisibility(View.GONE);
            mMuteVideoBtn.setVisibility(View.GONE);
        } else {
            live_btn_switch_camera.setVisibility(View.VISIBLE);
            beautyBtn.setVisibility(View.VISIBLE);
            mMuteAudioBtn.setVisibility(View.VISIBLE);
            mMuteVideoBtn.setVisibility(View.VISIBLE);
        }
    }


    private void performLikeAnimation() {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put("type", "10");
            Log.e("checkio", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("newMessage", getStreamRoomID(), jsonObject, "hdhd");
        flyObject(R.drawable.heart, 1500, Direction.BOTTOM, Direction.TOP, 1f);

    }


    private void roomBottomSheetOpen() {
        performReuestBottomSheetClick();
        View view = (View) LayoutInflater.from(mActivity).inflate(R.layout.fragment_request_user_bottom_sheet2, null);

        view.setBackgroundColor(Color.TRANSPARENT);
        view.setBackgroundTintMode(PorterDuff.Mode.CLEAR);
        view.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(mActivity);
        bottomSheetDialog.setContentView(view);

        viewRequestStream.setVisibility(View.GONE);
        int role = getIntent().getIntExtra(
                ConstantsA.KEY_CLIENT_ROLE,
                Constants.CLIENT_ROLE_AUDIENCE);
        TextView txLeava = view.findViewById(R.id.txLeaveJoinTheRoom);
        if (Integer.parseInt(checkJoin) == 1) {
            txLeava.setText(getResources().getString(R.string.leave_the_room));
        } else {
            txLeava.setText(getResources().getString(R.string.request_to_join));
        }
        txLeava.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                txLeava.setVisibility(View.GONE);
                if (isNetworkAvailable(mActivity)) {
                    performLeaveorJointheRoom(Integer.parseInt(checkJoin));
                    JoinUserPush();
                    getHostLiveData();
                } else {
                    showToast(mActivity, getResources().getString(R.string.internet_conection));
                }

            }
        });

        exceuteHostLivedetails(view, role);
        executeLiveUsersApi(view, role);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                bottomSheetDialog.show();

            }
        }, 500);

    }


    /*
     * Execute api
     * */
    private Map<String, String> mParams1() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        mMap.put("stream_id", getStreamID());
        mMap.put("stream_room", getStreamRoomID());
        Log.e("Param", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLiveUsersApi(View view, int role) {
        recyclerViewLiveViewers = view.findViewById(R.id.recyclerViewers);
        TextView txViewers = view.findViewById(R.id.txViewers);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getViewers(getAuthToken(), mParams1()).enqueue(new Callback<LiveViewersModel>() {
            @Override
            public void onResponse(Call<LiveViewersModel> call, Response<LiveViewersModel> response) {
                LiveViewersModel mGetDetailsModel = response.body();
                if (mGetDetailsModel != null) {
                    mArrayListViewers = (ArrayList<LiveViewersModel.DataL>) mGetDetailsModel.getData();

                    if (mGetDetailsModel.getStatus() == 1) {
                        txViewers.setVisibility(View.VISIBLE);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
                        recyclerViewLiveViewers.setLayoutManager(layoutManager);
                        liveUsersadapter = new LiveVieweradapter(mActivity, mArrayListViewers, new LiveVieweradapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(int positon, LiveViewersModel.DataL item, View view) {
                                switch (view.getId()) {
                                    case R.id.imUserPicViewer:
                                        reqUserid = item.getUser_id();
                                        openProfile();
                                        break;
                                    case R.id.txUsernameViewer:
                                        reqUserid = item.getUser_id();
                                        openProfile();
                                        break;

                                }


                            }
                        });
                        recyclerViewLiveViewers.setAdapter(liveUsersadapter);


                    } else {
                        txViewers.setVisibility(View.GONE);
                    }
                }

            }

            @Override
            public void onFailure(Call<LiveViewersModel> call, Throwable t) {
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }


    private void commentLikeBottomSheetOpen() {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.fragment_request_user_bottom_sheet3, null);
        view.setBackgroundColor(Color.TRANSPARENT);
        view.setBackgroundTintMode(PorterDuff.Mode.CLEAR);
        view.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(mActivity);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.show();
        viewRequestStream.setVisibility(View.GONE);
        int role = getIntent().getIntExtra(
                ConstantsA.KEY_CLIENT_ROLE,
                Constants.CLIENT_ROLE_AUDIENCE);
        TextView txOffComment = view.findViewById(R.id.txOffComment);
        TextView txOffLiking = view.findViewById(R.id.txOffLiking);
        if (commentEnabled.equals("1")) {
            txOffComment.setText(getResources().getString(R.string.turn_off_commenting));
        } else {
            txOffComment.setText(getResources().getString(R.string.turn_on_commenting));

        }
        if (likeEnabled.equals("1")) {
            txOffLiking.setText(getResources().getString(R.string.turn_off_liking));
        } else {
            txOffLiking.setText(getResources().getString(R.string.turn_on_liking));

        }
        txOffComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reqUserid = getUserID();
                typeAppRejKick = "2";
                if (commentEnabled.equals("1")) {
                    commentEnabled = "0";
                } else {
                    commentEnabled = "1";
                }
                executeDisableLikeCommentApi();
                bottomSheetDialog.dismiss();
            }
        });

        txOffLiking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reqUserid = getUserID();
                typeAppRejKick = "1";
                if (likeEnabled.equals("1")) {
                    likeEnabled = "0";
                } else {
                    likeEnabled = "1";
                }
                executeDisableLikeCommentApi();
                bottomSheetDialog.dismiss();
            }
        });
    }

    private void executeDisableLikeCommentApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.disableLikeComment(getAuthToken(), mParamsN()).enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                dismissProgressDialog();
                StatusModel mGetDetailsModel = response.body();
                Log.e("Response", "**PARAM**" + response.body().toString());
                if (mGetDetailsModel != null) {
                    if (mGetDetailsModel.getStatus() == 1) {
                        if (typeAppRejKick.equals("1")) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject();
                                jsonObject.put("type", "14");
                                jsonObject.put("enable", likeEnabled);
                                jsonObject.put("userid", getUserID());
                                Log.e("checkio", jsonObject.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mSocket.emit("newMessage", getStreamRoomID(), jsonObject, "hdhd");
                        } else {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject();
                                jsonObject.put("type", "15");
                                jsonObject.put("enable", commentEnabled);
                                jsonObject.put("userid", getUserID());
                                Log.e("checkio", jsonObject.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mSocket.emit("newMessage", getStreamRoomID(), jsonObject, "hdhd");

                        }

                    } else {
                        showAlertDialog(mActivity, mGetDetailsModel.getMessage());
                    }
                }


            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });

    }


    private void performLeaveorJointheRoom(int role) {
        if (Integer.parseInt(checkJoin) == 1) {
            int rolen = getIntent().getIntExtra(
                    ConstantsA.KEY_CLIENT_ROLE,
                    Constants.CLIENT_ROLE_AUDIENCE);
            if (rolen == 1) {

                finishLiveStream();
            } else {

                stopBroadcast();
                typeAppRejKick = "2";
                reqUserid = getUserID();
                showorHideButtons(2);
                executeApproveRejectOwnApi();
            }
        } else {
            executeRequestStreamApi();

        }
    }

    private void executeRequestStreamApi() {
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.requestRoom(getAuthToken(), mParams()).enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                StatusModel mGetDetailsModel = response.body();
                if (mGetDetailsModel.getStatus() == 1) {
                    //  showAlertDialog(mActivity,mGetDetailsModel.getMessage());
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject();
                        jsonObject.put("userid", getUserID());
                        jsonObject.put("type", "9");
                        Log.e("checkio", jsonObject.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mSocket.emit("newMessage", getStreamRoomID(), jsonObject, "hdhd");

                } else {
                    showAlertDialog(mActivity, mGetDetailsModel.getMessage());
                }

            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void exceuteHostLivedetails(View view, int role) {
        recyclerViewLiveUsers = view.findViewById(R.id.recyclerviewUsers);
        TextView txNoData = view.findViewById(R.id.txNoUserAvailable);
        LinearLayout llyNodata = view.findViewById(R.id.llyNoUsersavail);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getHostLiveDetails(getAuthToken(), mParams()).enqueue(new Callback<StreamListModel>() {
            @Override
            public void onResponse(Call<StreamListModel> call, Response<StreamListModel> response) {
                StreamListModel mGetDetailsModel = response.body();

                if (mGetDetailsModel != null) {
                    if (role == 1) {
                        mArrayListJoinedUsers = (ArrayList<StreamListModel.Joinuserdetails>) mGetDetailsModel.getJoinuserdetails();
                    } else {
                        mArrayListJoinedUsers = (ArrayList<StreamListModel.Joinuserdetails>) mGetDetailsModel.getJoinuserdetails();
                        for (int i = mArrayListJoinedUsers.size() - 1; i >= 0; i--) {
                            if (!mArrayListJoinedUsers.get(i).getStatus().equals("1")) {
                                mArrayListJoinedUsers.remove(i);
                            }
                        }
                    }
                    if (mGetDetailsModel.getStatus() == 1) {
                        if (mGetDetailsModel.getJoinuserdetails().size() > 0) {
                            try {
                                LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
                                recyclerViewLiveUsers.setLayoutManager(layoutManager);

                                requestUsersadapter = new RequestUsersadapter(mActivity, mArrayListJoinedUsers, roleSeeData, new RequestUsersadapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(int positon, StreamListModel.Joinuserdetails item, View view) {
                                        switch (view.getId()) {
                                            case R.id.imAccept:
                                                typeAppRejKick = "1";
                                                reqUserid = item.getUser_id();
                                                if (isNetworkAvailable(mActivity)) {
                                                    performAccept(positon, item, getString(R.string.accept), view, txNoData, llyNodata, recyclerViewLiveUsers);
                                                    JoinUserPush();
                                                }

                                                break;
                                            case R.id.imReject:
                                                typeAppRejKick = "2";
                                                reqUserid = item.getUser_id();
                                                if (isNetworkAvailable(mActivity)) {
                                                    performAccept(positon, item, getString(R.string.reject), view, txNoData, llyNodata, recyclerViewLiveUsers);
                                                    performPushReject(3);
                                                    JoinUserPush();
                                                } else {
                                                    showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                                                }

                                                break;
                                            case R.id.llyKick:
                                                typeAppRejKick = "2";
                                                reqUserid = item.getUser_id();
                                                if (isNetworkAvailable(mActivity)) {
                                                    performAccept(positon, item, getString(R.string.kick_n), view, txNoData, llyNodata, recyclerViewLiveUsers);
                                                    performPushReject(4);
                                                    JoinUserPush();
                                                } else {
                                                    showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                                                }
                                                break;
                                            case R.id.imUserPic:
                                                reqUserid = item.getUser_id();
                                                openProfile();
                                                break;
                                        }
                                    }
                                });
                                recyclerViewLiveUsers.setAdapter(requestUsersadapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            recyclerViewLiveUsers.setVisibility(View.GONE);
                            txNoData.setVisibility(View.VISIBLE);
                            llyNodata.setVisibility(View.VISIBLE);
                        }
                    } else {
                        recyclerViewLiveUsers.setVisibility(View.GONE);
                        txNoData.setVisibility(View.VISIBLE);
                        llyNodata.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<StreamListModel> call, Throwable t) {
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void openProfile() {
        Intent intent = new Intent(mActivity, SelectedUserProfile.class);
        intent.putExtra("profileUserId", reqUserid);
        startActivity(intent);
    }

    private void performPushReject(int i) {
        if (i == 3) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject();
                jsonObject.put("type", "3");
                jsonObject.put("userid", reqUserid);
                Log.e("checkio", jsonObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit("newMessage", getStreamRoomID(), jsonObject, "hdhd");
        } else {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject();
                jsonObject.put("type", "4");
                jsonObject.put("userid", reqUserid);
                Log.e("checkio", jsonObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit("newMessage", getStreamRoomID(), jsonObject, "hdhd");
        }
    }

    private void performAccept(int positon, StreamListModel.Joinuserdetails item, String acceptReject, View view, TextView txNoData, LinearLayout llyNodata, RecyclerView recyclerViewLiveUsers) {
        if (isNetworkAvailable(mActivity)) {
            executeApproveRejectApi(positon, item, acceptReject, view, txNoData, llyNodata, recyclerViewLiveUsers);
        } else {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        }
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParamsN() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", reqUserid);
        mMap.put("stream_id", getStreamID());
        mMap.put("stream_room", getStreamRoomID());
        mMap.put("type", typeAppRejKick);
        Log.e("Param", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeApproveRejectApi(int positon, StreamListModel.Joinuserdetails item, String acceptReject, View view1, TextView txNoData, LinearLayout llyNodata, RecyclerView recyclerViewLiveUsers) {
        //  showProgressDialog(mActivity);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.approveRejectStream(getAuthToken(), mParamsN()).enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                StatusModel mGetDetailsModel = response.body();
                if (mGetDetailsModel != null) {
                    if (mGetDetailsModel.getStatus() == 1) {
                        getHostLiveData();

                        pushNotifForJoinedUsers();
                        if (acceptReject.equals(getString(R.string.accept))) {
                            if (mArrayListJoinedUsers != null) {
                                mArrayListJoinedUsers.get(positon).setStatus("1");
                                requestUsersadapter.notifyDataSetChanged();
                            }
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject();
                                jsonObject.put("type", "11");
                                jsonObject.put("userid", reqUserid);
                                Log.e("checkio", jsonObject.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mSocket.emit("newMessage", getStreamRoomID(), jsonObject, "hdhd");

                        } else {
                            if (mArrayListJoinedUsers != null) {
                                mArrayListJoinedUsers.remove(positon);
                                requestUsersadapter.notifyDataSetChanged();
                            }
                            if (mArrayListJoinedUsers.size() == 0) {
                                recyclerViewLiveUsers.setVisibility(View.GONE);
                                txNoData.setVisibility(View.VISIBLE);
                                llyNodata.setVisibility(View.VISIBLE);
                            }
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject();
                                jsonObject.put("type", "13");
                                jsonObject.put("userid", item.getUser_id());
                                Log.e("checkio", jsonObject.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mSocket.emit("newMessage", getStreamRoomID(), jsonObject, "hdhd");

                        }

                    } else {
                        showAlertDialog(mActivity, mGetDetailsModel.getMessage());
                    }
                }


            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                dismissProgressDialog();
                showAlertDialog(mActivity, t.getMessage());
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void pushNotifForJoinedUsers() {
        JSONObject jsonObject0 = null;
        try {

            jsonObject0 = new JSONObject();
            jsonObject0.put("type", "16");
            Log.e("checkio", jsonObject0.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("newMessage", getStreamRoomID(), jsonObject0, "hdhd");

    }


    private void executeApproveRejectOwnApi() {
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.approveRejectStream(getAuthToken(), mParamsN()).enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                StatusModel mGetDetailsModel = response.body();
                Log.e("Response", "**PARAM**" + response.body().toString());
                if (mGetDetailsModel != null) {
                    if (mGetDetailsModel.getStatus() == 1) {
                        checkJoin = "2";
                    } else {
                        showAlertDialog(mActivity, mGetDetailsModel.getMessage());
                    }
                }


            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }


    private void sendComment() {
        imSend.setClickable(false);
        toServer = edComment.getText().toString();
        edComment.setText("");

        if (!toServer.equals("")) {
            toServerUnicodeEncoded = StringEscapeUtils.escapeJava(toServer);
            toServerUnicodeEncoded = StringEscapeUtils.unescapeJava(toServer);
            if (!isNetworkAvailable(mActivity)) {
                showToast(getResources().getString(R.string.internet_conection));
                imSend.setClickable(true);
            } else {
                fetchData();

            }
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams12() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        mMap.put("stream_id", getStreamID());
        mMap.put("comment", toServer);
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void fetchData() {

        StreamMessageModel.Data mModel = new StreamMessageModel.Data();
        mModel.setUser_id(getUserID());
        mModel.setComment(String.valueOf(toServer));
        mModel.setName(getUserName());
        mModel.setPhoto(getProfilePic());
        mModel.setType("1");
        mArrayList.add(mArrayList.size(), mModel);


        try {
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();

        }
        if (mArrayList.size() > 1) {
            recyclerViewChat.scrollToPosition(mArrayList.size() - 1);

        }
        imSend.setClickable(true);

        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.addStreamComment(getAuthToken(), mParams12()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dismissProgressDialog();

                if (mArrayList == null || mArrayList.size() < 1) {
                    executeChatDetailsApi();
                }

                toServerUnicodeEncoded = "";
                imSend.setClickable(true);
                dismissProgressDialog();
                JSONObject mJsonObject = null;
                try {
                    mJsonObject = new JSONObject(response.body().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    JSONObject mObject = mJsonObject.getJSONObject("data");
                    Log.e("checkio", mObject.toString());

                    mSocket.emit("newMessage", getStreamRoomID(), mObject, edComment.getText().toString().trim());
                    if (mArrayList.size() < 2) {
                        executeChatDetailsApi();
                    }
                    if (mArrayList != null && mArrayList.size() > 0) {
                        mArrayList.remove(mArrayList.size() - 1);
                    }
                    StreamMessageModel.Data model = new StreamMessageModel.Data();

                    model.setId(mObject.getString("id"));
                    model.setUser_id(mObject.getString("user_id"));
                    model.setStream_id(mObject.getString("stream_id"));
                    model.setStream_room(mObject.getString("stream_room"));
                    model.setComment(mObject.getString("comment"));
                    model.setType(mObject.getString("type"));
                    model.setComment_time(mObject.getString("comment_time"));
                    model.setName(mObject.getString("name"));
                    model.setPhoto(mObject.getString("photo"));


                    mArrayList.add(model);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                Log.e("", "**RESPONSE**" + response.body());


                if (adapter != null) {

                    adapter.notifyDataSetChanged();
                    recyclerViewChat.scrollToPosition(mArrayList.size() - 1);

                }


            }


            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }


    private void getDetails() {
        if (!isNetworkAvailable(mActivity)) {
            showToast(getResources().getString(R.string.internet_conection));
        } else {
            executeChatDetailsApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        mMap.put("stream_id", getStreamID());
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeChatDetailsApi() {
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getStreamChatListing(getAuthToken(), mParams()).enqueue(new Callback<StreamMessageModel>() {
            @Override
            public void onResponse(Call<StreamMessageModel> call, Response<StreamMessageModel> response) {
                StreamMessageModel mGetDetailsModel = response.body();
                if (mGetDetailsModel != null) {
                    if (mGetDetailsModel.getStatus() == 1) {
                        mArrayList = (ArrayList<StreamMessageModel.Data>) response.body().getData();
                        if (mArrayList != null) {
                            setAdapter0();

                        }

                    } else {
                        Log.e("Data", "0");
                    }
                }
            }

            @Override
            public void onFailure(Call<StreamMessageModel> call, Throwable t) {
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });


    }

    private void setAdapter0() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewChat.setLayoutManager(layoutManager);
        adapter = new LiveStream1Adapter(mActivity, (ArrayList<StreamMessageModel.Data>) mArrayList, getUserID());
        recyclerViewChat.setAdapter(adapter);
        if (mArrayList.size() > 1) {
            recyclerViewChat.scrollToPosition(mArrayList.size() - 1);

        }
    }

    private void setAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewChat.setLayoutManager(layoutManager);
        adapter = new LiveStream1Adapter(mActivity, (ArrayList<StreamMessageModel.Data>) mArrayList, getUserID());
        recyclerViewChat.setAdapter(adapter);
        if (mArrayList.size() > 1) {
            recyclerViewChat.scrollToPosition(mArrayList.size() - 1);

        }
    }

    private void initUI() {
        int role = getIntent().getIntExtra(
                ConstantsA.KEY_CLIENT_ROLE,
                Constants.CLIENT_ROLE_AUDIENCE);
        boolean isBroadcaster = (role == Constants.CLIENT_ROLE_BROADCASTER);
        mMuteVideoBtn.setActivated(isBroadcaster);

        mMuteAudioBtn.setActivated(isBroadcaster);

        beautyBtn.setActivated(true);
        rtcEngine().setBeautyEffectOptions(beautyBtn.isActivated(),
                ConstantsA.DEFAULT_BEAUTY_OPTIONS);

        mVideoGridContainer.setStatsManager(statsManager());
        rtcEngine().setClientRole(role);
         if (isBroadcaster) startBroadcast();

    }

    private void initData() {
        mVideoDimension = ConstantsA.VIDEO_DIMENSIONS[
                config().getVideoDimenIndex()];

        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.unknown_user)
                .error(R.drawable.unknown_user)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        if (getStreamUserPHOTO() != null) {
            try {
                Glide.with(getApplicationContext())
                        .load(getStreamUserPHOTO())
                        .apply(options)
                        .into(imProfilePic);
                Glide.with(getApplicationContext())
                        .load(getStreamUserPHOTO())
                        .apply(options)
                        .into(imProfilePicStream);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void startBroadcast() {
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
        SurfaceView surface = prepareRtcVideo(0, true);
        mVideoGridContainer.addUserVideoSurface(0, surface, false);
        mMuteAudioBtn.setActivated(true);
    }

    private void stopBroadcast() {
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
        removeRtcVideo(0, true);
        mVideoGridContainer.removeUserVideo(0, true);
        //  mMuteAudioBtn.setActivated(false);
    }


    @Override
    public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
        // Do nothing at the moment
        Log.e("Vishav", "AA" + channel + "  " + uid);
    }

    @Override
    public void onUserJoined(int uid, int elapsed) {
        // Do nothing at the moment
        peopleCount = peopleCount + 1;
        Log.e("Vishav", "KK" + "  " + peopleCount);
    }

    @Override
    public void onUserOffline(final int uid, int reason) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                removeRemoteUser(uid);
            }
        });
    }

    @Override
    public void onFirstRemoteVideoDecoded(final int uid, int width, int height, int elapsed) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                renderRemoteUser(uid);
            }
        });
    }

    private void renderRemoteUser(int uid) {

        SurfaceView surface = prepareRtcVideo(uid, false);
        mVideoGridContainer.addUserVideoSurface(uid, surface, false);
    }

    private void removeRemoteUser(int uid) {
        removeRtcVideo(uid, false);
        mVideoGridContainer.removeUserVideo(uid, false);
    }

    @Override
    public void onLocalVideoStats(IRtcEngineEventHandler.LocalVideoStats stats) {
        if (!statsManager().isEnabled()) return;

        LocalStatsData data = (LocalStatsData) statsManager().getStatsData(0);
        if (data == null) return;

        data.setWidth(mVideoDimension.width);
        data.setHeight(mVideoDimension.height);
        data.setFramerate(stats.sentFrameRate);
    }

    @Override
    public void onRtcStats(IRtcEngineEventHandler.RtcStats stats) {
        if (!statsManager().isEnabled()) return;

        LocalStatsData data = (LocalStatsData) statsManager().getStatsData(0);
        if (data == null) return;

        data.setLastMileDelay(stats.lastmileDelay);
        data.setVideoSendBitrate(stats.txVideoKBitRate);
        data.setVideoRecvBitrate(stats.rxVideoKBitRate);
        data.setAudioSendBitrate(stats.txAudioKBitRate);
        data.setAudioRecvBitrate(stats.rxAudioKBitRate);
        data.setCpuApp(stats.cpuAppUsage);
        data.setCpuTotal(stats.cpuAppUsage);
        data.setSendLoss(stats.txPacketLossRate);
        data.setRecvLoss(stats.rxPacketLossRate);
    }

    @Override
    public void onNetworkQuality(int uid, int txQuality, int rxQuality) {
        if (!statsManager().isEnabled()) return;

        StatsData data = statsManager().getStatsData(uid);
        if (data == null) return;

        data.setSendQuality(statsManager().qualityToString(txQuality));
        data.setRecvQuality(statsManager().qualityToString(rxQuality));
    }

    @Override
    public void onRemoteVideoStats(IRtcEngineEventHandler.RemoteVideoStats stats) {
        if (!statsManager().isEnabled()) return;

        RemoteStatsData data = (RemoteStatsData) statsManager().getStatsData(stats.uid);
        if (data == null) return;

        data.setWidth(stats.width);
        data.setHeight(stats.height);
        data.setFramerate(stats.rendererOutputFrameRate);
        data.setVideoDelay(stats.delay);
    }

    @Override
    public void onRemoteAudioStats(IRtcEngineEventHandler.RemoteAudioStats stats) {
        if (!statsManager().isEnabled()) return;

        RemoteStatsData data = (RemoteStatsData) statsManager().getStatsData(stats.uid);
        if (data == null) return;

        data.setAudioNetDelay(stats.networkTransportDelay);
        data.setAudioNetJitter(stats.jitterBufferDelay);
        data.setAudioLoss(stats.audioLossRate);
        data.setAudioQuality(statsManager().qualityToString(stats.quality));
    }

    @Override
    public void finish() {
        super.finish();
        statsManager().clearAllData();
    }

    public void onLeaveClicked(View view) {
        finish();
    }

    public void onSwitchCameraClicked(View view) {
        rtcEngine().switchCamera();
    }

    public void onBeautyClicked(View view) {
        view.setActivated(!view.isActivated());
        rtcEngine().setBeautyEffectOptions(view.isActivated(),
                ConstantsA.DEFAULT_BEAUTY_OPTIONS);
    }

    public void onMoreClicked(View view) {
        // Do nothing at the moment
        Log.e("Vishav", "mORE");
    }

    public void onPushStreamClicked(View view) {
        // Do nothing at the moment
        Log.e("Vishav", "pUSH STREAM");
    }

    public void onMuteAudioClicked(View view) {
        if (!mMuteVideoBtn.isActivated()) return;

        rtcEngine().muteLocalAudioStream(view.isActivated());
        view.setActivated(!view.isActivated());
    }

    public void onMuteVideoClicked(View view) {

        if (view.isActivated()) {

            //stopBroadcast();
            rtcEngine().disableVideo();
        } else {
            //startBroadcast();
            rtcEngine().enableVideo();
        }
        view.setActivated(!view.isActivated());
    }

    public void showAlertDialog(Activity mActivity, String strMessage, String uid) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert_live_stream);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        TextView btnOk = alertDialog.findViewById(R.id.btnOK);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                SurfaceView surface = prepareRtcVideo(Integer.parseInt(uid), false);
                mVideoGridContainer.addUserVideoSurface(Integer.parseInt(uid), surface, false);

            }
        });
        alertDialog.show();
    }

    private void perform() {
        Toast.makeText(mActivity, "Join", Toast.LENGTH_SHORT).show();
    }

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (!pushEndStremCheck) {
            if (doubleBackToExitPressedOnce) {
                finishLiveStream();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(mActivity, getString(R.string.back_text), Toast.LENGTH_SHORT).show();

            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;


                }
            }, 2000);
        } else {
            searchScreenExit();
        }
    }


    private void finishLiveStream() {
        JoinUserPush();
        if (recyclerViewChat != null && mArrayList.size() > 1) {
            recyclerViewChat.scrollToPosition(mArrayList.size() - 1);
        }
        if (Integer.parseInt(checkJoin) == 1 || getUserID().equals(getStreamUserID())) {
            new IOSDialog.Builder(this)
                    .setMessage(getString(R.string.end_live_stream))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (isNetworkAvailable(mActivity)) {
                                if (getStreamUserID().equals(getUserID())) {
                                    JSONObject jsonObject = null;
                                    try {
                                        jsonObject = new JSONObject();
                                        jsonObject.put("userid", getUserID());
                                        jsonObject.put("message", getUserName() + " left");
                                        jsonObject.put("type", "5");
                                        Log.e("checkio", jsonObject.toString());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    mSocket.emit("newMessage", getStreamRoomID(), jsonObject, "hdhd");
                                    if (!com.divineray.app.Utils.Constants.endStreamDone) {
                                        com.divineray.app.Utils.Constants.endStreamDone = true;
                                        if (isNetworkAvailable(mActivity)) {
                                            executeEndSteam();
                                        }
                                    }

                                } else {
                                    JSONObject jsonObject = null;
                                    try {
                                        jsonObject = new JSONObject();
                                        jsonObject.put("userName", getUserName());
                                        jsonObject.put("photo", getProfilePic());
                                        jsonObject.put("message", getUserName() + getString(R.string.left_stream));
                                        jsonObject.put("type", "6");
                                        Log.e("checkio", jsonObject.toString());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    mSocket.emit("newMessage", getStreamRoomID(), jsonObject, "hdhd");
                                    viewCount = 0;
                                    int typeVal = 100;
                                    executeViewCountApi(typeVal);
                                    executeLeaveSteam();
                                    pushNotifForJoinedUsers();
                                    searchScreenExit();
                                }
                            } else {
                                searchScreenExit();
                            }

                        }
                    })

                    .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            doubleBackToExitPressedOnce = false;
                            dialogInterface.dismiss();

                        }
                    }).show();
        } else {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject();
                jsonObject.put("userName", getUserName());
                jsonObject.put("photo", getProfilePic());
                jsonObject.put("message", getUserName() + getString(R.string.left_stream));
                jsonObject.put("type", "6");
                Log.e("checkio", jsonObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit("newMessage", getStreamRoomID(), jsonObject, "hdhd");
            viewCount = 0;
            int typeVal = 100;
            executeViewCountApi(typeVal);
            typeAppRejKick = "2";
            reqUserid = getUserID();
            executeApproveRejectOwnApi();
            //  executeLeaveSteam();
            searchScreenExit();
        }


    }

    private void searchScreenExit() {
        pushNotifForJoinedUsers();
        Intent intent = new Intent(mActivity, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
       // intent.putExtra("menuFragment", "searchMenuItem");
        startActivity(intent);
        finish();
    }

    private void searchScreenExitWithoutPush() {
        Intent intent = new Intent(mActivity, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      //  intent.putExtra("menuFragment", "searchMenuItem");
        startActivity(intent);
        finish();
    }


    //Like animation
    public void flyObject(final int resId, final int duration, final Direction from, final Direction to, final float scale) {

        ZeroGravityAnimation animation = new ZeroGravityAnimation();
        animation.setCount(1);
        animation.setScalingFactor(scale);
        animation.setOriginationDirection(from);
        animation.setDestinationDirection(to);
        animation.setImage(resId);
        animation.setDuration(duration);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        RelativeLayout container = findViewById(R.id.Fragment);

        animation.play(this, container);

    }


    @Override
    protected void onDestroy() {
        if (getStreamUserID().equals(getUserID())) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject();
                jsonObject.put("userid", getUserID());
                jsonObject.put("message", getUserName() + " left the stream");
                jsonObject.put("type", "5");
                Log.e("checkio", jsonObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit("newMessage", getStreamRoomID(), jsonObject, "hdhd");
            if (!com.divineray.app.Utils.Constants.endStreamDone) {
                com.divineray.app.Utils.Constants.endStreamDone = true;
                onDestroyMethodCall = true;
                if (isNetworkAvailable(mActivity)) {
                    executeEndSteam();
                }
            }

        }
        stopBroadcast();
        statsManager().clearAllData();
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("restartservice");
        broadcastIntent.setClass(this, Restarter.class);
        this.sendBroadcast(broadcastIntent);
        super.onDestroy();

    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        mMap.put("stream_id", getStreamID());
        mMap.put("cname", ConstantsA.channelNameA);
        mMap.put("resourceid", ConstantsA.resourceId);
        mMap.put("sid", ConstantsA.sid);
        Log.e(TAG, "**PARAM**" + mMap.toString() + "**AuthToken" + getAuthToken());
        return mMap;
    }


    void executeEndSteam() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.endLiveStreamV2(getAuthToken(), mParam()).enqueue(new Callback<EndStream2Model>() {
            @Override
            public void onResponse(Call<EndStream2Model> call, Response<EndStream2Model> response) {
                EndStream2Model mModel = response.body();
                Log.e("LiveStream::",mModel.toString());
                try {
                    dismissProgressDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                YourService mYourService;
                mYourService = new YourService();
                mServiceIntent = new Intent(LiveStreaming2Activity.this, mYourService.getClass());
                stopService(mServiceIntent);
                if(mModel!=null) {
                    if (mModel.getStatus() == 1|| mModel.getStatus() == 200) {
                        showToast(mModel.getMessage());
                        Intent intent = new Intent(mActivity, HomeActivity.class);
                        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("menuFragment", "searchMenuItem");
                        startActivity(intent);
                    } else {
                        streamLink = mModel.getVideoDetails().getFileList();
                        stopBroadcast();
                        statsManager().clearAllData();
                        if (!onDestroyMethodCall) {
//                        Intent intent = new Intent(mActivity, HomeActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        intent.putExtra("menuFragment", "searchMenuItem");
//                        startActivity(intent);
                            Glide.with(mActivity).load(R.drawable.bg_live_stream).into(imFinalUser);
                            llyExit.setVisibility(View.VISIBLE);
                            imProfilePicStream.setVisibility(View.VISIBLE);
                            imFinalUser.setVisibility(View.VISIBLE);
                            llyShare.setVisibility(View.VISIBLE);
                        }
                    }
                }


            }

            @Override
            public void onFailure(Call<EndStream2Model> call, Throwable t) {
                dismissProgressDialog();
                //  showAlertDialog2(mActivity, t.getMessage());
                searchScreenExit();
            }
        });


    }


    void executeLeaveSteam() {
        // showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.leaveLiveStream(getAuthToken(), mParam()).enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                Log.e(TAG, "LiveSream1" + response.body().toString());
                pushNotifForJoinedUsers();
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                dismissProgressDialog();
                showAlertDialog2(mActivity, t.getMessage());
            }
        });


    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("Service status", "Running");
                return true;
            }
        }
        Log.i("Service status", "Not running");
        return false;
    }


    //header Text fetch
    void JoinUserPush() {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put("type", "101");
            Log.e("checkio", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("newMessage", getStreamRoomID(), jsonObject, "hdhd");
    }

    void liveStremHeadset() {
        getHostLiveData();
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParamC() {
        String checkval = ConstantsA.tokenA;
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        mMap.put("cname", ConstantsA.channelNameA);
        mMap.put("token", String.valueOf(checkval).trim());
        Log.e(TAG, "**PARAM**:::" + getAuthToken() + "::::" + mMap.toString()+":::::STREAMAID"+ConstantsA.StramID);
        return mMap;
    }

    private void executeCloudRecordApi() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeCloudRecordStream(getAuthToken(), mParamC()).enqueue(new Callback<CloudRecordingResponse>() {
            @Override
            public void onResponse(Call<CloudRecordingResponse> call, Response<CloudRecordingResponse> response) {
                if (response.body().getStatus() == 1) {
                    CloudRecordingResponse mModel = response.body();

                    Log.e(TAG, "LiveSream11::" + mModel.toString());
                    Log.e(TAG, "LiveSream11:::" + mModel.getData().getSid());
                    ConstantsA.sid = mModel.getSid();
                    ConstantsA.resourceId = mModel.getResourceId();

                } else {
                    showToast(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<CloudRecordingResponse> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParamS() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        mMap.put("video_link", streamLink);
        Log.e(TAG, "**PARAM**:::" + getAuthToken() + "::::" + mMap.toString()+":::::STREAMAID"+ConstantsA.StramID);
        return mMap;
    }

    private void exceuteShareStreamApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.exceuteShareStream(getAuthToken(), mParamS()).enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                dismissProgressDialog();
                if (response.body().getStatus() == 1) {
                    showToast(getString(R.string.stream_shared));
                    searchScreenExitWithoutPush();
                } else {
                    showToast(response.body().getMessage());
                    searchScreenExitWithoutPush();
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }
}