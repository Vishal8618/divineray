package com.divineray.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.divineray.app.Profile.PersonDetails;
import com.divineray.app.R;
import com.divineray.app.adapters.LiveuserScreenAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LiveUsersScreen extends BaseActivity {

    Activity mActivity=LiveUsersScreen.this;

    @BindView(R.id.recyclerLiveUsers)
    RecyclerView recyclerView;
    @BindView(R.id.llyBack)
    LinearLayout llyBack;
    @BindView(R.id.txHeading)
    TextView txHeading;

    ArrayList<String> mArraylist=new ArrayList<String>();
    LiveuserScreenAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_live_users_screen);
        ButterKnife.bind(this);
        txHeading.setText(getResources().getString(R.string.live_users));
     //   setData();
    }

    @OnClick({R.id.llyBack})
    public  void  onViewCLicked(View view)
    {
        switch (view.getId())
        {
            case R.id.llyBack:
                finish();
                break;
        }
    }

    public  void  setData()
    {
//        int numofColumns=3;
//        recyclerView.setLayoutManager(new GridLayoutManager(this, numofColumns));
//        mArraylist.add("first");
//       adapter= new LiveuserScreenAdapter(mArraylist, this, new LiveuserScreenAdapter.onItemClickClickListener() {
//            @Override
//            public void onItemClick(int position, ArrayList<String> mArrayList, View view) {
//                switch (view.getId())
//                {
//                    case R.id.cdGoLive:
//                        performLiveClick();
//                        break;
//                }
//            }
//        });
//       recyclerView.setAdapter(adapter);
    }

    private void performLiveClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.out_to_top);
        startActivity(new Intent(mActivity, LiveStreaming1Activity.class));
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }
}