package com.divineray.app.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Settings.ContactUs;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.LoginModel;
import com.divineray.app.model.NottificationModel;
import com.divineray.app.model.VersionModel;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONObject;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    private static String VERSION_TO_COMPARE ="" ;
    /**
     * Getting the Current Class Name
     */
    String TAG = LoginActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = LoginActivity.this;

    /*
     * Widgets
     * */
    private static final String EMAIL = "email";
    ProfileTracker mProfileTracker;
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.txtLoginTV)
    TextView txtLoginTV;
    @BindView(R.id.txtForgotPassTV)
    TextView txtForgotPassTV;
    @BindView(R.id.txtDontHaveAccountTV)
    TextView txtDontHaveAccountTV;
    @BindView(R.id.txtFblogin)
    TextView txtFbLogin;
    @BindView(R.id.password_toggle)
    ImageView passwordToggle;
    @BindView(R.id.txtTwitterLogin)
    TextView txtTwitterLogin;
    @BindView(R.id.txTermsofService)
    TextView txtTermsofservice;
    @BindView(R.id.txPrivacyPoicy)
    TextView txPrivacyPolicy;
    @BindView(R.id.login_button_fb)
    LoginButton loginButton;
    String strDeviceToken="";
    boolean isChecked=true;
    boolean twitterCheck=true;
    private String fbEmail, fbLastName, fbFirstName, fbId, userName, fbSocialUserserName;
    String Username = "";
    String facebook_id = "";
    String email = "";
    String FacebookProfilePicture = "";
    private URL fbProfilePicture;
    CallbackManager mCallbackManager;

    /**
     * Register your here app https://dev.twitter.com/apps/new and get your
     * consumer key and secret
     */
    TwitterSession session;
    TwitterAuthToken authToken;
    TwitterAuthClient authClient;
    String twitter_img = "";
    String tw_strFristName = "", tw_strLastName = "";
    String tw_email ;
    String tw_token ;
    String tw_secret ;
    String tw_username;
    String tw_userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        TwitterAuthConfig mTwitterAuthConfig = new TwitterAuthConfig(getString(R.string.com_twitter_sdk_android_CONSUMER_KEY),
                getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET));
        TwitterConfig twitterConfig = new TwitterConfig.Builder(this)
                .twitterAuthConfig(mTwitterAuthConfig)
                .build();
        Twitter.initialize(twitterConfig);
        setContentView(R.layout.activity_login);

        //device token
        getDeviceToken();
        ButterKnife.bind(this);
        executeUpdate();
        authClient = new TwitterAuthClient();
        LoginManager.getInstance().logOut();
        passwordToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isChecked){
                    // show password
                    editPasswordET.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    editPasswordET.setSelection(editPasswordET.getText().length());
                    passwordToggle.setImageResource(R.drawable.ic_eye_close);
                    isChecked=true;
                    Log.i("checker", "true");
                }

                else{
                    Log.i("checker", "false");
                    // hide password
                    editPasswordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    editPasswordET.setSelection(editPasswordET.getText().length());
                    passwordToggle.setImageResource(R.drawable.ic_eye_open);
                    isChecked=false;
                }
            }
        });


        txtTwitterLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                authClient.authorize(mActivity, new com.twitter.sdk.android.core.Callback<TwitterSession>() {
                    @Override
                    public void success(Result<TwitterSession> result) {
                       // TwitterCore.getInstance().getSessionManager().clearActiveSession();
                        session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                         authToken = session.getAuthToken();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                authClient.requestEmail(session, new com.twitter.sdk.android.core.Callback<String>() {
                                    @Override
                                    public void success(Result<String> result) {
                                        showProgressDialog(mActivity);
                                        tw_email = result.data;
                                        tw_token = authToken.token;
                                        tw_secret = authToken.secret;
                                        tw_username = session.getUserName();
                                        long userId = session.getUserId();
                                        Log.e("neem", "success: \n Token =>  " + tw_token + "\n Secret: " + tw_username + " \n Username: " + tw_username + "\n Userid: " + userId + " \n Email: " + email);
                                        tw_userId = String.valueOf(session.getUserId());
                                        String[] firstLastName = {};

                                        if (tw_username != null && tw_username.contains(" ")) {
                                            firstLastName = tw_username.split(" ");
                                            tw_strFristName = firstLastName[0];
                                            tw_strLastName = firstLastName[1];
                                        } else {
                                            tw_strFristName = tw_username;
                                        }
                                    }

                                    @Override
                                    public void failure(TwitterException exception) {
                                        if (twitterCheck) {
                                            showAlertDialog(mActivity, "Device not supported. Please install Twitter app and then login.");
                                        }
                                        twitterCheck=false;
                                        Log.e("neem", "failure: " + exception.getMessage());
                                    }
                                });
                                TwitterCore.getInstance().getApiClient(result.data).getAccountService().verifyCredentials(false, true, false).enqueue(new com.twitter.sdk.android.core.Callback<User>() {
                                    @Override
                                    public void success(Result<User> userResult) {
                                        try {

                                            twitter_img = userResult.data.profileImageUrl;
//                                    twitter_username = userResult.data.screenName;
//                                    twitter_email = userResult.data.email;
//                                    Log.e("neem", "acount services: imageurl: " + twitter_img + "\n username: " + twitter_username);
                                            loginWithTwitter();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    @Override
                                    public void failure(TwitterException e) {
                                        if (twitterCheck) {
                                       //     showAlertDialog(mActivity, "Device not supported. Please install Twitter app and then login.");
                                        }
                                        twitterCheck=false;
                                        showToast(mActivity, "Please Try again");
                                        Log.e("Dids", e.getMessage());
                                    }
                                });


                            }},100);


                            }

                    @Override
                    public void failure(TwitterException exception) {

                        Log.e("Dids",exception.getMessage());
                      //  show
                    }
                });
            }
        });
    }





    private void getDeviceToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "**Get Instance Failed**", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        strDeviceToken = task.getResult().getToken();
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.DEVICETOKEN, strDeviceToken);
                        Log.e(TAG, "P:" + DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.DEVICETOKEN,null));
                    }
                });
    }

    @SuppressLint("NonConstantResourceId")
    @OnClick({R.id.txtForgotPassTV, R.id.txtLoginTV ,R.id.txtDontHaveAccountTV,R.id.txtFblogin,R.id.txtTwitterLogin,R.id.password_toggle,R.id.txPrivacyPoicy,R.id.txTermsofService})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtForgotPassTV:
                performForgotPassClick();
                break;
            case R.id.txtLoginTV:
                performLoginClick();
                break;
            case R.id.txtDontHaveAccountTV:
                performDonthaveAccountClick();
                break;
            case  R.id.txtFblogin:
                performFbLogin();
                break;
            case  R.id.password_toggle:
                break;
            case R.id.txPrivacyPoicy:
                switchActivty("privacypolicy",String.valueOf(R.string.privacy_policy));
                break;
            case R.id.txTermsofService:
                switchActivty("terms", String.valueOf(R.string.terms_of));
                break;

        }
    }

    private void switchActivty(String check,String heading) {
        Intent intent=new Intent(mActivity, ContactUs.class);
        intent.putExtra("pageType",check);
        intent.putExtra("pageText",heading);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE:
                authClient.onActivityResult(requestCode, resultCode, data);
                break;
            default:
                mCallbackManager.onActivityResult(requestCode, resultCode, data);
                break;

        }

    }





    private void loginWithTwitter() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            loginWithTwitterApi();
        }
    }

    /*
     * Execute twitter api
     * */
    private Map<String, String> mTwitterParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("email", tw_email);
        mMap.put("device_type", "2");
        mMap.put("device_token", strDeviceToken);
        mMap.put("name",session.getUserName());
       mMap.put("twitterId", String.valueOf(session.getUserId()));
        Log.e("Test", "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void loginWithTwitterApi() {
    ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.loginWithTwitterRequest(mTwitterParams()).enqueue(new retrofit2.Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                dismissProgressDialog();
                LoginModel mModel = response.body();

                if (mModel.getStatus().equals(1)) {
                    try {

                        DivineRayPrefernces.writeBoolean(mActivity,DivineRayPrefernces.ISLOGIN, true);
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.ID, mModel.getData().getUser_id());
                        DivineRayPrefernces.writeString(mActivity,DivineRayPrefernces.EMAIL, mModel.getData().getEmail());
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.NAME, mModel.getData().getName());
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.DEVICETYPE, "2");
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.DEVICETOKEN, strDeviceToken);
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.TWITTER_ID, mModel.getData().getTwitterId());
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.PASSWORD, response.body().getData().getPassword());
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.PHOTO, response.body().getData().getPhoto());
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.SECURITYTOKEN, response.body().getData().getSecuritytoken());
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.AUTHTOKEN, response.body().getData().getUsertoken());

                        executePush();
                    }
                    catch (Exception e)
                    {
                        Log.e("Checking","Tw:  "+DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));

                        e.printStackTrace();
                    }
                    finally {
                        Intent intent = new Intent(mActivity, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }

                } else if (response.body().getStatus().equals(0)) {
                  showAlertDialog(mActivity,"Try Again");
                }
                else
                {
                    showAlertDialog(mActivity,response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                dismissProgressDialog();

                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }





    private void performFbLogin() {
       if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            FacebookSdk.sdkInitialize(getApplicationContext());
            LoginManager.getInstance().logOut();
            AppEventsLogger.activateApp(getApplication());
            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
            mCallbackManager = CallbackManager.Factory.create();

            loginWithFacebook();
        }

    }


    private void performDonthaveAccountClick() {
        startActivity(new Intent(mActivity, SignUpActivity.class));
        finish();
    }

    private boolean isValidate() {
        boolean flag = true;
        if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        }
        else if (editPasswordET.getText().toString().trim().length()<6) {
            showAlertDialog(mActivity, getString(R.string.password_length));
            flag = false;
        }


        return flag;
    }

    private void performLoginClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeLoginApi();
            }
        }
    }

    private void performForgotPassClick() {
        startActivity(new Intent(mActivity, ForgotPasswordActivity.class));
        finish();
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("email", editEmailET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        mMap.put("device_type","2");
        mMap.put("device_token",strDeviceToken);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void executeLoginApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.loginRequest(mParam()).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                dismissProgressDialog();
                LoginModel mModel = response.body();
                if (mModel!=null) {
                    if (mModel.getStatus().equals(1)) {
                        try {
                            DivineRayPrefernces.writeBoolean(mActivity, DivineRayPrefernces.ISLOGIN, true);
                            DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.ID, mModel.getData().getUser_id());
                            DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.EMAIL, mModel.getData().getEmail());
                            DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.NAME, mModel.getData().getName());
                            DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.DEVICETYPE, "2");
                            DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.DEVICETOKEN, strDeviceToken);

                            DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.PASSWORD, response.body().getData().getPassword());
                            DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.PHOTO, response.body().getData().getPhoto());
                            DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.SECURITYTOKEN, response.body().getData().getSecuritytoken());
                            DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.AUTHTOKEN, response.body().getData().getUsertoken());
                            executePush();
                        } catch (Exception e) {
                            Log.e("Checking", "S:  " + DivineRayPrefernces.readString(mActivity, DivineRayPrefernces.USERID, null));

                            e.printStackTrace();
                        } finally {
                            Intent intent = new Intent(mActivity, HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }


                    } else if (response.body().getStatus().equals(0)) {


                        showAlertDialog(mActivity, response.body().getMessage());
                    }
                    else
                    {
                        showAlertDialog(mActivity, response.body().getMessage());
                    }
                }
                else
                {
                    showAlertDialog(mActivity,"Server error");
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
                showAlertDialog(mActivity, t.getMessage());
            }
        });
    }
//fb Login

    private void loginWithFacebook() {

        if (isNetworkAvailable(mActivity)) {
            LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "email"));
            LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.d("onSuccess: ", loginResult.getAccessToken().getToken());

                    getFacebookData(loginResult);
            }

                @Override
                public void onCancel() {
                }

                @Override
                public void onError(FacebookException error) {
                Log.e("error","is"+error.toString());
                }

            });
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }

    }

    private void getFacebookData(LoginResult loginResult) {
        showProgressDialog(mActivity);
        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                dismissProgressDialog();

                try {

                    if (object.has("id")) {
                        fbId = object.getString("id");
                        Log.e("LoginActivity", "id" + fbId);

                    }
                    //check permission first userName
                    if (object.has("first_name")) {
                        fbFirstName = object.getString("first_name");
                        Log.e("LoginActivity", "first_name" + fbFirstName);

                    }
                    //check permisson last userName
                    if (object.has("last_name")) {
                        fbLastName = object.getString("last_name");
                        Log.e("LoginActivity", "last_name" + fbLastName);
                    }
                    //check permisson email
                    if (object.has("email")) {
                        fbEmail = object.getString("email");
                        Log.e("LoginActivity", "email" + fbEmail);
                    }

                    fbSocialUserserName = fbFirstName + " " + fbLastName;

                    JSONObject jsonObject = new JSONObject(object.getString("picture"));
                    if (jsonObject != null) {
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        Log.e("Loginactivity", "json oject get picture" + dataObject);
                        fbProfilePicture = new URL("https://graph.facebook.com/" + fbId + "/picture?width=500&height=500");
                        Log.e("LoginActivity", "json object=>" + object.toString());
                    }

                    Username = fbSocialUserserName;
                    email = fbEmail;
                    facebook_id = fbId;

                    if (fbProfilePicture != null) {
                        FacebookProfilePicture = String.valueOf(fbProfilePicture);

                    } else {
                        FacebookProfilePicture = "";
                    }

                    DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.FACEBOOK_ID, facebook_id);

                    executeLoginWithFbApi();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
                });

                Bundle bundle = new Bundle();
                Log.e("LoginActivity", "bundle set");
                bundle.putString("fields", "id, first_name, last_name,email,picture,gender,location");
                graphRequest.setParameters(bundle);
                graphRequest.executeAsync();

            }

            /*
             * Execute fb api
             * */
            private Map<String, String> mParams() {
                Map<String, String> mMap = new HashMap<>();
                mMap.put("email", fbEmail);
                mMap.put("device_type", "2");
                mMap.put("facebookId", facebook_id);
                mMap.put("device_token", strDeviceToken);
                mMap.put("name", fbSocialUserserName);

                Log.e(TAG, "**PARAM**" + mMap.toString());
                return mMap;
            }

            private void executeLoginWithFbApi() {
                showProgressDialog(mActivity);
                //Toast.makeText(mActivity, ""+strDeviceToken, Toast.LENGTH_SHORT).show();
                Log.e("deviceToken",strDeviceToken);
                ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
                mApiInterface.loginWithFbRequest(mParams()).enqueue(new retrofit2.Callback<LoginModel>() {
                    @Override
                    public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                        dismissProgressDialog();
                         Log.e(TAG, "**RESPONSE**F" + response.body().getData().toString());
                        LoginModel mModel = response.body();
                        if (mModel.getStatus().equals(1)) {
         try {
             DivineRayPrefernces.writeBoolean(mActivity, DivineRayPrefernces.ISLOGIN, true);
             DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.ID, mModel.getData().getUser_id());
             DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.USERID, mModel.getData().getUser_id());
             DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.NAME, mModel.getData().getName());
             DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.EMAIL, mModel.getData().getEmail());
             DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.DEVICETYPE, "2");
             DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.DEVICETOKEN, strDeviceToken);
             DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.FACEBOOK_ID, mModel.getData().getFacebookId());
             DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.PASSWORD, response.body().getData().getPassword());
             DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.PHOTO, response.body().getData().getPhoto());
             DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.SECURITYTOKEN, response.body().getData().getSecuritytoken());
             DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.AUTHTOKEN, response.body().getData().getUsertoken());
             executePush();
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Log.e("Checking","FB:  "+DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
            }
            finally {

                Log.e("Datafb",""+response.body());
                 Intent intent = new Intent(mActivity, HomeActivity.class);
                startActivity(intent);
                finish();
            }


                        } else if (response.body().getStatus().equals(0)) {
                            showAlertDialog(mActivity, mModel.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginModel> call, Throwable t) {
                        dismissProgressDialog();
                        showAlertDialog(mActivity,t.getMessage());
                        Log.e(TAG, "**ERROR**" + t.getMessage());
                    }
                });
            }
    /*
     * Execute api
     * */
    private Map<String, String> mParam0() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id",  DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("allowPush", "1");
        mMap.put("device_type","2");
        mMap.put("device_token",strDeviceToken);

        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void executePush() {
       // showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.pushNotification(getAuthToken(),mParam0()).enqueue(new Callback<NottificationModel>() {
            @Override
            public void onResponse(Call<NottificationModel> call, Response<NottificationModel> response) {
//                dismissProgressDialog();
                DivineRayPrefernces.writeString(mActivity,DivineRayPrefernces.PUSHNOTIFICATION, "1");
                Log.e(TAG, "base_64_api_success" + response.body().toString());
                NottificationModel mModel = response.body();
                // Toast.makeText(mActivity, ""+response.body(), Toast.LENGTH_SHORT).show();



            }

            @Override
            public void onFailure(Call<NottificationModel> call, Throwable t) {

            }
        });
    }
    private void executeUpdate() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getVersion().enqueue(new Callback<VersionModel>() {
            @Override
            public void onResponse(Call<VersionModel> call, Response<VersionModel> response) {
                VersionModel mModel = response.body();
                if (mModel!=null) {
                    if (mModel.getStatus() == 1) {

                        VERSION_TO_COMPARE = mModel.getData().getAndroid();
                        try {

                            //get current Version Code
                            PackageManager manager = mActivity.getPackageManager();
                            PackageInfo info = manager.getPackageInfo(mActivity.getPackageName(), 0);
                            String currentVersionName = info.versionName;

                            if (!VERSION_TO_COMPARE.equals(currentVersionName)) {
                                //version string not the same, version is NOT up to date

                                Boolean updateNeeded = false;
                                String[] currentVersionCodeArray = currentVersionName.split("\\.");
                                String[] storeVersionCodeArray = VERSION_TO_COMPARE.split("\\.");

                                int maxLength = currentVersionCodeArray.length;
                                if (storeVersionCodeArray.length > maxLength) {
                                    maxLength = storeVersionCodeArray.length;
                                }

                                for (int i = 0; i < maxLength; i++) {

                                    try {
                                        if (Integer.parseInt(storeVersionCodeArray[i]) > Integer.parseInt(currentVersionCodeArray[i])) {
                                            updateNeeded = true;
                                            continue;
                                        }
                                    } catch (IndexOutOfBoundsException e) {
                                        //store version code length > current version length = version needs to be updated
                                        //if store version length is shorter, the if-statement already did the job
                                        if (storeVersionCodeArray.length > currentVersionCodeArray.length) {
                                            updateNeeded = true;
                                        }
                                    }


                                }

                                if (updateNeeded) {
                                    //current version of app, lower than VERSION_TO_COMPARE
                                    //do sth. in our case, show an update-dialog
                                    showAlertPDialog(mActivity, mModel.getData().getAndroid_title(),
                                            mModel.getData().getAndroid_description());
                                }


                            } else {
                                //do nothing: version is up to date
                            }

                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }

                    } else {

                    }
                    // Toast.makeText(mActivity, ""+response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<VersionModel> call, Throwable t) {

            }
        });

    }

    /*
     *
     * Error Alert Dialog
     * */
    @SuppressLint("SetTextI18n")
    public void showAlertPDialog(Activity mActivity, String heading, String description) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert_payment);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txHeading = alertDialog.findViewById(R.id.updateHeading);
        TextView txDescription = alertDialog.findViewById(R.id.updateDescription);
        TextView txCancel = alertDialog.findViewById(R.id.updateCancel);
        TextView txUpdate = alertDialog.findViewById(R.id.txUpdate);
        txHeading.setText(heading);
        txDescription.setText(description);
        txCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        txUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        alertDialog.show();
    }
    }

