package com.divineray.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.divineray.app.Agora.ConstantsA;
import com.divineray.app.Agora.activities.BaseActivity;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Search.RecyclerViewAdapter;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.adapters.LiveuserScreenAdapter;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.GoingLiveModel;
import com.divineray.app.model.LiveUsersModel;
import com.divineray.app.model.SearchModelget;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.agora.rtc.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchScreenActivity extends BaseActivity {

    Activity mActivity=SearchScreenActivity.this;

    List<SearchModelget.Datan> mQuotesArrayList = new ArrayList<>();

    public Dialog progressDialog;
    @BindView(R.id.search_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.search_searchtab_button)
    TextView tx_searchtab;
    @BindView(R.id.search_searchedittext)
    EditText editText_search;
    @BindView(R.id.text_no_result_search)
    TextView text_no_search;
    @BindView(R.id.p_bar_search)
    ProgressBar p_bar;
    @BindView(R.id.swiperefresh_search)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.shimmer_view_container)
    public ShimmerFrameLayout mShimmerViewContainer;
    @BindView(R.id.llySearchScreen)
    LinearLayout llySearchScreen;
    @BindView(R.id.recyclerLiveUsers)
    RecyclerView recyclerViewLiveUsers;

    @BindView(R.id.llySVideo)
    LinearLayout llySVideo;
    @BindView(R.id.llySLiveUsers)
    LinearLayout llySLiveUsers;
    @BindView(R.id.imSVideo)
    ImageView imSVideo;
    @BindView(R.id.imSLiveUsers)
    ImageView imSLiveUsers;
    @BindView(R.id.imBack)
    ImageView imBack;
    @BindView(R.id.txSLiveUsers)
    TextView txSLiveUsers;
    @BindView(R.id.txSVideo)
    TextView txSVideo;

    RecyclerViewAdapter adapter;

    boolean isSearchVideoScreen=true;

    ArrayList<String> mArraylist=new ArrayList<String>();
    ArrayList<LiveUsersModel.Data> mArraylistLiveUsers=new ArrayList<>();
    LiveuserScreenAdapter adapter1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(mActivity);
        setContentView(R.layout.activity_search_screen);
        ButterKnife.bind(this);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
        performSearchVideoScreenCheck();
        //for high quality streaming 1080*720p
        config().setVideoDimenIndex(5);

        swiperefresh.setProgressViewOffset(false, 0, 200);

        swiperefresh.setColorSchemeResources(R.color.black);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getDetails();
            }
        });

        //   getDetails();

        editText_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String string = editable.toString();
                if (mQuotesArrayList.size()>1) {
                    adapter.getFilter().filter(string);
                }
                else {}
            }
        });

        editText_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    closeKeyboard();
                    return true;
                }
                return false;
            }
        });
        setDataLiveUsers();
    }

     @OnClick({R.id.llySVideo,R.id.llySLiveUsers,R.id.imBack})
    public void onViewCLicked(View view)
    {
        switch (view.getId())
        {
            case R.id.llySVideo:
                isSearchVideoScreen=true;
                performSearchVideoScreenCheck();
                break;
            case R.id.llySLiveUsers:
                isSearchVideoScreen=false;
                performSearchVideoScreenCheck();
                break;
            case R.id.imBack:
                //finish();
                performClick();
                break;
        }
    }

    private void performClick() {
        Intent intent1 = new Intent(mActivity, HomeActivity.class);
        //intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent1);
    }


    private void performSearchVideoScreenCheck() {
        if (isSearchVideoScreen)
        {
            llySVideo.setBackgroundResource(R.drawable.bg_btn_blue);
            imSVideo.setBackgroundResource(R.drawable.ic_play_white);
            txSVideo.setTextColor(getResources().getColor(R.color.colorWhite));
            llySLiveUsers.setBackgroundResource(R.drawable.bg_et2);
            imSLiveUsers.setBackgroundResource(R.drawable.ic_broadcast_black);
            txSLiveUsers.setTextColor(getResources().getColor(R.color.black));

            recyclerViewLiveUsers.setVisibility(View.GONE);
            llySearchScreen.setVisibility(View.VISIBLE);

        }
        else {
            llySVideo.setBackgroundResource(R.drawable.bg_et2);
            imSVideo.setBackgroundResource(R.drawable.ic_play_black);
            txSVideo.setTextColor(getResources().getColor(R.color.black));
            llySLiveUsers.setBackgroundResource(R.drawable.bg_btn_blue);
            imSLiveUsers.setBackgroundResource(R.drawable.ic_broadcast_white);
            txSLiveUsers.setTextColor(getResources().getColor(R.color.colorWhite));

            llySearchScreen.setVisibility(View.GONE);
            recyclerViewLiveUsers.setVisibility(View.VISIBLE);


        }
    }

    private void closeKeyboard()
    {
        // this will give us the view
        // which is currently focus
        // in this layout
        View view = getCurrentFocus();

        // if nothing is currently
        // focus then this will protect
        // the app from crash
        if (view != null) {

            // now assign the system
            // service to InputMethodManager
            InputMethodManager manager
                    = (InputMethodManager)
                    getSystemService(
                            Context.INPUT_METHOD_SERVICE);
            manager
                    .hideSoftInputFromWindow(
                            view.getWindowToken(), 0);
        }
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.setVisibility(View.GONE);
        super.onPause();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (editText_search.getText().toString().equals(""))
        {
            executeDetailsApi();
            Log.e("T","if");
        }
        else
        {

            Log.e("T","else");
        }
       mShimmerViewContainer.setVisibility(View.VISIBLE);
    }

    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    private void getDetails() {
        if (!isNetworkAvailable(mActivity)) {
            Toast.makeText(mActivity, "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
            executeDetailsApi();
        }
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null));
        mMap.put("lastId","");
        mMap.put("perPage","100");
        mMap.put("tagString","");
        mMap.put("musicId","");
        mMap.put("isMusicData","0");
        Log.e("Vall", "**PARAM**"+ DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.USERID,null) + mMap.toString());
        return mMap;
    }
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null&&!progressDialog.isShowing())
            progressDialog.show();


    }
    private void executeDetailsApi() {
        String authtoken= DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.AUTHTOKEN,null);

       // showProgressDialog(getActivity());
        p_bar.setVisibility(View.VISIBLE);
        swiperefresh.setVisibility(View.VISIBLE);
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getSearchData(authtoken,mParams()).enqueue(new Callback<SearchModelget>() {
            @Override
            public void onResponse(Call<SearchModelget> call, Response<SearchModelget> response) {
                swiperefresh.setRefreshing(false);
                //dismissProgressDialog();
                p_bar.setVisibility(View.GONE);
                Log.e("kk", "**RESPONSE**" + response.body());
                SearchModelget mGetDetailsModel = response.body();

                assert mGetDetailsModel != null;
                if (mGetDetailsModel.getStatus() == 1) {
                    mQuotesArrayList = response.body().getData();
                    if (mQuotesArrayList != null) {
                        setSearchAdapter();
                    }

                } else {
                    Log.e("Data", "error fetch");
                }

            }

            @Override
            public void onFailure(Call<SearchModelget> call, Throwable t) {
                dismissProgressDialog();
                p_bar.setVisibility(View.GONE);
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setSearchAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
         adapter = new RecyclerViewAdapter(mActivity, (ArrayList<SearchModelget.Datan>) mQuotesArrayList,text_no_search,mShimmerViewContainer);
        recyclerView.setAdapter(adapter);
    }


    public  void  setDataLiveUsers()
    {
       executeLiveUsersApi();
    }

    private void performLiveClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.out_to_top);
        startActivity(new Intent(mActivity, LiveStreaming1Activity.class));
        mActivity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }





    void executeLiveUsersApi()
    {
       String authtoken= DivineRayPrefernces.readString(mActivity,DivineRayPrefernces.AUTHTOKEN,null);
      showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.showLiveUsers(authtoken).enqueue(new Callback<LiveUsersModel>() {
            @Override
            public void onResponse(Call<LiveUsersModel> call, Response<LiveUsersModel> response) {
                dismissProgressDialog();
                LiveUsersModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    mArraylistLiveUsers.addAll(mModel.getData());
                  setLiveUsersAdapter(mArraylistLiveUsers);
                }
                else
                {
                    setLiveUsersAdapter(mArraylistLiveUsers);
                }

            }

            @Override
            public void onFailure(Call<LiveUsersModel> call, Throwable t) {
                dismissProgressDialog();
            }
        });


    }

    private void setLiveUsersAdapter(ArrayList<LiveUsersModel.Data> mArraylistLiveUsers) {

        int numofColumns=3;
        recyclerViewLiveUsers.setLayoutManager(new GridLayoutManager(mActivity, numofColumns));
        adapter1= new LiveuserScreenAdapter(mArraylistLiveUsers, mActivity, new LiveuserScreenAdapter.onItemClickClickListener() {
            @Override
            public void onItemClick(int position, ArrayList<LiveUsersModel.Data> mArrayList, View view) {
                switch (view.getId())
                {
                    case R.id.cdGoLive:
                        performLiveClick();
                       // executezgoLIveApi();
                        break;
                    case R.id.imProfilePic:
                        performJoinLiveUser(mArrayList,position);
                        break;
                    case R.id.txUsername:
                        performJoinLiveUser(mArrayList,position);
                        break;

                }
            }
        });
        recyclerViewLiveUsers.setAdapter(adapter1);

    }

        /*
         * Execute api
         * */
        private Map<String, String> mParam() {
            Map<String, String> mMap = new HashMap<>();
            mMap.put("user_id", getUserID());
            //Log.e(TAG, "**PARAM**" + mMap.toString());
            return mMap;
        }

        private void executezgoLIveApi() {
            showProgressDialog(mActivity);
            ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
            mApiInterface.executeLiveStream(getAuthToken(),mParam()).enqueue(new Callback<GoingLiveModel>() {
                @Override
                public void onResponse(Call<GoingLiveModel> call, Response<GoingLiveModel> response) {
                    //Log.e(TAG, "LiveSream1" + response.body().toString());
                    dismissProgressDialog();
                    GoingLiveModel mModel = response.body();
                    if (mModel.getStatus()==0) {
                        showAlertDialog2(mActivity, mModel.getMessage());
                    } else  {
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.STREAM_USERID, getUserID());
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.STREAM_ROOMID,mModel.getStreamDetail().getStream_room());
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.STREAM_USERNAME, getUserName());
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.STREAM_ID, mModel.getStreamDetail().getStream_id());
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.STREAM_USERPHOTO, getProfilePic());
                        gotoRoleActivity(mModel.getStreamDetail().getStream_room(),getUserID());
                    }

                }

                @Override
                public void onFailure(Call<GoingLiveModel> call, Throwable t) {
                    dismissProgressDialog();
                    showAlertDialog2(mActivity,t.getMessage());
                }
            });
        }


    private void performJoinLiveUser(ArrayList<LiveUsersModel.Data> mArrayList, int position) {
        position=position-1;
        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.STREAM_USERID, mArrayList.get(position).getUser_id());
        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.STREAM_ROOMID,mArrayList.get(position).getStream_room());
        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.STREAM_USERNAME, mArrayList.get(position).getName());
        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.STREAM_ID, mArrayList.get(position).getStream_id());
        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.STREAM_USERPHOTO, mArrayList.get(position).getPhoto());
        gotoRole2Activity(mArrayList.get(position).getStream_room(),mArrayList.get(position).getUser_id());
   }


    public void gotoRoleActivity(String stream_room, String userID) {
        int mil= (int) (System.currentTimeMillis()/1000L);
        // Intent intent = new Intent(mActivity, LiveStreaming2Activity.class);
        String room = String.valueOf(mil);
        config().setChannelName("vishal");
        config().setUid("23");
        // startActivity(intent);
        //onJoinAsAudience();
        onJoinAsAudience();
        //    onJoinAsBroadcaster();
    }

    public void gotoRole2Activity(String stream_room, String userID) {
        int mil= (int) (System.currentTimeMillis()/1000L);
        // Intent intent = new Intent(mActivity, LiveStreaming2Activity.class);
        String room = String.valueOf(mil);
        config().setChannelName("vishal");
        config().setUid("24");
        // startActivity(intent);
        onJoinAsAudience();
         // onJoinAsAudience();
    }

    public void onJoinAsBroadcaster() {
        gotoLiveActivity(Constants.CLIENT_ROLE_BROADCASTER);
    }
    public void onJoinAsAudience() {
        gotoLiveActivity(Constants.CLIENT_ROLE_AUDIENCE);
    }

    private void gotoLiveActivity(int role) {
        Intent intent = new Intent();
        intent.putExtra(ConstantsA.KEY_CLIENT_ROLE, role);
        intent.setClass(mActivity, LiveStreaming2Activity.class);
        startActivity(intent);
    }



}