package com.divineray.app.activities;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Settings.ContactUs;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.Utils.FileCompressor;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.SignUpModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends BaseActivity{
    /*
      Getting the Current Class Name
     */
    String TAG = SignUpActivity.this.getClass().getSimpleName();

    /*
      Current Activity Instance
     */
    Activity mActivity = SignUpActivity.this;

    /*
      Widgets
      */
    @BindView(R.id.editUserNameET)
    EditText editUserNameET;

    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.txtSignUpTV)
    TextView txtSignUpTV;
    @BindView(R.id.txtAlreadyHaveAccountTV)
    TextView txtAlreadyHaveAccountTV;
    @BindView(R.id.signup_profilePic)
    ImageView signup_profilepic;
    @BindView(R.id.lly_signup_image)
    FrameLayout lly_signup_image;
    @BindView(R.id.password_toggle_si)
    ImageView passwordToggle;
    @BindView(R.id.password_toggle_confirm)
    ImageView passwordToggle_confirm;
    @BindView(R.id.txTermsofService)
    TextView txtTermsofservice;
    @BindView(R.id.txPrivacyPoicy)
    TextView txPrivacyPolicy;
    @BindView(R.id.editPasswordETconfirm)
    EditText editPasswordETconfirm;


    Integer REQUEST_CAMERA=1, SELECT_FILE=0;
    String imageFilePath;
    String imageUrl;
    String base64Image ;
    String strDeviceToken="";
    File mPhotoFile;
    boolean isChecked=true;
    boolean isChecked2=true;
    int status;
    FileCompressor mCompressor;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 101;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setTransparentStatusBarOnly(this);
        setContentView(R.layout.activity_sign_up);
        mCompressor = new FileCompressor(this);
        ButterKnife.bind(this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        //device_token
        getDeviceToken();
        builder.detectFileUriExposure();
        passwordToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    if(!isChecked){

                        // show password
                        editPasswordET.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        editPasswordET.setSelection(editPasswordET.getText().length());
                        passwordToggle.setImageResource(R.drawable.ic_eye_close);
                        isChecked=true;
                        Log.i("checker", "true");
                    }

                    else{
                        Log.i("checker", "false");


                        // hide password
                        editPasswordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        editPasswordET.setSelection(editPasswordET.getText().length());
                        passwordToggle.setImageResource(R.drawable.ic_eye_open);
                        isChecked=false;
                    }
            }
        });


        passwordToggle_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isChecked2){

                    // show password
                    editPasswordETconfirm.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    editPasswordETconfirm.setSelection(editPasswordETconfirm.getText().length());
                    passwordToggle_confirm.setImageResource(R.drawable.ic_eye_close);
                    isChecked2=true;

                }

                else{

                    // hide password
                    editPasswordETconfirm.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    editPasswordETconfirm.setSelection(editPasswordETconfirm.getText().length());
                    passwordToggle_confirm.setImageResource(R.drawable.ic_eye_open);
                    isChecked2=false;
                }
            }
        });



    }
    private void getDeviceToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "**Get Instance Failed**", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        strDeviceToken = task.getResult().getToken();
                        DivineRayPrefernces.writeString(mActivity, DivineRayPrefernces.DEVICETOKEN, strDeviceToken);
                        Log.e(TAG, "**Push Token**" + strDeviceToken);
                    }
                });




    }


    @OnClick({R.id.txtSignUpTV, R.id.txtAlreadyHaveAccountTV,R.id.lly_signup_image,R.id.txPrivacyPoicy,R.id.txTermsofService})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.lly_signup_image:
                editProfileImage();
                break;
            case R.id.txtSignUpTV:
                performSignUpClick();
                break;
            case R.id.txtAlreadyHaveAccountTV:
                onBackPressed();
                break;
            case R.id.txPrivacyPoicy:
                switchActivty("privacypolicy",String.valueOf(R.string.privacy_policy));
                break;
            case R.id.txTermsofService:
                switchActivty("terms", String.valueOf(R.string.terms_of));
                break;

        }
    }

    private void switchActivty(String check,String name) {
        Intent intent=new Intent(mActivity, ContactUs.class);
        intent.putExtra("pageType",check);
        intent.putExtra("pageText",name);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mActivity, LoginActivity.class));
        finish();
    }

    private void performSignUpClick() {
        txtSignUpTV.setClickable(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                txtSignUpTV.setClickable(true);
            }
        }, 1000);
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeSignUpApi();
            }
        }


    }

    //for camera
    private void editProfileImage() {

        final CharSequence[] items={"Camera","Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Add Image");

        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (items[i].equals("Camera")) {

                    if(checkAndRequestPermissions(mActivity)){
                        openCamera();
                    }


                } else if (items[i].equals("Gallery")) {
                    if(checkAndRequestPermissions(mActivity)) {

                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        //startActivityForResult(intent.createChooser(intent, "Select File"), SELECT_FILE);
                        startActivityForResult(intent, SELECT_FILE);
                    }
                }
            }
        });
        builder.show();

    }

    public static boolean checkAndRequestPermissions(final Activity context) {
        int ExtstorePermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (ExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                    .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(context, listPermissionsNeeded
                            .toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION,true);
        if (cameraIntent.resolveActivity(mActivity.getPackageManager())!=null)
        {
            File pictureFile = null;
            try {
                pictureFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(mActivity, "Photo can't be created", Toast.LENGTH_SHORT).show();
                return;
            }
            // Continue only if the File was successfully created
            if (pictureFile != null) {

                Uri photoURI = FileProvider.getUriForFile(mActivity,
                        mActivity.getPackageName()+".provider",
                        pictureFile);
                mPhotoFile=pictureFile;
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(cameraIntent,REQUEST_CAMERA );
            }
        }

    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = mActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".png",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        imageFilePath = image.getAbsolutePath();
        return image;
    }




    @Override
    public  void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode,data);

        if(resultCode== Activity.RESULT_OK){

            if(requestCode==REQUEST_CAMERA){

                try {
                    if (mPhotoFile!=null) {
                        Log.e("CompressBitmap",""+mPhotoFile);
                        File mPhotoFile2 = mCompressor.compressToFile(mPhotoFile);
                        Glide.with(mActivity)
                                .load(mPhotoFile2)
                                .apply(new RequestOptions().centerCrop()
                                        .circleCrop()
                                        .placeholder(R.drawable.unknown_user))
                                .into(signup_profilepic);



                        convertUrlToBase64(Uri.fromFile(mPhotoFile2));
                    }
                    else {
                        //  Toast.makeText(mActivity, ""+mPhotoFile, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }



            }else if(requestCode==SELECT_FILE){

                Uri selectedImageUri = data.getData();

                // signup_profilepic.setImageURI(selectedImageUri);
                Glide.with(mActivity).load(selectedImageUri).into(signup_profilepic);
                imageUrl=selectedImageUri.toString();
               //convertUrlToBase64(Uri.parse(imageUrl));
                CropImage.getPickImageResultUri(mActivity,data);
                if (CropImage.isReadExternalStoragePermissionsRequired(mActivity,selectedImageUri)) {
                    // request permissions and handle the result in onRequestPermissionsResult()

                } else {
                    // no permissions required or already grunted, can start crop image activity
                    startCropImageActivity(selectedImageUri);
                }

            }
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    try {
                        Log.e(TAG, "onActivityResult: " + result.getUri());
                        final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        selectedImage.compress(Bitmap.CompressFormat.PNG, 40, out);
                        signup_profilepic.setImageBitmap(selectedImage);
                        base64Image = encodeTobase64(selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    showToast(mActivity, "Cropping failed: " + result.getError());
                }
            }
        }
    }
    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(50, 50)
                .setRequestedSize(120, 120)
                .setMultiTouchEnabled(false)
                .start(mActivity);
    }

    public String convertUrlToBase64(Uri url)  {
        try {


            Log.e(TAG, "base_64_conversion_started" + base64Image);

            final InputStream imageStream = getContentResolver().openInputStream(url);

            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
            //  String path = MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), selectedImage, "Title", null);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            selectedImage.compress(Bitmap.CompressFormat.PNG, 5, baos);

            base64Image= encodeTobase64(selectedImage);
            Log.e(TAG, "base_64_conversion_done" + base64Image);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return base64Image;
    }
    private String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 5, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS:
                if (ContextCompat.checkSelfPermission(mActivity,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(),
                            "FlagUp Requires Access to Camara.", Toast.LENGTH_SHORT)
                            .show();
                    finish();
                } else if (ContextCompat.checkSelfPermission(mActivity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(),
                            "FlagUp Requires Access to Your Storage.",
                            Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    openCamera();
                }
                break;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    /*
      Check Validations
     */
    public boolean isValidate() {
        boolean flag = true;
        if (base64Image==null) {
            showAlertDialog(mActivity, getString(R.string.ed_profile_image_empty));
            flag = false;
        }
        else if (editUserNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter__name));
            flag = false;
        }
        else if (editUserNameET.getText().toString().length()<=2) {
            showAlertDialog(mActivity, getString(R.string.please_enter_max_chars));
            flag = false;
        }
        else if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        }
        else if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        }else if (editPasswordET.getText().toString().trim().length() < 6) {
            showAlertDialog(mActivity, getString(R.string.password_length));
            flag = false;
        }
        else if (editPasswordET.getText().toString().trim().length() > 13) {
            showAlertDialog(mActivity, getString(R.string.password_length_greater));
            flag = false;
        }
        else if (editPasswordETconfirm.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_confirm_password));
            flag = false;
        }
        else if (!editPasswordETconfirm.getText().toString().trim().equals(editPasswordET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.password_not_matched));
            flag = false;
        }


        return flag;
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("name", editUserNameET.getText().toString().trim());
        mMap.put("email", editEmailET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        mMap.put("photo",base64Image);
        mMap.put("device_type","2");
        mMap.put("device_token",strDeviceToken);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void executeSignUpApi() {
        Log.e(TAG, "base_64_api_request_started");
       showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.signUpRequest(mParam()).enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                Log.e(TAG, "base_64_api_success" + response.body().toString());
                dismissProgressDialog();
                SignUpModel mModel = response.body();

                if (mModel.getStatus().equals(0)) {

                    showAlertDialog2(mActivity, mModel.getMessage());
                    status=0;

                } else  {
                    status=1;
                    showAlertDialog2(mActivity, mModel.getMessage());

                }

            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(mActivity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog2(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (status==1) {
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        alertDialog.show();
    }



}