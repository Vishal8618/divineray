package com.divineray.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.divineray.app.Home.SelectedUserOwnProfile;
import com.divineray.app.Home.SelectedUserProfile;
import com.divineray.app.R;
import com.divineray.app.SelecteUserMore.AppointmentBooking;
import com.divineray.app.SplashVideoActivity;
import com.divineray.app.Utils.Constants;

import org.json.JSONObject;

import java.util.Set;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

public class SplashActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = SplashActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = SplashActivity.this;


    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        setContentView(R.layout.activity_main);
        Intent intent = getIntent();

     setSplash();
    }

    /*
   set splash
    */
    private void setSplash() {


    }

    @Override
    public void onStart() {
        super.onStart();

        Branch branch = Branch.getInstance(getApplicationContext());
        // Branch init
        branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    Log.e("BRANCH SDK", referringParams.toString());
                    initDeepLinkSignIn(referringParams);
                } else {
                    Log.e("BRANCH SDK", error.getMessage());
                    if (IsUserLogin()) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(mActivity, HomeActivity.class));
                                finish();
                            }
                        }, 500);

                    }else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(mActivity,LoginActivity.class));
                                finish();
                            }
                        }, 1500);
                    }
                }
            }
        }, this.getIntent().getData(), this);



    }

    private void initDeepLinkSignIn(JSONObject referringParams) {
        if (referringParams.has("profileUserId")) {
            try {
                switch (referringParams.getString("type")) {

                    case "1":
                        String val = referringParams.getString("profileUserId");
                        if (IsUserLogin()) {
                            try {
                                startActivity(new Intent(mActivity, HomeActivity.class));
                                finish();
                                Intent intent = new Intent(SplashActivity.this, SelectedUserProfile.class);
                                intent.putExtra("profileUserId", val);
                                startActivity(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    case "2" :
                        String val0 = referringParams.getString("profileUserId");
                        if (IsUserLogin()) {
                            try {
                                startActivity(new Intent(mActivity, HomeActivity.class));
                                Constants.Redirection_type=referringParams.getString("type");
                                Constants.Redirection_value=referringParams.getString("videoId");
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
           if (IsUserLogin()) {
               new Handler().postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       startActivity(new Intent(mActivity, HomeActivity.class));
                       finish();
                   }
               }, 500);

           }else {
               new Handler().postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       startActivity(new Intent(mActivity,LoginActivity.class));
                       finish();
                   }
               }, 500);
           }
        }
    }
}
