package com.divineray.app.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.divineray.app.Home.DownloadUtil;
import com.divineray.app.R;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StreamViewActivity extends BaseActivity implements Player.EventListener {
    @BindView(R.id.singlevid_playerview)
    PlayerView playerView;
    @BindView(R.id.llyClose)
    LinearLayout llyClose;

    boolean is_user_stop_video = false;
    SimpleExoPlayer player;
    String videouri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTransparentStatusBarOnly(mActivity);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_stream_view);
        ButterKnife.bind(this);
        getIntentData();
        setPlayer();


    }

    private void getIntentData() {
        if (getIntent()!=null)
        {
            videouri=getIntent().getStringExtra("videoUrl");
        }
    }


    @OnClick({R.id.llyClose})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.llyClose:
                finish();
                break;
        }
    }

    private void setPlayer() {
        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
//
        player = ExoPlayerFactory.newSimpleInstance(mActivity, trackSelector);

        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(mActivity,
                Util.getUserAgent(mActivity, "DivineRay"));
        CacheDataSourceFactory cacheDataSourceFactory= new CacheDataSourceFactory(DownloadUtil.getCache(mActivity),dataSourceFactory, CacheDataSource.FLAG_BLOCK_ON_CACHE|CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);

          MediaSource mediaSource = buildMediaSource(Uri.parse(videouri));

        player.prepare(mediaSource);
        player.seekTo(0);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        //player.setVideoScalingFMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
        playerView.setPlayer(player);
        player.setRepeatMode(Player.REPEAT_MODE_ALL);



        player.setPlayWhenReady(true);

        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {

                    case Player.STATE_BUFFERING:
                        // thumbnail.setVisibility(View.VISIBLE);
                        break;
                    case Player.STATE_IDLE:
                        break;

                    case Player.STATE_READY:

                        playerView.setVisibility(View.VISIBLE);
                        break;

                    default:
                        //  layout.findViewById(R.id.frame_thumbnail).setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });

        //  SimpleExoPlayer finalPlayer = player;
        playerView.setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(mActivity, new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                    super.onFling(e1, e2, velocityX, velocityY);
                    float deltaX = e1.getX() - e2.getX();
                    float deltaXAbs = Math.abs(deltaX);
                    // Only when swipe distance between minimal and maximal distance value then we treat it as effective swipe
                    if((deltaXAbs > 100) && (deltaXAbs < 1000)) {
                        if(deltaX > 0)
                        {
                            //  OpenProfile(item,true);
                        }
                    }


                    return true;
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    super.onSingleTapUp(e);
                    if(!player.getPlayWhenReady()){
                        is_user_stop_video=false;
                        player.setPlayWhenReady(true);
                    }else{
                        is_user_stop_video=true;
                        player.setPlayWhenReady(false);
                    }


                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);


                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {

                    if(!player.getPlayWhenReady()){
                        is_user_stop_video=false;
                        player.setPlayWhenReady(true);
                    }

                    return super.onDoubleTap(e);

                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });


    }


    private MediaSource buildMediaSource(Uri uri) {
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(mActivity, "exoplayer-codelab");
        return new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
    }
}