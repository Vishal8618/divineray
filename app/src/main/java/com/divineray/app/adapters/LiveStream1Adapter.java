package com.divineray.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.divineray.app.Chat.ChatMessageAdapter;
import com.divineray.app.Home.SelectedUserProfile;
import com.divineray.app.R;
import com.divineray.app.StringChange.StringFormatter;
import com.divineray.app.model.ChatMessageModel;
import com.divineray.app.model.HomeModel;
import com.divineray.app.model.StreamMessageModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class  LiveStream1Adapter extends RecyclerView.Adapter<LiveStream1Adapter.ViewHolder>  {
    Context mContext;
    ArrayList<StreamMessageModel.Data> mArrayList = new ArrayList<>();
    String myUserId;

    public LiveStream1Adapter(Context context, ArrayList<StreamMessageModel.Data> mArrayList, String userID) {
    this.mContext=context;
    this.mArrayList=mArrayList;
    this.myUserId=userID;
    }



    @NonNull
    @NotNull
    @Override
    public LiveStream1Adapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stream, parent, false);
        return new LiveStream1Adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull LiveStream1Adapter.ViewHolder holder, int position) {
        StreamMessageModel.Data mModel = mArrayList.get(position);
        if (mModel.getType().equals("8"))
        {
            holder.llyMessage.setVisibility(View.GONE);
            holder.llyJoinRequest.setVisibility(View.VISIBLE);
            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.unknown_user)
                    .error(R.drawable.unknown_user)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            Glide.with(mContext)
                    .load(mModel.getPhoto())
                    .apply(options)
                    .into(holder.imLeaveJoinMessage);
            holder.txLeaveJointext.setText(mModel.getName()+" "+ mContext.getString(R.string.join_stream));
        }
        else if (mModel.getType().equals("9"))
        {
            holder.llyMessage.setVisibility(View.GONE);
            holder.llyJoinRequest.setVisibility(View.VISIBLE);
            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.unknown_user)
                    .error(R.drawable.unknown_user)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            Glide.with(mContext)
                    .load(mModel.getPhoto())
                    .apply(options)
                    .into(holder.imLeaveJoinMessage);
            holder.txLeaveJointext.setText(mModel.getComment());
        }
        else if (mModel.getType().equals("3"))
        {
            holder.llyMessage.setVisibility(View.GONE);
            holder.llyJoinRequest.setVisibility(View.VISIBLE);
            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.unknown_user)
                    .error(R.drawable.unknown_user)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            Glide.with(mContext)
                    .load(mModel.getPhoto())
                    .apply(options)
                    .into(holder.imLeaveJoinMessage);
            holder.txLeaveJointext.setText(R.string.request_reject);
        }
        else if (mModel.getType().equals("4"))
        {
            holder.llyMessage.setVisibility(View.GONE);
            holder.llyJoinRequest.setVisibility(View.VISIBLE);
            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.unknown_user)
                    .error(R.drawable.unknown_user)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            Glide.with(mContext)
                    .load(mModel.getPhoto())
                    .apply(options)
                    .into(holder.imLeaveJoinMessage);
            holder.txLeaveJointext.setText(R.string.kicked_out);
        }
        else
        {
            holder.llyMessage.setVisibility(View.VISIBLE);
            holder.llyJoinRequest.setVisibility(View.GONE);
            if (!mModel.getUser_id().equals(myUserId)) {
                holder.txUsername.setText(StringFormatter.capitalizeWord(mModel.getName()));
            }
            else
            {
                holder.txUsername.setText("Me");
            }
            holder.txMessage.setText(mModel.getComment());
            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.unknown_user)
                    .error(R.drawable.unknown_user)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

                    Glide.with(mContext)
                            .load(mModel.getPhoto())
                            .apply(options)
                            .into(holder.imStreamUserphoto);

        }
//        else if(mModel.getType().equals("3")){
//            holder.llyMessage.setVisibility(View.GONE);
//            holder.llyJoinRequest.setVisibility(View.VISIBLE);
//            holder.txLeaveJointext.setText(mModel.getName()+ mContext.getResources().getString(R.string.want_to_join)+mContext.getResources().getString(R.string.user_wants_to_join_the_text));
//        }


        holder.imStreamUserphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!myUserId.equals(mModel.getUser_id())) {
                    Intent intent = new Intent(mContext, SelectedUserProfile.class);
                    intent.putExtra("profileUserId", mModel.getUser_id());
                    mContext.startActivity(intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //Message Side
        @BindView(R.id.imStreamUserphoto)
        CircleImageView imStreamUserphoto;
        @BindView(R.id.txUsername)
        TextView txUsername;
        @BindView(R.id.txMessage)
        TextView txMessage;
        @BindView(R.id.llyMessage)
        LinearLayout llyMessage;


        //Join/requests
        @BindView(R.id.imLeaveJoinMessage)
        CircleImageView imLeaveJoinMessage;
        @BindView(R.id.txLeaveJointext)
        TextView txLeaveJointext;
        @BindView(R.id.llyJoinRequest)
        LinearLayout llyJoinRequest;


        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
