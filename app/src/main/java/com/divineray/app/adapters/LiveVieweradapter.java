package com.divineray.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.divineray.app.R;
import com.divineray.app.model.LiveUsersModel;
import com.divineray.app.model.LiveViewersModel;
import com.divineray.app.model.StreamListModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LiveVieweradapter extends RecyclerView.Adapter<LiveVieweradapter.ViewHolder> {
    Activity mActivity;
    List<LiveViewersModel.DataL> liveUsers;
    private LiveVieweradapter.OnItemClickListener listener;


    public LiveVieweradapter(Activity mActivity, List<LiveViewersModel.DataL> liveUsers, LiveVieweradapter.OnItemClickListener listener) {
        this.mActivity=mActivity;
        this.liveUsers=liveUsers;
        this.listener = listener;

    }

    public interface OnItemClickListener {
        void onItemClick(int positon, LiveViewersModel.DataL item, View view);
    }

    @NonNull
    @Override
    public LiveVieweradapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.live_viewers_item, parent, false);
        return new LiveVieweradapter.ViewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        LiveViewersModel.DataL mModel=liveUsers.get(position);
        holder.bind(position,mModel,listener);
        holder.txUsernameViewer.setText(mModel.getName());
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.unknown_user)
                .error(R.drawable.unknown_user)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        Glide.with(mActivity).load(mModel.getPhoto()).apply(options).into(holder.imUserPic);


    }


    @Override
    public int getItemCount() {
        return liveUsers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imUserPicViewer)
        ImageView imUserPic;
        @BindView(R.id.txUsernameViewer)
        TextView txUsernameViewer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void bind(int position, LiveViewersModel.DataL mModel, OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });

            txUsernameViewer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });

            imUserPic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });

        }
    }
}

