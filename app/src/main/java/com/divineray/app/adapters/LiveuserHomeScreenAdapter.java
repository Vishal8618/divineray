package com.divineray.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.divineray.app.R;
import com.divineray.app.model.LiveUsersModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class LiveuserHomeScreenAdapter extends RecyclerView.Adapter<LiveuserHomeScreenAdapter.ViewHolder> {
    ArrayList<LiveUsersModel.Data> mArrayList;
   Context context;
   private  onItemClickClickListener listener;

    public interface  onItemClickClickListener
    {
        void onItemClick(int position,ArrayList<LiveUsersModel.Data> mArrayList,View view);
    }


    public LiveuserHomeScreenAdapter(ArrayList<LiveUsersModel.Data> mArraylist, Context context, onItemClickClickListener listener) {
   this.mArrayList=mArraylist;
   this.context=context;
   this.listener=listener;
    }



    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_live_user_new, parent, false);
        return new LiveuserHomeScreenAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {

   LiveUsersModel.Data mModel = mArrayList.get(position);
   Glide.with(context).load(mModel.getPhoto()).placeholder(R.drawable.unknown_user).into(holder.imProfilePic);
   Glide.with(context).load(mModel.getPhoto()).placeholder(R.drawable.unknown_user).into(holder.imLiveUserImage);
    holder.bind(position,mArrayList,listener);
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imProfilePic)
        CircleImageView imProfilePic;
        @BindView(R.id.imLiveUserImage)
        ImageView imLiveUserImage;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(int position, ArrayList<LiveUsersModel.Data> mArrayList, onItemClickClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mArrayList,v);
                }
            });

            imLiveUserImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mArrayList,v);
                }
            });


        }
    }
}
