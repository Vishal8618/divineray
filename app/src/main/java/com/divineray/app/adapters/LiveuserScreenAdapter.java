package com.divineray.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.divineray.app.R;
import com.divineray.app.model.LiveUsersModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class LiveuserScreenAdapter extends RecyclerView.Adapter<LiveuserScreenAdapter.ViewHolder> {
    ArrayList<LiveUsersModel.Data> mArrayList;
   Context context;
   private  onItemClickClickListener listener;

    public interface  onItemClickClickListener
    {
        void onItemClick(int position,ArrayList<LiveUsersModel.Data> mArrayList,View view);
    }


    public LiveuserScreenAdapter(ArrayList<LiveUsersModel.Data> mArraylist, Context context, onItemClickClickListener listener) {
   this.mArrayList=mArraylist;
   this.context=context;
   this.listener=listener;
    }



    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_live_users, parent, false);
        return new LiveuserScreenAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {

                if (position==0)
                {
                    holder.cdGoLive.setVisibility(View.VISIBLE);
                    holder.llyUser.setVisibility(View.GONE);
                }
                else {
                    if (mArrayList.size()>0) {
                        int newPoition=position-1;
                        LiveUsersModel.Data mModel = mArrayList.get(newPoition);

                        holder.cdGoLive.setVisibility(View.GONE);
                        holder.llyUser.setVisibility(View.VISIBLE);
                        Glide.with(context).load(mModel.getPhoto()).placeholder(R.drawable.unknown_user).into(holder.imProfilePic);
                        holder.txUsername.setText(mModel.getName());
                    }
               }
        holder.bind(position,mArrayList,listener);
    }

    @Override
    public int getItemCount() {
        return 1+mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cdGoLive)
        CardView cdGoLive;
        @BindView(R.id.llyUser)
        LinearLayout llyUser;
        @BindView(R.id.txUsername)
        TextView txUsername;
        @BindView(R.id.imProfilePic)
        CircleImageView imProfilePic;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(int position, ArrayList<LiveUsersModel.Data> mArrayList, onItemClickClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mArrayList,v);
                }
            });

            cdGoLive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mArrayList,v);
                }
            });
            llyUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mArrayList,v);
                }
            });
            imProfilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mArrayList,v);
                }
            });
            txUsername.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mArrayList,v);
                }
            });



        }
    }
}
