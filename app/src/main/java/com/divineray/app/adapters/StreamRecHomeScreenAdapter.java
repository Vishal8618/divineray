package com.divineray.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.divineray.app.R;
import com.divineray.app.activities.StreamViewActivity;
import com.divineray.app.model.DataItem;
import com.divineray.app.model.LiveUsersModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class StreamRecHomeScreenAdapter extends RecyclerView.Adapter<StreamRecHomeScreenAdapter.ViewHolder> {
    List<DataItem> mArraylist;
    Context context;


    public StreamRecHomeScreenAdapter(List<DataItem> mArraylist, Context context) {
        this.mArraylist=mArraylist;
        this.context=context;

    }



    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recorded_stream, parent, false);
        return new StreamRecHomeScreenAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {

        DataItem mModel = mArraylist.get(position);
        Glide.with(context).load(mModel.getPhoto()).placeholder(R.drawable.unknown_user).into(holder.imProfilePic);
        Glide.with(context).load(mModel.getPhoto()).placeholder(R.drawable.unknown_user).into(holder.imLiveUserImage);
        holder.txUserName.setText(mModel.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, StreamViewActivity.class);
                intent.putExtra("videoUrl",mModel.getVideoLink());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArraylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imProfilePic)
        CircleImageView imProfilePic;
        @BindView(R.id.imLiveUserImage)
        ImageView imLiveUserImage;
        @BindView(R.id.txUserName)
        TextView txUserName;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
}
