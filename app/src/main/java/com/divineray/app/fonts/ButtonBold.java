package com.divineray.app.fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;

import androidx.annotation.RequiresApi;

/**
 * Created by Dharmani Apps
 */


/*
 * Button with Custom font family
 * */


@SuppressLint("AppCompatCustomView")
public class ButtonBold extends Button {


    /*
     * Getting Current Class Name
     * */

    private String mTag = ButtonBold.this.getClass().getSimpleName();


    /*
     * Constructor with
     * #Context
     * */

    public ButtonBold(Context context) {
        super(context);
        applyCustomFont(context);
    }



    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * */

    public ButtonBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }


    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * */

    public ButtonBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }



    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * #int defStyleAttr
     * */

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ButtonBold(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }


    /*
     * Apply font.
     * */

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new PoppinsBold(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}
