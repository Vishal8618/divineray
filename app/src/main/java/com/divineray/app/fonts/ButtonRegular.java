package com.divineray.app.fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;

import androidx.annotation.RequiresApi;

/**
 * Created by Dharmani Apps on 1/18/2018.
 */


/*
 * Button with Custom font family
 * */

@SuppressLint("AppCompatCustomView")
public class ButtonRegular extends Button {

    /*
     * Getting Current Class Name
     * */
    private String mTag = ButtonRegular.this.getClass().getSimpleName();


    /*
     * Constructor with
     * #Context
     * */
    public ButtonRegular(Context context) {
        super(context);
        applyCustomFont(context);
    }


    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * */
    public ButtonRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }



    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * */
    public ButtonRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }



    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * #int defStyleAttr
     * */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ButtonRegular(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }


    /*
     * Apply font.
     * */
    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new PoppinsRegular(context).getFont());
        } catch (Exception e) {
            Log.e(mTag, e.toString());
        }
    }

}
