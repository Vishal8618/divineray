package com.divineray.app.fonts;

import android.content.Context;
import android.graphics.Typeface;

public class PoppinsBold {
    /*
     * Initialize Context
     * */
    Context mContext;
    /*
     * Initialize the Typeface
     * */
    private Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    private String path = "Poppins-Bold.ttf";

    /*
     * Default Constructor
     * */
    public PoppinsBold() {
    }

    /*
     * Constructor with Context
     * */
    public PoppinsBold(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null) {
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        }
        return fontTypeface;
    }

}
