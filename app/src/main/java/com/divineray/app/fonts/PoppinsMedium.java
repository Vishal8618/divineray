package com.divineray.app.fonts;

import android.content.Context;
import android.graphics.Typeface;


public class PoppinsMedium {
    /*
     * Initialize the Typeface
     * */
    public static Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    String path = "Poppins-Medium.ttf";
    /*
     * Initialize Context
     * */
    public Context mContext;
    /*
     * Default Constructor
     * */
    public PoppinsMedium() {
    }
    /*
     * Constructor with Context
     * */
    public PoppinsMedium(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null)
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
            return fontTypeface;
    }
}