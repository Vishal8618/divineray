package com.divineray.app.interfaces;
import com.divineray.app.Utils.Constants;
import com.divineray.app.model.AddModel;
import com.divineray.app.model.AppointmentBookingModel;
import com.divineray.app.model.BottomSheetVidModel;
import com.divineray.app.model.BuyCoinItemModel;
import com.divineray.app.model.BuyCoinSModel;
import com.divineray.app.model.BuyCoinTotalModel;
import com.divineray.app.model.ChangePassStatusModel;
import com.divineray.app.model.ChangePasswordModel;
import com.divineray.app.model.ChatMessageModel;
import com.divineray.app.model.ChatModel;
import com.divineray.app.model.CheckModel;
import com.divineray.app.model.ChhatUneadMessageModel;
import com.divineray.app.model.CloudRecordStartModel;
import com.divineray.app.model.DeepLinkModel;
import com.divineray.app.model.EditProfileStatusModel;
import com.divineray.app.model.EndStream2Model;
import com.divineray.app.model.FavModel;
import com.divineray.app.model.FollowFollowersModel;
import com.divineray.app.model.ForgotPasswordModel;
import com.divineray.app.model.GetAllGiftModel;
import com.divineray.app.model.GetAllUserModel;
import com.divineray.app.model.GetCommentsModel;
import com.divineray.app.model.GetProfileModel;
import com.divineray.app.model.GoingLiveModel;
import com.divineray.app.model.HomeModel;
import com.divineray.app.model.LiveUsersModel;
import com.divineray.app.model.LiveViewersModel;
import com.divineray.app.model.LoginModel;
import com.divineray.app.model.LogoutModel;
import com.divineray.app.model.NotificationAllModel;
import com.divineray.app.model.NottificationModel;
import com.divineray.app.model.ProductListingModel;
import com.divineray.app.model.ProfileGetVideoModel;
import com.divineray.app.model.RemoveModel;
import com.divineray.app.model.ReportAbuseModel;
import com.divineray.app.model.ReportVideoModel;
import com.divineray.app.model.Requestappointmentmodel;
import com.divineray.app.model.RoomModel;
import com.divineray.app.model.RootModel;
import com.divineray.app.model.SearchModelget;
import com.divineray.app.model.SearchModeltagged;
import com.divineray.app.model.ShareItemModel;
import com.divineray.app.model.SignUpModel;
import com.divineray.app.model.StatusModel;
import com.divineray.app.model.StreamListModel;
import com.divineray.app.model.StreamMessageModel;
import com.divineray.app.model.StreamModel;
import com.divineray.app.model.StreamRecordedModel;
import com.divineray.app.model.VersionModel;
import com.divineray.app.model.VideoAudioModel;
import com.divineray.app.model.VideofavModel;
import com.divineray.app.model.ViewCounModel;
import com.divineray.app.model.cloudre.CloudRecordingResponse;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @POST("SignUp.php")
    Call<SignUpModel> signUpRequest(@Body Map<String, String> mParams);

    @POST("Login.php")
    Call<LoginModel> loginRequest(@Body Map<String, String> mParams);

    @POST("ForgetPassword.php")
    Call<ForgotPasswordModel> forgotPasswordRequest(@Body Map<String, String> mParams);

    @POST("EditUserProfile.php")
    Call<EditProfileStatusModel> saveProfileDetailsRequest(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("GetAllTaggedVideos.php")
    Call<SearchModeltagged> searchTag(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("CheckRoomConnection.php")
    Call<RoomModel> checkRoom(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("AddUpdateMessage.php")
    Call<ChatMessageModel> addUpdateChatMessage(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("RemoveVideo.php")
    Call<RemoveModel> removeVideo(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("GetAllMusic.php")
    Call<BottomSheetVidModel> getAllMusic(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);


    @POST("GetAllChatLists.php")
    Call<ChatModel> getAllChats(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("GetAllChatMessages.php")
    Call<ChatMessageModel> getUserChats(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("GetProfileDetails.php")
    Call<GetProfileModel> getProfileDetailsRequest(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("GetAllUserVideos.php")
    Call<ProfileGetVideoModel> getProfileVideos(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("GetAllUserVideos.php")
    Call<SearchModeltagged> getProfileMusicVideos(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("GetAllTagsListingAndSearch.php")
    Call<SearchModelget> getSearchData(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);



    @POST("ChangePassword.php")
    Call<ChangePassStatusModel> addChangePassModel(@Body ChangePasswordModel changePasswordModel);

    @POST("GetAllVideos.php")
    Call<HomeModel> getVideos(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);


    @POST("FollowUnfollowUser.php")
    Call<RemoveModel> followUnfollow(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("GetAllComments.php")
    Call<GetCommentsModel> getComments(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);//user_id,videoId,lastId

    @POST("LikeViewVideo.php")
    Call<VideofavModel> addFavVideo(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("AddUpdateComment.php")
    Call<GetCommentsModel> addComment(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("GetAllFavVideos.php")
    Call<FavModel> getFavVideos(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @Multipart
    @POST("uploadVideo.php")
    Call<AddModel> addPostDataRequest(
            @Header("Token") String mAuthorization,
            @Part("user_id") RequestBody user_id,
            @Part MultipartBody.Part postImage,
            @Part MultipartBody.Part postVideo,
            @Part ("description") RequestBody description,
            @Part("tags") RequestBody tags,
            @Part ("postHeight") RequestBody postHeight,
            @Part ("postWidth") RequestBody postWidth,
            @Part ("musicId") RequestBody musicId);

    @Multipart
    @POST("EditUserProfile.php")
    Call<EditProfileStatusModel> editProfileRequest(
            @Part("email") RequestBody email,
            @Part("name") RequestBody name,
            @Part MultipartBody.Part photo,
            @Part ("user_id") RequestBody description,
            @Part("country") RequestBody tags,
            @Part ("countryCode") RequestBody postHeight,
            @Part ("description") RequestBody postWidth);


    @POST("FacebookLogin.php")
    Call<LoginModel> loginWithFbRequest(@Body Map<String, String> mParams);

    @POST("GetFollowUsers.php")
    Call<FollowFollowersModel> getFollowUsers(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("TwitterLogin.php")
    Call<LoginModel> loginWithTwitterRequest(@Body Map<String, String> mParams);

    @POST("Logout.php")
    Call<LogoutModel> logoutRequest(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @Headers({"Authorization: key=" + Constants.SERVER_KEY, "Content-Type:application/json"})
    @POST("fcm/send")
    Call<ResponseBody> sendNotification(@Body RootModel root);

    @POST("GetAllProductCoins.php")
    Call<BuyCoinTotalModel> getTotalCoins(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("PurchaseCoinsV2.php")
    Call<BuyCoinItemModel> buyCoin(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("GetAllUsersProductListing.php")
    Call<ProductListingModel> getProductPurchase(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);


    @POST("GetAllUsersSearch.php")
    Call<GetAllUserModel> getUserList(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("GetAllUsersMediaListing.php")
    Call<VideoAudioModel> getVideoAudio(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("GetAllGiftListing.php")
    Call<GetAllGiftModel> getAllGifts(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("ShareGift.php")
    Call<ShareItemModel> shareGift(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("GetAllAppointmentListingV3.php")
    Call<AppointmentBookingModel> getAllAppointment(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("GetAllReportReasons.php")
    Call<ReportVideoModel> reportVideo(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("AddReportAbuse.php")
    Call<ReportAbuseModel> reportVideosubmit(@Header("Token") String mAuthorization,@Body Map<String, String> mParams2);

    @POST("AddRequestAppointment.php")
    Call<Requestappointmentmodel> bookAppointment(@Header("Token") String mAuthorization,@Body Map<String, String> mParams2);

    @POST("UpdateNotification.php")
    Call<NottificationModel> pushNotification(@Header("Token") String mAuthorization,@Body Map<String, String> mParam0);

    @POST("GetAllNotificationsDetails.php")
    Call<NotificationAllModel> getAllNotifications(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("UpdateMessageSeen.php")
    Call<ChhatUneadMessageModel> sendReadMessage(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("UpdateNotificationSeen.php")
    Call<ChhatUneadMessageModel> sendReadNotification(@Header("Token") String mAuthorization,@Body Map<String, String> mParams2);

    @POST("PurchaseMedia.php")
    Call<CheckModel> buyMedia(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @POST("GetAllAppointmentListing.php")
    Call<AppointmentBookingModel> getAllAppointment2(@Body Map<String, String> mParams);

    @POST("ShareUserProfile.php")
    Call<DeepLinkModel> getDeepLInk(@Header("Token") String mAuthorization,@Body Map<String, String> mParams3);

    @POST("lUxss6ok.php")
    Call<BuyCoinSModel> buyCoinNew(@Header("Token") String mAuthorization,@Body Map<String, String> mParams);

    @GET("GetVersion.php")
    Call<VersionModel> getVersion();

    @POST("GoingliveV2.php")
    Call<GoingLiveModel> executeLiveStream(@Header("Token") String mAuthorization,@Body Map<String, String> mParam);

    @POST("GoingliveV3.php")
    Call<CloudRecordingResponse> executeCloudRecordStream(@Header("Token") String mAuthorization, @Body Map<String, String> mParam);

    @POST("GetAllstreamcommentsv2.php")
    Call<StreamMessageModel> getStreamChatListing(@Header("Token") String mAuthorization, @Body Map<String, String> mParams);

    @POST("Addstreamcommentsv2.php")
    Call<JsonObject> addStreamComment(@Header("Token")String authToken,@Body Map<String, String> mParams12);

    @POST("Hostlivedetails.php")
    Call<StreamListModel> getHostLiveDetails(@Header("Token")String authToken, @Body Map<String, String> mParams);

    @POST("Endstream.php")
    Call<StatusModel> endLiveStream(@Header("Token")String authToken,@Body Map<String, String> mParam);

    @POST("EndstreamV2.php")
    Call<EndStream2Model> endLiveStreamV2(@Header("Token")String authToken, @Body Map<String, String> mParam);

    @POST("Leavestreamv2.php")
    Call<StatusModel> leaveLiveStream(@Header("Token")String authToken,@Body Map<String, String> mParam);


    @GET("getLiveUsers.php")
    Call<LiveUsersModel> showLiveUsers(@Header("Token")String authToken);

    @POST("viewLiveStream.php")
    Call<ViewCounModel> getViewCounts(@Header("Token")String authToken,@Body Map<String, String> mParams);

    @POST("userstreamjoinrequestv2.php")
    Call<StatusModel> requestRoom(@Header("Token")String authToken,@Body Map<String, String> mParams);

    @POST("approveRejectStream.php")
    Call<StatusModel> approveRejectStream(@Header("Token")String authToken,@Body Map<String, String> mParams);

    @POST("disableLikeComment.php")
    Call<StatusModel> disableLikeComment(@Header("Token")String authToken,@Body Map<String, String> mParamsN);

    @POST("getAllLiveUserByStreamID.php")
    Call<LiveViewersModel> getViewers(@Header("Token")String authToken,@Body Map<String, String> mParams1);

    @POST("addStreamVideoLink.php")
    Call<StatusModel> exceuteShareStream(@Header("Token")String authToken,@Body Map<String, String> mParams1);

    @POST("getAllStreamVideo.php")
    Call<StreamRecordedModel> getAllStreams(@Header("Token")String authToken, @Body Map<String, String> mParams1);
}