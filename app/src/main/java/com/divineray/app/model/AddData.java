package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddData {
    @SerializedName("videoId")
    @Expose
    private String videoId;

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("postImage")
    @Expose
    private String postImage;

    @SerializedName("postVideo")
    @Expose
    private String postVideo;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("tags")
    @Expose
    private String tags;

    @SerializedName("userDetails")
    @Expose
    private UserDetails userDetails;

    @SerializedName("isLiked")
    @Expose
    private String isLiked;

    @SerializedName("totalLike")
    @Expose
    private String totalLike;

    @SerializedName("totalViews")
    @Expose
    private String totalViews;

    @SerializedName("totalComments")
    @Expose
    private String totalComments;

    public void setVideoId(String videoId){
        this.videoId = videoId;
    }
    public String getVideoId(){
        return this.videoId;
    }
    public void setUser_id(String user_id){
        this.user_id = user_id;
    }
    public String getUser_id(){
        return this.user_id;
    }
    public void setPostImage(String postImage){
        this.postImage = postImage;
    }
    public String getPostImage(){
        return this.postImage;
    }
    public void setPostVideo(String postVideo){
        this.postVideo = postVideo;
    }
    public String getPostVideo(){
        return this.postVideo;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public String getTitle(){
        return this.title;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public String getDescription(){
        return this.description;
    }
    public void setPrice(String price){
        this.price = price;
    }
    public String getPrice(){
        return this.price;
    }
    public void setTags(String tags){
        this.tags = tags;
    }
    public String getTags(){
        return this.tags;
    }
    public void setUserDetails(UserDetails userDetails){
        this.userDetails = userDetails;
    }
    public UserDetails getUserDetails(){
        return this.userDetails;
    }
    public void setIsLiked(String isLiked){
        this.isLiked = isLiked;
    }
    public String getIsLiked(){
        return this.isLiked;
    }
    public void setTotalLike(String totalLike){
        this.totalLike = totalLike;
    }
    public String getTotalLike(){
        return this.totalLike;
    }
    public void setTotalViews(String totalViews){
        this.totalViews = totalViews;
    }
    public String getTotalViews(){
        return this.totalViews;
    }
    public void setTotalComments(String totalComments){
        this.totalComments = totalComments;
    }
    public String getTotalComments(){
        return this.totalComments;
    }
}






