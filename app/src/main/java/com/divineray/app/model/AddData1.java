package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddData1 {
    @SerializedName("totalFollowing")
    @Expose
    private String totalFollowing;

    @SerializedName("totalFollowers")
    @Expose
    private String totalFollowers;

    @SerializedName("follow")
    @Expose
    private String follow;

    public void setTotalFollowing(String totalFollowing){
        this.totalFollowing = totalFollowing;
    }
    public String getTotalFollowing(){
        return this.totalFollowing;
    }
    public void setTotalFollowers(String totalFollowers){
        this.totalFollowers = totalFollowers;
    }
    public String getTotalFollowers(){
        return this.totalFollowers;
    }
    public void setFollow(String follow){
        this.follow = follow;
    }
    public String getFollow(){
        return this.follow;
    }
}
