package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.util.List;

public class AddModel {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<UserVideos> userVideos;
  //  private Data data;

   // private List<UserVideos> userVideos;

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public class UserVideos {

        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("tagId")
        @Expose
        private String tagId;

        @SerializedName("videoId")
        @Expose
        private String videoId;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTagId() {
            return tagId;
        }

        public void setTagId(String tagId) {
            this.tagId = tagId;
        }

        public String getVideoId() {
            return videoId;
        }

        public void setVideoId(String videoId) {
            this.videoId = videoId;
        }


        //        @SerializedName("videoId")
//        @Expose
//        private String videoId;
//
//        @SerializedName("user_id")
//        @Expose
//        private String user_id;
//
//        @SerializedName("postImage")
//        @Expose
//        private String postImage;
//
//        @SerializedName("postVideo")
//        @Expose
//        private String postVideo;
//
//        @SerializedName("title")
//        @Expose
//        private String title;
//
//        @SerializedName("description")
//        @Expose
//        private String description;
//
//        @SerializedName("price")
//        @Expose
//        private String price;
//
//        @SerializedName("tags")
//        @Expose
//        private String tags;
//
//        @SerializedName("userDetails")
//        @Expose
//        private UserDetailsAdd userDetails;
//
//        @SerializedName("isLiked")
//        @Expose
//        private String isLiked;
//
//        @SerializedName("totalLike")
//        @Expose
//        private String totalLike;
//
//        @SerializedName("totalViews")
//        @Expose
//        private String totalViews;
//
//        @SerializedName("totalComments")
//        @Expose
//        private String totalComments;
//
//        public void setVideoId(String videoId) {
//            this.videoId = videoId;
//        }
//
//        public String getVideoId() {
//            return this.videoId;
//        }
//
//        public void setUser_id(String user_id) {
//            this.user_id = user_id;
//        }
//
//        public String getUser_id() {
//            return this.user_id;
//        }
//
//        public void setPostImage(String postImage) {
//            this.postImage = postImage;
//        }
//
//        public String getPostImage() {
//            return this.postImage;
//        }
//
//        public void setPostVideo(String postVideo) {
//            this.postVideo = postVideo;
//        }
//
//        public String getPostVideo() {
//            return this.postVideo;
//        }
//
//        public void setTitle(String title) {
//            this.title = title;
//        }
//
//        public String getTitle() {
//            return this.title;
//        }
//
//        public void setDescription(String description) {
//            this.description = description;
//        }
//
//        public String getDescription() {
//            return this.description;
//        }
//
//        public void setPrice(String price) {
//            this.price = price;
//        }
//
//        public String getPrice() {
//            return this.price;
//        }
//
//        public void setTags(String tags) {
//            this.tags = tags;
//        }
//
//        public String getTags() {
//            return this.tags;
//        }
//
//        public UserDetailsAdd getUserDetails() {
//            return userDetails;
//        }
//
//        public void setUserDetails(UserDetailsAdd userDetails) {
//            this.userDetails = userDetails;
//        }
//
//        public void setIsLiked(String isLiked) {
//            this.isLiked = isLiked;
//        }
//
//        public String getIsLiked() {
//            return this.isLiked;
//        }
//
//        public void setTotalLike(String totalLike) {
//            this.totalLike = totalLike;
//        }
//
//        public String getTotalLike() {
//            return this.totalLike;
//        }
//
//        public void setTotalViews(String totalViews) {
//            this.totalViews = totalViews;
//        }
//
//        public String getTotalViews() {
//            return this.totalViews;
//        }
//
//        public void setTotalComments(String totalComments) {
//            this.totalComments = totalComments;
//        }
//
//        public String getTotalComments() {
//            return this.totalComments;
//        }
    }
    public class UserDetails
    {
        @SerializedName("follow")
        @Expose
        private String follow;

        public void setFollow(String follow){
            this.follow = follow;
        }
        public String getFollow(){
            return this.follow;
        }
    }
    public class Data
    {

        @SerializedName("totalFollowing")
        @Expose
        private String totalFollowing;

        @SerializedName("totalFollowers")
        @Expose
        private String totalFollowers;

        @SerializedName("follow")
        @Expose
        private String follow;

        public void setTotalFollowing(String totalFollowing){
            this.totalFollowing = totalFollowing;
        }
        public String getTotalFollowing(){
            return this.totalFollowing;
        }
        public void setTotalFollowers(String totalFollowers){
            this.totalFollowers = totalFollowers;
        }
        public String getTotalFollowers(){
            return this.totalFollowers;
        }
        public void setFollow(String follow){
            this.follow = follow;
        }
        public String getFollow(){
            return this.follow;
        }
    }
}

