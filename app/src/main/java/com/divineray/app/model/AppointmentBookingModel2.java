package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppointmentBookingModel2 {
        @SerializedName("status")
        @Expose
        private int status;

        @SerializedName("message")
        @Expose
        private String message;

        @SerializedName("data")
        @Expose
        private List<Data> data;

        public void setStatus(int status){
            this.status = status;
        }
        public int getStatus(){
            return this.status;
        }
        public void setMessage(String message){
            this.message = message;
        }
        public String getMessage(){
            return this.message;
        }
        public void setData(List<Data> data){
            this.data = data;
        }
        public List<Data> getData(){
            return this.data;
        }
        public class Data
        {
            @SerializedName("title")
            @Expose
            private String title;


            @SerializedName("data")
            @Expose
            private List<Dataf> data;


            public List<Dataf> getData() {
                return data;
            }

            public void setData(List<Dataf> data) {
                this.data = data;
            }

            public void setTitle(String title){
                this.title = title;
            }
            public String getTitle(){
                return this.title;
            }
            public void setData2(List<Dataf> data){
                this.data = data;
            }
            public List<Dataf> getData2(){
                return this.data;
            }


        }
        public  class Dataf
        {
            private String id;

            private String userID;

            private String startTime;

            private String endTime;
            private String price;
            private String appointmentRate;

            private String timeDuration;

            private String daystype;

            private String type;

            private  String advisory;

            private String createDate;
            private  String timeZone;

            public String getTimeZone() {
                return timeZone;
            }

            public void setTimeZone(String timeZone) {
                this.timeZone = timeZone;
            }

            public void setId(String id){
                this.id = id;
            }
            public String getId(){
                return this.id;
            }
            public void setUserID(String userID){
                this.userID = userID;
            }
            public String getUserID(){
                return this.userID;
            }
            public void setStartTime(String startTime){
                this.startTime = startTime;
            }
            public String getStartTime(){
                return this.startTime;
            }
            public void setEndTime(String endTime){
                this.endTime = endTime;
            }
            public String getEndTime(){
                return this.endTime;
            }
            public void setAppointmentRate(String appointmentRate){
                this.appointmentRate = appointmentRate;
            }
            public String getAppointmentRate(){
                return this.appointmentRate;
            }
            public void setTimeDuration(String timeDuration){
                this.timeDuration = timeDuration;
            }
            public String getTimeDuration(){
                return this.timeDuration;
            }
            public void setDaystype(String daystype){
                this.daystype = daystype;
            }
            public String getDaystype(){
                return this.daystype;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public void setCreateDate(String createDate){
                this.createDate = createDate;
            }
            public String getCreateDate(){
                return this.createDate;
            }

            public String getAdvisory() {
                return advisory;
            }

            public void setAdvisory(String advisory) {
                this.advisory = advisory;
            }

            @Override
            public String toString() {
                return "Dataf{" +
                        "id='" + id + '\'' +
                        ", userID='" + userID + '\'' +
                        ", startTime='" + startTime + '\'' +
                        ", endTime='" + endTime + '\'' +
                        ", appointmentRate='" + appointmentRate + '\'' +
                        ", timeDuration='" + timeDuration + '\'' +
                        ", daystype='" + daystype + '\'' +
                        ", status='" + status + '\'' +
                        ", advisory='" + advisory + '\'' +
                        ", createDate='" + createDate + '\'' +
                        '}';
            }
        }


}
