package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BottomSheetVidModel {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<DataVi> data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(List<DataVi> data){
        this.data = data;
    }
    public List<DataVi> getData(){
        return this.data;
    }

    @Override
    public String toString() {
        return "BottomSheetVidModel{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public  class DataVi
    {
        @SerializedName("musicId")
        @Expose
        private String musicId;

        @SerializedName("musicName")
        @Expose
        private String musicName;

        @SerializedName("musicFile")
        @Expose
        private String musicFile;

        @SerializedName("musicCat")
        @Expose
        private String musicCat;

        @SerializedName("musicImage")
        @Expose
        private String musicImage;

        @SerializedName("created")
        @Expose
        private String created;

        public void setMusicId(String musicId){
            this.musicId = musicId;
        }
        public String getMusicId(){
            return this.musicId;
        }
        public void setMusicName(String musicName){
            this.musicName = musicName;
        }
        public String getMusicName(){
            return this.musicName;
        }
        public void setMusicFile(String musicFile){
            this.musicFile = musicFile;
        }
        public String getMusicFile(){
            return this.musicFile;
        }
        public void setMusicCat(String musicCat){
            this.musicCat = musicCat;
        }
        public String getMusicCat(){
            return this.musicCat;
        }
        public void setMusicImage(String musicImage){
            this.musicImage = musicImage;
        }
        public String getMusicImage(){
            return this.musicImage;
        }
        public void setCreated(String created){
            this.created = created;
        }
        public String getCreated(){
            return this.created;
        }

        @Override
        public String toString() {
            return "DataVi{" +
                    "musicId='" + musicId + '\'' +
                    ", musicName='" + musicName + '\'' +
                    ", musicFile='" + musicFile + '\'' +
                    ", musicCat='" + musicCat + '\'' +
                    ", musicImage='" + musicImage + '\'' +
                    ", created='" + created + '\'' +
                    '}';
        }
    }
}
