package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BuyCoinItemModel {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("totalCoins")
    @Expose
    private String totalCoins;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setTotalCoins(String totalCoins){
        this.totalCoins = totalCoins;
    }
    public String getTotalCoins(){
        return this.totalCoins;
    }
}
