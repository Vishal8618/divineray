package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BuyCoinTotalModel {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("totalCoins")
    @Expose
    private String totalCoins;

    @SerializedName("ProductCoins")
    @Expose
    private List<ProductCoins> ProductCoins;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setTotalCoins(String totalCoins){
        this.totalCoins = totalCoins;
    }
    public String getTotalCoins(){
        return this.totalCoins;
    }
    public void setProductCoins(List<ProductCoins> ProductCoins){
        this.ProductCoins = ProductCoins;
    }
    public List<ProductCoins> getProductCoins(){
        return this.ProductCoins;
    }
    public class ProductCoins
    {
        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("price")
        @Expose
        private String price;



        @SerializedName("coins")
        @Expose
        private String coins;


        @SerializedName("appleId")
        @Expose
        private String appleId;

        @SerializedName("googlepayId")
        @Expose
        private String googlepayId;

        @SerializedName("image")
        @Expose
        private String image;

        @SerializedName("createDate")
        @Expose
        private String createDate;

        @SerializedName("updateDate")
        @Expose
        private String updateDate;

        @SerializedName("conis")
        @Expose
        private String conis;

        public void setId(String id){
            this.id = id;
        }
        public String getId(){
            return this.id;
        }
        public void setPrice(String price){
            this.price = price;
        }
        public String getPrice(){
            return this.price;
        }
        public void setCoins(String coins){
            this.coins = coins;
        }
        public String getCoins(){
            return this.coins;
        }
        public void setAppleId(String appleId){
            this.appleId = appleId;
        }
        public String getAppleId(){
            return this.appleId;
        }
        public void setGooglepayId(String googlepayId){
            this.googlepayId = googlepayId;
        }
        public String getGooglepayId(){
            return this.googlepayId;
        }
        public void setImage(String image){
            this.image = image;
        }
        public String getImage(){
            return this.image;
        }
        public void setCreateDate(String createDate){
            this.createDate = createDate;
        }
        public String getCreateDate(){
            return this.createDate;
        }
        public void setUpdateDate(String updateDate){
            this.updateDate = updateDate;
        }
        public String getUpdateDate(){
            return this.updateDate;
        }
        public void setConis(String conis){
            this.conis = conis;
        }
        public String getConis(){
            return this.conis;
        }
    }
}
