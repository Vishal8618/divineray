package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangePasswordModel {
        @SerializedName("user_id")
        @Expose
        private String user_id;
        @SerializedName("old_password")
        @Expose
        private String old_password;
        @SerializedName("new_password")
        @Expose
        private String new_password;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getOld_password() {
            return old_password;
        }

        public void setOld_password(String old_password) {
            this.old_password = old_password;
        }

        public String getNew_password() {
            return new_password;
        }

        public void setNew_password(String new_password) {
            this.new_password = new_password;
        }

    @Override
    public String toString() {
        return "ChangePasswordModel{" +
                "user_id='" + user_id + '\'' +
                ", old_password='" + old_password + '\'' +
                ", new_password='" + new_password + '\'' +
                '}';
    }
}
