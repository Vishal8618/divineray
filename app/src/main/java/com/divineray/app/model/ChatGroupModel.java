package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatGroupModel {
    @SerializedName("messageId")
    @Expose
    private String messageId;

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("roomId")
    @Expose
    private String roomId;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("messageTime")
    @Expose
    private String messageTime;

    @SerializedName("follow")
    @Expose
    private String follow;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("userProfileImage")
    @Expose
    private String userProfileImage;

    @SerializedName("userProfileId")
    @Expose
    private String userProfileId;


    public void setMessageId(String messageId){
        this.messageId = messageId;
    }
    public String getMessageId(){
        return this.messageId;
    }
    public void setUser_id(String user_id){
        this.user_id = user_id;
    }
    public String getUser_id(){
        return this.user_id;
    }
    public void setRoomId(String roomId){
        this.roomId = roomId;
    }
    public String getRoomId(){
        return this.roomId;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setMessageTime(String messageTime){
        this.messageTime = messageTime;
    }
    public String getMessageTime(){
        return this.messageTime;
    }
    public void setFollow(String follow){
        this.follow = follow;
    }
    public String getFollow(){
        return this.follow;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public String getUsername(){
        return this.username;
    }
    public void setUserProfileImage(String userProfileImage){
        this.userProfileImage = userProfileImage;
    }
    public String getUserProfileImage(){
        return this.userProfileImage;
    }
    public void setUserProfileId(String userProfileId){
        this.userProfileId = userProfileId;
    }
    public String getUserProfileId(){
        return this.userProfileId;
    }
}
