package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChatMessageModel {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("ChatUserDetails")
    @Expose
    private ChatUserDetails ChatUserDetails;

    @SerializedName("data")
    @Expose
    private List<DataChat> data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setChatUserDetails(ChatUserDetails ChatUserDetails){
        this.ChatUserDetails = ChatUserDetails;
    }
    public ChatUserDetails getChatUserDetails(){
        return this.ChatUserDetails;
    }
    public void setData(List<DataChat> data){
        this.data = data;
    }
    public List<DataChat> getData(){
        return this.data;
    }
    public static class DataChat{
        @SerializedName("messageId")
        @Expose
        private String messageId;

        @SerializedName("user_id")
        @Expose
        private String user_id;

        @SerializedName("roomId")
        @Expose
        private String roomId;

        @SerializedName("message")
        @Expose
        private String message;

        @SerializedName("messageTime")
        @Expose
        private String messageTime;

        @SerializedName("follow")
        @Expose
        private String follow;

        @SerializedName("username")
        @Expose
        private String username;

        @SerializedName("userProfileImage")
        @Expose
        private String userProfileImage;

        @SerializedName("userProfileId")
        @Expose
        private String userProfileId;


        public void setMessageId(String messageId){
            this.messageId = messageId;
        }
        public String getMessageId(){
            return this.messageId;
        }
        public void setUser_id(String user_id){
            this.user_id = user_id;
        }
        public String getUser_id(){
            return this.user_id;
        }
        public void setRoomId(String roomId){
            this.roomId = roomId;
        }
        public String getRoomId(){
            return this.roomId;
        }
        public void setMessage(String message){
            this.message = message;
        }
        public String getMessage(){
            return this.message;
        }
        public void setMessageTime(String messageTime){
            this.messageTime = messageTime;
        }
        public String getMessageTime(){
            return this.messageTime;
        }
        public void setFollow(String follow){
            this.follow = follow;
        }
        public String getFollow(){
            return this.follow;
        }
        public void setUsername(String username){
            this.username = username;
        }
        public String getUsername(){
            return this.username;
        }
        public void setUserProfileImage(String userProfileImage){
            this.userProfileImage = userProfileImage;
        }
        public String getUserProfileImage(){
            return this.userProfileImage;
        }
        public void setUserProfileId(String userProfileId){
            this.userProfileId = userProfileId;
        }
        public String getUserProfileId(){
            return this.userProfileId;
        }


        @Override
        public String toString() {
            return "DataChat{" +
                    "messageId='" + messageId + '\'' +
                    ", user_id='" + user_id + '\'' +
                    ", roomId='" + roomId + '\'' +
                    ", message='" + message + '\'' +
                    ", messageTime='" + messageTime + '\'' +
                    ", follow='" + follow + '\'' +
                    ", username='" + username + '\'' +
                    ", userProfileImage='" + userProfileImage + '\'' +
                    ", userProfileId='" + userProfileId + '\'' +
                    '}';
        }
    }
    public  class ChatUserDetails{
        @SerializedName("user_id")
        @Expose
        private String user_id;

        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("email")
        @Expose
        private String email;

        @SerializedName("password")
        @Expose
        private String password;

        @SerializedName("photo")
        @Expose
        private String photo;

        @SerializedName("country")
        @Expose
        private String country;

        @SerializedName("countryCode")
        @Expose
        private String countryCode;

        @SerializedName("twitterId")
        @Expose
        private String twitterId;

        @SerializedName("facebookId")
        @Expose
        private String facebookId;

        @SerializedName("appleId")
        @Expose
        private String appleId;

        @SerializedName("description")
        @Expose
        private String description;

        @SerializedName("verified")
        @Expose
        private String verified;

        @SerializedName("totalFollowing")
        @Expose
        private String totalFollowing;

        @SerializedName("totalFollowers")
        @Expose
        private String totalFollowers;

        @SerializedName("verificateCode")
        @Expose
        private String verificateCode;

        @SerializedName("created_at")
        @Expose
        private String created_at;

        @SerializedName("disabled")
        @Expose
        private String disabled;

        @SerializedName("allowPush")
        @Expose
        private String allowPush;

        @SerializedName("device_type")
        @Expose
        private String device_type;

        @SerializedName("device_token")
        @Expose
        private String device_token;


        public void setUser_id(String user_id){
            this.user_id = user_id;
        }
        public String getUser_id(){
            return this.user_id;
        }
        public void setName(String name){
            this.name = name;
        }
        public String getName(){
            return this.name;
        }
        public void setEmail(String email){
            this.email = email;
        }
        public String getEmail(){
            return this.email;
        }
        public void setPassword(String password){
            this.password = password;
        }
        public String getPassword(){
            return this.password;
        }
        public void setPhoto(String photo){
            this.photo = photo;
        }
        public String getPhoto(){
            return this.photo;
        }
        public void setCountry(String country){
            this.country = country;
        }
        public String getCountry(){
            return this.country;
        }
        public void setCountryCode(String countryCode){
            this.countryCode = countryCode;
        }
        public String getCountryCode(){
            return this.countryCode;
        }
        public void setTwitterId(String twitterId){
            this.twitterId = twitterId;
        }
        public String getTwitterId(){
            return this.twitterId;
        }
        public void setFacebookId(String facebookId){
            this.facebookId = facebookId;
        }
        public String getFacebookId(){
            return this.facebookId;
        }
        public void setAppleId(String appleId){
            this.appleId = appleId;
        }
        public String getAppleId(){
            return this.appleId;
        }
        public void setDescription(String description){
            this.description = description;
        }
        public String getDescription(){
            return this.description;
        }
        public void setVerified(String verified){
            this.verified = verified;
        }
        public String getVerified(){
            return this.verified;
        }
        public void setTotalFollowing(String totalFollowing){
            this.totalFollowing = totalFollowing;
        }
        public String getTotalFollowing(){
            return this.totalFollowing;
        }
        public void setTotalFollowers(String totalFollowers){
            this.totalFollowers = totalFollowers;
        }
        public String getTotalFollowers(){
            return this.totalFollowers;
        }
        public void setVerificateCode(String verificateCode){
            this.verificateCode = verificateCode;
        }
        public String getVerificateCode(){
            return this.verificateCode;
        }
        public void setCreated_at(String created_at){
            this.created_at = created_at;
        }
        public String getCreated_at(){
            return this.created_at;
        }
        public void setDisabled(String disabled){
            this.disabled = disabled;
        }
        public String getDisabled(){
            return this.disabled;
        }
        public void setAllowPush(String allowPush){
            this.allowPush = allowPush;
        }
        public String getAllowPush(){
            return this.allowPush;
        }
        public void setDevice_type(String device_type){
            this.device_type = device_type;
        }
        public String getDevice_type(){
            return this.device_type;
        }
        public void setDevice_token(String device_token){
            this.device_token = device_token;
        }
        public String getDevice_token(){
            return this.device_token;
        }
    }
}
