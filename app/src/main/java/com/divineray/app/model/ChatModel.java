package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChatModel {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<DataC> data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(List<DataC> data){
        this.data = data;
    }
    public List<DataC> getData(){
        return this.data;
    }
    public static class DataC
    {
        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("user_id")
        @Expose
        private String user_id;

        @SerializedName("chatUserId")
        @Expose
        private String chatUserId;

        @SerializedName("roomId")
        @Expose
        private String roomId;

        @SerializedName("roomCreated")
        @Expose
        private String roomCreated;

        @SerializedName("follow")
        @Expose
        private String follow;

        @SerializedName("username")
        @Expose
        private String username;

        @SerializedName("userProfileImage")
        @Expose
        private String userProfileImage;

        @SerializedName("userProfileId")
        @Expose
        private String userProfileId;


        @SerializedName("unReadCount")
        @Expose
        private String  unReadCount;
        @SerializedName("lastMessage")
        @Expose
        private String lastMessage;

        @SerializedName("lastMessageTime")
        @Expose
        private String lastMessageTime;

        public String getUnReadCount() {
            return unReadCount;
        }

        public void setUnReadCount(String unReadCount) {
            this.unReadCount = unReadCount;
        }

        public void setId(String id){
            this.id = id;
        }
        public String getId(){
            return this.id;
        }
        public void setUser_id(String user_id){
            this.user_id = user_id;
        }
        public String getUser_id(){
            return this.user_id;
        }
        public void setChatUserId(String chatUserId){
            this.chatUserId = chatUserId;
        }
        public String getChatUserId(){
            return this.chatUserId;
        }
        public void setRoomId(String roomId){
            this.roomId = roomId;
        }
        public String getRoomId(){
            return this.roomId;
        }
        public void setRoomCreated(String roomCreated){
            this.roomCreated = roomCreated;
        }
        public String getRoomCreated(){
            return this.roomCreated;
        }
        public void setFollow(String follow){
            this.follow = follow;
        }
        public String getFollow(){
            return this.follow;
        }
        public void setUsername(String username){
            this.username = username;
        }
        public String getUsername(){
            return this.username;
        }
        public void setUserProfileImage(String userProfileImage){
            this.userProfileImage = userProfileImage;
        }
        public String getUserProfileImage(){
            return this.userProfileImage;
        }
        public void setUserProfileId(String userProfileId){
            this.userProfileId = userProfileId;
        }
        public String getUserProfileId(){
            return this.userProfileId;
        }
        public void setLastMessage(String lastMessage){
            this.lastMessage = lastMessage;
        }
        public String getLastMessage(){
            return this.lastMessage;
        }
        public void setLastMessageTime(String lastMessageTime){
            this.lastMessageTime = lastMessageTime;
        }
        public String getLastMessageTime(){
            return this.lastMessageTime;
        }
    }
}
