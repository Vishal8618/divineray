package com.divineray.app.model;

//public class CloudRecordStartModel
//{
//    private String resourceId;
//
//    private String sid;
//
//    public void setResourceId(String resourceId){
//        this.resourceId = resourceId;
//    }
//    public String getResourceId(){
//        return this.resourceId;
//    }
//    public void setSid(String sid){
//        this.sid = sid;
//    }
//    public String getSid(){
//        return this.sid;
//    }
//
//    @Override
//    public String toString() {
//        return "CloudRecordStartModel{" +
//                "resourceId='" + resourceId + '\'' +
//                ", sid='" + sid + '\'' +
//                '}';
//    }
//}




public class CloudRecordStartModel
{
    private int status;

    private String message;

    private DataC data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(DataC data){
        this.data = data;
    }
    public DataC getData(){
        return this.data;
    }

    @Override
    public String toString() {
        return "CloudRecordStartModel{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public class DataC
    {
        private String resourceId;

        private String sid;

        public void setResourceId(String resourceId){
            this.resourceId = resourceId;
        }
        public String getResourceId(){
            return this.resourceId;
        }
        public void setSid(String sid){
            this.sid = sid;
        }
        public String getSid(){
            return this.sid;
        }

        @Override
        public String toString() {
            return "DataC{" +
                    "resourceId='" + resourceId + '\'' +
                    ", sid='" + sid + '\'' +
                    '}';
        }
    }
}