package com.divineray.app.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data
{
    @SerializedName("postHeight")
    @Expose
    private String postHeight;

    @SerializedName("postVideo")
    @Expose
    private String postVideo;

    @SerializedName("created")
    @Expose
    private String created;

    @SerializedName("isLiked")
    @Expose
    private String isLiked;

    @SerializedName("totalLike")
    @Expose
    private String totalLike;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("videoId")
    @Expose
    private String videoId;

    @SerializedName("postWidth")
    @Expose
    private String postWidth;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("userDetails")
    @Expose
    private UserDetails userDetails;

    @SerializedName("musicName")
    @Expose
    private String musicName;

    @SerializedName("tags")
    @Expose
    private String tags;

    @SerializedName("postImage")
    @Expose
    private String postImage;

    @SerializedName("totalComments")
    @Expose
    private String totalComments;

    @SerializedName("musicId")
    @Expose
    private String musicId;

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("disable")
    @Expose
    private String disable;

    @SerializedName("totalViews")
    @Expose
    private String totalViews;

    public String getPostHeight ()
    {
        return postHeight;
    }

    public void setPostHeight (String postHeight)
    {
        this.postHeight = postHeight;
    }

    public String getPostVideo ()
    {
        return postVideo;
    }

    public void setPostVideo (String postVideo)
    {
        this.postVideo = postVideo;
    }

    public String getCreated ()
    {
        return created;
    }

    public void setCreated (String created)
    {
        this.created = created;
    }

    public String getIsLiked ()
    {
        return isLiked;
    }

    public void setIsLiked (String isLiked)
    {
        this.isLiked = isLiked;
    }

    public String getTotalLike ()
    {
        return totalLike;
    }

    public void setTotalLike (String totalLike)
    {
        this.totalLike = totalLike;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getVideoId ()
    {
        return videoId;
    }

    public void setVideoId (String videoId)
    {
        this.videoId = videoId;
    }

    public String getPostWidth ()
    {
        return postWidth;
    }

    public void setPostWidth (String postWidth)
    {
        this.postWidth = postWidth;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public UserDetails getUserDetails ()
    {
        return userDetails;
    }

    public void setUserDetails (UserDetails userDetails)
    {
        this.userDetails = userDetails;
    }

    public String getMusicName ()
    {
        return musicName;
    }

    public void setMusicName (String musicName)
    {
        this.musicName = musicName;
    }

    public String getTags ()
    {
        return tags;
    }

    public void setTags (String tags)
    {
        this.tags = tags;
    }

    public String getPostImage ()
    {
        return postImage;
    }

    public void setPostImage (String postImage)
    {
        this.postImage = postImage;
    }

    public String getTotalComments ()
    {
        return totalComments;
    }

    public void setTotalComments (String totalComments)
    {
        this.totalComments = totalComments;
    }

    public String getMusicId ()
    {
        return musicId;
    }

    public void setMusicId (String musicId)
    {
        this.musicId = musicId;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getDisable ()
    {
        return disable;
    }

    public void setDisable (String disable)
    {
        this.disable = disable;
    }

    public String getTotalViews ()
    {
        return totalViews;
    }

    public void setTotalViews (String totalViews)
    {
        this.totalViews = totalViews;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [postHeight = "+postHeight+", postVideo = "+postVideo+", created = "+created+", isLiked = "+isLiked+", totalLike = "+totalLike+", description = "+description+", videoId = "+videoId+", postWidth = "+postWidth+", title = "+title+", userDetails = "+userDetails+", musicName = "+musicName+", tags = "+tags+", postImage = "+postImage+", totalComments = "+totalComments+", musicId = "+musicId+", user_id = "+user_id+", price = "+price+", disable = "+disable+", totalViews = "+totalViews+"]";
    }
}

