package com.divineray.app.model;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("video_link")
	private String videoLink;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("created")
	private String created;

	@SerializedName("video_id")
	private String videoId;

	@SerializedName("name")
	private String name;

	@SerializedName("photo")
	private String photo;

	public void setVideoLink(String videoLink){
		this.videoLink = videoLink;
	}

	public String getVideoLink(){
		return videoLink;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setCreated(String created){
		this.created = created;
	}

	public String getCreated(){
		return created;
	}

	public void setVideoId(String videoId){
		this.videoId = videoId;
	}

	public String getVideoId(){
		return videoId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	@Override
	public String toString() {
		return "DataItem{" +
				"videoLink='" + videoLink + '\'' +
				", userId='" + userId + '\'' +
				", created='" + created + '\'' +
				", videoId='" + videoId + '\'' +
				", name='" + name + '\'' +
				", photo='" + photo + '\'' +
				'}';
	}
}