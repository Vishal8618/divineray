package com.divineray.app.model;
public class EndStream2Model
{
    private int status;

    private String message;

    private String videoStatus;

    private VideoDetailsS videoDetails;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setVideoStatus(String videoStatus){
        this.videoStatus = videoStatus;
    }
    public String getVideoStatus(){
        return this.videoStatus;
    }
    public void setVideoDetails(VideoDetailsS videoDetails){
        this.videoDetails = videoDetails;
    }
    public VideoDetailsS getVideoDetails(){
        return this.videoDetails;
    }

    @Override
    public String toString() {
        return "EndStream2Model{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", videoStatus='" + videoStatus + '\'' +
                ", videoDetails=" + videoDetails +
                '}';
    }

    public class VideoDetailsS
    {
        private String fileListMode;

        private String fileList;

        private String uploadingStatus;

        public void setFileListMode(String fileListMode){
            this.fileListMode = fileListMode;
        }
        public String getFileListMode(){
            return this.fileListMode;
        }
        public void setFileList(String fileList){
            this.fileList = fileList;
        }
        public String getFileList(){
            return this.fileList;
        }
        public void setUploadingStatus(String uploadingStatus){
            this.uploadingStatus = uploadingStatus;
        }
        public String getUploadingStatus(){
            return this.uploadingStatus;
        }

        @Override
        public String toString() {
            return "VideoDetailsS{" +
                    "fileListMode='" + fileListMode + '\'' +
                    ", fileList='" + fileList + '\'' +
                    ", uploadingStatus='" + uploadingStatus + '\'' +
                    '}';
        }
    }

}


