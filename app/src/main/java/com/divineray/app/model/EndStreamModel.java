package com.divineray.app.model;

public class EndStreamModel {
    String message;

    String type;

    int userid;


    public String getMessage() {
        return message;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "EndStreamModel{" +
                "message='" + message + '\'' +
                ", type='" + type + '\'' +
                ", userid=" + userid +
                '}';
    }
}
