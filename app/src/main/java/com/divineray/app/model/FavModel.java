package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FavModel {


    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Datanew data;

    @SerializedName("FavVideos")
    @Expose
    public List<FavVideos> FavVideos;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }

    public Datanew getData() {
        return data;
    }

    public void setData(Datanew data) {
        this.data = data;
    }

    public void setFavVideos(List<FavVideos> FavVideos){
        this.FavVideos = FavVideos;
    }
    public List<FavVideos> getFavVideos(){
        return this.FavVideos;
    }
    public class Datanew
    {
        @SerializedName("user_id")
        @Expose
        private String user_id;

        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("email")
        @Expose
        private String email;

       @SerializedName("password")
       @Expose
        private String password;

       @SerializedName("photo")
       @Expose
        private String photo;

       @SerializedName("country")
       @Expose
       private String country;

        @SerializedName("countryCode")
        @Expose
        private String countryCode;

        @SerializedName("twitterId")
        @Expose
        private String twitterId;

        @SerializedName("facebookId")
        @Expose
        private String facebookId;

        @SerializedName("appleId")
        @Expose
        private String appleId;

        @SerializedName("description")
        @Expose
        private String description;

        @SerializedName("verified")
        @Expose
        private String verified;

        @SerializedName("totaFollowing")
        @Expose
        private String totalFollowing;

        @SerializedName("totalFollowers")
        @Expose
        private String totalFollowers;

        @SerializedName("verificateCode")
        @Expose
        private String verificateCode;

        @SerializedName("created_at")
        @Expose
        private String created_at;

        @SerializedName("disabled")
        @Expose
        private String disabled;

        @SerializedName("allowPush")
        @Expose
        private String allowPush;

        @SerializedName("device_type")
        @Expose
        private String device_type;

        @SerializedName("device_token")
        @Expose
        private String device_token;

        @SerializedName("follow")
        @Expose
        private String follow;
        public void setUser_id(String user_id){
            this.user_id = user_id;
        }
        public String getUser_id(){
            return this.user_id;
        }
        public void setName(String name){
            this.name = name;
        }
        public String getName(){
            return this.name;
        }
        public void setEmail(String email){
            this.email = email;
        }
        public String getEmail(){
            return this.email;
        }
        public void setPassword(String password){
            this.password = password;
        }
        public String getPassword(){
            return this.password;
        }
        public void setPhoto(String photo){
            this.photo = photo;
        }
        public String getPhoto(){
            return this.photo;
        }
        public void setCountry(String country){
            this.country = country;
        }
        public String getCountry(){
            return this.country;
        }
        public void setCountryCode(String countryCode){
            this.countryCode = countryCode;
        }
        public String getCountryCode(){
            return this.countryCode;
        }
        public void setTwitterId(String twitterId){
            this.twitterId = twitterId;
        }
        public String getTwitterId(){
            return this.twitterId;
        }
        public void setFacebookId(String facebookId){
            this.facebookId = facebookId;
        }
        public String getFacebookId(){
            return this.facebookId;
        }
        public void setAppleId(String appleId){
            this.appleId = appleId;
        }
        public String getAppleId(){
            return this.appleId;
        }
        public void setDescription(String description){
            this.description = description;
        }
        public String getDescription(){
            return this.description;
        }
        public void setVerified(String verified){
            this.verified = verified;
        }
        public String getVerified(){
            return this.verified;
        }
        public void setTotalFollowing(String totalFollowing){
            this.totalFollowing = totalFollowing;
        }
        public String getTotalFollowing(){
            return this.totalFollowing;
        }
        public void setTotalFollowers(String totalFollowers){
            this.totalFollowers = totalFollowers;
        }
        public String getTotalFollowers(){
            return this.totalFollowers;
        }
        public void setVerificateCode(String verificateCode){
            this.verificateCode = verificateCode;
        }
        public String getVerificateCode(){
            return this.verificateCode;
        }
        public void setCreated_at(String created_at){
            this.created_at = created_at;
        }
        public String getCreated_at(){
            return this.created_at;
        }
        public void setDisabled(String disabled){
            this.disabled = disabled;
        }
        public String getDisabled(){
            return this.disabled;
        }
        public void setAllowPush(String allowPush){
            this.allowPush = allowPush;
        }
        public String getAllowPush(){
            return this.allowPush;
        }
        public void setDevice_type(String device_type){
            this.device_type = device_type;
        }
        public String getDevice_type(){
            return this.device_type;
        }
        public void setDevice_token(String device_token){
            this.device_token = device_token;
        }
        public String getDevice_token(){
            return this.device_token;
        }
        public void setFollow(String follow){
            this.follow = follow;
        }
        public String getFollow(){
            return this.follow;
        }

    }

    public class FavVideos
    {   @SerializedName("videoId")
        @Expose
        private String videoId;

        @SerializedName("user_id")
        @Expose
        private String user_id;

        @SerializedName("musicId")
        @Expose
        private String musicId;

        @SerializedName("musicName")
        @Expose
        private String musicName;

        @SerializedName("postImage")
        @Expose
        private String postImage;

        @SerializedName("postVideo")
        @Expose
        private String postVideo;

        @SerializedName("title")
        @Expose
        private String title;

        @SerializedName("description")
        @Expose
        private String description;

        @SerializedName("price")
        @Expose
        private String price;

        @SerializedName("postHeight")
        @Expose
        private String postHeight;

        @SerializedName("postWidth")
        @Expose
        private String postWidth;

        @SerializedName("created")
        @Expose
        private String created;

        @SerializedName("disable")
        @Expose
        private String disable;

        @SerializedName("tags")
        @Expose
        private String tags;

        @SerializedName("userDetails")
        @Expose
        private UserDetails userDetails;

        @SerializedName("isLiked")
        @Expose
        private String isLiked;

        @SerializedName("totalLike")
        @Expose
        private String totalLike;

        @SerializedName("totalViews")
        @Expose
        private String totalViews;

        @SerializedName("totalComments")
        @Expose
        private String totalComments;

        @SerializedName("shareUrl")
        @Expose
        private String shareUrl;

        @SerializedName("likedTime")
        @Expose
        private String likedTime;


        @SerializedName("branchShareUrl")
        @Expose
        private String branchShareUrl;


        @Override
        public String toString() {
            return "FavVideos{" +
                    "postHeight='" + postHeight + '\'' +
                    ", postVideo='" + postVideo + '\'' +
                    ", created='" + created + '\'' +
                    ", isLiked='" + isLiked + '\'' +
                    ", totalLike='" + totalLike + '\'' +
                    ", description='" + description + '\'' +
                    ", videoId='" + videoId + '\'' +
                    ", postWidth='" + postWidth + '\'' +
                    ", title='" + title + '\'' +
                    ", userDetails=" + userDetails +
                    ", musicName='" + musicName + '\'' +
                    ", tags='" + tags + '\'' +
                    ", postImage='" + postImage + '\'' +
                    ", totalComments='" + totalComments + '\'' +
                    ", musicId='" + musicId + '\'' +
                    ", user_id='" + user_id + '\'' +
                    ", price='" + price + '\'' +
                    ", disable='" + disable + '\'' +
                    ", totalViews='" + totalViews + '\'' +
                    ", shareUrl='" + shareUrl + '\'' +
                    ", likedTime='" + likedTime + '\'' +
                    '}';
        }

        public void setVideoId(String videoId){
            this.videoId = videoId;
        }
        public String getVideoId(){
            return this.videoId;
        }
        public void setUser_id(String user_id){
            this.user_id = user_id;
        }
        public String getUser_id(){
            return this.user_id;
        }
        public void setMusicId(String musicId){
            this.musicId = musicId;
        }
        public String getMusicId(){
            return this.musicId;
        }
        public void setMusicName(String musicName){
            this.musicName = musicName;
        }
        public String getMusicName(){
            return this.musicName;
        }
        public void setPostImage(String postImage){
            this.postImage = postImage;
        }
        public String getPostImage(){
            return this.postImage;
        }
        public void setPostVideo(String postVideo){
            this.postVideo = postVideo;
        }
        public String getPostVideo(){
            return this.postVideo;
        }
        public void setTitle(String title){
            this.title = title;
        }
        public String getTitle(){
            return this.title;
        }
        public void setDescription(String description){
            this.description = description;
        }
        public String getDescription(){
            return this.description;
        }
        public void setPrice(String price){
            this.price = price;
        }
        public String getPrice(){
            return this.price;
        }
        public void setPostHeight(String postHeight){
            this.postHeight = postHeight;
        }
        public String getPostHeight(){
            return this.postHeight;
        }
        public void setPostWidth(String postWidth){
            this.postWidth = postWidth;
        }
        public String getPostWidth(){
            return this.postWidth;
        }
        public void setCreated(String created){
            this.created = created;
        }
        public String getCreated(){
            return this.created;
        }
        public void setDisable(String disable){
            this.disable = disable;
        }
        public String getDisable(){
            return this.disable;
        }
        public void setTags(String tags){
            this.tags = tags;
        }
        public String getTags(){
            return this.tags;
        }
        public void setUserDetails(UserDetails userDetails){
            this.userDetails = userDetails;
        }
        public UserDetails getUserDetails(){
            return this.userDetails;
        }
        public void setIsLiked(String isLiked){
            this.isLiked = isLiked;
        }
        public String getIsLiked(){
            return this.isLiked;
        }
        public void setTotalLike(String totalLike){
            this.totalLike = totalLike;
        }
        public String getTotalLike(){
            return this.totalLike;
        }
        public void setTotalViews(String totalViews){
            this.totalViews = totalViews;
        }
        public String getTotalViews(){
            return this.totalViews;
        }
        public void setTotalComments(String totalComments){
            this.totalComments = totalComments;
        }
        public String getTotalComments(){
            return this.totalComments;
        }
        public void setShareUrl(String shareUrl){
            this.shareUrl = shareUrl;
        }
        public String getShareUrl(){
            return this.shareUrl;
        }
        public void setLikedTime(String likedTime){
            this.likedTime = likedTime;
        }
        public String getLikedTime(){
            return this.likedTime;
        }

        public String getBranchShareUrl() {
            return branchShareUrl;
        }

        public void setBranchShareUrl(String branchShareUrl) {
            this.branchShareUrl = branchShareUrl;
        }
    }

}
