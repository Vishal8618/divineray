package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAllGiftModel {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<Data> data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(List<Data> data){
        this.data = data;
    }
    public List<Data> getData(){
        return this.data;
    }
    public class Data
    {
        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("title")
        @Expose
        private String title;

        @SerializedName("description")
        @Expose
        private String description;

        @SerializedName("coins")
        @Expose
        private String coins;

        @SerializedName("image")
        @Expose
        private String image;

        @SerializedName("createDate")
        @Expose
        private String createDate;

        @SerializedName("updateDate")
        @Expose
        private String updateDate;

        public void setId(String id){
            this.id = id;
        }
        public String getId(){
            return this.id;
        }
        public void setTitle(String title){
            this.title = title;
        }
        public String getTitle(){
            return this.title;
        }
        public void setDescription(String description){
            this.description = description;
        }
        public String getDescription(){
            return this.description;
        }
        public void setCoins(String coins){
            this.coins = coins;
        }
        public String getCoins(){
            return this.coins;
        }
        public void setImage(String image){
            this.image = image;
        }
        public String getImage(){
            return this.image;
        }
        public void setCreateDate(String createDate){
            this.createDate = createDate;
        }
        public String getCreateDate(){
            return this.createDate;
        }
        public void setUpdateDate(String updateDate){
            this.updateDate = updateDate;
        }
        public String getUpdateDate(){
            return this.updateDate;
        }
    }
}
