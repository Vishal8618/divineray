package com.divineray.app.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetCommentsModel {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("totalComments")
    @Expose
    private String totalComments;

    @SerializedName("data")
    @Expose
    private List<Dataw> data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setTotalComments(String totalComments){
        this.totalComments = totalComments;
    }
    public String getTotalComments(){
        return this.totalComments;
    }
    public void setData(List<Dataw> data){
        this.data = data;
    }
    public List<Dataw> getData(){
        return this.data;
    }

    public class Dataw {
        @SerializedName("commentId")
        @Expose
        private String commentId;

        @SerializedName("user_id")
        @Expose
        private String user_id;


        @SerializedName("videoId")
        @Expose
        private String videoId;


        @SerializedName("comment")
        @Expose
        private String comment;


        @SerializedName("commentTime")
        @Expose
        private String commentTime;


        @SerializedName("disabled")
        @Expose
        private String disabled;


        @SerializedName("comment_by")
        @Expose
        private String comment_by;


        @SerializedName("image_user")
        @Expose
        private String image_user;

        public void setCommentId(String commentId){
            this.commentId = commentId;
        }
        public String getCommentId(){
            return this.commentId;
        }
        public void setUser_id(String user_id){
            this.user_id = user_id;
        }
        public String getUser_id(){
            return this.user_id;
        }
        public void setVideoId(String videoId){
            this.videoId = videoId;
        }
        public String getVideoId(){
            return this.videoId;
        }
        public void setComment(String comment){
            this.comment = comment;
        }
        public String getComment(){
            return this.comment;
        }
        public void setCommentTime(String commentTime){
            this.commentTime = commentTime;
        }
        public String getCommentTime(){
            return this.commentTime;
        }
        public void setDisabled(String disabled){
            this.disabled = disabled;
        }
        public String getDisabled(){
            return this.disabled;
        }
        public void setComment_by(String comment_by){
            this.comment_by = comment_by;
        }
        public String getComment_by(){
            return this.comment_by;
        }
        public void setImage_user(String image_user){
            this.image_user = image_user;
        }
        public String getImage_user(){
            return this.image_user;
        }



    }

}
