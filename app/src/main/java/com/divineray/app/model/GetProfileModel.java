package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetProfileModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private LoginData data;
    @SerializedName("UserVideos")
    @Expose
    private List<UserVideos> userVideos=null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public LoginData getData() {
        return data;
    }

    public List<UserVideos> getUserVideos() {
        return userVideos;
    }

    public void setUserVideos(List<UserVideos> userVideos) {
        this.userVideos = userVideos;
    }

    public void setData(LoginData data) {
        this.data = data;
    }
    @Override
    public String toString() {
        return "ProfileModelStatus{" +
                "message='" + message + '\'' +
                ", status='" + status + '\'' +
                ", data=" + data +
                ", UserVideos=" + userVideos +
                '}';
    }
    public class UserVideos {

        @SerializedName("videoId")
        @Expose
        private String videoId;
        @SerializedName("user_id")
        @Expose
        private String user_id;
        @SerializedName("postImage")
        @Expose
        private String postImage;
        @SerializedName("postVideo")
        @Expose
        private String postVideo;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("disable")
        @Expose
        private String disable;
        @SerializedName("tags")
        @Expose
        private String tags;
        @SerializedName("userDetails")
        @Expose
        private LoginData data;
        @SerializedName("isLiked")
        @Expose
        private String isLiked;
        @SerializedName("totalLike")
        @Expose
        private String totalLike;
        @SerializedName("totalViews")
        @Expose
        private String totalViews;
        @SerializedName("totalComments")
        @Expose
        private String totalComments;



        public UserVideos() {
        }

        public String getVideoId() {
            return videoId;
        }

        public void setVideoId(String videoId) {
            this.videoId = videoId;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getPostImage() {
            return postImage;
        }

        public void setPostImage(String postImage) {
            this.postImage = postImage;
        }

        public String getPostVideo() {
            return postVideo;
        }

        public void setPostVideo(String postVideo) {
            this.postVideo = postVideo;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

        public String getTags() {
            return tags;
        }

        public void setTags(String tags) {
            this.tags = tags;
        }

        public LoginData getData() {
            return data;
        }

        public void setData(LoginData data) {
            this.data = data;
        }

        public String getIsLiked() {
            return isLiked;
        }

        public void setIsLiked(String isLiked) {
            this.isLiked = isLiked;
        }

        public String getTotalLike() {
            return totalLike;
        }

        public void setTotalLike(String totalLike) {
            this.totalLike = totalLike;
        }

        public String getTotalViews() {
            return totalViews;
        }

        public void setTotalViews(String totalViews) {
            this.totalViews = totalViews;
        }

        public String getTotalComments() {
            return totalComments;
        }

        public void setTotalComments(String totalComments) {
            this.totalComments = totalComments;
        }

    }


}