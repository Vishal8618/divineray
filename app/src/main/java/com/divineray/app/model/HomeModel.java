package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeModel
{
    @SerializedName("status")
    @Expose
    public Integer status;

    @SerializedName("message")
    @Expose
    public String message;

//    @SerializedName("promotionVideo")
//    @Expose
//    public String promotionVideo;


    @SerializedName("data")
    @Expose
    public List<Data> data;


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
//
//    public String getPromotionVideo() {
//        return promotionVideo;
//    }
//
//    public void setPromotionVideo(String promotionVideo) {
//        this.promotionVideo = promotionVideo;
//    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+", message = "+message+", status = "+status+"]";

    }
    public class Data  {

        @SerializedName("videoId")
        @Expose
        private String videoId;

        @SerializedName("user_id")
        @Expose
        private String user_id;

        @SerializedName("musicId")
        @Expose
        private String musicId;

        @SerializedName("musicName")
        @Expose
        private String musicName;

        @SerializedName("postImage")
        @Expose
        private String postImage;

        @SerializedName("postVideo")
        @Expose
        private String postVideo;

        @SerializedName("title")
        @Expose
        private String title;

        @SerializedName("description")
        @Expose
        private String description;

        @SerializedName("price")
        @Expose
        private String price;

        @SerializedName("postHeight")
        @Expose
        private String postHeight;

        @SerializedName("postWidth")
        @Expose
        private String postWidth;

        @SerializedName("created")
        @Expose
        private String created;

        @SerializedName("disable")
        @Expose
        private String disable;

        @SerializedName("tags")
        @Expose
        private String tags;

        @SerializedName("userDetails")
        @Expose
        private UserDetails userDetails;

        @SerializedName("isLiked")
        @Expose
        private String isLiked;

        @SerializedName("totalLike")
        @Expose
        private String totalLike;

        @SerializedName("totalViews")
        @Expose
        private String totalViews;

        @SerializedName("totalComments")
        @Expose
        private String totalComments;

        @SerializedName("shareUrl")
        @Expose
        private String shareUrl;

        @SerializedName("branchShareUrl")
        @Expose
        private String branchShareUrl;




        public String getShareUrl() {
            return shareUrl;
        }

        public void setShareUrl(String shareUrl) {
            this.shareUrl = shareUrl;
        }

        public String getPostHeight ()
        {
            return postHeight;
        }

        public void setPostHeight (String postHeight)
        {
            this.postHeight = postHeight;
        }

        public String getPostVideo ()
        {
            return postVideo;
        }

        public void setPostVideo (String postVideo)
        {
            this.postVideo = postVideo;
        }

        public String getCreated ()
        {
            return created;
        }

        public void setCreated (String created)
        {
            this.created = created;
        }

        public String getIsLiked ()
        {
            return isLiked;
        }

        public void setIsLiked (String isLiked)
        {
            this.isLiked = isLiked;
        }

        public String getTotalLike ()
        {
            return totalLike;
        }

        public void setTotalLike (String totalLike)
        {
            this.totalLike = totalLike;
        }

        public String getDescription ()
        {
            return description;
        }

        public void setDescription (String description)
        {
            this.description = description;
        }

        public String getVideoId ()
        {
            return videoId;
        }

        public void setVideoId (String videoId)
        {
            this.videoId = videoId;
        }

        public String getPostWidth ()
        {
            return postWidth;
        }

        public void setPostWidth (String postWidth)
        {
            this.postWidth = postWidth;
        }

        public String getTitle ()
        {
            return title;
        }

        public void setTitle (String title)
        {
            this.title = title;
        }

        public UserDetails getUserDetails ()
        {
            return userDetails;
        }

        public void setUserDetails (UserDetails userDetails)
        {
            this.userDetails = userDetails;
        }

        public String getMusicName ()
        {
            return musicName;
        }

        public void setMusicName (String musicName)
        {
            this.musicName = musicName;
        }

        public String getTags ()
        {
            return tags;
        }

        public void setTags (String tags)
        {
            this.tags = tags;
        }

        public String getPostImage ()
        {
            return postImage;
        }

        public void setPostImage (String postImage)
        {
            this.postImage = postImage;
        }

        public String getTotalComments ()
        {
            return totalComments;
        }

        public void setTotalComments (String totalComments)
        {
            this.totalComments = totalComments;
        }

        public String getMusicId ()
        {
            return musicId;
        }

        public void setMusicId (String musicId)
        {
            this.musicId = musicId;
        }

        public String getUser_id ()
        {
            return user_id;
        }

        public void setUser_id (String user_id)
        {
            this.user_id = user_id;
        }

        public String getPrice ()
        {
            return price;
        }

        public void setPrice (String price)
        {
            this.price = price;
        }

        public String getDisable ()
        {
            return disable;
        }

        public void setDisable (String disable)
        {
            this.disable = disable;
        }

        public String getTotalViews ()
        {
            return totalViews;
        }

        public void setTotalViews (String totalViews)
        {
            this.totalViews = totalViews;
        }

        public String getBranchShareUrl() {
            return branchShareUrl;
        }

        public void setBranchShareUrl(String branchShareUrl) {
            this.branchShareUrl = branchShareUrl;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [postHeight = "+postHeight+", postVideo = "+postVideo+", created = "+created+", isLiked = "+isLiked+", totalLike = "+totalLike+", description = "+description+", videoId = "+videoId+", postWidth = "+postWidth+", title = "+title+", userDetails = "+userDetails+", musicName = "+musicName+", tags = "+tags+", postImage = "+postImage+", totalComments = "+totalComments+", musicId = "+musicId+", user_id = "+user_id+", price = "+price+", disable = "+disable+", totalViews = "+totalViews+"]";
        }
    }
}

