package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogoutModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("logout_id")
    @Expose
    private String logoutId;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLogoutId() {
        return logoutId;
    }

    public void setLogoutId(String logoutId) {
        this.logoutId = logoutId;
    }

    @Override
    public String toString() {
        return "LogoutModel{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", logoutId='" + logoutId + '\'' +
                '}';
    }
}