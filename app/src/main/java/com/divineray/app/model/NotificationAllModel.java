package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
public class NotificationAllModel {
    private int status;

    private String message;

    private List<Data0> data;

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public List<Data0> getData() {
        return data;
    }

    public void setData(List<Data0> data) {
        this.data = data;
    }

    public class Data0
    {
        private String notificationId;

        private String detailId;

        private String user_id;

        private String notifyBy;

        private String notificationType;

        private String typeId;

        private String message;

        private String notificationTime;

        private String seen;

        private UserDetailsq1 userDetails;

        private String name;

        private String photo;

        private Video_detailsq1 video_details;

        @Override
        public String toString() {
            return "Data0{" +
                    "notificationId='" + notificationId + '\'' +
                    ", detailId='" + detailId + '\'' +
                    ", user_id='" + user_id + '\'' +
                    ", notifyBy='" + notifyBy + '\'' +
                    ", notificationType='" + notificationType + '\'' +
                    ", typeId='" + typeId + '\'' +
                    ", message='" + message + '\'' +
                    ", notificationTime='" + notificationTime + '\'' +
                    ", seen='" + seen + '\'' +
                    ", userDetails=" + userDetails +
                    ", name='" + name + '\'' +
                    ", photo='" + photo + '\'' +
                    ", video_details=" + video_details +
                    '}';
        }

        public void setNotificationId(String notificationId){
            this.notificationId = notificationId;
        }
        public String getNotificationId(){
            return this.notificationId;
        }
        public void setDetailId(String detailId){
            this.detailId = detailId;
        }
        public String getDetailId(){
            return this.detailId;
        }
        public void setUser_id(String user_id){
            this.user_id = user_id;
        }
        public String getUser_id(){
            return this.user_id;
        }
        public void setNotifyBy(String notifyBy){
            this.notifyBy = notifyBy;
        }
        public String getNotifyBy(){
            return this.notifyBy;
        }
        public void setNotificationType(String notificationType){
            this.notificationType = notificationType;
        }
        public String getNotificationType(){
            return this.notificationType;
        }
        public void setTypeId(String typeId){
            this.typeId = typeId;
        }
        public String getTypeId(){
            return this.typeId;
        }
        public void setMessage(String message){
            this.message = message;
        }
        public String getMessage(){
            return this.message;
        }
        public void setNotificationTime(String notificationTime){
            this.notificationTime = notificationTime;
        }
        public String getNotificationTime(){
            return this.notificationTime;
        }
        public void setSeen(String seen){
            this.seen = seen;
        }
        public String getSeen(){
            return this.seen;
        }

        public void setName(String name){
            this.name = name;
        }
        public String getName(){
            return this.name;
        }
        public void setPhoto(String photo){
            this.photo = photo;
        }
        public String getPhoto(){
            return this.photo;
        }

        public UserDetailsq1 getUserDetails() {
            return userDetails;
        }

        public void setUserDetails(UserDetailsq1 userDetails) {
            this.userDetails = userDetails;
        }

        public Video_detailsq1 getVideo_details() {
            return video_details;
        }

        public void setVideo_details(Video_detailsq1 video_details) {
            this.video_details = video_details;
        }

        public class Video_detailsq1
        {
            private String videoId;

            private String user_id;

            private String musicId;

            private String musicName;

            private String postImage;

            private String postVideo;

            private String title;

            private String description;

            private String price;

            private String postHeight;

            private String postWidth;

            private String created;

            private String disable;

            private String tags;

            private UserDetailsq0 userDetails;

            private String isLiked;

            private String totalLike;

            private String totalViews;

            private String totalComments;

            private String shareUrl;

            public void setVideoId(String videoId){
                this.videoId = videoId;
            }
            public String getVideoId(){
                return this.videoId;
            }
            public void setUser_id(String user_id){
                this.user_id = user_id;
            }
            public String getUser_id(){
                return this.user_id;
            }
            public void setMusicId(String musicId){
                this.musicId = musicId;
            }
            public String getMusicId(){
                return this.musicId;
            }
            public void setMusicName(String musicName){
                this.musicName = musicName;
            }
            public String getMusicName(){
                return this.musicName;
            }
            public void setPostImage(String postImage){
                this.postImage = postImage;
            }
            public String getPostImage(){
                return this.postImage;
            }
            public void setPostVideo(String postVideo){
                this.postVideo = postVideo;
            }
            public String getPostVideo(){
                return this.postVideo;
            }
            public void setTitle(String title){
                this.title = title;
            }
            public String getTitle(){
                return this.title;
            }
            public void setDescription(String description){
                this.description = description;
            }
            public String getDescription(){
                return this.description;
            }
            public void setPrice(String price){
                this.price = price;
            }
            public String getPrice(){
                return this.price;
            }
            public void setPostHeight(String postHeight){
                this.postHeight = postHeight;
            }
            public String getPostHeight(){
                return this.postHeight;
            }
            public void setPostWidth(String postWidth){
                this.postWidth = postWidth;
            }
            public String getPostWidth(){
                return this.postWidth;
            }
            public void setCreated(String created){
                this.created = created;
            }
            public String getCreated(){
                return this.created;
            }
            public void setDisable(String disable){
                this.disable = disable;
            }
            public String getDisable(){
                return this.disable;
            }
            public void setTags(String tags){
                this.tags = tags;
            }
            public String getTags(){
                return this.tags;
            }

            public UserDetailsq0 getUserDetails() {
                return userDetails;
            }

            public void setUserDetails(UserDetailsq0 userDetails) {
                this.userDetails = userDetails;
            }

            public void setIsLiked(String isLiked){
                this.isLiked = isLiked;
            }
            public String getIsLiked(){
                return this.isLiked;
            }
            public void setTotalLike(String totalLike){
                this.totalLike = totalLike;
            }
            public String getTotalLike(){
                return this.totalLike;
            }
            public void setTotalViews(String totalViews){
                this.totalViews = totalViews;
            }
            public String getTotalViews(){
                return this.totalViews;
            }
            public void setTotalComments(String totalComments){
                this.totalComments = totalComments;
            }
            public String getTotalComments(){
                return this.totalComments;
            }
            public void setShareUrl(String shareUrl){
                this.shareUrl = shareUrl;
            }
            public String getShareUrl(){
                return this.shareUrl;
            }


            }

            public class UserDetailsq0
            {
                private String user_id;

                private String paypal_token;

                private String paypal_verification;

                private String paypal_id;

                private String name;

                private String email;

                private String password;

                private String photo;

                private String country;

                private String countryCode;

                private String twitterId;

                private String facebookId;

                private String appleId;

                private String description;

                private String verified;

                private String totalFollowing;

                private String totalFollowers;

                private String verificateCode;

                private String created_at;

                private String disabled;

                private String allowPush;

                private String device_type;

                private String total_coins;

                private String device_token;

                private String follow;

                public void setUser_id(String user_id){
                    this.user_id = user_id;
                }
                public String getUser_id(){
                    return this.user_id;
                }
                public void setPaypal_token(String paypal_token){
                    this.paypal_token = paypal_token;
                }
                public String getPaypal_token(){
                    return this.paypal_token;
                }
                public void setPaypal_verification(String paypal_verification){
                    this.paypal_verification = paypal_verification;
                }
                public String getPaypal_verification(){
                    return this.paypal_verification;
                }
                public void setPaypal_id(String paypal_id){
                    this.paypal_id = paypal_id;
                }
                public String getPaypal_id(){
                    return this.paypal_id;
                }
                public void setName(String name){
                    this.name = name;
                }
                public String getName(){
                    return this.name;
                }
                public void setEmail(String email){
                    this.email = email;
                }
                public String getEmail(){
                    return this.email;
                }
                public void setPassword(String password){
                    this.password = password;
                }
                public String getPassword(){
                    return this.password;
                }
                public void setPhoto(String photo){
                    this.photo = photo;
                }
                public String getPhoto(){
                    return this.photo;
                }
                public void setCountry(String country){
                    this.country = country;
                }
                public String getCountry(){
                    return this.country;
                }
                public void setCountryCode(String countryCode){
                    this.countryCode = countryCode;
                }
                public String getCountryCode(){
                    return this.countryCode;
                }
                public void setTwitterId(String twitterId){
                    this.twitterId = twitterId;
                }
                public String getTwitterId(){
                    return this.twitterId;
                }
                public void setFacebookId(String facebookId){
                    this.facebookId = facebookId;
                }
                public String getFacebookId(){
                    return this.facebookId;
                }
                public void setAppleId(String appleId){
                    this.appleId = appleId;
                }
                public String getAppleId(){
                    return this.appleId;
                }
                public void setDescription(String description){
                    this.description = description;
                }
                public String getDescription(){
                    return this.description;
                }
                public void setVerified(String verified){
                    this.verified = verified;
                }
                public String getVerified(){
                    return this.verified;
                }
                public void setTotalFollowing(String totalFollowing){
                    this.totalFollowing = totalFollowing;
                }
                public String getTotalFollowing(){
                    return this.totalFollowing;
                }
                public void setTotalFollowers(String totalFollowers){
                    this.totalFollowers = totalFollowers;
                }
                public String getTotalFollowers(){
                    return this.totalFollowers;
                }
                public void setVerificateCode(String verificateCode){
                    this.verificateCode = verificateCode;
                }
                public String getVerificateCode(){
                    return this.verificateCode;
                }
                public void setCreated_at(String created_at){
                    this.created_at = created_at;
                }
                public String getCreated_at(){
                    return this.created_at;
                }
                public void setDisabled(String disabled){
                    this.disabled = disabled;
                }
                public String getDisabled(){
                    return this.disabled;
                }
                public void setAllowPush(String allowPush){
                    this.allowPush = allowPush;
                }
                public String getAllowPush(){
                    return this.allowPush;
                }
                public void setDevice_type(String device_type){
                    this.device_type = device_type;
                }
                public String getDevice_type(){
                    return this.device_type;
                }
                public void setTotal_coins(String total_coins){
                    this.total_coins = total_coins;
                }
                public String getTotal_coins(){
                    return this.total_coins;
                }
                public void setDevice_token(String device_token){
                    this.device_token = device_token;
                }
                public String getDevice_token(){
                    return this.device_token;
                }
                public void setFollow(String follow){
                    this.follow = follow;
                }
                public String getFollow(){
                    return this.follow;
                }
            }
        }

        public class UserDetailsq1
        {
            private String user_id;

            private String paypal_token;

            private String paypal_verification;

            private String paypal_id;

            private String name;

            private String email;

            private String password;

            private String photo;

            private String country;

            private String countryCode;

            private String twitterId;

            private String facebookId;

            private String appleId;

            private String description;

            private String verified;

            private String totalFollowing;

            private String totalFollowers;

            private String verificateCode;

            private String created_at;

            private String disabled;

            private String allowPush;

            private String device_type;

            private String total_coins;

            private String device_token;

            public void setUser_id(String user_id){
                this.user_id = user_id;
            }
            public String getUser_id(){
                return this.user_id;
            }
            public void setPaypal_token(String paypal_token){
                this.paypal_token = paypal_token;
            }
            public String getPaypal_token(){
                return this.paypal_token;
            }
            public void setPaypal_verification(String paypal_verification){
                this.paypal_verification = paypal_verification;
            }
            public String getPaypal_verification(){
                return this.paypal_verification;
            }
            public void setPaypal_id(String paypal_id){
                this.paypal_id = paypal_id;
            }
            public String getPaypal_id(){
                return this.paypal_id;
            }
            public void setName(String name){
                this.name = name;
            }
            public String getName(){
                return this.name;
            }
            public void setEmail(String email){
                this.email = email;
            }
            public String getEmail(){
                return this.email;
            }
            public void setPassword(String password){
                this.password = password;
            }
            public String getPassword(){
                return this.password;
            }
            public void setPhoto(String photo){
                this.photo = photo;
            }
            public String getPhoto(){
                return this.photo;
            }
            public void setCountry(String country){
                this.country = country;
            }
            public String getCountry(){
                return this.country;
            }
            public void setCountryCode(String countryCode){
                this.countryCode = countryCode;
            }
            public String getCountryCode(){
                return this.countryCode;
            }
            public void setTwitterId(String twitterId){
                this.twitterId = twitterId;
            }
            public String getTwitterId(){
                return this.twitterId;
            }
            public void setFacebookId(String facebookId){
                this.facebookId = facebookId;
            }
            public String getFacebookId(){
                return this.facebookId;
            }
            public void setAppleId(String appleId){
                this.appleId = appleId;
            }
            public String getAppleId(){
                return this.appleId;
            }
            public void setDescription(String description){
                this.description = description;
            }
            public String getDescription(){
                return this.description;
            }
            public void setVerified(String verified){
                this.verified = verified;
            }
            public String getVerified(){
                return this.verified;
            }
            public void setTotalFollowing(String totalFollowing){
                this.totalFollowing = totalFollowing;
            }
            public String getTotalFollowing(){
                return this.totalFollowing;
            }
            public void setTotalFollowers(String totalFollowers){
                this.totalFollowers = totalFollowers;
            }
            public String getTotalFollowers(){
                return this.totalFollowers;
            }
            public void setVerificateCode(String verificateCode){
                this.verificateCode = verificateCode;
            }
            public String getVerificateCode(){
                return this.verificateCode;
            }
            public void setCreated_at(String created_at){
                this.created_at = created_at;
            }
            public String getCreated_at(){
                return this.created_at;
            }
            public void setDisabled(String disabled){
                this.disabled = disabled;
            }
            public String getDisabled(){
                return this.disabled;
            }
            public void setAllowPush(String allowPush){
                this.allowPush = allowPush;
            }
            public String getAllowPush(){
                return this.allowPush;
            }
            public void setDevice_type(String device_type){
                this.device_type = device_type;
            }
            public String getDevice_type(){
                return this.device_type;
            }
            public void setTotal_coins(String total_coins){
                this.total_coins = total_coins;
            }
            public String getTotal_coins(){
                return this.total_coins;
            }
            public void setDevice_token(String device_token){
                this.device_token = device_token;
            }
            public String getDevice_token(){
                return this.device_token;
            }
        }


    }