package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NottificationModel {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Data data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(Data data){
        this.data = data;
    }
    public Data getData(){
        return this.data;
    }
    public class Data
    {
        private String user_id;

        private String paypal_token;

        private String paypal_verification;

        private String paypal_id;

        private String name;

        private String email;

        private String password;

        private String photo;

        private String country;

        private String countryCode;

        private String twitterId;

        private String facebookId;

        private String appleId;

        private String description;

        private String verified;

        private String totalFollowing;

        private String totalFollowers;

        private String verificateCode;

        private String created_at;

        private String disabled;

        private String allowPush;

        private String device_type;

        private String total_coins;

        private String device_token;

        public void setUser_id(String user_id){
            this.user_id = user_id;
        }
        public String getUser_id(){
            return this.user_id;
        }
        public void setPaypal_token(String paypal_token){
            this.paypal_token = paypal_token;
        }
        public String getPaypal_token(){
            return this.paypal_token;
        }
        public void setPaypal_verification(String paypal_verification){
            this.paypal_verification = paypal_verification;
        }
        public String getPaypal_verification(){
            return this.paypal_verification;
        }
        public void setPaypal_id(String paypal_id){
            this.paypal_id = paypal_id;
        }
        public String getPaypal_id(){
            return this.paypal_id;
        }
        public void setName(String name){
            this.name = name;
        }
        public String getName(){
            return this.name;
        }
        public void setEmail(String email){
            this.email = email;
        }
        public String getEmail(){
            return this.email;
        }
        public void setPassword(String password){
            this.password = password;
        }
        public String getPassword(){
            return this.password;
        }
        public void setPhoto(String photo){
            this.photo = photo;
        }
        public String getPhoto(){
            return this.photo;
        }
        public void setCountry(String country){
            this.country = country;
        }
        public String getCountry(){
            return this.country;
        }
        public void setCountryCode(String countryCode){
            this.countryCode = countryCode;
        }
        public String getCountryCode(){
            return this.countryCode;
        }
        public void setTwitterId(String twitterId){
            this.twitterId = twitterId;
        }
        public String getTwitterId(){
            return this.twitterId;
        }
        public void setFacebookId(String facebookId){
            this.facebookId = facebookId;
        }
        public String getFacebookId(){
            return this.facebookId;
        }
        public void setAppleId(String appleId){
            this.appleId = appleId;
        }
        public String getAppleId(){
            return this.appleId;
        }
        public void setDescription(String description){
            this.description = description;
        }
        public String getDescription(){
            return this.description;
        }
        public void setVerified(String verified){
            this.verified = verified;
        }
        public String getVerified(){
            return this.verified;
        }
        public void setTotalFollowing(String totalFollowing){
            this.totalFollowing = totalFollowing;
        }
        public String getTotalFollowing(){
            return this.totalFollowing;
        }
        public void setTotalFollowers(String totalFollowers){
            this.totalFollowers = totalFollowers;
        }
        public String getTotalFollowers(){
            return this.totalFollowers;
        }
        public void setVerificateCode(String verificateCode){
            this.verificateCode = verificateCode;
        }
        public String getVerificateCode(){
            return this.verificateCode;
        }
        public void setCreated_at(String created_at){
            this.created_at = created_at;
        }
        public String getCreated_at(){
            return this.created_at;
        }
        public void setDisabled(String disabled){
            this.disabled = disabled;
        }
        public String getDisabled(){
            return this.disabled;
        }
        public void setAllowPush(String allowPush){
            this.allowPush = allowPush;
        }
        public String getAllowPush(){
            return this.allowPush;
        }
        public void setDevice_type(String device_type){
            this.device_type = device_type;
        }
        public String getDevice_type(){
            return this.device_type;
        }
        public void setTotal_coins(String total_coins){
            this.total_coins = total_coins;
        }
        public String getTotal_coins(){
            return this.total_coins;
        }
        public void setDevice_token(String device_token){
            this.device_token = device_token;
        }
        public String getDevice_token(){
            return this.device_token;
        }
    }
}
