package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductListingModel {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<Data> data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(List<Data> data){
        this.data = data;
    }
    public List<Data> getData(){
        return this.data;
    }

    public class Data {
        @SerializedName("productId")
        @Expose
        private String productId;

        @SerializedName("userId")
        @Expose
        private String userId;

        @SerializedName("productTitle")
        @Expose
        private String productTitle;

        @SerializedName("productDescription")
        @Expose
        private String productDescription;

        @SerializedName("productImage")
        @Expose
        private String productImage;

        @SerializedName("createDate")
        @Expose
        private String createDate;

        @SerializedName("updateDate")
        @Expose
        private String updateDate;


        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getProductId() {
            return this.productId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserId() {
            return this.userId;
        }

        public void setProductTitle(String productTitle) {
            this.productTitle = productTitle;
        }

        public String getProductTitle() {
            return this.productTitle;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }

        public String getProductDescription() {
            return this.productDescription;
        }

        public void setProductImage(String productImage) {
            this.productImage = productImage;
        }

        public String getProductImage() {
            return this.productImage;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getCreateDate() {
            return this.createDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public String getUpdateDate() {
            return this.updateDate;
        }
    }
    }
