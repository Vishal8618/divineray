package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileModel {
    @SerializedName("user_id")
    @Expose
    private String user_id;

    public ProfileModel() {
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return "ProfileModel{" +
                "user_id='" + user_id + '\'' +
                '}';
    }
}
