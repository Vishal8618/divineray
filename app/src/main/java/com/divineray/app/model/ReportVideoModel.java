package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReportVideoModel {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<Data> data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(List<Data> data){
        this.data = data;
    }
    public List<Data> getData(){
        return this.data;
    }
    public class Data
    {
        @SerializedName("reasonId")
        @Expose
        private String reasonId;


        @SerializedName("reportReasons")
        @Expose
        private String reportReasons;

        public void setReasonId(String reasonId){
            this.reasonId = reasonId;
        }
        public String getReasonId(){
            return this.reasonId;
        }
        public void setReportReasons(String reportReasons){
            this.reportReasons = reportReasons;
        }
        public String getReportReasons(){
            return this.reportReasons;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "reasonId='" + reasonId + '\'' +
                    ", reportReasons='" + reportReasons + '\'' +
                    '}';
        }
    }

}
