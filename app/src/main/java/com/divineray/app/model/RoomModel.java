package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RoomModel {
    public static Object DataR;
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<DataR> data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(List<DataR> data){
        this.data = data;
    }
    public List<DataR> getData(){
        return this.data;
    }
    public  class DataR
    {
        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("user_id")
        @Expose
        private String user_id;

        @SerializedName("chatUserId")
        @Expose
        private String chatUserId;

        @SerializedName("roomId")
        @Expose
        private String roomId;

        @SerializedName("roomCreated")
        @Expose
        private String roomCreated;

        public void setId(String id){
            this.id = id;
        }
        public String getId(){
            return this.id;
        }
        public void setUser_id(String user_id){
            this.user_id = user_id;
        }
        public String getUser_id(){
            return this.user_id;
        }
        public void setChatUserId(String chatUserId){
            this.chatUserId = chatUserId;
        }
        public String getChatUserId(){
            return this.chatUserId;
        }
        public void setRoomId(String roomId){
            this.roomId = roomId;
        }
        public String getRoomId(){
            return this.roomId;
        }
        public void setRoomCreated(String roomCreated){
            this.roomCreated = roomCreated;
        }
        public String getRoomCreated(){
            return this.roomCreated;
        }
    }
}
