package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchModelget {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    public List<Datan> data;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(List<Datan> data){
        this.data = data;
    }
    public List<Datan> getData(){
        return this.data;
    }



 public class Datan
{
    @SerializedName("tagId")
    @Expose
    private String tagId;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("viewTotal")
    @Expose
    private String viewTotal;

    @SerializedName("tagTitle")
    @Expose
    private String tagTitle;

    @SerializedName("tagDesc")
    @Expose
    private String tagDesc;

    @SerializedName("created")
    @Expose
    private String created;

    @SerializedName("enabled")
    @Expose
    private String enabled;

    @SerializedName("videoId")
    @Expose
    private String videoId;

    @SerializedName("totalVideosViews")
    @Expose
    private String totalVideosViews;

    @SerializedName("totalVideos")
    @Expose
    private String totalVideos;

    @SerializedName("tagVideos")
    @Expose
    private List<TagVideos> tagVideos;

    public void setTagId(String tagId){
        this.tagId = tagId;
    }
    public String getTagId(){
        return this.tagId;
    }
    public void setType(String type){
        this.type = type;
    }
    public String getType(){
        return this.type;
    }
    public void setUser_id(String user_id){
        this.user_id = user_id;
    }
    public String getUser_id(){
        return this.user_id;
    }
    public void setViewTotal(String viewTotal){
        this.viewTotal = viewTotal;
    }
    public String getViewTotal(){
        return this.viewTotal;
    }
    public void setTagTitle(String tagTitle){
        this.tagTitle = tagTitle;
    }
    public String getTagTitle(){
        return this.tagTitle;
    }
    public void setTagDesc(String tagDesc){
        this.tagDesc = tagDesc;
    }
    public String getTagDesc(){
        return this.tagDesc;
    }
    public void setCreated(String created){
        this.created = created;
    }
    public String getCreated(){
        return this.created;
    }
    public void setEnabled(String enabled){
        this.enabled = enabled;
    }
    public String getEnabled(){
        return this.enabled;
    }
    public void setVideoId(String videoId){
        this.videoId = videoId;
    }
    public String getVideoId(){
        return this.videoId;
    }
    public void setTotalVideosViews(String totalVideosViews){
        this.totalVideosViews = totalVideosViews;
    }
    public String getTotalVideosViews(){
        return this.totalVideosViews;
    }
    public void setTotalVideos(String totalVideos){
        this.totalVideos = totalVideos;
    }
    public String getTotalVideos(){
        return this.totalVideos;
    }
    public void setTagVideos(List<TagVideos> tagVideos){
        this.tagVideos = tagVideos;
    }
    public List<TagVideos> getTagVideos(){
        return this.tagVideos;
    }
}
 public class TagVideos {
        @SerializedName("videoId")
        @Expose
        private String videoId;

     @SerializedName("user_id")
     @Expose
     private String user_id;

     @SerializedName("musicId")
     @Expose
     private String musicId;

     @SerializedName("musicName")
     @Expose
     private String musicName;

     @SerializedName("postImage")
     @Expose
     private String postImage;

     @SerializedName("postVideo")
     @Expose
     private String postVideo;

     @SerializedName("title")
     @Expose
     private String title;

     @SerializedName("description")
     @Expose
     private String description;

     @SerializedName("price")
     @Expose
     private String price;

     @SerializedName("postHeight")
     @Expose
     private String postHeight;

     @SerializedName("postWidth")
     @Expose
     private String postWidth;

     @SerializedName("created")
     @Expose
     private String created;

     @SerializedName("disable")
     @Expose
     private String disable;

     @SerializedName("tags")
     @Expose
     private String tags;

     @SerializedName("userDetails")
     @Expose
     private UserDetails userDetails;

     @SerializedName("isLiked")
     @Expose
     private String isLiked;

     @SerializedName("totalLike")
     @Expose
     private String totalLike;

     @SerializedName("totalViews")
     @Expose
     private String totalViews;

     @SerializedName("totalComments")
     @Expose
     private String totalComments;

     @SerializedName("shareUrl")
     @Expose
     private String shareUrl;

     public void setVideoId(String videoId) {
         this.videoId = videoId;
     }

     public String getVideoId() {
         return this.videoId;
     }

     public void setUser_id(String user_id) {
         this.user_id = user_id;
     }

     public String getUser_id() {
         return this.user_id;
     }

     public void setMusicId(String musicId) {
         this.musicId = musicId;
     }

     public String getMusicId() {
         return this.musicId;
     }

     public void setMusicName(String musicName) {
         this.musicName = musicName;
     }

     public String getMusicName() {
         return this.musicName;
     }

     public void setPostImage(String postImage) {
         this.postImage = postImage;
     }

     public String getPostImage() {
         return this.postImage;
     }

     public void setPostVideo(String postVideo) {
         this.postVideo = postVideo;
     }

     public String getPostVideo() {
         return this.postVideo;
     }

     public void setTitle(String title) {
         this.title = title;
     }

     public String getTitle() {
         return this.title;
     }

     public void setDescription(String description) {
         this.description = description;
     }

     public String getDescription() {
         return this.description;
     }

     public void setPrice(String price) {
         this.price = price;
     }

     public String getPrice() {
         return this.price;
     }

     public void setPostHeight(String postHeight) {
         this.postHeight = postHeight;
     }

     public String getPostHeight() {
         return this.postHeight;
     }

     public void setPostWidth(String postWidth) {
         this.postWidth = postWidth;
     }

     public String getPostWidth() {
         return this.postWidth;
     }

     public void setCreated(String created) {
         this.created = created;
     }

     public String getCreated() {
         return this.created;
     }

     public void setDisable(String disable) {
         this.disable = disable;
     }

     public String getDisable() {
         return this.disable;
     }

     public void setTags(String tags) {
         this.tags = tags;
     }

     public String getTags() {
         return this.tags;
     }

     public void setUserDetails(UserDetails userDetails) {
         this.userDetails = userDetails;
     }

     public UserDetails getUserDetails() {
         return this.userDetails;
     }

     public void setIsLiked(String isLiked) {
         this.isLiked = isLiked;
     }

     public String getIsLiked() {
         return this.isLiked;
     }

     public void setTotalLike(String totalLike) {
         this.totalLike = totalLike;
     }

     public String getTotalLike() {
         return this.totalLike;
     }

     public void setTotalViews(String totalViews) {
         this.totalViews = totalViews;
     }

     public String getTotalViews() {
         return this.totalViews;
     }

     public void setTotalComments(String totalComments) {
         this.totalComments = totalComments;
     }

     public String getTotalComments() {
         return this.totalComments;
     }

     public void setShareUrl(String shareUrl) {
         this.shareUrl = shareUrl;
     }

     public String getShareUrl() {
         return this.shareUrl;
     }
 }
}