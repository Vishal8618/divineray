package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchModeltagged {

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    public List<Data> data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(List<Data> data){
        this.data = data;
    }
    public List<Data> getData(){
        return this.data;
    }
    public class Data
    {
        @SerializedName("postHeight")
        @Expose
        private String postHeight;

        @SerializedName("postVideo")
        @Expose
        private String postVideo;

        @SerializedName("created")
        @Expose
        private String created;

        @SerializedName("isLiked")
        @Expose
        private String isLiked;

        @SerializedName("totalLike")
        @Expose
        private String totalLike;

        @SerializedName("description")
        @Expose
        private String description;

        @SerializedName("videoId")
        @Expose
        private String videoId;

        @SerializedName("postWidth")
        @Expose
        private String postWidth;

        @SerializedName("title")
        @Expose
        private String title;

        @SerializedName("userDetails")
        @Expose
        private UserDetails userDetails;

        @SerializedName("musicName")
        @Expose
        private String musicName;

        @SerializedName("tags")
        @Expose
        private String tags;

        @SerializedName("tagTitle")
        @Expose
        private String tagTitle;

        @SerializedName("postImage")
        @Expose
        private String postImage;

        @SerializedName("totalComments")
        @Expose
        private String totalComments;

        @SerializedName("musicId")
        @Expose
        private String musicId;

        @SerializedName("user_id")
        @Expose
        private String user_id;

        @SerializedName("price")
        @Expose
        private String price;

        @SerializedName("disable")
        @Expose
        private String disable;

        @SerializedName("totalViews")
        @Expose
        private String totalViews;

        @SerializedName("shareUrl")
        @Expose
        private  String shareUrl;

        @SerializedName("branchShareUrl")
        @Expose
        private String branchShareUrl;




        public String getTagTitle() {
            return tagTitle;
        }

        public void setTagTitle(String tagTitle) {
            this.tagTitle = tagTitle;
        }

        public String getShareUrl() {
            return shareUrl;
        }

        public void setShareUrl(String shareUrl) {
            this.shareUrl = shareUrl;
        }

        public String getPostHeight ()
        {
            return postHeight;
        }

        public void setPostHeight (String postHeight)
        {
            this.postHeight = postHeight;
        }

        public String getPostVideo ()
        {
            return postVideo;
        }

        public void setPostVideo (String postVideo)
        {
            this.postVideo = postVideo;
        }

        public String getCreated ()
        {
            return created;
        }

        public void setCreated (String created)
        {
            this.created = created;
        }

        public String getIsLiked ()
        {
            return isLiked;
        }

        public void setIsLiked (String isLiked)
        {
            this.isLiked = isLiked;
        }

        public String getTotalLike ()
        {
            return totalLike;
        }

        public void setTotalLike (String totalLike)
        {
            this.totalLike = totalLike;
        }

        public String getDescription ()
        {
            return description;
        }

        public void setDescription (String description)
        {
            this.description = description;
        }

        public String getVideoId ()
        {
            return videoId;
        }

        public void setVideoId (String videoId)
        {
            this.videoId = videoId;
        }

        public String getPostWidth ()
        {
            return postWidth;
        }

        public void setPostWidth (String postWidth)
        {
            this.postWidth = postWidth;
        }

        public String getTitle ()
        {
            return title;
        }

        public void setTitle (String title)
        {
            this.title = title;
        }

        public UserDetails getUserDetails ()
        {
            return userDetails;
        }

        public void setUserDetails (UserDetails userDetails)
        {
            this.userDetails = userDetails;
        }

        public String getMusicName ()
        {
            return musicName;
        }

        public void setMusicName (String musicName)
        {
            this.musicName = musicName;
        }

        public String getTags ()
        {
            return tags;
        }

        public void setTags (String tags)
        {
            this.tags = tags;
        }

        public String getPostImage ()
        {
            return postImage;
        }

        public void setPostImage (String postImage)
        {
            this.postImage = postImage;
        }

        public String getTotalComments ()
        {
            return totalComments;
        }

        public void setTotalComments (String totalComments)
        {
            this.totalComments = totalComments;
        }

        public String getMusicId ()
        {
            return musicId;
        }

        public void setMusicId (String musicId)
        {
            this.musicId = musicId;
        }

        public String getUser_id ()
        {
            return user_id;
        }

        public void setUser_id (String user_id)
        {
            this.user_id = user_id;
        }

        public String getPrice ()
        {
            return price;
        }

        public void setPrice (String price)
        {
            this.price = price;
        }

        public String getDisable ()
        {
            return disable;
        }

        public void setDisable (String disable)
        {
            this.disable = disable;
        }

        public String getTotalViews ()
        {
            return totalViews;
        }

        public void setTotalViews (String totalViews)
        {
            this.totalViews = totalViews;
        }

        public String getBranchShareUrl() {
            return branchShareUrl;
        }

        public void setBranchShareUrl(String branchShareUrl) {
            this.branchShareUrl = branchShareUrl;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [postHeight = "+postHeight+", postVideo = "+postVideo+", created = "+created+", isLiked = "+isLiked+", totalLike = "+totalLike+", description = "+description+", videoId = "+videoId+", postWidth = "+postWidth+", title = "+title+", userDetails = "+userDetails+", musicName = "+musicName+", tags = "+tags+", postImage = "+postImage+", totalComments = "+totalComments+", musicId = "+musicId+", user_id = "+user_id+", price = "+price+", disable = "+disable+", totalViews = "+totalViews+"]";
        }
    }
    public class UserDetails
    {
        @SerializedName("country")
        @Expose
        private String country;

        @SerializedName("facebookId")
        @Expose
        private String facebookId;

        @SerializedName("totalFollowing")
        @Expose
        private String totalFollowing;

        @SerializedName("verified")
        @Expose
        private String verified;

        @SerializedName("photo")
        @Expose
        private String photo;

        @SerializedName("description")
        @Expose
        private String description;

        @SerializedName("created at")
        @Expose
        private String created_at;

        @SerializedName("allowPush")
        @Expose
        private String allowPush;

        @SerializedName("device_type")
        @Expose
        private String device_type;

        @SerializedName("follow")
        @Expose
        private String follow;

        @SerializedName("appleId")
        @Expose
        private String appleId;

        @SerializedName("twitterId")
        @Expose
        private String twitterId;

        @SerializedName("password")
        @Expose
        private String password;

        @SerializedName("user_id")
        @Expose
        private String user_id;

        @SerializedName("countryCode")
        @Expose
        private String countryCode;

        @SerializedName("device_token")
        @Expose
        private String device_token;

        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("verificationCode")
        @Expose
        private String verificateCode;

        @SerializedName("disabled")
        @Expose
        private String disabled;

        @SerializedName("totalFollowers")
        @Expose
        private String totalFollowers;

        @SerializedName("email")
        @Expose
        private String email;

        public String getCountry ()
        {
            return country;
        }

        public void setCountry (String country)
        {
            this.country = country;
        }

        public String getFacebookId ()
        {
            return facebookId;
        }

        public void setFacebookId (String facebookId)
        {
            this.facebookId = facebookId;
        }

        public String getTotalFollowing ()
        {
            return totalFollowing;
        }

        public void setTotalFollowing (String totalFollowing)
        {
            this.totalFollowing = totalFollowing;
        }

        public String getVerified ()
        {
            return verified;
        }

        public void setVerified (String verified)
        {
            this.verified = verified;
        }

        public String getPhoto ()
        {
            return photo;
        }

        public void setPhoto (String photo)
        {
            this.photo = photo;
        }

        public String getDescription ()
        {
            return description;
        }

        public void setDescription (String description)
        {
            this.description = description;
        }

        public String getCreated_at ()
        {
            return created_at;
        }

        public void setCreated_at (String created_at)
        {
            this.created_at = created_at;
        }

        public String getAllowPush ()
        {
            return allowPush;
        }

        public void setAllowPush (String allowPush)
        {
            this.allowPush = allowPush;
        }

        public String getDevice_type ()
        {
            return device_type;
        }

        public void setDevice_type (String device_type)
        {
            this.device_type = device_type;
        }

        public String getFollow ()
        {
            return follow;
        }

        public void setFollow (String follow)
        {
            this.follow = follow;
        }

        public String getAppleId ()
        {
            return appleId;
        }

        public void setAppleId (String appleId)
        {
            this.appleId = appleId;
        }

        public String getTwitterId ()
        {
            return twitterId;
        }

        public void setTwitterId (String twitterId)
        {
            this.twitterId = twitterId;
        }

        public String getPassword ()
        {
            return password;
        }

        public void setPassword (String password)
        {
            this.password = password;
        }

        public String getUser_id ()
        {
            return user_id;
        }

        public void setUser_id (String user_id)
        {
            this.user_id = user_id;
        }

        public String getCountryCode ()
        {
            return countryCode;
        }

        public void setCountryCode (String countryCode)
        {
            this.countryCode = countryCode;
        }

        public String getDevice_token ()
        {
            return device_token;
        }

        public void setDevice_token (String device_token)
        {
            this.device_token = device_token;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public String getVerificateCode ()
        {
            return verificateCode;
        }

        public void setVerificateCode (String verificateCode)
        {
            this.verificateCode = verificateCode;
        }

        public String getDisabled ()
        {
            return disabled;
        }

        public void setDisabled (String disabled)
        {
            this.disabled = disabled;
        }

        public String getTotalFollowers ()
        {
            return totalFollowers;
        }

        public void setTotalFollowers (String totalFollowers)
        {
            this.totalFollowers = totalFollowers;
        }

        public String getEmail ()
        {
            return email;
        }

        public void setEmail (String email)
        {
            this.email = email;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [country = "+country+", facebookId = "+facebookId+", totalFollowing = "+totalFollowing+", verified = "+verified+", photo = "+photo+", description = "+description+", created_at = "+created_at+", allowPush = "+allowPush+", device_type = "+device_type+", follow = "+follow+", appleId = "+appleId+", twitterId = "+twitterId+", password = "+password+", user_id = "+user_id+", countryCode = "+countryCode+", device_token = "+device_token+", name = "+name+", verificateCode = "+verificateCode+", disabled = "+disabled+", totalFollowers = "+totalFollowers+", email = "+email+"]";
        }
    }
}
