package com.divineray.app.model;

import java.util.List;

public class StreamListModel {
    private String message;

    private int status;

    private UserDetail userDetail;

    private List<StreamDetail> streamDetail;

    private String likecount;

    private String viewcount;

    private List<Joinuserdetails> joinuserdetails;

    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setUserDetail(UserDetail userDetail){
        this.userDetail = userDetail;
    }
    public UserDetail getUserDetail(){
        return this.userDetail;
    }
    public void setStreamDetail(List<StreamDetail> streamDetail){
        this.streamDetail = streamDetail;
    }
    public List<StreamDetail> getStreamDetail(){
        return this.streamDetail;
    }
    public void setLikecount(String likecount){
        this.likecount = likecount;
    }
    public String getLikecount(){
        return this.likecount;
    }
    public void setViewcount(String viewcount){
        this.viewcount = viewcount;
    }
    public String getViewcount(){
        return this.viewcount;
    }
    public void setJoinuserdetails(List<Joinuserdetails> joinuserdetails){
        this.joinuserdetails = joinuserdetails;
    }
    public List<Joinuserdetails> getJoinuserdetails(){
        return this.joinuserdetails;
    }

    public class UserDetail
    {
        private String user_id;

        private String paypal_verification;

        private String paypal_id;

        private String paypal_token;

        private String name;

        private String email;

        private String password;

        private String photo;

        private String country;

        private String state;

        private String countryCode;

        private String twitterId;

        private String facebookId;

        private String appleId;

        private String description;

        private String verified;

        private String totalFollowing;

        private String totalFollowers;

        private String verificateCode;

        private String created_at;

        private String disabled;

        private String allowPush;

        private String device_type;

        private String total_coins;

        private String device_token;

        private String usertoken;

        private String securitytoken;

        private String certificate_status;

        private String certificate;

        public void setUser_id(String user_id){
            this.user_id = user_id;
        }
        public String getUser_id(){
            return this.user_id;
        }
        public void setPaypal_verification(String paypal_verification){
            this.paypal_verification = paypal_verification;
        }
        public String getPaypal_verification(){
            return this.paypal_verification;
        }
        public void setPaypal_id(String paypal_id){
            this.paypal_id = paypal_id;
        }
        public String getPaypal_id(){
            return this.paypal_id;
        }
        public void setPaypal_token(String paypal_token){
            this.paypal_token = paypal_token;
        }
        public String getPaypal_token(){
            return this.paypal_token;
        }
        public void setName(String name){
            this.name = name;
        }
        public String getName(){
            return this.name;
        }
        public void setEmail(String email){
            this.email = email;
        }
        public String getEmail(){
            return this.email;
        }
        public void setPassword(String password){
            this.password = password;
        }
        public String getPassword(){
            return this.password;
        }
        public void setPhoto(String photo){
            this.photo = photo;
        }
        public String getPhoto(){
            return this.photo;
        }
        public void setCountry(String country){
            this.country = country;
        }
        public String getCountry(){
            return this.country;
        }
        public void setState(String state){
            this.state = state;
        }
        public String getState(){
            return this.state;
        }
        public void setCountryCode(String countryCode){
            this.countryCode = countryCode;
        }
        public String getCountryCode(){
            return this.countryCode;
        }
        public void setTwitterId(String twitterId){
            this.twitterId = twitterId;
        }
        public String getTwitterId(){
            return this.twitterId;
        }
        public void setFacebookId(String facebookId){
            this.facebookId = facebookId;
        }
        public String getFacebookId(){
            return this.facebookId;
        }
        public void setAppleId(String appleId){
            this.appleId = appleId;
        }
        public String getAppleId(){
            return this.appleId;
        }
        public void setDescription(String description){
            this.description = description;
        }
        public String getDescription(){
            return this.description;
        }
        public void setVerified(String verified){
            this.verified = verified;
        }
        public String getVerified(){
            return this.verified;
        }
        public void setTotalFollowing(String totalFollowing){
            this.totalFollowing = totalFollowing;
        }
        public String getTotalFollowing(){
            return this.totalFollowing;
        }
        public void setTotalFollowers(String totalFollowers){
            this.totalFollowers = totalFollowers;
        }
        public String getTotalFollowers(){
            return this.totalFollowers;
        }
        public void setVerificateCode(String verificateCode){
            this.verificateCode = verificateCode;
        }
        public String getVerificateCode(){
            return this.verificateCode;
        }
        public void setCreated_at(String created_at){
            this.created_at = created_at;
        }
        public String getCreated_at(){
            return this.created_at;
        }
        public void setDisabled(String disabled){
            this.disabled = disabled;
        }
        public String getDisabled(){
            return this.disabled;
        }
        public void setAllowPush(String allowPush){
            this.allowPush = allowPush;
        }
        public String getAllowPush(){
            return this.allowPush;
        }
        public void setDevice_type(String device_type){
            this.device_type = device_type;
        }
        public String getDevice_type(){
            return this.device_type;
        }
        public void setTotal_coins(String total_coins){
            this.total_coins = total_coins;
        }
        public String getTotal_coins(){
            return this.total_coins;
        }
        public void setDevice_token(String device_token){
            this.device_token = device_token;
        }
        public String getDevice_token(){
            return this.device_token;
        }
        public void setUsertoken(String usertoken){
            this.usertoken = usertoken;
        }
        public String getUsertoken(){
            return this.usertoken;
        }
        public void setSecuritytoken(String securitytoken){
            this.securitytoken = securitytoken;
        }
        public String getSecuritytoken(){
            return this.securitytoken;
        }
        public void setCertificate_status(String certificate_status){
            this.certificate_status = certificate_status;
        }
        public String getCertificate_status(){
            return this.certificate_status;
        }
        public void setCertificate(String certificate){
            this.certificate = certificate;
        }
        public String getCertificate(){
            return this.certificate;
        }
    }

    public class StreamDetail
    {
        private String stream_id;

        private String user_id;

        private String stream_room;

        private String stream_start_time;

        private String stream_end_time;

        public void setStream_id(String stream_id){
            this.stream_id = stream_id;
        }
        public String getStream_id(){
            return this.stream_id;
        }
        public void setUser_id(String user_id){
            this.user_id = user_id;
        }
        public String getUser_id(){
            return this.user_id;
        }
        public void setStream_room(String stream_room){
            this.stream_room = stream_room;
        }
        public String getStream_room(){
            return this.stream_room;
        }
        public void setStream_start_time(String stream_start_time){
            this.stream_start_time = stream_start_time;
        }
        public String getStream_start_time(){
            return this.stream_start_time;
        }
        public void setStream_end_time(String stream_end_time){
            this.stream_end_time = stream_end_time;
        }
        public String getStream_end_time(){
            return this.stream_end_time;
        }
    }

    public class Joinuserdetails
    {
        private String user_id;

        private String paypal_verification;

        private String paypal_id;

        private String paypal_token;

        private String name;

        private String email;

        private String password;

        private String photo;

        private String country;

        private String state;

        private String countryCode;

        private String twitterId;

        private String facebookId;

        private String appleId;

        private String description;

        private String verified;

        private String totalFollowing;

        private String totalFollowers;

        private String verificateCode;

        private String created_at;

        private String disabled;

        private String allowPush;

        private String device_type;

        private String total_coins;

        private String device_token;

        private String usertoken;

        private String securitytoken;

        private String certificate_status;

        private String certificate;

        private String status;

        public void setUser_id(String user_id){
            this.user_id = user_id;
        }
        public String getUser_id(){
            return this.user_id;
        }
        public void setPaypal_verification(String paypal_verification){
            this.paypal_verification = paypal_verification;
        }
        public String getPaypal_verification(){
            return this.paypal_verification;
        }
        public void setPaypal_id(String paypal_id){
            this.paypal_id = paypal_id;
        }
        public String getPaypal_id(){
            return this.paypal_id;
        }
        public void setPaypal_token(String paypal_token){
            this.paypal_token = paypal_token;
        }
        public String getPaypal_token(){
            return this.paypal_token;
        }
        public void setName(String name){
            this.name = name;
        }
        public String getName(){
            return this.name;
        }
        public void setEmail(String email){
            this.email = email;
        }
        public String getEmail(){
            return this.email;
        }
        public void setPassword(String password){
            this.password = password;
        }
        public String getPassword(){
            return this.password;
        }
        public void setPhoto(String photo){
            this.photo = photo;
        }
        public String getPhoto(){
            return this.photo;
        }
        public void setCountry(String country){
            this.country = country;
        }
        public String getCountry(){
            return this.country;
        }
        public void setState(String state){
            this.state = state;
        }
        public String getState(){
            return this.state;
        }
        public void setCountryCode(String countryCode){
            this.countryCode = countryCode;
        }
        public String getCountryCode(){
            return this.countryCode;
        }
        public void setTwitterId(String twitterId){
            this.twitterId = twitterId;
        }
        public String getTwitterId(){
            return this.twitterId;
        }
        public void setFacebookId(String facebookId){
            this.facebookId = facebookId;
        }
        public String getFacebookId(){
            return this.facebookId;
        }
        public void setAppleId(String appleId){
            this.appleId = appleId;
        }
        public String getAppleId(){
            return this.appleId;
        }
        public void setDescription(String description){
            this.description = description;
        }
        public String getDescription(){
            return this.description;
        }
        public void setVerified(String verified){
            this.verified = verified;
        }
        public String getVerified(){
            return this.verified;
        }
        public void setTotalFollowing(String totalFollowing){
            this.totalFollowing = totalFollowing;
        }
        public String getTotalFollowing(){
            return this.totalFollowing;
        }
        public void setTotalFollowers(String totalFollowers){
            this.totalFollowers = totalFollowers;
        }
        public String getTotalFollowers(){
            return this.totalFollowers;
        }
        public void setVerificateCode(String verificateCode){
            this.verificateCode = verificateCode;
        }
        public String getVerificateCode(){
            return this.verificateCode;
        }
        public void setCreated_at(String created_at){
            this.created_at = created_at;
        }
        public String getCreated_at(){
            return this.created_at;
        }
        public void setDisabled(String disabled){
            this.disabled = disabled;
        }
        public String getDisabled(){
            return this.disabled;
        }
        public void setAllowPush(String allowPush){
            this.allowPush = allowPush;
        }
        public String getAllowPush(){
            return this.allowPush;
        }
        public void setDevice_type(String device_type){
            this.device_type = device_type;
        }
        public String getDevice_type(){
            return this.device_type;
        }
        public void setTotal_coins(String total_coins){
            this.total_coins = total_coins;
        }
        public String getTotal_coins(){
            return this.total_coins;
        }
        public void setDevice_token(String device_token){
            this.device_token = device_token;
        }
        public String getDevice_token(){
            return this.device_token;
        }
        public void setUsertoken(String usertoken){
            this.usertoken = usertoken;
        }
        public String getUsertoken(){
            return this.usertoken;
        }
        public void setSecuritytoken(String securitytoken){
            this.securitytoken = securitytoken;
        }
        public String getSecuritytoken(){
            return this.securitytoken;
        }
        public void setCertificate_status(String certificate_status){
            this.certificate_status = certificate_status;
        }
        public String getCertificate_status(){
            return this.certificate_status;
        }
        public void setCertificate(String certificate){
            this.certificate = certificate;
        }
        public String getCertificate(){
            return this.certificate;
        }
        public void setStatus(String status){
            this.status = status;
        }
        public String getStatus(){
            return this.status;
        }

        @Override
        public String toString() {
            return "Joinuserdetails{" +
                    "user_id='" + user_id + '\'' +
                    ", paypal_verification='" + paypal_verification + '\'' +
                    ", paypal_id='" + paypal_id + '\'' +
                    ", paypal_token='" + paypal_token + '\'' +
                    ", name='" + name + '\'' +
                    ", email='" + email + '\'' +
                    ", password='" + password + '\'' +
                    ", photo='" + photo + '\'' +
                    ", country='" + country + '\'' +
                    ", state='" + state + '\'' +
                    ", countryCode='" + countryCode + '\'' +
                    ", twitterId='" + twitterId + '\'' +
                    ", facebookId='" + facebookId + '\'' +
                    ", appleId='" + appleId + '\'' +
                    ", description='" + description + '\'' +
                    ", verified='" + verified + '\'' +
                    ", totalFollowing='" + totalFollowing + '\'' +
                    ", totalFollowers='" + totalFollowers + '\'' +
                    ", verificateCode='" + verificateCode + '\'' +
                    ", created_at='" + created_at + '\'' +
                    ", disabled='" + disabled + '\'' +
                    ", allowPush='" + allowPush + '\'' +
                    ", device_type='" + device_type + '\'' +
                    ", total_coins='" + total_coins + '\'' +
                    ", device_token='" + device_token + '\'' +
                    ", usertoken='" + usertoken + '\'' +
                    ", securitytoken='" + securitytoken + '\'' +
                    ", certificate_status='" + certificate_status + '\'' +
                    ", certificate='" + certificate + '\'' +
                    ", status='" + status + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "StreamListModel{" +
                "message='" + message + '\'' +
                ", status=" + status +
                ", userDetail=" + userDetail +
                ", streamDetail=" + streamDetail +
                ", likecount='" + likecount + '\'' +
                ", viewcount='" + viewcount + '\'' +
                ", joinuserdetails=" + joinuserdetails +
                '}';
    }
}
