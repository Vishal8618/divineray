package com.divineray.app.model;

public class StreamModel {
    private  int status;

    private String message;

    private Data data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(Data data){
        this.data = data;
    }
    public Data getData(){
        return this.data;
    }

    public static class Data
    {
        private String id;

        private String user_id;

        private String stream_id;

        private String stream_room;

        private String comment;

        private String type;

        private String comment_time;

        private String name;

        private String photo;

        public void setId(String id){
            this.id = id;
        }
        public String getId(){
            return this.id;
        }
        public void setUser_id(String user_id){
            this.user_id = user_id;
        }
        public String getUser_id(){
            return this.user_id;
        }
        public void setStream_id(String stream_id){
            this.stream_id = stream_id;
        }
        public String getStream_id(){
            return this.stream_id;
        }
        public void setStream_room(String stream_room){
            this.stream_room = stream_room;
        }
        public String getStream_room(){
            return this.stream_room;
        }
        public void setComment(String comment){
            this.comment = comment;
        }
        public String getComment(){
            return this.comment;
        }
        public void setType(String type){
            this.type = type;
        }
        public String getType(){
            return this.type;
        }
        public void setComment_time(String comment_time){
            this.comment_time = comment_time;
        }
        public String getComment_time(){
            return this.comment_time;
        }
        public void setName(String name){
            this.name = name;
        }
        public String getName(){
            return this.name;
        }
        public void setPhoto(String photo){
            this.photo = photo;
        }
        public String getPhoto(){
            return this.photo;
        }
    }
}
