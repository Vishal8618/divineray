package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UserDetails
{
    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("photo")
    @Expose
    private String photo;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("countryCode")
    @Expose
    private String countryCode;

    @SerializedName("twitterId")
    @Expose
    private String twitterId;

    @SerializedName("facebookId")
    @Expose
    private String facebookId;

    @SerializedName("appleId")
    @Expose
    private String appleId;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("verified")
    @Expose
    private String verified;

    @SerializedName("totalFollowing")
    @Expose
    private String totalFollowing;

    @SerializedName("totalFollowers")
    @Expose
    private String totalFollowers;

    @SerializedName("verificateCode")
    @Expose
    private String verificateCode;

    @SerializedName("created_at")
    @Expose
    private String created_at;

    @SerializedName("disabled")
    @Expose
    private String disabled;

    @SerializedName("allowPush")
    @Expose
    private String allowPush;

    @SerializedName("device_type")
    @Expose
    private String device_type;

    @SerializedName("device_token")
    @Expose
    private String device_token;

    @SerializedName("follow")
    @Expose
    private String follow;


    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        this.country = country;
    }

    public String getFacebookId ()
    {
        return facebookId;
    }

    public void setFacebookId (String facebookId)
    {
        this.facebookId = facebookId;
    }

    public String getTotalFollowing ()
    {
        return totalFollowing;
    }

    public void setTotalFollowing (String totalFollowing)
    {
        this.totalFollowing = totalFollowing;
    }

    public String getVerified ()
    {
        return verified;
    }

    public void setVerified (String verified)
    {
        this.verified = verified;
    }

    public String getPhoto ()
    {
        return photo;
    }

    public void setPhoto (String photo)
    {
        this.photo = photo;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getAllowPush ()
    {
        return allowPush;
    }

    public void setAllowPush (String allowPush)
    {
        this.allowPush = allowPush;
    }

    public String getDevice_type ()
    {
        return device_type;
    }

    public void setDevice_type (String device_type)
    {
        this.device_type = device_type;
    }

    public String getFollow ()
    {
        return follow;
    }

    public void setFollow (String follow)
    {
        this.follow = follow;
    }

    public String getAppleId ()
    {
        return appleId;
    }

    public void setAppleId (String appleId)
    {
        this.appleId = appleId;
    }

    public String getTwitterId ()
    {
        return twitterId;
    }

    public void setTwitterId (String twitterId)
    {
        this.twitterId = twitterId;
    }

    public String getPassword ()
    {
        return password;
    }

    public void setPassword (String password)
    {
        this.password = password;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getCountryCode ()
    {
        return countryCode;
    }

    public void setCountryCode (String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String getDevice_token ()
    {
        return device_token;
    }

    public void setDevice_token (String device_token)
    {
        this.device_token = device_token;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getVerificateCode ()
    {
        return verificateCode;
    }

    public void setVerificateCode (String verificateCode)
    {
        this.verificateCode = verificateCode;
    }

    public String getDisabled ()
    {
        return disabled;
    }

    public void setDisabled (String disabled)
    {
        this.disabled = disabled;
    }

    public String getTotalFollowers ()
    {
        return totalFollowers;
    }

    public void setTotalFollowers (String totalFollowers)
    {
        this.totalFollowers = totalFollowers;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [country = "+country+", facebookId = "+facebookId+", totalFollowing = "+totalFollowing+", verified = "+verified+", photo = "+photo+", description = "+description+", created_at = "+created_at+", allowPush = "+allowPush+", device_type = "+device_type+", follow = "+follow+", appleId = "+appleId+", twitterId = "+twitterId+", password = "+password+", user_id = "+user_id+", countryCode = "+countryCode+", device_token = "+device_token+", name = "+name+", verificateCode = "+verificateCode+", disabled = "+disabled+", totalFollowers = "+totalFollowers+", email = "+email+"]";
    }
}

