package com.divineray.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetailsAdd {
    @SerializedName("follow")
    @Expose
    private String follow;

    public void setFollow(String follow){
        this.follow = follow;
    }
    public String getFollow(){
        return this.follow;
    }
}
