package com.divineray.app.model;

public class VersionModel {
    private int status;

    private String message;

    private Data2 data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }

    public Data2 getData() {
        return data;
    }

    public void setData(Data2 data) {
        this.data = data;
    }

    public class Data2
    {
        private String id;

        private String android;

        private String ios;

        private String ios_description;

        private String ios_title;

        private String android_title;

        private String android_description;

        private String forceUpdate;

        private String ios_redirect;

        private String android_redirect;

        private String created;

        public void setId(String id){
            this.id = id;
        }
        public String getId(){
            return this.id;
        }
        public void setAndroid(String android){
            this.android = android;
        }
        public String getAndroid(){
            return this.android;
        }
        public void setIos(String ios){
            this.ios = ios;
        }
        public String getIos(){
            return this.ios;
        }
        public void setIos_description(String ios_description){
            this.ios_description = ios_description;
        }
        public String getIos_description(){
            return this.ios_description;
        }
        public void setIos_title(String ios_title){
            this.ios_title = ios_title;
        }
        public String getIos_title(){
            return this.ios_title;
        }
        public void setAndroid_title(String android_title){
            this.android_title = android_title;
        }
        public String getAndroid_title(){
            return this.android_title;
        }
        public void setAndroid_description(String android_description){
            this.android_description = android_description;
        }
        public String getAndroid_description(){
            return this.android_description;
        }
        public void setForceUpdate(String forceUpdate){
            this.forceUpdate = forceUpdate;
        }
        public String getForceUpdate(){
            return this.forceUpdate;
        }
        public void setIos_redirect(String ios_redirect){
            this.ios_redirect = ios_redirect;
        }
        public String getIos_redirect(){
            return this.ios_redirect;
        }
        public void setAndroid_redirect(String android_redirect){
            this.android_redirect = android_redirect;
        }
        public String getAndroid_redirect(){
            return this.android_redirect;
        }
        public void setCreated(String created){
            this.created = created;
        }
        public String getCreated(){
            return this.created;
        }
    }


}
