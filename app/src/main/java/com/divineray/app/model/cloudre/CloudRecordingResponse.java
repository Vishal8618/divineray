package com.divineray.app.model.cloudre;

import com.google.gson.annotations.SerializedName;

public class CloudRecordingResponse{

	@SerializedName("status")
	private int status;

	@SerializedName("message")
	private String message;

	@SerializedName("resourceId")
	private String resourceId;


	@SerializedName("sid")
	private String sid;


	@SerializedName("data")
	private Data data;



	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}


	@Override
	public String toString() {
		return "CloudRecordingResponse{" +
				"status=" + status +
				", message='" + message + '\'' +
				", resourceId='" + resourceId + '\'' +
				", sid='" + sid + '\'' +
				", data=" + data +
				'}';
	}
}