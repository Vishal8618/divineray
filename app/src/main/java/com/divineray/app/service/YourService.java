package com.divineray.app.service;

import android.app.IntentService;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;


import com.divineray.app.Agora.ConstantsA;
import com.divineray.app.Utils.DivineRayAppplication;
import com.divineray.app.R;
import com.divineray.app.RetrofitApi.ApiClient;
import com.divineray.app.Utils.Constants;
import com.divineray.app.Utils.DivineRayPrefernces;
import com.divineray.app.activities.HomeActivity;
import com.divineray.app.interfaces.ApiInterface;
import com.divineray.app.model.EndStream2Model;
import com.divineray.app.model.StatusModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import io.socket.client.Socket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressWarnings("deprecation")
public class YourService extends Service {
    public int counter = 0;
    public static final String ACTION_1 = "action_1";
    static public NotificationManager notificationManager;
    static  public boolean alreadyStop=false;
    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            startMyOwnForeground();
        } else {
           startMyOwnForeground();
        }
    }

    private void startMyOwnForeground() {
        Intent action1Intent = new Intent(this, NotificationActionService.class)
                .setAction(ACTION_1);

        PendingIntent action1PendingIntent = PendingIntent.getService(this, 0,
                action1Intent, PendingIntent.FLAG_ONE_SHOT);


        final String CHANNEL_ID = "my_channel";
        final String CHANNEL_NAME = "my_channel";


        NotificationChannel defaultChannel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            defaultChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
        }

        Notification notification2 = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.you))
                .setContentText(getString(R.string.show_live))
                .setSmallIcon(R.drawable.notif_sys_show_stream)
                .setChannelId(CHANNEL_ID)
                .addAction(new NotificationCompat.Action(0,
                        getString(R.string.end_stream), action1PendingIntent))
                .build();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(defaultChannel);
        }
        startForeground(2, notification2);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        startTimer();
        return START_NOT_STICKY;
    }


    public static boolean isDeviceLocked(Context context) {
        boolean isLocked = false;

        // First we check the locked state
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        boolean inKeyguardRestrictedInputMode = keyguardManager.inKeyguardRestrictedInputMode();

        if (inKeyguardRestrictedInputMode) {
            isLocked = true;

        } else {
            // If password is not set in the settings, the inKeyguardRestrictedInputMode() returns false,
            // so we need to check if screen on for this case

            PowerManager powerManager = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
                isLocked = !powerManager.isInteractive();
            } else {
                //noinspection deprecation
                isLocked = !powerManager.isScreenOn();
            }
        }

        Log.d(""+"Now device is %s.", isLocked ? "locked" : "unlocked");
        return isLocked;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stoptimertask();

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("restartservice");
        broadcastIntent.setClass(this, Restarter.class);
        this.sendBroadcast(broadcastIntent);
    }


    private Timer timer;
    private TimerTask timerTask;

    public void startTimer() {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                Log.e("Count", "=========  " + (counter++));
            }
        };
        timer.schedule(timerTask, 1000, 1000); //
    }

    public void stoptimertask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(this,DivineRayPrefernces.USERID,null));
        mMap.put("stream_id", DivineRayPrefernces.readString(this,DivineRayPrefernces.STREAM_ID,null));
        return mMap;
    }




    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public static class NotificationActionService extends IntentService {
        public NotificationActionService() {
            super(NotificationActionService.class.getSimpleName());
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            String action = intent.getAction();
            Log.e("Received_action", "" + action);
            if (ACTION_1.equals(action)) {
                // TODO: handle action 1.
                if (!Constants.endStreamDone) {
                    Constants.endStreamDone=true;
                    executeEndSteam();
                }
            }
        }





    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", DivineRayPrefernces.readString(this,DivineRayPrefernces.USERID,null));
        mMap.put("stream_id", DivineRayPrefernces.readString(this,DivineRayPrefernces.STREAM_ID,null));
        mMap.put("cname", ConstantsA.channelNameA);
        mMap.put("resourceid", ConstantsA.resourceId);
        mMap.put("sid", ConstantsA.sid);
        return mMap;
    }


   public void executeEndSteam() {
       String mAiuthToken = DivineRayPrefernces.readString(this, DivineRayPrefernces.AUTHTOKEN, null);
       ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
       mApiInterface.endLiveStreamV2(mAiuthToken, mParam()).enqueue(new Callback<EndStream2Model>() {
           @Override
           public void onResponse(Call<EndStream2Model> call, Response<EndStream2Model> response) {
               alreadyStop=true;
               Intent mServiceIntent;
              stopForeground(true);
               YourService mYourService;
               mYourService = new YourService();
               mServiceIntent = new Intent(NotificationActionService.this, mYourService.getClass());
               stopService(mServiceIntent);
               new Handler().postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       DivineRayAppplication app = (DivineRayAppplication) getApplication();
                       Socket mSocket;
                       mSocket = app.getSocket();
                       mSocket.emit("ConncetedChat", DivineRayPrefernces.readString(getApplicationContext(),DivineRayPrefernces.STREAM_ID,null));
                       JSONObject jsonObject=null;
                       try {
                           jsonObject=new JSONObject();
                           jsonObject.put("userid",DivineRayPrefernces.readString(getApplicationContext(),DivineRayPrefernces.STREAM_ID,null));
                           jsonObject.put("message",DivineRayPrefernces.readString(getApplicationContext(),DivineRayPrefernces.STREAM_USERNAME,null)+" left the stream");
                           jsonObject.put("type","5");
                           Log.e("checkio", jsonObject.toString());
                       } catch (JSONException e) {
                           e.printStackTrace();
                       }
                       mSocket.emit("newMessage", DivineRayPrefernces.readString(getApplicationContext(),DivineRayPrefernces.STREAM_ROOMID,null), jsonObject,"hdhd");

                       Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                       intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                       //intent.putExtra("menuFragment", "searchMenuItem");
                       intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                       startActivity(intent);

                   }
               },200);

           }

           @Override
           public void onFailure(Call<EndStream2Model> call, Throwable t) {
               Intent mServiceIntent;
               stopForeground(true);
               YourService mYourService;
               mYourService = new YourService();
               mServiceIntent = new Intent(NotificationActionService.this, mYourService.getClass());
               stopService(mServiceIntent);
               new Handler().postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                       intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    //  intent.putExtra("menuFragment", "searchMenuItem");
                       intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                       startActivity(intent);
                   }
               },200);

           }
       });


   }

    }
}
