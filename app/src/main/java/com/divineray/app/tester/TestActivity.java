package com.divineray.app.tester;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.divineray.app.R;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
    }
}